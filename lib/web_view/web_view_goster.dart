import '../global_degisken/global_degisken.dart';
import '../yardimci_widgetlar/appbar_widgets/appbar_text.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebViewGoster extends StatelessWidget {
  final String url;
  final String appBarBaslik;

  const WebViewGoster({Key key,@required this.url,@required this.appBarBaslik}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 3,
        backgroundColor: GlobalDegiskenler.appBarColor,
        centerTitle: true,
        title: BaslikText(
          label: appBarBaslik ?? '',
        ),
      ),
      body: WebView(
        initialUrl: url,
      ),
    );
  }
}
