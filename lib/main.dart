 import 'package:connectivity_wrapper/connectivity_wrapper.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'app.dart';
import 'yardimci_fonksiyonlar/shared_sepete_ekle/sepet_shared.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'bloc/blocs_exports.dart';
import 'locator.dart';
import 'bloc/sepet_sayac_bloc/sepet_sayac_observer.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  setupLocator();

  await SharedPrefLib.initialize();

  //for android
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarBrightness: Brightness.dark,
    statusBarColor: Colors.transparent,
  ));

/*
  //for ios
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarBrightness: Brightness.dark,
  ));
*/
///bloc la olan hatadan dolayı local bildirim kapatıldı
 // local bildirimi başlatıryoruz
  // LocalNotifications().init();
  //sepet sayacı için bloc uğu başlatıyoruz
  Bloc.observer =  SayacSepetObserver();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
        //internet kontrolu için yazıldı
        runApp(ConnectivityAppWrapper(app: BlocProvider(
            create: (_) => SepetSayacCubit(0),
            child: MobilSiparisApp())));
  });
}
