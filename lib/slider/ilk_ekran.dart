import 'package:flutter/material.dart';
import '../global_degisken/global_degisken.dart';
import '../responsive_class/size_config.dart';
import '../yardimci_widgetlar/label_text/label_widget.dart';

class IlkSayfa extends StatelessWidget {
  const IlkSayfa({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Padding(
      padding: EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Flexible(
            flex: 7,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: SizeConfig.safeBlockHorizontal * 85,
                  decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black12,
                        spreadRadius:1,
                        blurRadius: 10,
                        offset: Offset(0, 1),
                      ),
                    ],
                  ),
                  child: Image.asset(
                    "assets/images/anasayfa.png",
                  ),
                ),
              ],
            ),
          ),
          Flexible(
            flex: 3,
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 24),
              child: LabelCard(
                textAlign: TextAlign.center,
                satirSayisi: 4,
                fontSize: SizeConfig.safeBlockHorizontal * 4.5,
                color: Colors.black,
                fontFamily: GlobalDegiskenler.genelFontStyle,
                fontWeight: FontWeight.bold,
                label:
                    'Mobil uygulamamıza hoşgeldiniz. Anamenüde bulunan saat simgesine tıklayarak hizmet alabileceğiniz saatleri öğrenebilirsiniz.',
              ),
            ),
          ),
        ],
      ),
    );
  }
}
