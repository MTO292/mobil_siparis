import 'package:flutter/material.dart';
import '../global_degisken/global_degisken.dart';
import '../responsive_class/size_config.dart';
import '../yardimci_widgetlar/label_text/label_widget.dart';

class UcuncuSayfa extends StatelessWidget {
  const UcuncuSayfa({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Padding(
      padding: EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Flexible(
            flex: 7,
            child: Center(
              child: Image(
                width: SizeConfig.safeBlockHorizontal * 85,
                image: AssetImage("assets/images/splash_logo_slider.png"),
              ),
            ),
          ),
          SizedBox(height: SizeConfig.safeBlockVertical * 5),
          Flexible(
            flex: 3,
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 24),
              child: LabelCard(
                textAlign: TextAlign.center,
                satirSayisi: 7,
                fontSize: SizeConfig.safeBlockHorizontal * 4.5,
                color: Colors.black,
                fontFamily: GlobalDegiskenler.genelFontStyle,
                fontWeight: FontWeight.bold,
                label:
                    "Sizlere daha iyi hizmet verebilmek adına uygulamamızı puanlayıp yorum yaparsanız geliştirme yapmamıza katkı sağlamış olursunuz, bizleri tercih ettiğiniz için teşekkür ederiz :)",
              ),
            ),
          ),
        ],
      ),
    );
  }
}
