import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import '../anamenu/ana_menu.dart';
import '../global_degisken/global_degisken.dart';
import '../responsive_class/size_config.dart';
import '../slider/ucuncu_ekran.dart';
import '../yardimci_widgetlar/label_text/label_widget.dart';
import 'package:line_awesome_icons/line_awesome_icons.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'ikinci_ekran.dart';
import 'ilk_ekran.dart';

class MySliderWidget extends StatefulWidget {
  MySliderWidget({Key key}) : super(key: key);

  @override
  _MySliderWidgetState createState() => _MySliderWidgetState();
}

class _MySliderWidgetState extends State<MySliderWidget> {
  SharedPreferences prefs;
  final int _toplamSayfa = 3;
  final PageController _pageController = PageController(initialPage: 0);
  int _currentPage = 0;

  List<Widget> _yuvarlakIconOlustur() {
    List<Widget> list = [];
    for (var i = 0; i < _toplamSayfa; i++) {
      list.add(i == _currentPage ? _indicator(true) : _indicator(false));
    }
    return list;
  }

  //Resimlerin altında çıkan noktaların oluşturulması
  Widget _indicator(bool aktifMi) {
    return AnimatedContainer(
      margin: EdgeInsets.symmetric(horizontal: 8.0),
      duration: Duration(milliseconds: 150),
      height: SizeConfig.safeBlockHorizontal * 4,
      width: aktifMi ? 20.0 : 12.0,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: aktifMi ? Colors.black : Colors.black12,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      floatingActionButton: _currentPage != _toplamSayfa - 1
          ? bottomSheetButonu('İleri')
          : bottomSheetButonu('Tamamla'),
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: Container(
          decoration: BoxDecoration(
              color: _currentPage == 0
                  ? Colors.teal
                  : _currentPage == 1
                      ? Colors.deepPurple
                      : Color.fromRGBO(5, 85, 144, 1)),
          child: Padding(
            padding: EdgeInsets.only(top: SizeConfig.blockSizeHorizontal * 5),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                //Genel container
                Container(
                  alignment: Alignment.center,
                  height: SizeConfig.safeBlockVertical * 80,
                  child: PageView(
                    physics: ScrollBehavior().getScrollPhysics(context),
                    controller: _pageController,
                    onPageChanged: (int value) {
                      setState(() {
                        _currentPage = value;
                      });
                    },
                    //Gösterilen resimleri temsli eden yapı
                    children: <Widget>[
                      IlkSayfa(),
                      IkinciSayfa(),
                      UcuncuSayfa(),
                    ],
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: _yuvarlakIconOlustur(),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  //Alt barda çıkan ileri- tamamla butonu
  Widget bottomSheetButonu(String label) {
    return Container(
      height: SizeConfig.safeBlockVertical * 7,
      child: OutlineButton.icon(
        highlightedBorderColor: Colors.black,
        borderSide: BorderSide(
          color: Colors.black,
          style: BorderStyle.solid,
          width: 1,
        ),
        onPressed: () async {
          prefs = await SharedPreferences.getInstance();
          if (_currentPage != _toplamSayfa - 1) {
            _pageController.nextPage(
                duration: Duration(milliseconds: 450), curve: Curves.easeIn);
          } else {
            prefs.setBool('sliderAktifMi', true);
            Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (context) => AnaMenu()));
          }
        },
        label: labelCard(label, 4.5, Colors.black),
        icon: Icon(
          LineAwesomeIcons.arrow_circle_o_right,
          color: Colors.black,
          size: SizeConfig.safeBlockHorizontal * 7.5,
        ),
      ),
    );
  }

  //Buton içindeki yazının labeli
  Widget labelCard(String label, double fontSize, Color color) {
    return LabelCard(
      label: label,
      fontFamily: GlobalDegiskenler.tlSimgesi,
      color: color,
      fontWeight: FontWeight.w600,
      fontSize: SizeConfig.safeBlockHorizontal * fontSize,
    );
  }
}
