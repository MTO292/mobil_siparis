import 'package:flutter_bloc/flutter_bloc.dart';

/// Sepet sayacında olan değişiklikleri görmek için oluşturuldu
///onChange hem Bloc hem de Cubit örnekleri için aynı şekilde çalışır.
class SayacSepetObserver extends BlocObserver {

  @override
  void onChange(Cubit cubit, Change change) {
    super.onChange(cubit, change);
  }
}
