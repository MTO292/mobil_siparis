import 'package:flutter_bloc/flutter_bloc.dart';

///sepet sayac kontrolu için bloc state managment yazıldı
class SepetSayacCubit extends Cubit<int>{
  SepetSayacCubit(int state) : super(state);

  void artir() => emit(state + 1);

  void azalt() => emit(state - 1);

  void esitle(int deger) => emit(deger);

  void sifirla() => emit(0);
}