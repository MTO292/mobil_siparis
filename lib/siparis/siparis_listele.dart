import 'package:connectivity/connectivity.dart';
import 'package:connectivity_wrapper/connectivity_wrapper.dart';
import 'package:flutter/material.dart';
import '../api/response/islem_donus.dart';
import '../api/model/sepet_liste_model.dart';
import '../global_degisken/global_degisken.dart';
import '../responsive_class/size_config.dart';
import '../siparis/siparis_detay.dart';
import '../yardimci_fonksiyonlar/form_mesaj_dondur/mesaj_dondur.dart';
import '../yardimci_widgetlar/appbar_widgets/appbar_text.dart';
import '../yardimci_widgetlar/label_text/label_widget.dart';
import '../yardimci_fonksiyonlar/form_mesaj_dondur/net_kontrol.dart'
    as net;
import '../global_degisken/global_degisken.dart' as global;

class SiparisListele extends StatefulWidget {
  final durumKod;
  final cId;
  final baslik;

  SiparisListele(
      {Key key,
      @required this.durumKod,
      @required this.cId,
      @required this.baslik})
      : super(key: key);

  @override
  _SiparisListeleState createState() => _SiparisListeleState();
}

class _SiparisListeleState extends State<SiparisListele> {
  List<SepetListe> siparisListe = [];
  IslemDonus islemDonus = IslemDonus();
  Future<List<SepetListe>> spListe;
  var yaziValue = 3.5;

  var saat;
  var urunDurum;

  void siparisGetir() {
    spListe = islemDonus.siparisListe(widget.cId, widget.durumKod);
    spListe.then((gelenDeger) {
      siparisListe = gelenDeger;
    });
  }

  /*Sayfa yüklenirken gerekli listeler doluyor, id değeri shared ile getiriliyor*/
  @override
  void initState() {
    super.initState();
    siparisGetir();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return WillPopScope(
      onWillPop: () {
        Navigator.of(context).pop(true);
        return null;
      },
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: GlobalDegiskenler.appBarColor,
          title: BaslikText(
            label: widget.baslik,
          ),
        ),
        body: ConnectivityWidgetWrapper(
          disableInteraction: false,
          offlineWidget: net.netKontrol(),
          child: Container(
            child: FutureBuilder(
              future: spListe,
              builder: (context, snapshot) {
                /*Veriler yüklenirken bu ekran gösterilecek*/
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return global.progressDondur();
                  /*Veri varmı kontrol ediyoruz*/
                } else if (snapshot.connectionState == ConnectionState.done &&
                    !snapshot.hasError &&
                    snapshot.hasData) {
                  if (siparisListe.length == 0) {
                    /*Veri yokken gösterilecek olan ekran*/
                    return mesajGoster('Bilgi',
                        'Kayıtlı siparişiniz bulunamadı.', Icons.error_outline);
                  } else {
                    /*Veri varsa gösterilecek olan ekran*/
                    return listele();
                  }
                } else if (snapshot.hasError) {
                  return mesajGoster(
                      'Hata',
                      'Lütfen internetinizi kontrol ediniz.',
                      Icons.error_outline);
                } else if (snapshot.data == null && snapshot.hasData == false) {
                  return mesajGoster(
                      'Hata',
                      'Lütfen Tekrar yüklemeyi deneyiniz.',
                      Icons.error_outline);
                }
                return null;
              },
            ),
          ),
        ),
      ),
    );
  }

  //region Fonksiyonlar

  siparisiIpttalEt(int index) {
    return () {
      Navigator.of(context, rootNavigator: true).pop();
      islemDonus
          .siparisIptal(widget.cId, siparisListe[index].aId)
          .then((gelenDeger) {
        {
          if (gelenDeger.cariBasariDonus == "1") {
            setState(() {
              siparisListe.removeAt(index);
            });
          } else if (gelenDeger.cariBasariDonus == "2") {
            MesajGoster.mesajGoster(
                context,
                "Sipariş silinirken bir hata oluştu. Lütfen tekrar deneyiniz.",
                SoruTipi.Hata);
          } else if (gelenDeger.cariBasariDonus == "3") {
            MesajGoster.mesajGoster(
                context,
                "Beklenmedik bir hata oluştu. Lütfen daha sonra tekrar deneyiniz.",
                SoruTipi.Hata);
          } else if (gelenDeger.cariBasariDonus == "11") {
            MesajGoster.mesajGoster(
                context,
                "Sipariş durumunuzu kontrol edip tekrar iptal etmeyi deneyiniz.",
                SoruTipi.Hata)
                .whenComplete(() {
              setState(() {
                siparisGetir();
              });
            });
          } else if (gelenDeger.cariBasariDonus ==
              numarator.uygulamaVersiyonuEski) {
            logoEkraninaGit(context);
          } else if (gelenDeger.cariBasariDonus ==
              numarator.apiVersiyonuEski) {
            logoEkraninaGit(context);
          } else if (gelenDeger.cariBasariDonus ==
              numarator.hizmetDisi) {
            logoEkraninaGit(context);
          }
        }
      });
    };
  }

  iptalEtButonu(index) {
    return () async {
      try {
        var result = await Connectivity().checkConnectivity();
        if (result == ConnectivityResult.none) {
          MesajGoster.mesajGoster(
              context, "Cihazınızı internete bağlayınız.", SoruTipi.Uyari);
        } else {
          if (siparisListe[index].durumIki.toString() == "0-Bekleyen") {
             MesajGoster.soruSor(
                context,
                'Siparişi iptal etmek istediğinize emin misiniz ?',
                SoruTipi.Soru,
                evet: siparisiIpttalEt(index));
          } else {
            MesajGoster.mesajGoster(
                context,
                "Sipariş durumu sadece bekleyen durumunda iken iptal edilebilir.",
                SoruTipi.Uyari);
          }
        }
      } catch (e) {}
    };
  }

  //endregion

  //region Widgetler

  Widget listele() {
    return ListView.builder(
      itemCount: siparisListe.length,
      physics: ScrollBehavior().getScrollPhysics(context),
      itemBuilder: (context, index) {
        saat = siparisListe[index].aSaat.split(" ");
        urunDurum = siparisListe[index].durumIki.split("-");
        return Container(
          margin: EdgeInsets.only(
            top: SizeConfig.safeBlockVertical * 1,
            bottom: SizeConfig.safeBlockVertical * 1 ,
            left: SizeConfig.safeBlockHorizontal * 6,
            right: SizeConfig.safeBlockHorizontal * 6,
          ),
          padding: EdgeInsets.only(
            top: SizeConfig.safeBlockVertical * 1,
            left: SizeConfig.safeBlockHorizontal * 2.5,
            right: SizeConfig.safeBlockHorizontal * 2.5,
          ),
          decoration: GlobalDegiskenler.kartSablon,
          child: cart(index),
        );
      },
    );
  }

  Widget cart(index) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        labelCard("Ürün Adedi : " + siparisListe[index].urunsayisi + " adet"),
        labelCard("Sipariş Tarihi : " + siparisListe[index].aTarih.toString()),
        labelCard("Sipariş Saati : " + saat[1].toString()),
        labelCard("Sipariş Durumu : " + urunDurum[1].toString()),
        /*Sipariş Detay Göster butonu*/
        Container(
          margin: EdgeInsets.only(
            top: SizeConfig.safeBlockVertical * 0.5,
            bottom: SizeConfig.safeBlockVertical * 0.2,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              /*Sipariş detay butonu*/
              Tooltip(
                message: 'Sipariş detay sayfasına\nyönlendirme yapar',
                child: Container(
                  child: RaisedButton.icon(
                    splashColor: GlobalDegiskenler.splashColor,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                    color: GlobalDegiskenler.butonArkaPlanRengi,
                    onPressed: () async {
                      try {
                        var result = await Connectivity().checkConnectivity();
                        if (result == ConnectivityResult.none) {
                          MesajGoster.mesajGoster(
                              context,
                              "Cihazınızı internete bağlayınız.",
                              SoruTipi.Uyari);
                        } else {
                          global.Navigate().navigatePushNoAnimation(
                              context,
                              SiparisDetay(
                                cId: widget.cId,
                                aId: siparisListe[index].aId,
                                saat: siparisListe[index].aSaat,
                                tarih: siparisListe[index].aTarih,
                                adresId: siparisListe[index].aTadresId,
                                durum: siparisListe[index].durumBir,
                              ));
                        }
                      } catch (e) {}
                    },
                    icon: Icon(
                      Icons.fast_forward,
                      color: Colors.white,
                    ),
                    label: LabelCard(
                      fontFamily: GlobalDegiskenler.tlSimgesi,
                      fontSize: SizeConfig.safeBlockHorizontal * 4.5,
                      color: Colors.white,
                      label: 'Sipariş Detay',
                    ),
                  ),
                ),
              ),
              SizedBox(
                width: SizeConfig.safeBlockHorizontal * 2,
              ),
              Container(
                child: RaisedButton.icon(
                  splashColor: GlobalDegiskenler.splashColor,
                  color: siparisListe[index].durumIki.toString() == "0-Bekleyen"
                      ? Colors.red
                      : Colors.grey.withOpacity(0.4),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0),
                  ),
                  icon: Icon(
                    Icons.cancel,
                    color: Colors.white,
                  ),
                  onPressed: iptalEtButonu(index),
                  label: Tooltip(
                    message: 'Sipariş iptal butonu',
                    child: LabelCard(
                      fontFamily: GlobalDegiskenler.tlSimgesi,
                      fontSize: SizeConfig.safeBlockHorizontal * 4.5,
                      color: Colors.white,
                      label: 'İptal Et',
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget sizedBox() {
    return SizedBox(
      height: SizeConfig.blockSizeVertical * 1,
    );
  }

  Widget labelCard(String value) {
    return LabelCard(
      fontFamily: GlobalDegiskenler.tlSimgesi,
      fontWeight: FontWeight.w500,
      fontSize: SizeConfig.safeBlockHorizontal * yaziValue,
      label: value,
      color: Colors.black,
    );
  }

  Widget mesajGoster(String baslik, String aciklama, IconData icon) {
    return Container(
      padding: const EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
      child: global.aciklama(baslik, aciklama, icon),
    );
  }


//endregion
}
