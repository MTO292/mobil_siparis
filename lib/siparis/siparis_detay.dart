import 'package:connectivity_wrapper/connectivity_wrapper.dart';
import 'package:flutter/cupertino.dart';
import '../yardimci_fonksiyonlar/form_mesaj_dondur/mesaj_dondur.dart';
import '../yardimci_fonksiyonlar/sayfa_kadirma_efekti/scrool.dart';
import '../yardimci_widgetlar/manu_kartlar/siparis_cart.dart';
import 'package:flutter/material.dart';
import '../api/response/islem_donus.dart';
import '../api/model/sepet_detay_model.dart';
import '../global_degisken/global_degisken.dart';
import '../responsive_class/size_config.dart';
import '../yardimci_widgetlar/appbar_widgets/appbar_text.dart';
import '../yardimci_widgetlar/label_text/label_widget.dart';
import '../yardimci_fonksiyonlar/form_mesaj_dondur/net_kontrol.dart'
    as net;
import '../global_degisken/global_degisken.dart' as global;

class SiparisDetay extends StatefulWidget {
  final tarih;
  final saat;
  final cId;
  final aId;
  final adresId;
  final durum;

  SiparisDetay(
      {Key key,
      @required this.tarih,
      @required this.saat,
      @required this.cId,
      @required this.aId,
      @required this.adresId,
      @required this.durum})
      : super(key: key);

  @override
  _SiparisDetayState createState() => _SiparisDetayState();
}

class _SiparisDetayState extends State<SiparisDetay> {
  List<SiparisDetayModel> spDetayListe = [];
  IslemDonus islemDonus;
  Future<List<SiparisDetayModel>> spDetay;
  var saat;
  String sGoster;
  var yGoster;
  var yaziValue = 3.5;

  @override
  void initState() {
    super.initState();
    islemDonus = IslemDonus();
    spDetay = islemDonus.siparisDetayGetir(widget.cId, widget.aId, widget.adresId,widget.durum);
    spDetay.then((gelenDeger) {
      //gelen degeri ana ürüne ve ekstra ürüne bölüyoruz
      gelenDeger.forEach((element) {
        if (element.refSira == '0')
          spDetayListe.add(element);
        else
          spDetayListe.last.ekstra.add(element);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    sGoster = widget.saat;
    yGoster = sGoster.split(" ");
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: GlobalDegiskenler.appBarColor,
        title: BaslikText(
          label: 'Sipariş Detay',
        ),
      ),
      body: ScrollConfiguration(
        behavior: MyBehavior(),
        child: ConnectivityWidgetWrapper(
          offlineWidget: net.netKontrol(),
          disableInteraction: false,
          child: FutureBuilder(
            future: spDetay,
            builder: (context, snapshot) {
              /*Veriler yüklenirken bu ekran gösterilecek*/
              if (snapshot.connectionState == ConnectionState.waiting) {
                return progressDondur();
                /*Veri varmı kontrol ediyoruz*/
              } else if (snapshot.connectionState == ConnectionState.done &&
                  !snapshot.hasError &&
                  snapshot.hasData) {
                if (spDetayListe.length == 0) {
                  /*Veri yokken gösterilecek olan ekran*/
                  return Padding(
                    padding: const EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
                    child: aciklama(
                        'Bilgi',
                        'Siparişin detaylarına ulaşılamadı.',
                        Icons.shopping_basket),
                  );
                } else {
                  /*Veri varsa gösterilecek olan ekran*/
                  return urunListele();
                }
              } else if (snapshot.hasError) {
                return hataGoster(snapshot.error.toString());
              } else if (snapshot.data == null && snapshot.hasData == false) {
                return beklenmedikHata();
              }
              return null;
            },
          ),
        ),
      ),
    );
  }

  //region
  Widget sizedBox() {
    return SizedBox(
      height: SizeConfig.safeBlockVertical * 1,
    );
  }

  Widget genelOzellikLabel(String value, {double fontSize, int satirSayisi,fontWeight}) {
    return LabelCard(
      fontFamily: GlobalDegiskenler.tlSimgesi,
      textAlign: TextAlign.start,
      satirSayisi: satirSayisi != null ? satirSayisi : 3,
      fontSize: fontSize,
      color: Colors.black,
      fontWeight:fontWeight ?? FontWeight.normal,
      label: value,
    );
  }

  Widget urunListele() {
    return SingleChildScrollView(
      child: Container(
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.symmetric(
                horizontal: SizeConfig.screenWidth * 0.04,
                vertical: SizeConfig.screenHeight * 0.02,
              ),
              margin: EdgeInsets.symmetric(
                horizontal: SizeConfig.screenWidth * 0.04,
                vertical: SizeConfig.screenHeight * 0.01,
              ),
              decoration: GlobalDegiskenler.kartSablon,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  //Üst kısımda yer alan genel sipariş bilgileri
                  Center(
                      child: genelOzellikLabel('Genel Sipariş Özellikleri',fontWeight: FontWeight.w800,
                          fontSize: SizeConfig.safeBlockHorizontal * 4.3)),
                  sizedBox(),
                  genelOzellikLabel(
                      "Ödeme Türü : " + spDetayListe[0].odemeTur.toString(),
                      fontSize: SizeConfig.safeBlockHorizontal * yaziValue),
                  sizedBox(),
                  genelOzellikLabel(
                      "Sipariş Tarihi : " +
                          widget.tarih +
                          ", " +
                          yGoster[1].toString(),
                      fontSize: SizeConfig.safeBlockHorizontal * yaziValue),
                  spDetayListe[0].aNot.length != 0
                      ? Padding(
                          padding: EdgeInsets.only(
                            top: SizeConfig.safeBlockVertical * 1,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              LabelCard(
                                satirSayisi: 3,
                                fontWeight: FontWeight.bold,
                                label: "Sepet Notu :  ",
                                fontFamily: GlobalDegiskenler.tlSimgesi,
                                fontSize:
                                    SizeConfig.safeBlockHorizontal * yaziValue,
                              ),
                              GestureDetector(
                                child: LabelCard(
                                  satirSayisi: 3,
                                  fontWeight: FontWeight.bold,
                                  label: "Görüntüle",
                                  color: global
                                      .GlobalDegiskenler.butonArkaPlanRengi,
                                  fontFamily: GlobalDegiskenler.tlSimgesi,
                                  fontSize: SizeConfig.safeBlockHorizontal *
                                      yaziValue,
                                ),
                                onTap: () => MesajGoster.mesajGoster(context,
                                    spDetayListe[0].aNot, SoruTipi.NotGoster),
                              ),
                            ],
                          ),
                        )
                      : SizedBox(),
                  sizedBox(),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      genelOzellikLabel("Sipariş Adresi : "),
                      Expanded(
                        child: genelOzellikLabel(
                            spDetayListe[0].adres.toString(),
                            fontSize:
                                SizeConfig.safeBlockHorizontal * yaziValue,
                            satirSayisi: 4),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            //Sipariş detay kısmı
            urunCart(),
          ],
        ),
      ),
    );
  }

  Widget beklenmedikHata() {
    return Container(
      child: Center(
          child: LabelCard(
        fontFamily: GlobalDegiskenler.tlSimgesi,
        textAlign: TextAlign.center,
        satirSayisi: 2,
        color: Colors.black,
        fontSize: SizeConfig.safeBlockHorizontal * 5,
        label: 'Beklenmeyen bir hata oluştu. Lütfen sayfayı tekrar yükleyiniz.',
      )),
    );
  }

  Widget hataGoster(text) {
    return Container(
      child: Center(
        child: LabelCard(
          fontFamily: GlobalDegiskenler.tlSimgesi,
          satirSayisi: 6,
          label: text +
              "İnternet bağlantınızda bir problem tespit edildi. Lütfen tekrar deneyiniz.",
          fontSize: SizeConfig.safeBlockHorizontal * 6,
        ),
      ),
    );
  }

  Widget urunCart() {
    return Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.symmetric(
            horizontal: SizeConfig.screenWidth * 0.04,
            vertical: SizeConfig.screenHeight * 0.01,
          ),
          margin: EdgeInsets.symmetric(
            horizontal: SizeConfig.screenWidth * 0.04,
            vertical: SizeConfig.screenHeight * 0.02,
          ),
          decoration: GlobalDegiskenler.kartSablon,
          child: ListView.builder(
            physics: ScrollBehavior().getScrollPhysics(context),
            shrinkWrap: true,
            itemCount: spDetayListe.length,
            itemBuilder: (context, index) {
              return Column(
                children: <Widget>[
                  SiparisCart(
                    name: spDetayListe[index].urunAdi,
                    img:
                        "${global.resimUrl + global.key + "/s-" + spDetayListe[index].urunId + ".jpg"}",
                    Fiy: spDetayListe[index].toplamTutar,
                    piece: spDetayListe[index].urunAdet,
                    not: spDetayListe[index].urunNot,
                    notTop: () => MesajGoster.mesajGoster(context,
                        spDetayListe[index].urunNot, SoruTipi.NotGoster),
                    ekstra:
                        spDetayListe[index].ekstra.length == 0 ? false : true,
                    ekstraTop: () => showModalBottomSheet(
                      backgroundColor: Colors.transparent,
                      context: context,
                      builder: (context) {
                        return ScrollConfiguration(
                          behavior: MyBehavior(),
                          child: Container(
                            height: SizeConfig.screenHeight * 0.8,
                            child: Column(
                              children: <Widget>[
                                //status bar kadar bi şefaf container ouştururyoruz
                                Expanded(
                                  child: Container(
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(8),
                                          topRight: Radius.circular(8),
                                        )),
                                    child: Column(
                                      children: <Widget>[
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Container(
                                          width: 40,
                                          height: 4,
                                          decoration: BoxDecoration(
                                            color: Color.fromRGBO(
                                                213, 213, 215, 1),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(32)),
                                          ),
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Expanded(
                                          child: CupertinoScrollbar(
                                            child: ListView.builder(
                                                shrinkWrap: true,
                                                itemCount: spDetayListe[index]
                                                    .ekstra
                                                    .length,
                                                itemBuilder:
                                                    (context, ekstraIndex) {
                                                  double fiy = toDouble(
                                                      spDetayListe[index]
                                                          .ekstra[ekstraIndex]
                                                          .toplamTutar);
                                                  return ListTile(
                                                    title: labelCard(
                                                        spDetayListe[index]
                                                            .ekstra[ekstraIndex]
                                                            .urunAdi),
                                                    trailing: labelCard(
                                                      fiy != 0
                                                          ? (fiy.toStringAsFixed(
                                                                  2) +
                                                              " ₺")
                                                          : "Ücretsiz",
                                                      size: SizeConfig
                                                              .safeBlockHorizontal *
                                                          3.5,
                                                    ),
                                                    subtitle: labelCard(
                                                      spDetayListe[index]
                                                              .ekstra[
                                                                  ekstraIndex]
                                                              .urunAdet +
                                                          ' Adet',
                                                      size: SizeConfig
                                                              .safeBlockHorizontal *
                                                          3,
                                                    ),
                                                  );
                                                }),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    ),
                  )
                ],
              );
            },
          ),
        ),
      ],
    );
  }

  double toDouble(String aSfiyat) {
    var sp = aSfiyat.split(',');
    var sp2 = aSfiyat.split('.');
    if (sp.length == 1 && sp2.length == 1) {
      return int.parse(aSfiyat) * 1.0;
    } else if (sp.length == 1) {
      return double.parse(aSfiyat);
    } else {
      var a = sp[0] + "." + sp[1];
      return double.parse(a);
    }
  }

  labelCard(String txt, {double size}) {
    return LabelCard(
      satirSayisi: 1,
      fontWeight: FontWeight.w500,
      label: txt,
      fontFamily: GlobalDegiskenler.tlSimgesi,
      fontSize: size ?? SizeConfig.safeBlockHorizontal * 4.5,
    );
  }
//endregion
}
