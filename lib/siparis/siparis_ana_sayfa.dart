import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:connectivity_wrapper/connectivity_wrapper.dart';
import '../anamenu/ana_menu.dart';
import '../yardimci_fonksiyonlar/sayfa_kadirma_efekti/scrool.dart';
import 'package:flutter/material.dart';
import '../api/response/islem_donus.dart';
import '../api/model/siparis_durum_model.dart';
import '../global_degisken/global_degisken.dart';
import '../responsive_class/size_config.dart';
import '../siparis/siparis_listele.dart';
import '../yardimci_fonksiyonlar/form_mesaj_dondur/mesaj_dondur.dart';
import '../yardimci_fonksiyonlar/shared_sepete_ekle/sepet_shared.dart';
import '../yardimci_widgetlar/appbar_widgets/appbar_text.dart';
import '../yardimci_widgetlar/label_text/label_widget.dart';
import '../yardimci_fonksiyonlar/form_mesaj_dondur/net_kontrol.dart'
    as net;
import '../global_degisken/global_degisken.dart' as global;
import 'package:line_awesome_icons/line_awesome_icons.dart';

class SiparisAnaSayfa extends StatefulWidget {
  //sipariş verildiüinde true olacak ve öylece pop durumunu kontrol edecez
  bool siparisModu;

  SiparisAnaSayfa(this.siparisModu);

  @override
  _SiparisAnaSayfaState createState() =>
      _SiparisAnaSayfaState(this.siparisModu);
}

class _SiparisAnaSayfaState extends State<SiparisAnaSayfa> {
  List<SiparisDurum> spDurum = [];
  IslemDonus islemDonus;
  Future<List<SiparisDurum>> spDurumListe;

  int cId;

  //sipariş verildiüinde true olacak ve öylece pop durumunu kontrol edecez
  bool siparisModu;

  _SiparisAnaSayfaState(this.siparisModu);

  /*Sayfa yüklenirken gerekli listeler doluyor, id değeri shared ile getiriliyor.*/
  @override
  void initState() {
    super.initState();
    islemDonus = IslemDonus();
    loadId();
  }

  loadId() async {
    cId = await SharedPrefLib.cariId();
    setState(() {
      spDurumListe = islemDonus.siparisIlkSayfa(cId);
      spDurumListe.then((gelenListe) {
        spDurum = gelenListe;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return WillPopScope(
      onWillPop: () {
        siparisModu
            ? global.NavigateAndRemoveUntil()
                .navigatePushNoAnimation(context, AnaMenu())
            : Navigator.of(context).pop();
        return null;
      },
      child: Scaffold(
        appBar: AppBar(
          //  leading: GlobalDegiskenler.appCarIconResim,
          backgroundColor: GlobalDegiskenler.appBarColor,
          centerTitle: true,
          title: BaslikText(
            label: 'Sipariş',
          ),
          leading: InkWell(
            borderRadius: BorderRadius.circular(300),
              onTap: () {
                siparisModu
                    ? global.NavigateAndRemoveUntil()
                        .navigatePushNoAnimation(context, AnaMenu())
                    : Navigator.of(context).pop();
                return null;
              },
              child: Icon(Platform.isAndroid ? Icons.arrow_back : Icons.arrow_back_ios)),
        ),
        body: ScrollConfiguration(
          behavior: MyBehavior(),
          child: ConnectivityWidgetWrapper(
            disableInteraction: false,
            offlineWidget: net.netKontrol(),
            child: FutureBuilder(
              future: spDurumListe,
              builder: (context, snapshot) {
                /*Veriler yüklenirken bu ekran gösterilecek*/
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return global.progressDondur();
                }
                /*Veri varmı kontrol ediyoruz*/
                else if (snapshot.connectionState == ConnectionState.done &&
                    !snapshot.hasError &&
                    snapshot.hasData) {
                  if (spDurum.length == 0) {
                    /*Veri yokken gösterilecek olan ekran*/
                    return Padding(
                      padding: const EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
                      child: aciklama('Bilgi', 'Kayıtlı siparişiniz bulunamadı.', Icons.shopping_basket),
                    );
                  } else {
                    /*Veri varsa gösterilecek olan ekran*/
                    return listele();
                  }
                } else if (snapshot.hasError) {
                  return mesajGoster('Hata', 'Lütfen internetinizi kontrol ediniz.', Icons.error_outline);
                } else if (snapshot.data == null) {
                  return mesajGoster('Hata', 'Lütfen Tekrar yüklemeyi deneyiniz.', Icons.error_outline);
                }
                return null;
              },
            ),
          ),
        ),
      ),
    );
  }

  //region Fonksyonlar
  Future siparisListeGider(BuildContext context, int index) async {
    try {
      var result = await Connectivity().checkConnectivity();
      if (result == ConnectivityResult.none) {
        MesajGoster.mesajGoster(
            context, "Cihazınızı internete bağlayınız.", SoruTipi.Uyari);
      } else {
        bool deger = await global.Navigate().navigatePushNoAnimation(
          context,
          SiparisListele(
            durumKod: spDurum[index].aDurum,
            cId: cId,
            baslik: spDurum[index].aDurum == "0"
                ? "Hazırlanan Siparişler"
                : "Önceki Siparişler",
          ),
        );
        if (deger != null && deger) {
          setState(() {
            loadId();
          });
        }
      }
    } catch (e) {}
  }

  //endregion

  //region Widgetlar
  Widget listele() {
    return Container(
      child: ListView.builder(
        itemCount: spDurum.length,
        itemBuilder: (context, index) {
          return cart(index);
        },
      ),
    );
  }

  Widget cart(index) {
    return Padding(
      padding: EdgeInsets.only(
        top: SizeConfig.safeBlockVertical * 2,
        left: SizeConfig.safeBlockHorizontal * 6,
        right: SizeConfig.safeBlockHorizontal * 6,
      ),
      child: Container(
        decoration: BoxDecoration(
          color: spDurum[index].aDurum == "0"
              ? Color.fromRGBO(230, 255, 247, 1)
              : Color.fromRGBO(255, 235, 230, 1),
          borderRadius: BorderRadius.circular(8.0),
          border: Border.all(color: Colors.grey.withOpacity(0.5)),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.3),
              spreadRadius: 0.5,
              blurRadius: 2,
              offset: Offset(0, 1),
            )
          ],
        ),
        child: Material(
          color: Colors.transparent,
          child: InkWell(
            splashColor: Colors.transparent,
            borderRadius: BorderRadius.circular(12.0),
            onTap: () async {
              await siparisListeGider(context, index);
            },
            child: Padding(
              padding: EdgeInsets.symmetric(
                vertical: SizeConfig.safeBlockVertical * 2,
                horizontal: SizeConfig.safeBlockHorizontal * 2.5,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  LabelCard(
                    fontFamily: GlobalDegiskenler.tlSimgesi,
                    fontWeight: FontWeight.w500,
                    fontSize: SizeConfig.safeBlockHorizontal * 5,
                    label: spDurum[index].aDurum == "0"
                        ? "HAZIRLANAN SİPARİŞLER"
                        : "GEÇMİŞ SİPARİŞLER",
                    satirSayisi: 2,
                  ),
                  SizedBox(height: SizeConfig.blockSizeVertical * 1),
                  LabelCard(
                    fontFamily: GlobalDegiskenler.tlSimgesi,
                    fontWeight: FontWeight.w500,
                    fontSize: SizeConfig.safeBlockHorizontal * 5,
                    satirSayisi: 2,
                    label: spDurum[index].adet + " adet siparişiniz var",
                  ),
                  sizedBox(),
                  /*Yönlendirme butonu*/
                  Container(
                    child: Tooltip(
                      message: 'Sipariş Liste sayfasına yönlendirir.',
                      child: GestureDetector(
                        onTap: () async {
                          await siparisListeGider(context, index);
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(
                              LineAwesomeIcons.arrow_circle_o_right,
                              color: Colors.black,
                              size: SizeConfig.safeBlockHorizontal * 10,
                            ),
                            SizedBox(
                              width: 10.0,
                            ),
                            LabelCard(
                              label: 'Sipariş Listele',
                              color: Colors.black,
                              fontSize: SizeConfig.blockSizeVertical * 3,
                              fontWeight: FontWeight.w600,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget sizedBox() {
    return SizedBox(
      height: SizeConfig.blockSizeVertical * 1,
    );
  }

  Widget mesajGoster(String baslik , String aciklama , IconData icon) {
    return Container(
      padding: const EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
      child: global.aciklama(baslik,aciklama, icon),
    );
  }

//endregion
}
