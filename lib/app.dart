import 'package:country_code_picker/country_localizations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'global_degisken/global_degisken.dart';
import 'splash/splash_ekrani.dart';

class MobilSiparisApp extends MaterialApp {
  MobilSiparisApp({Key key})
      : super(
            key: key,
            supportedLocales: [
              //Ülke kodunda ülkeler hangi dilde gelsin Türkçe desteklenmiyor türkçe secildiği zaman tüm ülkelerin adı kendi dil ile yazılıryor.
              const Locale('en', 'US'),
              const Locale('he', 'IL'),
            ],
            localizationsDelegates: [
              //Ülke Kodu getirildiğinde bölgeye göre sıralamaya yarar
              CountryLocalizations.delegate,
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
            ],
            theme: ThemeData(
              /*Text-field içinde seçilen yazıdan sonra çıkan butonların boyutunu ayarlıyor*/
              buttonTheme: ButtonThemeData(minWidth: 11),
              textTheme: TextTheme(button: TextStyle(fontSize: 11)),
              primaryColor: Colors.black,
              accentColor: GlobalDegiskenler.butonArkaPlanRengi,
              appBarTheme: AppBarTheme(
                iconTheme: IconThemeData(color: Colors.black),
                elevation: 0,
                brightness: Brightness.light,
                color: Colors.white,
              ),
              scaffoldBackgroundColor: Colors.white,
            ),
            debugShowCheckedModeBanner: false,
           home: SplashGoster());
//home: WebViewExample());
}
