import '../anamenu/ana_menu.dart';
import '../bloc/blocs_exports.dart';
import '../global_degisken/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:store_redirect/store_redirect.dart';
import '../locator.dart';
import '../responsive_class/size_config.dart';
import '../yardimci_fonksiyonlar/form_mesaj_dondur/mesaj_dondur.dart';
import '../yardimci_fonksiyonlar/sayfa_kadirma_efekti/scrool.dart';
import 'package:flutter/material.dart';
import '../slider/slider.dart';
import '../yardimci_fonksiyonlar/shared_sepete_ekle/sepet_shared.dart';
import '../global_degisken/global_degisken.dart' as global;
import '../api/response/islem_donus.dart';
import 'package:package_info/package_info.dart';
import 'package:flutter_bloc/flutter_bloc.dart';


class SplashGoster extends StatefulWidget {
  bool versiyonKontrol = false;

  SplashGoster({Key key, this.versiyonKontrol}) : super(key: key);

  @override
  _SplashGosterState createState() => _SplashGosterState(versiyonKontrol);
}

class _SplashGosterState extends State<SplashGoster> {
  bool ilkGirisKontrol = false;
  bool girisYapildi = false;
  bool sliderAktifMi = false;
  bool uygGuncellendimi = true;
  double imageOpacity = 0;
  IslemDonus islemDonus = IslemDonus();
  bool versiyonKontrol;

  _SplashGosterState(this.versiyonKontrol);


  @override
  void initState() {
    super.initState();
    sharedKontrol();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      setState(() {
        imageOpacity = 1;
      });
    });
    getAppInfo();
  }



  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return  Scaffold(
        body: AnimatedOpacity(
          duration: Duration(seconds: 1),
          opacity: imageOpacity,
          child: ScrollConfiguration(
            behavior: MyBehavior(),
            child: Container(
              decoration: BoxDecoration(color: Colors.white),
              child: Center(
                child: Column(
                  children: <Widget>[
                    Spacer(),
                    Container(
                      height: SizeConfig.safeBlockVertical * 35,
                      width: SizeConfig.safeBlockHorizontal * 75,
                      child: Image.asset(
                        "assets/images/splash_logo.png",
                        fit: BoxFit.contain,
                      ),
                    ),
                    Spacer(
                      flex: 2,
                    ),
                    global.progressDondur(
                      backgroundColor: Colors.transparent,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
    );
  }


  Future sharedKontrol() async {
    ilkGirisKontrol = SharedPrefLib.ilkGiris;
    sliderAktifMi = SharedPrefLib.sliderAktifMi;
    uygGuncellendimi = SharedPrefLib.uygulamaGuncelledi;
    girisYapildi = SharedPrefLib.girisYapildi;
    locatorKontrol();
    //sepet iconunun bildirimi için oluşturuldu
    var gelenDeger = await SharedPrefLib.listeyiGetir();
    //sepet sayacı için bloc la değerini atıyoruz
    context.bloc<SepetSayacCubit>().esitle(gelenDeger.length);
    setState(() {});
  }

  void locatorKontrol() {
    if (girisYapildi) {
      locator<Get>().loggedin = true;
    } else {
      locator<Get>().loggedin = false;
    }

  }

  guncelle() {
    return () {
      try {
        StoreRedirect.redirect(
            androidAppId: global.AppInfo.paketIsmi,
            iOSAppId: global.AppInfo.appleStoreAppId);
      } catch (e) {}
    };
  }

  //splash ekranında versiyon sorgulaması yapacak
  getVersiyon() {
    islemDonus.versiyonKontrols().then((gelenDeger) async {
      if (gelenDeger.cariBasariDonus == global.numarator.basarili) {
        if (gelenDeger.versiyon == global.numarator.basarili) {
          //güncel versiyon
          sayfaYonlendir();
        } else if (gelenDeger.versiyon == global.numarator.apiVersiyonuEski) {
          //api versiyonu eski
          MesajGoster.mesajGoster(
              context,
              "Sunucu versiyonu eski. Lütfen daha sonra tekrar deneyiniz.",
              SoruTipi.Hata,
              butonGorunurluk: false);
        } else if (gelenDeger.versiyon ==
            global.numarator.uygulamaVersiyonuEski) {
          //app versiyonu eski
          //eger uygulama versiyonu eski ise SharedPreferencesi temizle
          SharedPreferences pref = await SharedPreferences.getInstance();
          pref.clear();
          MesajGoster.soruSor(
              context,
              'Uygulama versiyonu eski. Lütfen uygulamanızı mağazadan güncelleyiniz.',
              SoruTipi.Soru,
              evet: guncelle(),
              icerikEvet: 'Güncelle');
        } else if (gelenDeger.versiyon == global.numarator.hizmetDisi) {
          //hizmet dışı
          MesajGoster.mesajGoster(
              context,
              "Bakımdan dolayı hizmet veremiyoruz. Lütfen daha sonra tekrar deneyiniz.",
              SoruTipi.Hata,
              butonGorunurluk: false);
        } else {
          //hata
          MesajGoster.mesajGoster(context,
              "Hata Oluştu. Lütfen daha sonra tekrar deneyiniz.", SoruTipi.Hata,
              butonGorunurluk: false);
        }
      } else {
        //hata
        MesajGoster.mesajGoster(context,
            "Hata Oluştu. Lütfen daha sonra tekrar deneyiniz.", SoruTipi.Hata,
            butonGorunurluk: false);
      }
    });
  }

  //Uygulama Bilgilerini Çekmek için oluşturuldu
  getAppInfo() {
    PackageInfo.fromPlatform().then((PackageInfo packageInfo) {
      if (packageInfo.appName != null) {
        global.AppInfo.uygulamaAdi = packageInfo.appName;
      }
      global.AppInfo.paketIsmi = packageInfo.packageName;
      global.AppInfo.versiyon = packageInfo.version;
      global.AppInfo.buildNumarasi = packageInfo.buildNumber;
      versiyonKontrol == true ? getVersiyon() : sayfaYonlendir();
    });
  }

  Future sayfaYonlendir() async {
    Future.delayed(Duration(seconds: 3), () async {
      if (uygGuncellendimi) {
        SharedPreferences pref = await SharedPreferences.getInstance();
        pref.clear();
        //sepet sayacı sıfırlıyoruz
        context.bloc<SepetSayacCubit>().sifirla();
        pref.setBool("uygulamaGuncellendi", false);
        locator<Get>().loggedin = false;
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => MySliderWidget()));
      } else if (!sliderAktifMi) {
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => MySliderWidget()));
      } else {
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => AnaMenu()));
      }
    });
  }

}
