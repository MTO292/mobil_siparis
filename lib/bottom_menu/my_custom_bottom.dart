import 'dart:io';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../arama/arama_ana.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../bloc/blocs_exports.dart';
import '../profil/gy_profil/gy_profil_ana.dart';
import '../profil/profil/profil_ana.dart';
import '../anamenu/ana_menu.dart';
import '../global_degisken/get_it.dart';
import '../global_degisken/global_degisken.dart' as global;
import '../responsive_class/size_config.dart';
import '../sepetim/sepet_ana_sayfa.dart';
import 'package:line_awesome_icons/line_awesome_icons.dart';
import '../locator.dart';

///Alt barda cıkan bottonları temsil ediyor
class MyBottomNavBar extends StatefulWidget {
  int index;

  MyBottomNavBar(this.index);

  @override
  _MyBottomNavBarState createState() => _MyBottomNavBarState();
}

class _MyBottomNavBarState extends State<MyBottomNavBar> {

  var anaMenuKey = PageStorageKey('ana_menu_key');

  GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  List<Widget> login = [
    AramaAna(),
    SepetAnaSayfa(),
    ProfilTemelSayfa(),
  ];

  List<Widget> logout = [
    AramaAna(),
    SepetAnaSayfa(),
    GirisYokProfil(),
  ];

  logoutDondur(index){
    return index == 0 ? AnaMenu(key:anaMenuKey) : logout[index - 1];
  }

  loginDondur(index){
    return index == 0 ? AnaMenu(key:anaMenuKey) : login[index - 1];
  }

  items(int aded) {
    return [
      BottomNavigationBarItem(
          icon: Icon(!Platform.isAndroid
              ? CupertinoIcons.home
              : LineAwesomeIcons.home),
          activeIcon: Icon(
            !Platform.isAndroid ? CupertinoIcons.home : Icons.home,
            /*size: SizeConfig.safeBlockHorizontal * 8.5*/
          ),
          title: Text('Anasayfa')),
      BottomNavigationBarItem(
          icon: Icon(
            !Platform.isAndroid ? CupertinoIcons.search : Icons.search,
          ),
          activeIcon: Icon(
            !Platform.isAndroid ? CupertinoIcons.search : Icons.search,
            /* size: SizeConfig.safeBlockHorizontal * 8.5*/
          ),
          title: Text('Arama')),
      BottomNavigationBarItem(
          icon: Container(
            child: Stack(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                  child: Icon(!Platform.isAndroid
                      ? CupertinoIcons.shopping_cart
                      : LineAwesomeIcons.shopping_cart),
                ),
                aded != 0 ? Positioned(
                  top: 0,
                  right: 0,
                  child: CircleAvatar(
                    child: Text(
                      aded.toString(),
                      style: TextStyle(fontSize: 8.0, color: Colors.white),
                    ),
                    backgroundColor: Colors.red,
                    maxRadius: 7.5,
                  ),
                ):SizedBox(),
              ],
              overflow: Overflow.clip,
            ),
          ),
          title: Text('Sepetim')),
      BottomNavigationBarItem(
          icon: Icon(!Platform.isAndroid
              ? CupertinoIcons.person
              : Icons.perm_identity),
          activeIcon: Icon(
            !Platform.isAndroid ? CupertinoIcons.person_solid : Icons.person,
            /*size: SizeConfig.safeBlockHorizontal * 8.5*/
          ),
          title: Text('Profil')),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SepetSayacCubit,int>(
      builder: (context,state){
        return  BottomNavigationBar(
          currentIndex: widget.index,
          unselectedLabelStyle: TextStyle(
            fontFamily: 'TL',
          ),
          selectedLabelStyle: TextStyle(
            fontFamily: 'TL',
          ),
          selectedItemColor: global.GlobalDegiskenler.butonArkaPlanRengi,
          unselectedItemColor: Colors.black,
          type: BottomNavigationBarType.fixed,
          backgroundColor: global.GlobalDegiskenler.bottomMenuColor,
          iconSize: SizeConfig.safeBlockHorizontal * 7.5,
          selectedFontSize: SizeConfig.safeBlockHorizontal * 2.5,
          unselectedFontSize: SizeConfig.safeBlockHorizontal * 2.5,
          onTap: (tappedIndex) {
            //ana menüye tıkland ise page routeyı sil
            tappedIndex != 0 ? Navigator.push(context,PageRouteBuilder(
              pageBuilder: (context, anim1, anim2) => locator<Get>().loggedin? loginDondur(tappedIndex): logoutDondur(tappedIndex),
              transitionsBuilder: tappedIndex != 2 ? (context, anim1, anim2, child) => Container(child: child): (context, animation, secondaryAnimation, child) {
                var begin = Offset(0.0, 1.0);var end = Offset.zero;var curve = Curves.ease; var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));
                return SlideTransition(
                  position: animation.drive(tween),
                  child: child,
                );
              },
              transitionDuration:
              Duration(milliseconds: tappedIndex != 2 ? 70 : 300),
            ),
            ):Navigator.pushAndRemoveUntil(context,PageRouteBuilder(
              pageBuilder: (context, anim1, anim2) => locator<Get>().loggedin? loginDondur(tappedIndex): logoutDondur(tappedIndex),
              transitionsBuilder:  (context, anim1, anim2, child) => Container(child: child),
              transitionDuration:
              Duration(milliseconds: tappedIndex != 2 ? 70 : 300),
            ),(a)=> false
            );
          },
          items: items(state),
        );
      },
    );
  }
}
