import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../../global_degisken/global_degisken.dart';
import '../../../profil/Profil_guncelle/view_model/guncelle_view_model.dart';
import '../../../yardimci_fonksiyonlar/form_mesaj_dondur/form_focus_node.dart';
import '../../../yardimci_fonksiyonlar/form_regex_fonksiyonlar/regex_kontrol.dart';
import '../../../yardimci_widgetlar/form_label_widgets/text_form_field.dart';
import 'package:provider/provider.dart';

class PgKullaniciAdi extends StatefulWidget {
  @override
  _PgKullaniciAdiState createState() => _PgKullaniciAdiState();
}

class _PgKullaniciAdiState extends State<PgKullaniciAdi> {
  var isimController = new TextEditingController();

  FocusNode _adFocus = FocusNode();

  String _ad;

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    _ad = Provider.of<GuncelleViewModel>(context).cari.isim;
    isimController.text = _ad;
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<GuncelleViewModel>(
      builder: (BuildContext context , provider , widget) => MyTextFormField(
        icon: formIcon(
          !Platform.isAndroid ? CupertinoIcons.person_solid : Icons.person,
        ),
        labelText: 'Kullanıcı Adı',
        karakterSayisi: 50,
        ileriTusu: TextInputAction.next,
        focusNode: _adFocus,
        onFieldSubmitted: (ileri) {
          fieldFocusChange(
              context, _adFocus, provider.mailFocus);
        },
        satirSayisi: 1,
        fontWeight: FontWeight.bold,
        onsavedGelenDeger: (String gelenDeger) {
          provider.cari.isim = gelenDeger.trim();
        },
        kontroller: isimController,
        validatorFonksiyon: (String girilen) {
          String sonuc = RegexKontrol.isimKontrol(girilen);
          if (sonuc == null) {
            provider.validateAdSonuc = true;
          }else {
            provider.validateAdSonuc = false;
          }
          return sonuc;
        },
        textStyle: TextStyle(
          fontSize: MediaQuery.of(context).size.width * (0.04),
        ),
      ),
    );
  }
}
