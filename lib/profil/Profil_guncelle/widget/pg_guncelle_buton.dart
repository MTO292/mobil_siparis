import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import '../../../api/response/islem_donus.dart';
import '../../../global_degisken/global_degisken.dart';
import '../../../profil/Profil_guncelle/view_model/guncelle_view_model.dart';
import '../../../responsive_class/size_config.dart';
import '../../../yardimci_fonksiyonlar/form_mesaj_dondur/dr_dialog.dart';
import '../../../yardimci_fonksiyonlar/form_mesaj_dondur/mesaj_dondur.dart';
import '../../../yardimci_widgetlar/label_text/label_widget.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PgGuncelleButon extends StatefulWidget {
  @override
  _PgGuncelleButonState createState() => _PgGuncelleButonState();
}

class _PgGuncelleButonState extends State<PgGuncelleButon> {
  IslemDonus _islemDonus = IslemDonus();

  @override
  Widget build(BuildContext context) {
    return Consumer<GuncelleViewModel>(
      builder: (BuildContext context, provider, widget) => Container(
        height: SizeConfig.screenHeight * 0.06,
        width: double.infinity,
        child: RaisedButton(
          splashColor: GlobalDegiskenler.splashColor,
          /*buton text label card*/
          child: LabelCard(
            fontFamily: GlobalDegiskenler.tlSimgesi,
            label: 'Kaydet',
            color: Colors.white,
            fontSize: SizeConfig.safeBlockHorizontal * 5,
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(
              4,
            ),
          ),
          color: GlobalDegiskenler.butonArkaPlanRengi,
          onPressed: () async {
            var result = await Connectivity().checkConnectivity();
            if (result == ConnectivityResult.none) {
              MesajGoster.mesajGoster(
                  context, "Cihazınızı internete bağlayınız.", SoruTipi.Uyari);
            } else {
              butonIslemleri(provider);
            }
          },
        ),
      ),
    );
  }

  void butonIslemleri(GuncelleViewModel provider) {
    try {
      provider.formKeySave();
      provider.textFieldAutoKontrol();
      provider.formKeyValidate();
      if (provider.validateAdSonuc && provider.validateMailSonuc) {
        /*Güncelleme işlemi yapılırken json body kısmının doldurulduğu yer*/
        progress().prBaslat(context, "Lüten bekleyiniz.");
        /*Güncelleme işlemi yapacak olan metodun parametrelerinin doldurulduğu yer*/
        _islemDonus
            .uyeGuncelle(provider.cari.cariKod, provider.cari.isim, provider.cari.mail, provider.cari.telefonNo)
            .then((basarili) async {
          if (basarili.cariBasariDonus == "1") {
            SharedPreferences prefs = await SharedPreferences.getInstance();
            prefs.setString('cariMailDonus', provider.cari.mail);
            prefs.setString('cariTelefonDonus', provider.cari.telefonNo);
            prefs.setString('cariKodDonus', provider.cari.cariKod);
            prefs.setString("cariAdDonus", provider.cari.isim);
            MesajGoster.mesajGoster(
                    context,
                    "Kullanıcı başarılı bir şekilde güncellendi.",
                    SoruTipi.Basarili)
                .whenComplete(() {
              progress().prBitir(context);
              Navigator.pop(context, true);
            });
          } else if (basarili.cariBasariDonus == "7") {
            MesajGoster.mesajGoster(
                    context,
                    "Kullanıcı bilgileri bulunamadı. Lütfen tekrar deneyiniz.",
                    SoruTipi.Hata)
                .whenComplete(() {
              progress().prBitir(context);
            });
          } else if (basarili.cariBasariDonus == "0") {
            MesajGoster.mesajGoster(
                    context,
                    "Güncelleme işlemi başarısızlıkla sonuçlandı.",
                    SoruTipi.Hata)
                .whenComplete(() {
              progress().prBitir(context);
            });
          } else if (basarili.cariBasariDonus == "3") {
            MesajGoster.mesajGoster(
                    context,
                    "Beklenmedik bir hata oluştu. Lütfen daha sonra tekrar deneyiniz.",
                    SoruTipi.Hata)
                .whenComplete(() {
              progress().prBitir(context);
            });
          } else if (basarili.cariBasariDonus ==
              numarator.uygulamaVersiyonuEski) {
            progress().prBitir(context);
            logoEkraninaGit(context);
          } else if (basarili.cariBasariDonus == numarator.apiVersiyonuEski) {
            progress().prBitir(context);
            logoEkraninaGit(context);
          } else if (basarili.cariBasariDonus == numarator.hizmetDisi) {
            progress().prBitir(context);
            logoEkraninaGit(context);
          }
        });
      } else {
        MesajGoster.mesajGoster(
            context, 'Bilgilerinizi doğru şekilde giriniz.', SoruTipi.Uyari);
      }
      setState(() {});
    } catch (e) {}
  }
}
