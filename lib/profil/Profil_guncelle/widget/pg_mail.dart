import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../../global_degisken/global_degisken.dart';
import '../../../profil/Profil_guncelle/view_model/guncelle_view_model.dart';
import '../../../responsive_class/size_config.dart';
import '../../../yardimci_fonksiyonlar/form_regex_fonksiyonlar/regex_kontrol.dart';
import '../../../yardimci_widgetlar/form_label_widgets/text_form_field.dart';
import 'package:provider/provider.dart';

class PgMail extends StatefulWidget {
  @override
  _PgMailState createState() => _PgMailState();
}

class _PgMailState extends State<PgMail> {
  var _mailController = new TextEditingController();

  String _mail;

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    _mail = Provider.of<GuncelleViewModel>(context).cari.mail;
    _mailController.text = _mail;
  }

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<GuncelleViewModel>(context);
    return  MyTextFormField(
        icon: formIcon(
            !Platform.isAndroid ? CupertinoIcons.mail_solid : Icons.email),
        labelText: 'E-Posta',
        karakterSayisi: 50,
        focusNode: provider.mailFocus,
        onFieldSubmitted: (ileri) {},
        ileriTusu: TextInputAction.done,
        satirSayisi: 1,
        fontWeight: FontWeight.bold,
        textInputType: TextInputType.emailAddress,
        kontroller: _mailController,
        validatorFonksiyon: (String girilen) {
          String sonuc = RegexKontrol.mailKontrol(girilen);
          if (sonuc == null) {
            provider.validateMailSonuc = true;
          }else{
            provider.validateMailSonuc = false;
          }
          return sonuc;
        },
        onsavedGelenDeger: (String gelenDeger) {
          provider.cari.mail = gelenDeger.trim();
        },
        textStyle: TextStyle(
          fontSize: SizeConfig.screenWidth * (0.04),
        ),
    );
  }
}
