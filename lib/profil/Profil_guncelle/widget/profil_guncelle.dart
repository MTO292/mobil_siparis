import '../../../global_degisken/global_degisken.dart' as global;
import 'package:connectivity_wrapper/connectivity_wrapper.dart';
import 'package:flutter/material.dart';
import '../../../global_degisken/global_degisken.dart';
import '../../../profil/Profil_guncelle/view_model/guncelle_view_model.dart';
import '../../../profil/Profil_guncelle/widget/pg_guncelle_buton.dart';
import '../../../profil/Profil_guncelle/widget/pg_kullanici_adi.dart';
import '../../../profil/Profil_guncelle/widget/pg_mail.dart';
import '../../../responsive_class/size_config.dart';
import '../../../yardimci_fonksiyonlar/form_mesaj_dondur/net_kontrol.dart'
    as net;
import '../../../yardimci_fonksiyonlar/sayfa_kadirma_efekti/scrool.dart';
import '../../../yardimci_widgetlar/appbar_widgets/appbar_text.dart';
import 'package:provider/provider.dart';

import 'cari_aciklama_goster.dart';

class ProfilGuncelle extends StatefulWidget {
  @override
  _ProfilGuncelleState createState() => _ProfilGuncelleState();
}

class _ProfilGuncelleState extends State<ProfilGuncelle> {
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    context.read<GuncelleViewModel>().sayfaYukle();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      /*Sayfadaki en üst başlığı temsil ediyor*/
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: GlobalDegiskenler.appBarColor,
        title: BaslikText(
          label: 'Profil Bilgileri',
        ),
      ),
      body: ConnectivityWidgetWrapper(
        offlineWidget: net.netKontrol(),
        disableInteraction: false,
        child: Consumer<GuncelleViewModel>(builder:
            (BuildContext context, GuncelleViewModel provider, Widget child) {
          if (provider.durum == CariDurum.Yukleniyor ||
              provider.durum == CariDurum.Init) return global.progressDondur();
          if (provider.durum == CariDurum.Yuklendi) {
            if (provider.cari == null) {
              return PgAciklamaGoster(
                  'Hata', 'Tekrar yüklemeyi deneyiniz.', Icons.error_outline);
            } else {
              return Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  // border: Border.all(color: Colors.grey.withOpacity(0.5)),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.3),
                      spreadRadius: 0.5,
                      blurRadius: 2,
                      offset: Offset(0, 1),
                    )
                  ],
                ),
                child: ScrollConfiguration(
                  behavior: MyBehavior(),
                  child: SingleChildScrollView(
                    physics: ScrollBehavior().getScrollPhysics(context),
                    child: Container(
                      color: Colors.white,
                      child: Container(
                        padding: EdgeInsets.only(
                          top: MediaQuery.of(context).size.height * (0.04),
                        ),
                        child: Column(
                          children: <Widget>[
                            Container(
                              /*İçerideki forma margin veriyor*/
                              margin: EdgeInsets.only(
                                  left: SizeConfig.safeBlockHorizontal * 8,
                                  right: SizeConfig.safeBlockHorizontal * 8,
                                  bottom: SizeConfig.safeBlockVertical * 4),
                              child: Form(
                                key: provider.formKey,
                                autovalidate: provider.otoKontrol,
                                child: Column(
                                  children: <Widget>[
                                    PgKullaniciAdi(),
                                    /*Kullanıcı Adı text field alanını temsil eder*/
                                    SizedBox(
                                        height:
                                            MediaQuery.of(context).size.height *
                                                (0.02)),
                                    /*Mail - text form field temsil eder*/
                                    PgMail(),
                                    SizedBox(
                                        height:
                                            MediaQuery.of(context).size.height *
                                                (0.02)),
                                    /*Bilgilerini güncelle butonunu temsil ediyor.*/
                                    PgGuncelleButon(),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              );
            }
          } else {
            return PgAciklamaGoster(
                'Hata', 'Tekrar yüklemeyi deneyiniz.', Icons.error_outline);
          }
        }),
      ),
    );
  }
}
