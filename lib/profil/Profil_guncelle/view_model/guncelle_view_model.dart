import 'package:flutter/cupertino.dart';
import '../../../api/model/profil_cari_model.dart';
import '../../../profil/Profil_guncelle/data/profil_guncelle_prefs.dart';

enum CariDurum { Init, Yukleniyor, Yuklendi, Hata }

class GuncelleViewModel with ChangeNotifier {
  ProfilCariModel _cari;
  ProfilGncellePrefs _prefs;
  var _formKey;
  bool _otoKontrol;
  bool _validateAdSonuc;
  bool _validateMailSonuc;
  FocusNode _mailFocus;
  CariDurum _durum;

  GuncelleViewModel() {
    _durum = CariDurum.Init;
    _cari = ProfilCariModel();
    _prefs = ProfilGncellePrefs();
    _formKey = GlobalKey<FormState>();
    _mailFocus = FocusNode();
    _otoKontrol = false;
    _validateAdSonuc = false;
    _validateMailSonuc = false;
  }

  CariDurum get durum => _durum;

  bool get validateMailSonuc => _validateMailSonuc;

  bool get validateAdSonuc => _validateAdSonuc;

  ProfilCariModel get cari => _cari;

  get formKey => _formKey;

  bool get otoKontrol => _otoKontrol;

  FocusNode get mailFocus => _mailFocus;

  set durum(CariDurum value) {
    _durum = value;
    notifyListeners();
  }

  set cari(ProfilCariModel value) {
    _cari = value;
  }

  set validateMailSonuc(bool value) {
    _validateMailSonuc = value;
  }

  set validateAdSonuc(bool value) {
    _validateAdSonuc = value;
  }

  set formKey(value) {
    _formKey = value;
    notifyListeners();
  }

  set otoKontrol(bool value) {
    _otoKontrol = value;
    notifyListeners();
  }

  set mailFocus(FocusNode value) {
    _mailFocus = value;
  }

  formKeySave() {
    _formKey.currentState.save();
  }

  formKeyValidate() {
    _formKey.currentState.validate();
    notifyListeners();
  }

  textFieldAutoKontrol() {
    _otoKontrol = true;
  }

  Future sayfaYukle() async {
    _durum = CariDurum.Yukleniyor;
    try {
      cari = await _prefs.sfYukle();
      durum = CariDurum.Yuklendi;
    } catch (e) {
      durum = CariDurum.Hata;
    }
  }
}
