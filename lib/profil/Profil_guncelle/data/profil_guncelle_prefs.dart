import '../../../api/model/profil_cari_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfilGncellePrefs {
  SharedPreferences _prefs ;
  ProfilCariModel _cari;

  ProfilGncellePrefs(){
    _cari = ProfilCariModel();
  }

  /*Güncelleme işlem için kullanılacak olan bilgilerin shared preferences ile değişkenlere aktarılması*/
  Future sfYukle() async {
    _prefs = await SharedPreferences.getInstance();
    _cari.isim = (_prefs.getString('cariAdDonus') ?? '');
    _cari.mail = (_prefs.getString("cariMailDonus") ?? '');
    _cari.cariKod = (_prefs.getString("cariKodDonus") ?? '');
    _cari.cariId = (_prefs.getInt("cariIdDonus") ?? '');
    _cari.telefonNo = (_prefs.getString("cariTelefonDonus") ?? '');
    return _cari;
  }
}