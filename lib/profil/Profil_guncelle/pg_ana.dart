import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../profil/Profil_guncelle/view_model/guncelle_view_model.dart';
import '../../profil/Profil_guncelle/widget/profil_guncelle.dart';
import '../../responsive_class/size_config.dart';
import 'package:provider/provider.dart';

class PbGuncelle extends StatefulWidget {

  @override
  _PbGuncelleState createState() => _PbGuncelleState();
}

class _PbGuncelleState extends State<PbGuncelle> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      child: ChangeNotifierProvider<GuncelleViewModel>(
          create: (context) => GuncelleViewModel(),
          child: ProfilGuncelle()),
    );
  }
}
