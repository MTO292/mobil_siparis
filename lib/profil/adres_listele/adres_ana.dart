import '../../profil/adres_listele/view_model/adres_view_model.dart';
import '../adres_listele/widget/adres.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../responsive_class/size_config.dart';

class AdresGetir extends StatefulWidget {
  bool siparisModu = false;
  double genelTopFiy;

  AdresGetir({
    Key key,
    @required this.siparisModu,
    @required this.genelTopFiy,
  }) : super(key: key);

  @override
  _AdresState createState() => _AdresState(this.siparisModu, this.genelTopFiy);
}

class _AdresState extends State<AdresGetir> {
  //true ise step bar ve radio buto gorunur olur
  bool siparisModu;

  double toplamTutar;

  _AdresState(this.siparisModu, this.toplamTutar);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return ChangeNotifierProvider<AdresViewModel>(
        create: (context) => AdresViewModel(siparisModu, toplamTutar),
        child: Adres());
  }
}
