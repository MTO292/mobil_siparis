import 'package:flutter/material.dart';
import '../../../global_degisken/global_degisken.dart';
import '../../../responsive_class/size_config.dart';
import '../../../yardimci_widgetlar/label_text/label_widget.dart';

class AdresLabel extends StatelessWidget {
  String label;
  FontWeight weight;
  double fontSize;
  Color color;
  EdgeInsets padding;

  AdresLabel({this.label, this.weight, this.fontSize, this.color,this.padding});

  @override
  Widget build(BuildContext context) {
    return LabelCard(
      color: color,
      fontFamily: GlobalDegiskenler.tlSimgesi,
      textAlign: TextAlign.start,
      label: label,
      fontWeight: weight,
      satirSayisi: 2,
      fontSize: fontSize ?? SizeConfig.safeBlockHorizontal * 3.5,
      padding: padding,
    );
  }
}
