import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import '../../../global_degisken/global_degisken.dart' as global;
import '../view_model/adres_view_model.dart';
import '../../../sepetim/siparis_tamamla.dart';
import '../../../yardimci_fonksiyonlar/form_mesaj_dondur/mesaj_dondur.dart';
import '../../../yardimci_fonksiyonlar/shared_sepete_ekle/sepet_shared.dart';
import '../../../yardimci_widgetlar/alt_bar_buton/alt_bar_buton.dart';
import 'package:provider/provider.dart';

class AdresSepetOnayla extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<AdresViewModel>(context);

    /*Sepeti tamamla kısmı*/
    return AltBarButon(
      onTap: () async {
        try {
          var result = await Connectivity().checkConnectivity();
          if (result == ConnectivityResult.none) {
            MesajGoster.mesajGoster(
                context, "Cihazınızı internete bağlayınız.", SoruTipi.Uyari);
          } else if (provider.secilenAdresIndex == null) {
            MesajGoster.mesajGoster(
                context,
                "Devam edebilmek için teslimat adresi seçiniz.",
                SoruTipi.Uyari);
          } else {
            SharedPrefLib.siparisAdres(
              provider.adres[provider.secilenAdresIndex].detay +
                  '/' +
                  provider.adres[provider.secilenAdresIndex].semtAdi +
                  '/' +
                  provider.adres[provider.secilenAdresIndex].ilceAdi +
                  '/' +
                  provider.adres[provider.secilenAdresIndex].ilAdi +
                  '/' +
                  provider.adres[provider.secilenAdresIndex].ulkeAdi,
            );
            SharedPrefLib.adrsId(
                provider.adres[provider.secilenAdresIndex].sira);
            global.Navigate().navigatePushNoAnimation(
                context,
                SiparisTamamla(
                  toplamTutar: provider.toplamTutar,
                ));
          }
        } catch (e) {}
      },
      tutar: provider.toplamTutar,butontTxt: 'Adres Seç',
    );
  }
}
