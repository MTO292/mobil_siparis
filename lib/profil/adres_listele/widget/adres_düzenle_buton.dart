import 'package:flutter/material.dart';
import '../../../api/model/adres_model.dart';
import '../../../global_degisken/global_degisken.dart';
import '../../../global_degisken/global_degisken.dart' as global;
import '../../../profil/adres_ekle/adres_ekle_ana.dart';
import '../view_model/adres_view_model.dart';
import '../widget/adres_label.dart';
import '../../../responsive_class/size_config.dart';
import 'package:provider/provider.dart';

class AdresDuzenleButon extends StatelessWidget {
   Adress _adres;

  AdresDuzenleButon(this._adres);

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<AdresViewModel>(context);
    return Container(
      child: RaisedButton(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(4))),
        onPressed: () async {
          var deger;
          deger = await global.Navigate().navigatePushNoAnimation(
              context, Adres_Sec(adres:_adres));
          if (deger != null && deger) {
            provider.sayfaYukle();
          }
        },
        child: Tooltip(
          message: 'Adres düzenleme butonu',
          child: Row(
            children: <Widget>[
              Icon(Icons.edit,
                  color: Colors.white,
                  size: SizeConfig.safeBlockHorizontal * 8),
              SizedBox(
                width: SizeConfig.safeBlockHorizontal * 2,
              ),
              AdresLabel(
                  label: 'Düzenle',
                  weight: FontWeight.bold,
                  color: Colors.white)
            ],
          ),
        ),
        color: GlobalDegiskenler.duzenlemeIcon,
      ),
    );
  }
}
