import 'package:flutter/material.dart';
import '../../../api/model/adres_model.dart';
import '../../../global_degisken/global_degisken.dart' as global;
import '../../../global_degisken/global_degisken.dart';
import '../../../profil/adres_ekle/adres_ekle_ana.dart';
import '../view_model/adres_view_model.dart';
import '../../../responsive_class/size_config.dart';
import '../../../yardimci_widgetlar/label_text/label_widget.dart';
import 'package:provider/provider.dart';

class AdresEkleBar extends StatelessWidget {
  int _cariId;

  AdresEkleBar(this._cariId);

  Adress _adress = Adress();

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<AdresViewModel>(context);
    return Container(
      decoration: global.GlobalDegiskenler.kartSablon,
      margin: EdgeInsets.only(
        top: SizeConfig.safeBlockVertical * 1,
        left: SizeConfig.safeBlockHorizontal * 4,
        right: SizeConfig.safeBlockHorizontal * 4,
      ),
      width: double.infinity,
      child: Material(
        color: Colors.transparent,
        child: InkWell(
            borderRadius: BorderRadius.circular(8.0),
            child: Padding(
              padding: EdgeInsets.symmetric(
                vertical: SizeConfig.screenHeight * 0.015,
                horizontal: SizeConfig.screenWidth * 0.03,
              ),
              child: Row(
                children: <Widget>[
                  Icon(
                    Icons.home,
                    size: SizeConfig.safeBlockHorizontal * 6.5,
                    color: global.GlobalDegiskenler.butonArkaPlanRengi,
                  ),
                  Spacer(
                    flex: 1,
                  ),
                  LabelCard(
                    fontFamily: GlobalDegiskenler.tlSimgesi,
                    label: 'Adres ekle',
                    color: Colors.black,
                    fontSize: SizeConfig.safeBlockHorizontal * 4.5,
                  ),
                  Spacer(
                    flex: 9,
                  ),
                  Icon(
                    Icons.add,
                    size: SizeConfig.safeBlockHorizontal * 6.5,
                    color: global.GlobalDegiskenler.butonArkaPlanRengi,
                  ),
                ],
              ),
            ),
            onTap: () async {
              var deger;
              deger = await global.Navigate().navigatePushNoAnimation(context, Adres_Sec(adres: _adress));
              if (deger != null && deger) {
                provider.sayfaYukle();
              }
            }),
      ),
    );
  }
}
