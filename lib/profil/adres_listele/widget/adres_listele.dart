import 'package:flutter/material.dart';
import '../../../global_degisken/global_degisken.dart';
import '../view_model/adres_view_model.dart';
import '../widget/adres_ekle_bar.dart';
import '../widget/adres_label.dart';
import '../widget/adres_radio_buton.dart';
import '../widget/adres_sepet_onayla.dart';
import '../widget/adres_step_bar.dart';
import '../../../responsive_class/size_config.dart';
import 'package:provider/provider.dart';
import 'adres_düzenle_buton.dart';
import 'adres_sil_buton.dart';

class AdresListele extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<AdresViewModel>(
      builder: (BuildContext context, AdresViewModel provider, Widget child) =>
          Column(
        children: <Widget>[
          Expanded(
            child: ListView(
              shrinkWrap: true,
              children: <Widget>[
                provider.siparisModu ? AdresStepBar() : SizedBox(),
                provider.adres.length < 2 ? AdresEkleBar(provider.adres[0].cariId) : SizedBox(),
                SizedBox(
                  height: SizeConfig.safeBlockVertical * 2,
                ),
                provider.adres[0].cariBasariDonus != '4'
                    ? ListView.builder(
                        shrinkWrap: true,
                        itemCount: provider.adres.length,
                        itemBuilder: (context, index) {
                          provider.index = index;
                          return Container(
                              margin: EdgeInsets.only(
                                bottom: SizeConfig.safeBlockVertical * 1.5,
                                left: SizeConfig.safeBlockHorizontal * 4,
                                right: SizeConfig.safeBlockHorizontal * 4,
                              ),
                              alignment: Alignment.center,
                              width: double.infinity,
                              decoration: GlobalDegiskenler.kartSablon,
                              child: Material(
                                color: Colors.transparent,
                                child: InkWell(
                                  splashColor: !provider.siparisModu
                                      ? Colors.transparent
                                      : Theme.of(context).splashColor,
                                  highlightColor: !provider.siparisModu
                                      ? Colors.transparent
                                      : Theme.of(context).highlightColor,
                                  borderRadius: BorderRadius.circular(8.0),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      /*Adres label*/
                                      Padding(
                                        padding: EdgeInsets.only(
                                          top: SizeConfig.safeBlockVertical * 1,
                                          left: SizeConfig.safeBlockHorizontal *
                                              3,
                                          right:
                                              SizeConfig.safeBlockHorizontal *
                                                  3,
                                        ),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: <Widget>[
                                                AdresLabel(
                                                    label: provider
                                                        .adres[index].adresAdi,
                                                    weight: FontWeight.bold,
                                                    fontSize: SizeConfig
                                                            .safeBlockHorizontal *
                                                        4),
                                                provider.siparisModu
                                                    ? AdresRadioButon()
                                                    : SizedBox(),
                                              ],
                                            ),
                                            AdresLabel(
                                              label:
                                                  provider.adres[index].detay,
                                              weight: FontWeight.w400,
                                            ),
                                            AdresLabel(
                                              label: provider
                                                      .adres[index].semtAdi +
                                                  '/' +
                                                  provider
                                                      .adres[index].ilceAdi +
                                                  '/' +
                                                  provider.adres[index].ilAdi +
                                                  '/' +
                                                  provider.adres[index].ulkeAdi,
                                              weight: FontWeight.w400,
                                            ),
                                          ],
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(
                                          top: SizeConfig.safeBlockVertical *
                                              0.5,
                                          bottom: SizeConfig.safeBlockVertical *
                                              0.2,
                                        ),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: <Widget>[
                                            SizedBox(
                                              width: SizeConfig
                                                      .safeBlockHorizontal *
                                                  2,
                                            ),
                                            /*Adres silme iconu*/
                                            AdresSilButon(provider.adres[index]),
                                            SizedBox(
                                              width: SizeConfig
                                                      .safeBlockHorizontal *
                                                  2,
                                            ),
                                            /*Adres güncelleme iconu*/
                                            AdresDuzenleButon(provider.adres[index]),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                  onTap: () {
                                    provider.secilenAdresIndex = index;
                                    provider.radioGrup = index;
                                  },
                                ),
                              ));
                        },
                      )
                    : SizedBox(),
              ],
            ),
          ),
          provider.siparisModu ? AdresSepetOnayla() : SizedBox(),
        ],
      ),
    );
  }

}
