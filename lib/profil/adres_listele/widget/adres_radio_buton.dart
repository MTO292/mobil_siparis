import 'package:flutter/material.dart';
import '../../../global_degisken/global_degisken.dart';
import '../view_model/adres_view_model.dart';
import 'package:provider/provider.dart';

class AdresRadioButon extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<AdresViewModel>(context);
    return  Container(
        height: 25,
        width: 25,
        child: Radio(
            value: provider.index,
            groupValue: provider.radioGrup,
            activeColor: GlobalDegiskenler.butonArkaPlanRengi,
            onChanged: (deger) {
              provider.secilenAdresIndex = deger;
              provider.radioGrup = deger;
              print('Radio');
            }),

    );
  }
}
