import 'package:connectivity_wrapper/connectivity_wrapper.dart';
import 'package:flutter/material.dart';
import '../../../global_degisken/global_degisken.dart' as global;
import '../../../global_degisken/global_degisken.dart';
import '../view_model/adres_view_model.dart';
import '../widget/adres_aciklama_goster.dart';
import '../widget/adres_listele.dart';
import '../../../yardimci_fonksiyonlar/form_mesaj_dondur/net_kontrol.dart'
    as net;
import '../../../yardimci_fonksiyonlar/sayfa_kadirma_efekti/scrool.dart';
import '../../../yardimci_widgetlar/appbar_widgets/appbar_text.dart';
import 'package:provider/provider.dart';

class Adres extends StatefulWidget {
  @override
  _AdresState createState() => _AdresState();
}

class _AdresState extends State<Adres> {
  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    context.read<AdresViewModel>().sayfaYukle();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      /*Sayfanın en üstteki labeli temsil eden kısım*/
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: global.GlobalDegiskenler.appBarColor,
        title: BaslikText(label: 'Adres Bilgileri'),
      ),
      body: ConnectivityWidgetWrapper(
        disableInteraction: false,
        offlineWidget: net.netKontrol(),
        child: ScrollConfiguration(
          behavior: MyBehavior(),
          child: Consumer(builder:
              (BuildContext context, AdresViewModel provider, Widget child) {
            if (provider.durum == AdresDurum.Yukleniyor) {
              return global.progressDondur();
            }
            if (provider.durum == AdresDurum.Init) {
              return global.progressDondur();
            }
            if (provider.durum == AdresDurum.Yuklendi) {
              if (provider.adres.length == 0) {
                return AdresAciklamaGoster(
                    'Hata', 'Tekrar yüklemeyi deneyiniz.', Icons.error_outline);
              } else if (provider.adres[0].cariBasariDonus ==
                  numarator.uygulamaVersiyonuEski) {
                logoEkraninaGit(context);
                return SizedBox();
              } else if (provider.adres[0].cariBasariDonus ==
                  numarator.apiVersiyonuEski) {
                logoEkraninaGit(context);
                return SizedBox();
              } else if (provider.adres[0].cariBasariDonus ==
                  numarator.hizmetDisi) {
                logoEkraninaGit(context);
                return SizedBox();
              } else if (provider.adres[0].cariBasariDonus ==
                      numarator.basarili ||
                  provider.adres[0].cariBasariDonus == '4') {
                /*Adres gösterme kısmı*/
                return AdresListele();
              } else {
                return AdresAciklamaGoster(
                    'Hata', 'Tekrar yüklemeyi deneyiniz.', Icons.error_outline);
              }
            } else {
              return AdresAciklamaGoster(
                  'Hata', 'Tekrar yüklemeyi deneyiniz.', Icons.error_outline);
            }
          }),
        ),
      ),
    );
  }
}
