import 'package:flutter/material.dart';
import '../../../global_degisken/global_degisken.dart' as global;

class AdresAciklamaGoster extends StatelessWidget {
  String baslik;
  String aciklama;
  IconData icon;

  AdresAciklamaGoster(
      @override this.baslik, @override this.aciklama, @override this.icon);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
      child: global.aciklama(baslik, aciklama, icon),
    );
  }
}
