import 'package:flutter/material.dart';
import '../../../global_degisken/global_degisken.dart';
import '../../../yardimci_widgetlar/step_bar/step_bar.dart';

class AdresStepBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StepBar(
      step1Color: Colors.white,
      text1Color: GlobalDegiskenler.butonArkaPlanRengi,
      text2Color: Colors.white54,
      backgroundColor: Colors.black54,
      icon: Icon(
        Icons.arrow_forward,
        color: Colors.white54,
      ),
    );
  }
}
