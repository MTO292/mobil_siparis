import '../../../api/model/adres_model.dart';
import '../../../api/response/islem_donus.dart';
import '../../../global_degisken/global_degisken.dart' as global;
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import '../../../global_degisken/global_degisken.dart';
import '../view_model/adres_view_model.dart';
import '../widget/adres_label.dart';
import '../../../responsive_class/size_config.dart';
import '../../../yardimci_fonksiyonlar/form_mesaj_dondur/mesaj_dondur.dart';
import 'package:provider/provider.dart';

class AdresSilButon extends StatelessWidget {
  IslemDonus islemDonus = IslemDonus();
  Adress _adres;


  AdresSilButon(this._adres);

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<AdresViewModel>(context);
    return Container(
      child: RaisedButton(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(4))),
        onPressed: () {
          MesajGoster.soruSor(
              context,
              'Adresi silmek istediğinize emin misiniz ?',
              SoruTipi.Soru, evet: () async {
            try {
              var result = await Connectivity().checkConnectivity();
              if (result == ConnectivityResult.none) {
                MesajGoster.mesajGoster(context,
                    "Cihazınızı internete bağlayınız.", SoruTipi.Uyari);
              } else {
                islemDonus
                    .adresSec(cariId: provider.cariId,islemKodu: '6',adresSira: _adres.sira)
                    .then((gelenDeger) {
                  if (gelenDeger[0].cariBasariDonus == "1") {
                    MesajGoster.toastMesaj(
                        "Adres başarılı bir şekilde silindi.");
                    provider.sayfaYukle();
                  } else if (gelenDeger[0].cariBasariDonus == "4") {
                    MesajGoster.toastMesaj(
                        "Beklenmedik bir hata oluştu. Lütfen tekrar deneyiniz.");
                  } else if (gelenDeger[0].cariBasariDonus ==
                      numarator.uygulamaVersiyonuEski) {
                    logoEkraninaGit(context);
                  } else if (gelenDeger[0].cariBasariDonus ==
                      numarator.apiVersiyonuEski) {
                    logoEkraninaGit(context);
                  } else if (gelenDeger[0].cariBasariDonus ==
                      numarator.hizmetDisi) {
                    logoEkraninaGit(context);
                  }
                });

                Navigator.of(context, rootNavigator: true).pop();
              }
            } catch (e) {}
          });
        },
        color: global.GlobalDegiskenler.silmeIcon,
        child: Tooltip(
          message: 'Adres silme butonu',
          child: Row(
            children: <Widget>[
              Icon(
                Icons.delete,
                size: SizeConfig.safeBlockHorizontal * 8,
                color: Colors.white,
              ),
              SizedBox(
                width: SizeConfig.safeBlockHorizontal * 2,
              ),
              AdresLabel(
                  label: 'Sil', weight: FontWeight.bold, color: Colors.white)
            ],
          ),
        ),
      ),
    );
  }
}
