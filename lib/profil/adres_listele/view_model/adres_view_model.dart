import 'package:flutter/cupertino.dart';
import '../../../api/model/adres_model.dart';
import '../../../api/response/islem_donus.dart';
import '../../../yardimci_fonksiyonlar/shared_sepete_ekle/sepet_shared.dart';

enum AdresDurum { Init, Yukleniyor, Yuklendi, Hata }

class AdresViewModel with ChangeNotifier {
  int _index;
  bool _siparisModu;
  List<Adress> _adres;
  int _secilenAdresIndex;
  int _radioGrup;
  int _cariId;
  double _toplamTutar;
  AdresDurum _durum;

  AdresViewModel(@override this._siparisModu, @override this._toplamTutar) {
    _durum = AdresDurum.Init;
    adres = List();
  }


  AdresDurum get durum => _durum;

  set durum(AdresDurum value) {
    _durum = value;
  }

  List<Adress> get adres => _adres;

  double get toplamTutar => _toplamTutar;

  int get cariId => _cariId;

  bool get siparisModu => _siparisModu;

  int get index => _index;

  int get radioGrup => _radioGrup;

  int get secilenAdresIndex => _secilenAdresIndex;

  set adres(List<Adress> value) {
    _adres = value;
    notifyListeners();
  }

  set toplamTutar(double value) {
    _toplamTutar = value;
    notifyListeners();
  }

  set cariId(int value) {
    _cariId = value;
  }

  set index(int value) {
    _index = value;
  }

  set siparisModu(bool value) {
    _siparisModu = value;
  }

  set radioGrup(int value) {
    _radioGrup = value;
    notifyListeners();
  }

  set secilenAdresIndex(int value) {
    _secilenAdresIndex = value;
  }

  Future sayfaYukle() async {
    durum = AdresDurum.Yukleniyor;
    await SharedPrefLib.cariId().then((value) async {
      cariId = value;
      try {
        IslemDonus islemDonus = IslemDonus();
        adres = await islemDonus.adresGetir(cariId: cariId);
        durum = AdresDurum.Yuklendi;
      } catch (e) {
        durum = AdresDurum.Hata;
      }
      return adres;
    }
  );
}
}
