import 'package:flutter/material.dart';
import '../../../profil/adres_ekle/view_model/adres_ekle_view_model.dart';
import '../../../responsive_class/size_config.dart';
import '../../../yardimci_fonksiyonlar/form_regex_fonksiyonlar/regex_kontrol.dart';
import '../../../global_degisken/global_degisken.dart' as global;
import '../../../yardimci_widgetlar/form_label_widgets/text_form_field.dart';
import 'package:provider/provider.dart';

class AdresEkleAdDetay extends StatefulWidget {
  String ad;
  String detay;

  AdresEkleAdDetay(this.ad, this.detay);

  @override
  _AdresEkleAdDetayState createState() => _AdresEkleAdDetayState();
}

class _AdresEkleAdDetayState extends State<AdresEkleAdDetay> {
  TextEditingController detayController = TextEditingController();

  TextEditingController adController = TextEditingController();

  final FocusNode detayFocus = FocusNode();

  final FocusNode adFocus = FocusNode();

  @override
  void dispose() {
    // TODO: implement dispose
    detayController.dispose();
    adController.dispose();
    adFocus.dispose();
    detayFocus.dispose();
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    detayController.text =
        Provider.of<AdresEkleViewModel>(context).secilenAdres.detay;
    adController.text =
        Provider.of<AdresEkleViewModel>(context).secilenAdres.adresAdi;
  }

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<AdresEkleViewModel>(context);
    return Form(
        autovalidate: provider.adDetayAutoValidate,
        key: provider.formKey,
        child: Column(
          children: <Widget>[
            Padding(
              padding: paddings(),
              child: MyTextFormField(
                labelText: 'Adres',
                focusNode: detayFocus,
                icon: Icon(null),
                validatorFonksiyon: RegexKontrol.adresKontrol,
                onFieldSubmitted: (ileri) {
                  detayFocus.unfocus();
                  adFocus.requestFocus();
                },
                satirSayisi: 3,
                karakterSayisi: 100,
                onsavedGelenDeger: (String gelenDeger) {
                  provider.secilenAdres.detay = gelenDeger.trim();
                },
                kontroller: detayController,
                ileriTusu: TextInputAction.go,
                label: 'Cadde, bina numarası ve diğer bilgilerinizi giriniz.',
              ),
            ),
            Padding(
              padding: paddings(),
              child: MyTextFormField(
                labelText: 'Adres Adı',
                focusNode: adFocus,
                icon: global.formIcon(
                  Icons.location_city,
                ),
                validatorFonksiyon: RegexKontrol.adresAdiKontrol,
                satirSayisi: 1,
                karakterSayisi: 10,
                kontroller: adController,
                onsavedGelenDeger: (String gelenDeger) {
                  provider.secilenAdres.adresAdi = gelenDeger.trim();
                },
                onFieldSubmitted: (ileri) {
                  adFocus.unfocus();
                },
                ileriTusu: TextInputAction.done,
                label: 'Örnek: Evim, İş yeri, vb.',
                contentPadding: EdgeInsets.symmetric(
                    vertical: 0, horizontal: SizeConfig.screenWidth * 0.03),
              ),
            ),
          ],
        ));
  }

  EdgeInsets paddings() {
    return EdgeInsets.only(bottom: SizeConfig.screenHeight * 0.02);
  }
}
