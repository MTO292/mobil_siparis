import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import '../../../api/model/adres_ekle_model.dart';
import '../../../global_degisken/global_degisken.dart';
import '../../../profil/adres_ekle/view_model/adres_ekle_view_model.dart';
import '../../../responsive_class/size_config.dart';
import '../../../yardimci_fonksiyonlar/form_mesaj_dondur/mesaj_dondur.dart';
import '../../../yardimci_widgetlar/label_text/label_widget.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:provider/provider.dart';

class AdresEkleKayitButon extends StatelessWidget {
  ProgressDialog pr;

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<AdresEkleViewModel>(context);
    pr = ProgressDialog(context);
    return Container(
      height: SizeConfig.screenHeight * 0.06,
      width: double.infinity,
      child: RaisedButton(
        splashColor: GlobalDegiskenler.splashColor,
        /*buton text label card*/
        child: LabelCard(
          fontFamily: GlobalDegiskenler.tlSimgesi,
          label: 'Adresi Kaydet',
          color: Colors.white,
          fontSize: SizeConfig.safeBlockHorizontal * 5,
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(
            4,
          ),
        ),
        color: GlobalDegiskenler.butonArkaPlanRengi,
        onPressed: adresKayitEt(context, provider),
      ),
    );
  }

  adresKayitEt(BuildContext context, AdresEkleViewModel provider) {
    return () async {
      var result = await Connectivity().checkConnectivity();
      if (result == ConnectivityResult.none) {
        MesajGoster.mesajGoster(
            context, "Cihazınızı internete bağlayınız.", SoruTipi.Uyari);
      } else {
        progressGoster("Lütfen bekleyiniz.");
        await pr.show();
        provider.adresKaydet().then((deger) {
          List<AdresSec> gelen = deger;
          pr.hide();
          if(provider.adresKaydetDurum == AdresEkleDurum.Yuklendi){
            if (gelen[0].cariBasariDonus == "1") {
                Navigator.pop(context, true);
            } else if (gelen[0].cariBasariDonus ==
                numarator.uygulamaVersiyonuEski) {
              logoEkraninaGit(context);
            } else if (gelen[0].cariBasariDonus ==
                numarator.apiVersiyonuEski) {
              logoEkraninaGit(context);
            } else if (gelen[0].cariBasariDonus == numarator.hizmetDisi) {
              logoEkraninaGit(context);
            } else if (gelen[0].cariBasariDonus == "4") {
              MesajGoster.mesajGoster(context,
                  "Hata oluştu. Lütfen tekrar deneyiniz.", SoruTipi.Hata);
            } else if (gelen[0].cariBasariDonus == null) {
              MesajGoster.mesajGoster(context,"Hata oluştu. Lütfen tekrar deneyiniz.", SoruTipi.Hata);
            }
          } else if (provider.adresKaydetDurum == AdresEkleDurum.Hata){
            MesajGoster.mesajGoster(context,"Hata oluştu. Lütfen tekrar deneyiniz.", SoruTipi.Hata);
          } else if (provider.adresKaydetDurum == AdresEkleDurum.validateHata){
            MesajGoster.mesajGoster(
                context, "Lütfen bilgilerinizi tam giriniz.", SoruTipi.Uyari)
                .whenComplete(() {
              provider.adDetayAutoValidate = true;
            });
          }else {
            MesajGoster.mesajGoster(context,"Hata oluştu. Lütfen tekrar deneyiniz.", SoruTipi.Hata);
          }
        });
      }
    };
  }

  Future<void> progressGoster(String mesaj) {
    //ProgressDialog paketine style verdik
    pr.style(
      backgroundColor: GlobalDegiskenler.progressBarColor,
      progress: 4.0,
      message: mesaj,
      progressTextStyle: TextStyle(color: Colors.black),
      borderRadius: 15.0,
      elevation: 6.0,
      insetAnimCurve: Curves.easeIn,
      messageTextStyle: TextStyle(
        fontFamily: GlobalDegiskenler.tlSimgesi,
        fontSize: SizeConfig.safeBlockHorizontal * 5,
        color: Colors.black,
      ),
    );
  }
}
