import 'package:flutter/cupertino.dart';
import '../../../global_degisken/global_degisken.dart';
import '../../../profil/adres_ekle/view_model/adres_ekle_view_model.dart';
import '../../../responsive_class/size_config.dart';
import '../../../yardimci_fonksiyonlar/sayfa_kadirma_efekti/scrool.dart';
import '../../../yardimci_widgetlar/appbar_widgets/appbar_text.dart';
import 'package:flutter/material.dart';
import '../../../global_degisken/global_degisken.dart' as global;
import 'package:provider/provider.dart';
import 'adres_ekle_listele.dart';
import 'adres_ekle_aciklama_goster.dart';

class AdresEkle extends StatefulWidget {
  @override
  _AdresEkleState createState() => _AdresEkleState();
}

class _AdresEkleState extends State<AdresEkle> {
  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    //ilk girişte gönderilen request
    (context).read<AdresEkleViewModel>().sayfaYukle();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: GlobalDegiskenler.appBarColor,
        title: BaslikText(label: 'Adres Ekle'),
      ),
      body: ScrollConfiguration(
        behavior: MyBehavior(),
        child: !Provider.of<AdresEkleViewModel>(context).duzenlemeModu
            ? Consumer(builder: (BuildContext context,
                AdresEkleViewModel provider, Widget child) {
                /*Veriler yüklenirken bu ekran gösterilecek*/
                if (provider.adresGetirDurum == AdresEkleDurum.Yukleniyor ||
                    provider.adresGetirDurum == AdresEkleDurum.Init) {
                  return Center(
                    child: global.progressDondur(),
                  );
                  /*Veri varmı kontrol ediyoruz*/
                } else if (provider.adresGetirDurum ==
                    AdresEkleDurum.Yuklendi) {
                  if (provider.adres.length == 0 ||
                      provider.adres.length == null) {
                    return AdresEkleAciklamaGoster(
                        'Hata',
                        'Lütfen Tekrar yüklemeyi deneyiniz.',
                        Icons.error_outline);
                    /*Menü varsa bu ekran gösterilecek*/
                  } else if (provider.adres[0].cariBasariDonus ==
                      numarator.uygulamaVersiyonuEski) {
                    logoEkraninaGit(context);
                    return SizedBox();
                  } else if (provider.adres[0].cariBasariDonus ==
                      numarator.apiVersiyonuEski) {
                    logoEkraninaGit(context);
                    return SizedBox();
                  } else if (provider.adres[0].cariBasariDonus ==
                      numarator.hizmetDisi) {
                    logoEkraninaGit(context);
                    return SizedBox();
                  } else if (provider.adres[0].cariBasariDonus == '4') {
                    return AdresEkleAciklamaGoster(
                        'Hata',
                        'Lütfen Tekrar yüklemeyi deneyiniz.',
                        Icons.error_outline);
                  } else {
                    return AdresEkleListele();
                  }
                  /*Veri yoksa ve hata varsa burası gösterilecek*/
                } else if (provider.adresGetirDurum == AdresEkleDurum.Hata) {
                  return AdresEkleAciklamaGoster(
                      'Hata',
                      'Lütfen internetinizi kontrol ediniz.',
                      Icons.error_outline);
                }
                return AdresEkleAciklamaGoster(
                    'Hata',
                    'Lütfen internetinizi kontrol ediniz.',
                    Icons.error_outline);
              })
            : AdresEkleListele(),
      ),
    );
  }
}
