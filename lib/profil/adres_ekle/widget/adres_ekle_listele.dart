import 'dart:io';
import 'package:flutter/cupertino.dart';
import '../../../global_degisken/global_degisken.dart' as global;
import 'package:flutter/material.dart';
import '../../../global_degisken/global_degisken.dart';
import '../../../profil/adres_ekle/view_model/adres_ekle_view_model.dart';
import '../../../profil/adres_ekle/widget/adres_ekle_ad_detay.dart';
import '../../../profil/adres_ekle/widget/adres_ekle_kaydet_buton.dart';
import '../../../responsive_class/size_config.dart';
import '../../../yardimci_fonksiyonlar/form_mesaj_dondur/mesaj_dondur.dart';
import '../../../yardimci_fonksiyonlar/sayfa_kadirma_efekti/scrool.dart';
import '../../../yardimci_widgetlar/label_text/label_widget.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:provider/provider.dart';

import 'adres_ekle_Input_decorator.dart';

class AdresEkleListele extends StatefulWidget {
  @override
  _AdresEkleListeleState createState() => _AdresEkleListeleState();
}

class _AdresEkleListeleState extends State<AdresEkleListele> {
  ProgressDialog pr;

  TextEditingController searchController = TextEditingController();

  var searchList = List();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    pr = ProgressDialog(context);
  }

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<AdresEkleViewModel>(context);
    return ListView(
      physics: ScrollBehavior().getScrollPhysics(context),
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            color: Colors.white,
            // border: Border.all(color: Colors.grey.withOpacity(0.5)),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.3),
                spreadRadius: 0.5,
                blurRadius: 2,
                offset: Offset(0, 1),
              )
            ],
          ),
          padding: EdgeInsets.symmetric(
            horizontal: SizeConfig.screenWidth * 0.04,
            vertical: SizeConfig.screenHeight * 0.04,
          ),
          child: Column(
            children: <Widget>[
              Padding(
                padding: paddings(),
                child: AdresEkleInputDecorator(
                  text: provider.secilenAdres.ulkeAdi,
                  hata: provider.adresOtoKontrol
                      ? provider.secilenAdres.ulkeAdi == null ||
                              provider.secilenAdres.ulkeAdi == ''
                          ? true
                          : false
                      : false,
                  icon: global.formIcon(
                    Icons.location_on,
                  ),
                  hintText: 'Ülke Seçiniz',
                  label: "Ülke",
                  onTap: () {
                    FocusScope.of(context).unfocus();
                    return adresGetir(provider, '1');
                  },
                ),
              ),
              provider.aktifAdim >= 2
                  ? Padding(
                      padding: paddings(),
                      child: AdresEkleInputDecorator(
                        text: provider.secilenAdres.ilAdi,
                        hata: provider.adresOtoKontrol
                            ? provider.secilenAdres.ilAdi == null ||
                                    provider.secilenAdres.ilAdi == ''
                                ? true
                                : false
                            : false,
                        icon: Icon(null),
                        hintText: 'İl Seçiniz',
                        label: "İl",
                        onTap: () {
                          FocusScope.of(context).unfocus();
                          return adresGetir(provider, '2');
                        },
                      ),
                    )
                  : SizedBox(),
              provider.aktifAdim >= 3
                  ? Padding(
                      padding: paddings(),
                      child: AdresEkleInputDecorator(
                        text: provider.secilenAdres.ilceAdi,
                        hata: provider.adresOtoKontrol
                            ? provider.secilenAdres.ilceAdi == null ||
                                    provider.secilenAdres.ilceAdi == ''
                                ? true
                                : false
                            : false,
                        icon: Icon(null),
                        hintText: 'İlçe Seçiniz',
                        label: "İlçe",
                        onTap: () {
                          FocusScope.of(context).unfocus();
                          return adresGetir(provider, '3');
                        },
                      ),
                    )
                  : SizedBox(),
              provider.aktifAdim >= 4
                  ? Padding(
                      padding: paddings(),
                      child: AdresEkleInputDecorator(
                        text: provider.secilenAdres.semtAdi,
                        hata: provider.adresOtoKontrol
                            ? provider.secilenAdres.semtAdi == null ||
                                    provider.secilenAdres.semtAdi == ''
                                ? true
                                : false
                            : false,
                        icon: Icon(null),
                        hintText: 'Semt Seçiniz',
                        label: "Semt",
                        onTap: () {
                          FocusScope.of(context).unfocus();
                          return adresGetir(provider, '4');
                        },
                      ),
                    )
                  : SizedBox(),
              AdresEkleAdDetay(
                  provider.secilenAdres.adresAdi, provider.secilenAdres.detay),
              AdresEkleKayitButon(),
            ],
          ),
        )
      ],
    );
  }

  dropdownGoster(AdresEkleViewModel provider, var listItems,
      String acilanBottomSheet) async {
    return showModalBottomSheet(
        backgroundColor: Colors.transparent,
        isScrollControlled: true,
        context: context,
        builder: (BuildContext context) {
          return Padding(
            padding: EdgeInsets.only(
                bottom: MediaQuery.of(context).viewInsets.bottom),
            child: StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
                return ScrollConfiguration(
                  behavior: MyBehavior(),
                  child: Container(
                    height: SizeConfig.screenHeight * 0.8,
                    child: Column(
                      children: <Widget>[
                        //status bar kadar bi şefaf container ouştururyoruz
                        Container(
                          color: Colors.transparent,
                          height: SizeConfig.statusBarHeight,
                        ),
                        Expanded(
                          child: Container(
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(8),
                                  topRight: Radius.circular(8),
                                )),
                            child: Column(
                              children: <Widget>[
                                SizedBox(
                                  height: 10,
                                ),
                                Container(
                                  width: 40,
                                  height: 4,
                                  decoration: BoxDecoration(
                                    color: Color.fromRGBO(213, 213, 215, 1),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(32)),
                                  ),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Container(
                                  margin: EdgeInsets.symmetric(
                                      horizontal:
                                          SizeConfig.screenWidth * 0.02),
                                  height: SizeConfig.safeBlockVertical * 4,
                                  child: TextField(
                                    controller: searchController,
                                    onChanged: (deger) {
                                      deger = deger.trim();
                                      //her gitişte bi önceki listey siliyoruz
                                      searchList.clear();
                                      //listenin uzunulugu kadar döndürüuoruz
                                      listItems.forEach((element) {
                                        //Girilen değer listede var ise arama listesine atıyoryz
                                        if (element.adres
                                            .toUpperCase()
                                            .startsWith(deger.toUpperCase())) {
                                          searchList.add(element);
                                        }
                                      });
                                      if (deger == "") {
                                        searchList.clear();
                                        setState(() {});
                                      } else {
                                        setState(() {});
                                      }
                                    },
                                    decoration: InputDecoration(
                                      contentPadding: EdgeInsets.only(
                                          bottom:
                                              SizeConfig.safeBlockVertical * 2),
                                      hintText: 'Ara',
                                      icon: Icon(
                                        !Platform.isAndroid
                                            ? CupertinoIcons.search
                                            : Icons.search,
                                        color: Colors.black,
                                      ),
                                      focusedBorder: InputBorder.none,
                                      enabledBorder: InputBorder.none,
                                    ),
                                    textInputAction: TextInputAction.done,
                                    onSubmitted: (bitti) {
                                      FocusScope.of(context).unfocus();
                                    },
                                    maxLines: 1,
                                    style: TextStyle(
                                      fontSize:
                                          MediaQuery.of(context).size.width *
                                              (0.04),
                                    ),
                                  ),
                                ),
                                Divider(
                                  color: Color.fromRGBO(213, 213, 215, 1),
                                ),
                                Expanded(
                                  child: listItems != null
                                      ? CupertinoScrollbar(
                                          child: ListView.builder(
                                              itemCount: searchList.length == 0
                                                  ? listItems.length
                                                  : searchList.length,
                                              itemBuilder:
                                                  (BuildContext context,
                                                      int index) {
                                                return InkWell(
                                                  onTap: () {
                                                    Navigator.pop(
                                                        context,
                                                        searchList.length == 0
                                                            ? listItems[index]
                                                            : searchList[
                                                                index]);
                                                  },
                                                  child: ListTile(
                                                    title: LabelCard(
                                                      label: searchList
                                                                  .length ==
                                                              0
                                                          ? listItems[index]
                                                              .adres
                                                          : searchList[index]
                                                              .adres,
                                                      //label: searchList.length ==0? isFocused.value[index].adres : searchList[index].adres,
                                                      fontSize: SizeConfig
                                                              .safeBlockHorizontal *
                                                          4,
                                                    ),
                                                  ),
                                                );
                                              }),
                                        )
                                      : SizedBox(),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              },
            ),
          );
        }).then((secilenAdres) {
      searchList.clear();
      searchController.clear();
      provider.bottomShetKaydet(secilenAdres, int.parse(acilanBottomSheet));
    });
  }

  adresGetir(AdresEkleViewModel provider, String islemKod) async {
    progressGoster("Lütfen bekleyiniz.");
    await pr.show();
    await provider.adresGetir(islemKod).then((value) {
      pr.hide();
      if (provider.adresGetirDurum == AdresEkleDurum.Yuklendi) {
        if (value[0].cariBasariDonus == '4') {
          MesajGoster.toastMesaj("Hata oluştu. Lütfen tekrar deneyiniz.");
        } else if (value[0].cariBasariDonus == '1') {
          dropdownGoster(provider, value, islemKod);
        } else {
          MesajGoster.toastMesaj("Hata oluştu. Lütfen tekrar deneyiniz.");
        }
      } else if (provider.adresGetirDurum == AdresEkleDurum.Hata) {
        MesajGoster.toastMesaj("Hata oluştu. Lütfen tekrar deneyiniz.");
      }
    });
  }

  Future<void> progressGoster(String mesaj) {
    //ProgressDialog paketine style verdik
    pr.style(
      backgroundColor: GlobalDegiskenler.progressBarColor,
      progress: 4.0,
      message: mesaj,
      progressTextStyle: TextStyle(color: Colors.black),
      borderRadius: 15.0,
      elevation: 6.0,
      insetAnimCurve: Curves.easeIn,
      messageTextStyle: TextStyle(
        fontFamily: GlobalDegiskenler.tlSimgesi,
        fontSize: SizeConfig.safeBlockHorizontal * 5,
        color: Colors.black,
      ),
    );
  }

  EdgeInsets paddings() {
    return EdgeInsets.only(bottom: SizeConfig.screenHeight * 0.02);
  }
}
