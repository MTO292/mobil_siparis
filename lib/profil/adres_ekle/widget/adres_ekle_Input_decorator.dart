import 'package:flutter/cupertino.dart';
import '../../../global_degisken/global_degisken.dart';
import '../../../responsive_class/size_config.dart';
import '../../../yardimci_widgetlar/label_text/label_widget.dart';
import 'package:flutter/material.dart';

class AdresEkleInputDecorator extends StatefulWidget {
  String label;
  String hintText;
  String text; //listeden secilen adres adı
  Function onTap;
  Widget icon;
  bool hata;

  @override
  _State createState() => _State();

  AdresEkleInputDecorator({
    Key key,
    @required this.onTap,
    @required this.label,
    @required this.hintText,
    @required this.text,
    @required this.icon,
    @required this.hata,
  }) : super(key: key);
}

class _State extends State<AdresEkleInputDecorator> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: InkWell(
        onTap: () {
          widget.onTap();
        },
        child: Container(
            height: SizeConfig.screenHeight * 0.06,
            child: FormField(
              initialValue: 'ini',
              builder: (state) {
                return InputDecorator(
                  decoration: InputDecoration(
                    fillColor: Colors.transparent,
                    filled: true,
                    icon: widget.icon,
                    contentPadding: EdgeInsets.symmetric(
                        horizontal: SizeConfig.screenWidth * 0.03),
                    labelStyle: TextStyle(
                        color: Colors.black,
                        fontFamily: GlobalDegiskenler.tlSimgesi,
                        fontSize: SizeConfig.safeBlockHorizontal * 4),
                    border: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: Colors.black,
                          width: 1),
                      borderRadius: BorderRadius.circular(
                        4,
                      ),
                    ),
                    labelText: widget.label ?? "",
                    hintText: widget.label ?? "",
                  ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: LabelCard(
                          label: widget.text != '' && widget.text != null
                              ? widget.text
                              : widget.hintText,
                          fontFamily: GlobalDegiskenler.tlSimgesi,
                          fontWeight: FontWeight.w500,
                          fontSize: SizeConfig.safeBlockHorizontal * 3.5,
                        ),
                      ),
                      widget.hata ? Icon(Icons.error_outline,size: SizeConfig.safeBlockVertical * 3.0,color: Colors.red,):SizedBox(),
                      Icon(Icons.arrow_drop_down,size: SizeConfig.safeBlockHorizontal * 5.6,),
                    ],
                  ),
                );
              },
            )),
      ),
    );
  }
}
