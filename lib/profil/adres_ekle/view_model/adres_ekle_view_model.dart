import 'package:flutter/cupertino.dart';
import '../../../api/model/adres_ekle_model.dart';
import '../../../api/model/adres_model.dart';
import '../../../api/response/islem_donus.dart';
import '../../../yardimci_fonksiyonlar/shared_sepete_ekle/sepet_shared.dart';

enum AdresEkleDurum { Init, Yukleniyor, Yuklendi, Hata, validateHata }

class AdresEkleViewModel with ChangeNotifier {
  Adress _secilenAdres = Adress();
  List<AdresSec> _adres;
  var _formKey;
  bool _adDetayAutoValidate;
  bool _duzenlemeModu;
  bool _otoKontrol;
  //eğer true ise gidip adres alanlarını kontrol eder seçilmiş mi seçilmemiş mi
  bool _adresOtoKontrol;
  int _aktifAdim;
  AdresEkleDurum _adresGetirDurum;
  AdresEkleDurum _adresKaydetDurum;

  AdresEkleViewModel(this._secilenAdres) {
    _formKey = GlobalKey<FormState>();
    _adres = List();
    _adDetayAutoValidate = false;
    _adresOtoKontrol = false;
    //adresin getirin future durumunu giriyoruz
    _adresGetirDurum = AdresEkleDurum.Init;
    //adres düzeneleme işlemi olup olmadğını anlamak için işlem yapıyoruz
    if (_secilenAdres.cariBasariDonus != null) {
      duzenlemeModu = true;
      aktifAdim = 4;
    } else {
      duzenlemeModu = false;
      aktifAdim = 1;
    }
  }

  AdresEkleDurum get adresGetirDurum => _adresGetirDurum;

  bool get otoKontrol => _otoKontrol;

  bool get duzenlemeModu => _duzenlemeModu;

  bool get adDetayAutoValidate => _adDetayAutoValidate;

  Adress get secilenAdres => _secilenAdres;

  get formKey => _formKey;

  List<AdresSec> get adres => _adres;

  int get aktifAdim => _aktifAdim;

  AdresEkleDurum get adresKaydetDurum => _adresKaydetDurum;

  bool get adresOtoKontrol => _adresOtoKontrol;

  set adresOtoKontrol(bool value) {
    _adresOtoKontrol = value;
    notifyListeners();
  }

  set adresKaydetDurum(AdresEkleDurum value) {
    _adresKaydetDurum = value;
    notifyListeners();
  }

  set aktifAdim(int value) {
    _aktifAdim = value;
    notifyListeners();
  }

  set adres(List<AdresSec> value) {
    _adres = value;
  }

  set otoKontrol(bool value) {
    _otoKontrol = value;
  }

  set formKey(value) {
    _formKey = value;
  }

  set adresGetirDurum(AdresEkleDurum value) {
    _adresGetirDurum = value;
    notifyListeners();
  }

  set duzenlemeModu(bool value) {
    _duzenlemeModu = value;
  }

  set secilenAdres(Adress value) {
    _secilenAdres = value;
  }

  set adDetayAutoValidate(bool value) {
    _adDetayAutoValidate = value;
    notifyListeners();
  }

  formKeySave() {
    _formKey.currentState.save();
    notifyListeners();
  }

  formKeyValidate() {
    _formKey.currentState.validate();
    notifyListeners();
  }

  textFieldAutoKontrol() {
    _otoKontrol = true;
  }

  Future sayfaYukle() async {
    if (!duzenlemeModu) {
      //Düzeenleme modunda düzelticek adres içinde zaten cari id olduğundan bitek yeni adres eklemede getiriyor
      _secilenAdres.cariId = await SharedPrefLib.cariId();
      await adresGetir("1");
    }
  }

  //bottom sheette adresi listelemek için kullnılmakta
  Future adresGetir(String islem) async {
    //isteğin durumunu atıyoruz
    _adresGetirDurum = AdresEkleDurum.Yukleniyor;
    try {
      // 1 => ülke, 2 => il, 3 => ilçe, 4 => smahalle
      //eger düzenleme modu ise ilk girişte veri gertirmeuecek
      _adres = await IslemDonus().adresSec(
          islemKodu: islem,
          ulkeId: _secilenAdres.ulkeId,
          ilId: _secilenAdres.ilId,
          ilceId: _secilenAdres.ilceId);

      //eger duzenleme modu ise otomatik olarak ikinci adıma geçsin diye yazıldı
      if (!duzenlemeModu && aktifAdim == 1) {
        _aktifAdim = 2;
        //gelen ilk deger, yani ülkeyi otomatik olarak giriyoruz
        _secilenAdres.ulkeAdi = _adres[0].adres;
        _secilenAdres.ulkeId = _adres[0].id;
      }
      //isteğin durumunu atıyoruz
      adresGetirDurum = AdresEkleDurum.Yuklendi;
    } catch (e) {
      //isteğin durumunu atıyoruz
      adresGetirDurum = AdresEkleDurum.Hata;
    }
    return _adres;
  }

  //adresi kaydetmek için kullnılmakta
  Future adresKaydet() async {
    List<AdresSec> gelen = List();
    //adres adını ve detayını kayıt ediyoruz key kullanarak
    formKeySave();
    //isteğin durumunu atıyoruz
    _adresKaydetDurum = AdresEkleDurum.Yukleniyor;
    // her hangi biri girilmemiş mi diye kontrol ediyoruz
    if (secilenAdres.detay == '' ||
        secilenAdres.ulkeAdi == '' ||
        secilenAdres.ilAdi == '' ||
        secilenAdres.ilceAdi == '' ||
        secilenAdres.adresAdi == '' ||
        secilenAdres.semtAdi == '' ||
        secilenAdres.detay == null ||
        secilenAdres.ulkeAdi == null ||
        secilenAdres.ilAdi == null ||
        secilenAdres.ilceAdi == null ||
        secilenAdres.adresAdi == null ||
        secilenAdres.semtAdi == null) {
      _adresOtoKontrol = true;
      adresKaydetDurum = AdresEkleDurum.validateHata;
    } else {
      if (formKey.currentState.validate()) {
        try {
          gelen = await IslemDonus().adresSec(
            islemKodu: "5",
            ulkeId: _secilenAdres.ulkeId,
            ilId: _secilenAdres.ilId,
            ilceId: _secilenAdres.ilceId,
            semtId: _secilenAdres.semtId,
            cariId: _secilenAdres.cariId,
            detay: _secilenAdres.detay,
            adresSira: _secilenAdres.sira,
            adresAd: _secilenAdres.adresAdi,
          );
          //isteğin durumunu atıyoruz
          adresKaydetDurum = AdresEkleDurum.Yuklendi;
        } catch (e) {
          //isteğin durumunu atıyoruz
          adresKaydetDurum = AdresEkleDurum.Hata;
        }
      } else {
        //adres Ad Detay için otomatik kontrolu aktif ediyoruz
        adDetayAutoValidate = true;
      }
    }
    return gelen;
  }

  //bottom sheet kapandıktan sonra seçilen adresi kayıt ediyoruz
  bottomShetKaydet(AdresSec gelenAdres,int acilanBottomSheet) {
    if (acilanBottomSheet == 1 && gelenAdres != null) {
      secilenAdres.ulkeAdi = gelenAdres.adres;
      secilenAdres.ulkeId = gelenAdres.id;
      secilenAdres.ilAdi = '';
      aktifAdim = 2;
    } else if (acilanBottomSheet == 2 && gelenAdres != null) {
      secilenAdres.ilAdi = gelenAdres.adres;
      secilenAdres.ilId = gelenAdres.id;
      secilenAdres.ilceAdi = '';
      aktifAdim = 3;
    } else if (acilanBottomSheet == 3 && gelenAdres != null) {
      secilenAdres.ilceAdi = gelenAdres.adres;
      secilenAdres.ilceId = gelenAdres.id;
      secilenAdres.semtAdi = '';
      aktifAdim = 4;
    } else if (gelenAdres != null) {
      secilenAdres.semtAdi = gelenAdres.adres;
      secilenAdres.semtId = gelenAdres.id;
      //tekrar aktif adıma 4 vermemizin sebebi setState tekrar build etmesi içindir
      aktifAdim = 4;
    }
  }
}
