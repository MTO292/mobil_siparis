import 'package:flutter/cupertino.dart';
import '../../api/model/adres_model.dart';
import '../adres_ekle/view_model/adres_ekle_view_model.dart';
import '../adres_ekle/widget/adres_ekle.dart';
import '../../responsive_class/size_config.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Adres_Sec extends StatefulWidget {
  Adress adres;

  Adres_Sec({Key key, @required this.adres}) : super(key: key);

  @override
  _Adres_SecState createState() => _Adres_SecState(this.adres);
}

class _Adres_SecState extends State<Adres_Sec> {
  Adress _adres;

  _Adres_SecState(this._adres) {
    //bi önceki ekrandan gelen adres nesnesini klonlayarak kopyalıyoruz
    _adres = Adress().clone(_adres);
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return ChangeNotifierProvider<AdresEkleViewModel>(
        create: (context) => AdresEkleViewModel(this._adres),
        child: AdresEkle());
  }
}
