import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../../giris/stepper_giris.dart';
import '../../../global_degisken/global_degisken.dart';
import '../../../profil/gy_profil/widget/gy_list_tile.dart';


class GyGirisYap extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: GlobalDegiskenler.kartSablon,
      child: Tooltip(
        message: 'Giriş Sayfasına Yönlendirir',
        child: Material(
          color: Colors.transparent,
          child: InkWell(
            borderRadius: BorderRadius.circular(8.0),
            splashColor: Colors.transparent,
            onTap: () {
              Navigator.of(context, rootNavigator: true)
                  .push(MaterialPageRoute(builder: (context) => StepperGiris()));
            },
            child: GyListTile(
                baslik: 'Giriş Yap',
                  icon:!Platform.isAndroid
                      ? CupertinoIcons.person_add_solid
                      : Icons.person_add,
                )),
          ),
        ),
      );
  }
}
