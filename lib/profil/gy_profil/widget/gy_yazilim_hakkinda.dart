import 'dart:io';
import 'package:flutter/material.dart';
import '../../../global_degisken/global_degisken.dart';
import '../../../profil/gy_profil/widget/gy_list_tile.dart';
import '../../../profil/yazilim_hakkinda/yazilim_hakkinda_ana.dart';

class GyYazilimHakkinda extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: GlobalDegiskenler.kartSablon,
      child: Tooltip(
        message: 'Yazılım hakkında bilgi verir',
        child: Material(
          color: Colors.transparent,
          child: InkWell(
            splashColor: Colors.transparent,
            borderRadius: BorderRadius.circular(8.0),
            onTap: () async {
              Navigate().navigatePushNoAnimation(context, YazilimHakkindaAna());
            },
            child: GyListTile(
              baslik: 'Yazılım Hakkında',
              icon:
                  Platform.isAndroid ? Icons.phone_android : Icons.phone_iphone,
            ),
          ),
        ),
      ),
    );
  }
}
