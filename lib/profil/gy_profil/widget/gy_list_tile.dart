import 'package:flutter/material.dart';
import '../../../global_degisken/global_degisken.dart';
import '../../../responsive_class/size_config.dart';
import '../../../yardimci_widgetlar/label_text/label_widget.dart';

class GyListTile extends StatelessWidget {
  Color iconColor;
  double iconSize;
  IconData icon;
  Color titleColor;
  int satirSayisi;
  double size;
  String baslik;

  GyListTile(
      {this.iconColor,
      this.iconSize,
      @override this.icon,
      this.titleColor,
      this.satirSayisi,
      this.size,
      @override this.baslik});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: LabelCard(
        color: titleColor ?? Colors.black,
        fontFamily: GlobalDegiskenler.tlSimgesi,
        fontWeight: FontWeight.w600,
        alignment: Alignment.centerLeft,
        satirSayisi: satirSayisi ?? 1,
        fontSize: size ?? MediaQuery.of(context).size.width * 0.04,
        label: baslik,
      ),
      leading: Icon(
        icon,
        color: iconColor ?? GlobalDegiskenler.butonArkaPlanRengi,
        size: iconSize ?? SizeConfig.safeBlockHorizontal * 7,
      ),
      trailing: Icon( Icons.navigate_next,
        color: iconColor ?? GlobalDegiskenler.butonArkaPlanRengi,
        size: iconSize ?? SizeConfig.safeBlockHorizontal * 7,
      ),
    );
  }
}
