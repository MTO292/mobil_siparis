import 'package:flutter/material.dart';
import '../../../profil/gy_profil/widget/gy_yazilim_hakkinda.dart';
import '../../../responsive_class/size_config.dart';
import '../../../yardimci_fonksiyonlar/sayfa_kadirma_efekti/scrool.dart';
import 'gy_giris_yap.dart';

class GyProfil extends StatefulWidget {
  @override
  _GyProfilState createState() => _GyProfilState();
}

class _GyProfilState extends State<GyProfil> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ScrollConfiguration(
        behavior: MyBehavior(),
        child: Padding(
          padding: EdgeInsets.only(
            left: SizeConfig.safeBlockHorizontal * 4,
            right: SizeConfig.safeBlockHorizontal * 4,
          ),
          child: ListView(
            shrinkWrap: true,
            children: <Widget>[
              Column(
                children: <Widget>[
                  SizedBox(height: SizeConfig.screenHeight * 0.02),
                  GyGirisYap(),
                  SizedBox(height: SizeConfig.screenHeight * 0.01,),
                  GyYazilimHakkinda(),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
