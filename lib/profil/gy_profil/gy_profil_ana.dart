import 'package:flutter/cupertino.dart';
import '../../anamenu/ana_menu.dart';
import 'package:flutter/material.dart';
import '../../bottom_menu/my_custom_bottom.dart';
import '../../global_degisken/global_degisken.dart' as global;
import '../../global_degisken/global_degisken.dart';
import '../../profil/gy_profil/widget/gy_Profil.dart';
import '../../responsive_class/size_config.dart';
import '../../yardimci_fonksiyonlar/notifications/local_notifications.dart';
import '../../yardimci_widgetlar/appbar_widgets/appbar_text.dart';

class GirisYokProfil extends StatefulWidget {
  const GirisYokProfil({Key key}) : super(key: key);

  @override
  _GirisYokProfilState createState() => _GirisYokProfilState();
}

class _GirisYokProfilState extends State<GirisYokProfil> {


  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return WillPopScope(
      onWillPop: () {
        global.NavigateAndRemoveUntil()
            .navigatePushNoAnimation(context, AnaMenu());
        return null;
      },
      child: Scaffold(
        bottomNavigationBar:MyBottomNavBar(3),
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: GlobalDegiskenler.appBarColor,
          title: BaslikText(
            label: 'Profil',
          ),
        ),
        body: GyProfil(),
      ),
    );
  }
}
