import '../../../responsive_class/size_config.dart';
import '../../../global_degisken/global_degisken.dart';
import '../../../yardimci_widgetlar/label_text/label_widget.dart';
import 'package:flutter/material.dart';

class YhLabelCard extends StatelessWidget {
  final String label;

  const YhLabelCard({Key key, this.label}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return LabelCard(
      color: Colors.black,
      fontFamily: GlobalDegiskenler.tlSimgesi,
      fontWeight: FontWeight.w500,
      alignment: Alignment.centerLeft,
      satirSayisi: 1,
      fontSize: SizeConfig.screenWidth * 0.04,
      label: label,
    );
  }
}
