import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'yh_list.dart';
import 'yh_uygulama_icon.dart';
import '../../../responsive_class/size_config.dart';
import '../../../yardimci_fonksiyonlar/sayfa_kadirma_efekti/scrool.dart';
import '../view_model/sube_bilgi_view_model.dart';


class YazilimHakkinda extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<SubeBilgiViewModel>(
      create: (context) => SubeBilgiViewModel(),
      child: ScrollConfiguration(
        behavior: MyBehavior(),
        child: Container(
            padding: EdgeInsets.only(top: 8),
            color: Colors.white,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.fromLTRB(SizeConfig.safeBlockHorizontal * 6,
                      0, SizeConfig.safeBlockHorizontal * 6, 0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      YhUygulamaIcon(),
                      Divider(
                        height: SizeConfig.screenHeight * 0.04,
                        color: Colors.black54,
                      ),
                      YhList(),
                    ],
                  ),
                ),
              ],
            )),
      ),
    );
  }
}
