import '../../../global_degisken/global_degisken.dart';
import 'package:flutter/material.dart';
import 'package:share/share.dart';
import 'yh_list_tile.dart';

class YhMail extends StatelessWidget {
  String ePosta = "bilgi@bym.net.tr";
  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        highlightColor: Colors.transparent,
        borderRadius: BorderRadius.all(Radius.circular(12)),
        onTap: () {
          try {
            Share.share(ePosta, subject: key + ' Mobil Sipariş');
          } catch (e) {}
        },
        child: YhListTile(
          icon: Icons.mail_outline,
          baslik: ePosta,
        ),
      ),
    );
  }
}
