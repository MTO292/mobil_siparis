import 'package:flutter_phone_direct_caller/flutter_phone_direct_caller.dart';
import '../../../global_degisken/global_degisken.dart';
import '../../../profil/yazilim_hakkinda/view_model/sube_bilgi_view_model.dart';
import '../../../responsive_class/size_config.dart';
import '../../../yardimci_widgetlar/FocusedMenuItem/focused_menu.dart';
import '../../../yardimci_widgetlar/FocusedMenuItem/menu_focus_model.dart';
import '../view_model/sube_bilgi_view_model.dart';
import 'package:provider/provider.dart';
import 'package:flutter/material.dart';
import 'yh_label_card.dart';
import 'yh_magaza.dart';
import 'yh_mail.dart';
import 'yh_telefon.dart';
import 'yh_web.dart';

class YhList extends StatefulWidget {
  @override
  _YhListState createState() => _YhListState();
}

class _YhListState extends State<YhList> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    //providerle sübe bilgilerini çekiyoruz
    context.read<SubeBilgiViewModel>().subeGetir();
  }

  String bym = 'Bym Yazılım';
  String musteri = 'Müşteri Hizmetleri';
  String bymTel = '+90 (850) 480 0296';

  final List<Widget> list = [
    YhMagaza(),
    YhTelefon(),
    YhMail(),
    YhWeb(),
  ];

  final List<Widget> listProgress = [
    YhMagaza(),
    YhTelefon(
      withProgress: true,
    ),
    YhMail(),
    YhWeb(),
  ];

  FocusedMenuItem focusMenuListele(String baslik, String tel) {
    return FocusedMenuItem(
      title: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          YhLabelCard(label: baslik),
          YhLabelCard(label: tel),
        ],
      ),
      trailingIcon: Icon(
        Icons.call_made,
        color: GlobalDegiskenler.butonArkaPlanRengi,
        size: SizeConfig.blockSizeHorizontal * 7.5,
      ),
      onPressed: () async {
        try {
          await FlutterPhoneDirectCaller.callNumber(tel);
        } catch (e) {}
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<SubeBilgiViewModel>(context);
    return Column(
      children: [0, 1, 2, 3]
          .map((index) => FocusedMenuHolder(
        openAciton: provider.durum != SubeBilgiDurum.Init &&
            provider.durum != SubeBilgiDurum.Yukleniyor
        //yüklendi ise focus menü açılabilir olsun yokse açılmasın
            ? OpenAction.tap
            : OpenAction.lock,
        menuWidth: SizeConfig.safeBlockHorizontal * 60,
        blurSize: 5.0,
        menuItemExtent: SizeConfig.blockSizeHorizontal * 14,
        menuBoxDecoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(5.0))),
        duration: Duration(milliseconds: 100),
        animateMenuItems: true,
        blurBackgroundColor: Colors.black54,
        bottomOffsetHeight: 100,
        menuItems:  provider.sube.telefon != ''  &&
            provider.sube.cariBasariDonus == '1'
            ? <FocusedMenuItem>[
          focusMenuListele(musteri, provider.sube.telefon),
          focusMenuListele(bym, bymTel),
        ]
            : <FocusedMenuItem>[
          focusMenuListele(bym, bymTel),
        ],
        onPressed: () {},
        child: provider.durum != SubeBilgiDurum.Init &&
            provider.durum != SubeBilgiDurum.Yukleniyor
        //yüklenmedi ise telefon item yanında progress gözüksün yok ise gözükmesin
            ? list[index]:listProgress[index],
      ))
          .toList(),
    );
  }
}
