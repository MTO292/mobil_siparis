import 'package:flutter/material.dart';
import '../../../global_degisken/global_degisken.dart';
import '../../../responsive_class/size_config.dart';
import '../../../yardimci_widgetlar/label_text/label_widget.dart';

class YhListTile extends StatelessWidget {
  Color baslikColor;
  double baslikSize;
  int satirSayi;
  FontWeight fontWeights;
  IconData icon;
  double iconSize;
  Color iconColor;
  String baslik;
  String altBaslik;
  Widget leadingWidget;
  Widget trailingWidget;

  YhListTile(
      {@override this.baslik,
      this.altBaslik,
      this.baslikColor,
      this.baslikSize,
      this.satirSayi,
      this.fontWeights,
      @override this.icon,
      this.iconSize,
      this.iconColor,
      this.leadingWidget,
      this.trailingWidget,
      });

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: leadingWidget ?? Icon(
        icon,
        color: iconColor ?? GlobalDegiskenler.butonArkaPlanRengi,
        size: iconSize ?? SizeConfig.safeBlockHorizontal * 7,
      ),
      title: LabelCard(
        color: baslikColor ?? Colors.black,
        fontFamily: GlobalDegiskenler.tlSimgesi,
        fontWeight: fontWeights ?? FontWeight.w600,
        alignment: Alignment.centerLeft,
        satirSayisi: satirSayi ?? 1,
        fontSize: baslikSize ?? SizeConfig.screenWidth * 0.04,
        label: baslik,
      ),
      subtitle: altBaslik != null ? LabelCard(
        label: altBaslik ?? '',
        fontWeight: FontWeight.w100,
        fontSize: SizeConfig.screenWidth * 0.032,
        color: Colors.black,
        fontFamily: GlobalDegiskenler.tlSimgesi,
        alignment: Alignment.centerLeft,
        satirSayisi: 1,
      ):null,
      trailing: trailingWidget ?? SizedBox()
    );
  }
}
