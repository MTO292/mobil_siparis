import 'package:flutter/material.dart';
import '../../../responsive_class/size_config.dart';

class YhAltBar extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      height: SizeConfig.screenHeight * 0.1,
      width: SizeConfig.screenWidth,
      decoration: BoxDecoration(
          color: Colors.grey.shade100,
          border: Border(
            top: BorderSide(
              color: Colors.black54,
              width: 0.5,
            ),
          )),
      child: Padding(
        padding: EdgeInsets.symmetric(
          vertical: SizeConfig.safeBlockVertical * 1.5,
          horizontal: SizeConfig.safeBlockHorizontal * 6,
        ),
        child: Container(
          alignment: Alignment.centerLeft,
          child: Container(
            color: Colors.transparent,
            child: Image.asset('assets/images/bymlogo.png'),
          ),
        ),
      ),
    );
  }
}
