import 'package:flutter/material.dart';
import 'package:line_awesome_icons/line_awesome_icons.dart';
import 'package:url_launcher/url_launcher.dart';
import 'yh_list_tile.dart';

class YhWeb extends StatelessWidget {
  String url = "http://bymyazilim.com.tr/";
  String site = "www.bymyazilim.com.tr";
  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        highlightColor: Colors.transparent,
        borderRadius: BorderRadius.all(Radius.circular(12)),
        onTap: () async {
          try {
            if (await canLaunch(url)) {
              await launch(url);
            } else {
              throw 'Hata ';
            }
          } catch (e) {}
        },
        child: YhListTile(
          icon: LineAwesomeIcons.globe,
          baslik: site,
        ),
      ),
    );
  }
}
