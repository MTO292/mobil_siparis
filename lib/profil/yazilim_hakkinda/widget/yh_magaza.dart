import 'package:flutter/material.dart';
import '../../../global_degisken/global_degisken.dart';
import 'package:line_awesome_icons/line_awesome_icons.dart';
import 'package:store_redirect/store_redirect.dart';
import 'yh_list_tile.dart';

class YhMagaza extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        highlightColor: Colors.transparent,
        borderRadius: BorderRadius.all(Radius.circular(12)),
        onTap: () {
          try {
            StoreRedirect.redirect(
                androidAppId: AppInfo.paketIsmi,
                iOSAppId: AppInfo.appleStoreAppId);
          } catch (e) {}
        },
        child: YhListTile(
          icon: Theme.of(context).platform == TargetPlatform.android
              ? LineAwesomeIcons.play
              : LineAwesomeIcons.apple,
          baslik: "Mağazaya Git",
        ),
      ),
    );
  }
}
