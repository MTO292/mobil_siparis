import 'package:flutter/material.dart';
import '../../../global_degisken/global_degisken.dart';
import 'yh_list_tile.dart';

class YhUygulamaIcon extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return YhListTile(
      baslik: AppInfo.uygulamaAdi,
      altBaslik: "v" + AppInfo.versiyon,
      leadingWidget: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(4)),
          boxShadow: [
            BoxShadow(
              color: Colors.black12,
              blurRadius: 1,
              spreadRadius: 0.4,
            )
          ],
        ),
        child: ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(4)),
            child: Image.asset("assets/images/icon.png")),
      ),
    );
  }
}
