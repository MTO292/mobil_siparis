import '../../../responsive_class/size_config.dart';
import 'package:flutter/material.dart';
import 'package:line_awesome_icons/line_awesome_icons.dart';
import 'yh_list_tile.dart';
import '../../../global_degisken/global_degisken.dart' as global;


class YhTelefon extends StatelessWidget {

  final bool withProgress;

  const YhTelefon({Key key, this.withProgress = false}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
        child: YhListTile(
          icon: LineAwesomeIcons.phone,
          baslik: 'İletişim Bilgileri',
          trailingWidget: SizedBox(
            height: SizeConfig.safeBlockHorizontal * 6,
            width: SizeConfig.safeBlockHorizontal * 6,
            child: withProgress ? global.progressDondur(tur: 1,strokeWidth: 1.5):null,
          ),
        ),
    );
  }
}
