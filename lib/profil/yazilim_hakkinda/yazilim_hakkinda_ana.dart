import 'package:flutter/material.dart';
import '../../global_degisken/global_degisken.dart';
import '../../profil/yazilim_hakkinda/widget/yh_alt_bar.dart';
import '../../responsive_class/size_config.dart';
import '../../yardimci_widgetlar/appbar_widgets/appbar_text.dart';
import 'widget/yazilim_hakkinda.dart';

class YazilimHakkindaAna extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: GlobalDegiskenler.appBarColor,
        centerTitle: true,
        title: BaslikText(
          label: 'Yazılım Hakkında',
        ),
      ),
      bottomSheet: YhAltBar(),
      body: YazilimHakkinda(),
    );
  }
}
