import '../../../api/model/sube_bilgi_model.dart';
import '../../../api/response/islem_donus.dart';
import 'package:flutter/cupertino.dart';

enum SubeBilgiDurum { Init, Yukleniyor, Yuklendi, Hata }

class SubeBilgiViewModel with ChangeNotifier {
  SubeBilgiModel _sube;
  SubeBilgiDurum _durum;

  SubeBilgiModel get sube => _sube;

  SubeBilgiDurum get durum => _durum;

  set durum(SubeBilgiDurum value) {
    _durum = value;
    notifyListeners();
  }

  set sube(SubeBilgiModel value) {
    _sube = value;
  }

  SubeBilgiViewModel() {
    durum = SubeBilgiDurum.Init;
    _sube = SubeBilgiModel();
  }

  Future subeGetir() async {
    _durum = SubeBilgiDurum.Yukleniyor;
    try {
      IslemDonus islemDonus = IslemDonus();
      sube = await islemDonus.subeBilgiGetir();
      durum = SubeBilgiDurum.Yuklendi;
    } catch (e) {
      durum = SubeBilgiDurum.Hata;
    }
  }
}
