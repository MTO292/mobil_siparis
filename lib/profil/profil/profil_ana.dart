import 'package:flutter/cupertino.dart';
import '../../anamenu/ana_menu.dart';
import '../../responsive_class/size_config.dart';
import '../../global_degisken/global_degisken.dart' as global;
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'view_model/profil_view_model.dart';
import 'widget/profil.dart';

class ProfilTemelSayfa extends StatefulWidget {
  @override
  _ProfilTemelSayfaState createState() => _ProfilTemelSayfaState();
}

class _ProfilTemelSayfaState extends State<ProfilTemelSayfa> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return ChangeNotifierProvider<ProfilViewModel>(
      create: (context) => ProfilViewModel(),
      child: WillPopScope(
        onWillPop: (){
         return global.NavigateAndRemoveUntil().navigatePushNoAnimation(context, AnaMenu());
        },
        child:Profil(),
      ),
    );
  }
}
