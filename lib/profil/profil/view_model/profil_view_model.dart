import 'package:flutter/cupertino.dart';
import '../../../api/model/profil_cari_model.dart';

class ProfilViewModel with ChangeNotifier {
  ProfilCariModel _cari;

  ProfilViewModel() {
    _cari = ProfilCariModel();
  }

  ProfilCariModel get cari => _cari;

  set cari(ProfilCariModel value) {
    _cari = value;
    notifyListeners();
  }
}
