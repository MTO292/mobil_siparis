import 'package:flutter/material.dart';
import '../../../global_degisken/global_degisken.dart';
import '../../../responsive_class/size_config.dart';
import '../../../siparis/siparis_ana_sayfa.dart';
import 'profil_label.dart';

class ProfilGecmisSiparis extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Tooltip(
      message: 'Siparişler sayfasına gider',
      child: InkWell(
        onTap: () {
          Navigate().navigatePushNoAnimation(context, SiparisAnaSayfa(false));
        },
        child: ListTile(
          title: ProfilLabel(baslik:'Geçmiş Siparişler'),
          leading: Icon(Icons.shopping_basket,
            color:  GlobalDegiskenler.butonArkaPlanRengi,
            size:  SizeConfig.safeBlockHorizontal * 7,),
          trailing: Icon(Icons.navigate_next,
            color:  GlobalDegiskenler.butonArkaPlanRengi,
            size:  SizeConfig.safeBlockHorizontal * 7,),
        ),
      ),
    );
  }
}
