import 'package:connectivity_wrapper/connectivity_wrapper.dart';
import 'package:flutter/cupertino.dart';
import '../../../bottom_menu/my_custom_bottom.dart';
import '../../../profil/profil/data/profil_prefs.dart';
import '../../../profil/profil/view_model/profil_view_model.dart';
import 'package:provider/provider.dart';
import 'profil_gecmis_siparis.dart';
import 'profil_adreslerim.dart';
import 'profil_cikis.dart';
import '../../../yardimci_fonksiyonlar/form_mesaj_dondur/net_kontrol.dart'
    as net;
import '../../../yardimci_fonksiyonlar/sayfa_kadirma_efekti/scrool.dart';
import 'package:flutter/material.dart';
import '../../../global_degisken/global_degisken.dart';
import '../../../responsive_class/size_config.dart';
import '../../../yardimci_widgetlar/appbar_widgets/appbar_text.dart';
import 'profil_yazilim_hakkinda.dart';
import 'profil_isim.dart';
import 'profil_mail.dart';
import 'profil_telefon_no.dart';

class Profil extends StatefulWidget {
  @override
  _ProfilState createState() => _ProfilState();
}

class _ProfilState extends State<Profil> {
  ProfilPrefs _prefs;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _prefs = ProfilPrefs();
    providerGuncelle();
  }

  Future<void> providerGuncelle() async {
    context.read<ProfilViewModel>().cari = await _prefs.sfYukle();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      bottomNavigationBar:MyBottomNavBar(3),
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: GlobalDegiskenler.appBarColor,
        title: BaslikText(
          label: 'Profil',
        ),
      ),
      body: ConnectivityWidgetWrapper(
        offlineWidget: net.netKontrol(),
        disableInteraction: false,
        child: Container(
          child: ScrollConfiguration(
            behavior: MyBehavior(),
            child: Container(
              child: Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: SizeConfig.safeBlockHorizontal * 4,
                ),
                child: ListView(
                  shrinkWrap: true,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        sizeBox(),
                        Container(
                          decoration: GlobalDegiskenler.kartSablon,
                          child: Column(
                            children: <Widget>[
                              ProfilIsim(),
                              ProfilMail(),
                              ProfilTelefonNo(),
                            ],
                          ),
                        ),
                        sizeBox(size: SizeConfig.screenHeight * 0.02),
                        Container(
                          decoration: GlobalDegiskenler.kartSablon,
                          child: Column(
                            children: <Widget>[
                              ProfilAdreslerim(),
                              divider(),
                              ProfilGecmisSiparis(),
                              divider(),
                              ProfilYazilimHakkinda(),
                              divider(),
                              ProfilCikis(),
                            ],
                          ),
                        ),
                        sizeBox(),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  /*Tek bir fonksiyon olarak dividerı çağırmak için tanımlandı.*/
  Widget divider() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: SizeConfig.screenWidth * 0.04),
      child: Divider(
        color: Colors.grey.withOpacity(0.4),
        thickness: 0.5,
        height: 2.0,
      ),
    );
  }

  sizeBox({double size}) {
    return SizedBox(
      height: size ?? SizeConfig.screenHeight * 0.01,
    );
  }
}
