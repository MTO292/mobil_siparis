import 'package:flutter/material.dart';
import '../../../anamenu/ana_menu.dart';
import '../../../bloc/blocs_exports.dart';
import '../../../global_degisken/get_it.dart';
import '../../../global_degisken/global_degisken.dart';
import '../../../responsive_class/size_config.dart';
import '../../../yardimci_fonksiyonlar/form_mesaj_dondur/mesaj_dondur.dart';
import '../../../yardimci_fonksiyonlar/notifications/local_notifications.dart';
import 'package:line_awesome_icons/line_awesome_icons.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../../locator.dart';
import 'profil_label.dart';
import 'package:flutter_bloc/flutter_bloc.dart';


class ProfilCikis extends StatelessWidget {
  ///bloc la olan hatadan dolayı local bildirim kapatıldı
  // LocalNotifications bildirim = LocalNotifications();

  @override
  Widget build(BuildContext context) {
    return Tooltip(
      message: 'Çıkış Yap butonu',
      child: InkWell(
        /*Çıkış değeri olarak false veriliyor bu sayede ana sayfa yüklenirken kontrol edip giriş ekranına atıyor.*/
        onTap: () async {
          MesajGoster.soruSor(
            context,
            "Çıkış yapmak istediğinize\n emin misiniz ?",
            SoruTipi.Soru,
            evet: cikisYap(context),
          );
        },
        /*Çıkış yap label*/
        child: ListTile(
          title: ProfilLabel(baslik: 'Çıkış Yap', color: Colors.red),
          leading: Icon(
            LineAwesomeIcons.sign_out,
            color: GlobalDegiskenler.butonArkaPlanRengi,
            size: SizeConfig.safeBlockHorizontal * 7,
          ),
          trailing: Icon(
            Icons.navigate_next,
            color: GlobalDegiskenler.butonArkaPlanRengi,
            size: SizeConfig.safeBlockHorizontal * 7,
          ),
        ),
      ),
    );
  }

  cikisYap(BuildContext context) {
    return () async {
      Navigator.of(context, rootNavigator: true).pop();
      /*Shared preferences ile çıkış yaptırıp login sayfasından başlamasını sağlıyoruz.*/
      SharedPreferences pref = await SharedPreferences.getInstance();
      pref.clear();
      //sepet Sayacını Sıfırlıyoruz
      //sepet sayacı için bloc la değerini atıyoruz
      context.bloc<SepetSayacCubit>().sifirla();
      ///bloc la olan hatadan dolayı local bildirim kapatıldı
      // bildirimi iptal ediyotuz
      //bildirim.tumBildirimleriIptalET();
      pref.setBool("uygulamaGuncellendi", false);
      pref.setBool("girisYapildi", false);
      locator<Get>().loggedin = false;
      pref.setBool('sliderAktifMi', true);
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => AnaMenu()),
          (e) => false);
    };
  }
}
