import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../../global_degisken/global_degisken.dart';
import '../../../profil/Profil_guncelle/pg_ana.dart';
import '../../../profil/profil/data/profil_prefs.dart';
import '../../../profil/profil/view_model/profil_view_model.dart';
import '../../../responsive_class/size_config.dart';
import 'package:line_awesome_icons/line_awesome_icons.dart';
import 'package:provider/provider.dart';

import 'profil_label.dart';

class ProfilIsim extends StatefulWidget {
  @override
  _ProfilIsimState createState() => _ProfilIsimState();
}

class _ProfilIsimState extends State<ProfilIsim> {
  ProfilPrefs _prefs;

  Future<void> providerGuncelle() async {
    context.read<ProfilViewModel>().cari = await _prefs.sfYukle();
  }

  @override
  Widget build(BuildContext context) {
    var cariProvider = Provider.of<ProfilViewModel>(context);
    return Padding(
      padding: EdgeInsets.all(SizeConfig.screenHeight * 0.02),
      child: ListTile(
        title: ProfilLabel(baslik: cariProvider.cari.isim ?? '', satirSayi: 2),
        leading: Container(
          decoration: BoxDecoration(
              color: GlobalDegiskenler.butonArkaPlanRengi,
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.3),
                  spreadRadius: 1,
                  blurRadius: 4,
                  offset: Offset(0, 1),
                )
              ]),
          child: Icon(
            !Platform.isAndroid ? CupertinoIcons.person_solid : Icons.person,
            size: SizeConfig.safeBlockHorizontal * 14,
            color: Colors.white,
          ),
        ),
        trailing: Tooltip(
          message: "Düzenle",
          child: InkWell(
            onTap: () async {
              bool deger = await Navigate().navigatePushNoAnimation(context, PbGuncelle());
              if (deger != null || deger == true){
                _prefs = ProfilPrefs();
                providerGuncelle();
              }
            },
            child: Container(
              child: Icon(
                LineAwesomeIcons.edit,
                size: SizeConfig.safeBlockHorizontal * 8,
                color: GlobalDegiskenler.butonArkaPlanRengi,
              ),
              decoration: BoxDecoration(color: Colors.white, boxShadow: [
                BoxShadow(
                  color: GlobalDegiskenler.butonArkaPlanRengi.withOpacity(0.2),
                  spreadRadius: 0.5,
                  blurRadius: 4,
                  offset: Offset(0, 1),
                )
              ]),
            ),
          ),
        ),
      ),
    );
  }
}

