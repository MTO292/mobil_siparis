import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../../global_degisken/global_degisken.dart';
import '../../../profil/adres_listele/adres_ana.dart';
import '../../../responsive_class/size_config.dart';
import 'profil_label.dart';

class ProfilAdreslerim extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Tooltip(
      message: 'Adreslerim sayfasına gider',
      child: InkWell(
        onTap: () {
          Navigate().navigatePushNoAnimation(
              context,
              AdresGetir(
                siparisModu: false,
              ));
        },
        child: ListTile(
          title: ProfilLabel(baslik: 'Adreslerim'),
          leading: Icon(
            !Platform.isAndroid
                ? CupertinoIcons.location_solid
                : Icons.location_on,
            color: GlobalDegiskenler.butonArkaPlanRengi,
            size: SizeConfig.safeBlockHorizontal * 7,
          ),
          trailing: Icon(
            Icons.navigate_next,
            color: GlobalDegiskenler.butonArkaPlanRengi,
            size: SizeConfig.safeBlockHorizontal * 7,
          ),
        ),
      ),
    );
  }
}
