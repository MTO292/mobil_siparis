import 'dart:io';
import 'package:flutter/material.dart';
import '../../../global_degisken/global_degisken.dart';
import '../../../profil/yazilim_hakkinda/yazilim_hakkinda_ana.dart';
import '../../../responsive_class/size_config.dart';
import 'profil_label.dart';

class ProfilYazilimHakkinda extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Tooltip(
      message: 'Yazılım hakkında bilgi verir',
      child: InkWell(
        onTap: () {
          Navigate().navigatePushNoAnimation(context, YazilimHakkindaAna());
        },
        child: ListTile(
          title: ProfilLabel(baslik: 'Yazılım Hakkında'),
          leading: Icon(
            Platform.isAndroid ? Icons.phone_android : Icons.phone_iphone,
            color: GlobalDegiskenler.butonArkaPlanRengi,
            size: SizeConfig.safeBlockHorizontal * 7,
          ),
          trailing: Icon(
            Icons.navigate_next,
            color: GlobalDegiskenler.butonArkaPlanRengi,
            size: SizeConfig.safeBlockHorizontal * 7,
          ),
        ),
      ),
    );
  }
}
