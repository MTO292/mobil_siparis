import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../../global_degisken/global_degisken.dart';
import '../../../profil/profil/view_model/profil_view_model.dart';
import '../../../responsive_class/size_config.dart';
import 'package:provider/provider.dart';
import 'profil_label.dart';

class ProfilMail extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var cariProvider = Provider.of<ProfilViewModel>(context);
    return ListTile(
      title: ProfilLabel(
        baslik: cariProvider.cari.mail != "" ? cariProvider.cari.mail : "Posta Adresi Eklenmedi",
        color: Colors.grey,
        size: MediaQuery.of(context).size.width * 0.035,
      ),
      leading: Icon(
        !Platform.isAndroid ? CupertinoIcons.mail_solid : Icons.email,
        color: GlobalDegiskenler.butonArkaPlanRengi,
        size: SizeConfig.safeBlockHorizontal * 7,
      ),
    );
  }

}

