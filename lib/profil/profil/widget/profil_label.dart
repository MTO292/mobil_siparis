import 'package:flutter/material.dart';
import '../../../global_degisken/global_degisken.dart';
import '../../../responsive_class/size_config.dart';
import '../../../yardimci_widgetlar/label_text/label_widget.dart';

class ProfilLabel extends StatefulWidget {
  String baslik;
  Color color;
  double size;
  int satirSayi;

  ProfilLabel(
      {Key key, @override this.baslik, this.color, this.size, this.satirSayi})
      : super(key: key);

  @override
  _ProfilLabelState createState() => _ProfilLabelState();
}

class _ProfilLabelState extends State<ProfilLabel> {
  @override
  Widget build(BuildContext context) {
    return LabelCard(
      color: widget.color ?? Colors.black,
      fontFamily: GlobalDegiskenler.tlSimgesi,
      fontWeight: FontWeight.w600,
      alignment: Alignment.centerLeft,
      satirSayisi: widget.satirSayi ?? 1,
      fontSize: widget.size ?? SizeConfig.screenWidth * 0.04,
      label: widget.baslik ?? '',
    );
  }
}
