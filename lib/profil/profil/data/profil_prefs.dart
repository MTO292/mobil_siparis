import '../../../api/model/profil_cari_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfilPrefs {
  SharedPreferences _prefs ;
  ProfilCariModel _cari;

  ProfilPrefs(){
    _cari = ProfilCariModel();
  }

  sfYukle() async {
    _prefs = await SharedPreferences.getInstance();
    //prefs baslatılmışsa alıyoruz
    _cari.isim = _prefs == null ? "" :(_prefs.getString('cariAdDonus') ?? '');
    _cari.mail = _prefs == null ? "" :(_prefs.getString("cariMailDonus") ?? '');
    _cari.cariKod = _prefs == null ? "" :(_prefs.getString("cariKodDonus") ?? '');
    _cari.cariId = _prefs == null ? 0 :(_prefs.getInt("cariIdDonus") ?? '');
    _cari.telefonNo = _prefs == null ? "" :(_prefs.getString("cariTelefonDonus") ?? '');
    return _cari;
  }
}