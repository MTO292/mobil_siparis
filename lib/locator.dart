import 'package:get_it/get_it.dart';
import 'global_degisken/get_it.dart';

GetIt locator = GetIt.instance;

void setupLocator() {
  locator.registerSingleton<Get>(Get());
}
