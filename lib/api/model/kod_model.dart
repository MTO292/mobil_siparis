
import 'dart:convert';

Kod kodFromJson(String str) => Kod.fromJson(json.decode(str));

String kodToJson(Kod data) => json.encode(data.toJson());

class Kod {
  String durum;
  String kod;
  String mesaj;
  String adSoyad;


  Kod({
    this.durum,
    this.kod,
    this.mesaj,
    this.adSoyad,
  });

  factory Kod.fromJson(Map<String, dynamic> json) => Kod(
    durum: json["durum"],
    kod: json["kod"],
    mesaj: json["mesaj"],
    adSoyad: json["cari_ad"],
  );

  Map<String, dynamic> toJson() => {
    "durum": durum,
    "kod": kod,
    "mesaj": mesaj,
    "cari_ad": adSoyad,
  };
}
