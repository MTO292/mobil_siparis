
import 'dart:convert';

Menu menuFromJson(String str) => Menu.fromJson(json.decode(str));

String menuToJson(Menu data) => json.encode(data.toJson());

Grupurunler grupurunlerFromJson(String str) => Grupurunler.fromJson(json.decode(str));

String grupurunlerToJson(Grupurunler data) => json.encode(data.toJson());

Anagrup AnagrupFromJson(String str) => Anagrup.fromJson(json.decode(str));

String AnagrupToJson(Grupurunler data) => json.encode(data.toJson());

class Menu {
    Menu({
        this.cariBasariDonus,
        this.anagrup,
        this.slider,
    });

    String cariBasariDonus;
    List<Anagrup> anagrup;
    List<Sliders> slider;

    factory Menu.fromJson(Map<String, dynamic> json) => Menu(
        cariBasariDonus: json["cari_basari_donus"],
        anagrup: List<Anagrup>.from(json["ANAGRUP"].map((x) => Anagrup.fromJson(x))),
        slider: List<Sliders>.from(json["SLIDER"].map((x) => Sliders.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "ANAGRUP": List<dynamic>.from(anagrup.map((x) => x.toJson())),
        "SLIDER": List<dynamic>.from(slider.map((x) => x.toJson())),
    };
}

class Anagrup {
    Anagrup({
        this.cariBasariDonus,
        this.anagrupId,
        this.anagrupGid,
        this.anagrupBaslik,
        this.grupurunler,
        this.altgruplar,
    });

    String cariBasariDonus;
    String anagrupId;
    String anagrupGid;
    String anagrupBaslik;
    List<Grupurunler> grupurunler;
    List<Altgruplar> altgruplar;

    factory Anagrup.fromJson(Map<String, dynamic> json) => Anagrup(
        cariBasariDonus: json["cari_basari_donus"],
        anagrupId: json["anagrup_id"],
        anagrupGid: json["anagrup_gid"],
        anagrupBaslik: json["anagrup_baslik"],
        grupurunler: List<Grupurunler>.from(json["grupurunler"].map((x) => Grupurunler.fromJson(x))),
        altgruplar: List<Altgruplar>.from(json["altgruplar"].map((x) => Altgruplar.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "cari_basari_donus": cariBasariDonus,
        "anagrup_id": anagrupId,
        "anagrup_gid": anagrupGid,
        "anagrup_baslik": anagrupBaslik,
        "grupurunler": List<dynamic>.from(grupurunler.map((x) => x.toJson())),
        "altgruplar": List<dynamic>.from(altgruplar.map((x) => x.toJson())),
    };
}

class Altgruplar {
    Altgruplar({
        this.altgrupId,
        this.altgrupGid,
        this.altgrupAdi,
        this.altgrupurunler,
    });

    String altgrupId;
    String altgrupGid;
    String altgrupAdi;
    List<Grupurunler> altgrupurunler;

    factory Altgruplar.fromJson(Map<String, dynamic> json) => Altgruplar(
        altgrupId: json["altgrup_id"],
        altgrupGid: json["altgrup_gid"],
        altgrupAdi: json["altgrup_adi"],
        altgrupurunler: List<Grupurunler>.from(json["altgrupurunler"].map((x) => x)),
    );

    Map<String, dynamic> toJson() => {
        "altgrup_id": altgrupId,
        "altgrup_gid": altgrupGid,
        "altgrup_adi": altgrupAdi,
        "altgrupurunler": List<Grupurunler>.from(altgrupurunler.map((x) => x)),
    };
}

class   Grupurunler {
    Grupurunler({
        this.aId,
        this.aAdi,
        this.aSfiyat,
        this.aAcik,
        this.aciklama,
        this.aBirim,
    });

    String aId;
    String aAdi;
    String aSfiyat;
    String aAcik; //siparis not
    String aciklama;//ürün Not
    String aBirim;

    factory Grupurunler.fromJson(Map<String, dynamic> json) => Grupurunler(
        aId: json["a_id"],
        aAdi: json["a_adi"],
        aSfiyat: json["a_sfiyat"],
        aAcik: json["a_acik"],
        aciklama : json["aciklama"],
        aBirim: json["a_birim"],
    );

    Map<String, dynamic> toJson() => {
        "a_id": aId,
        "a_adi": aAdi,
        "a_sfiyat": aSfiyat,
        "a_acik": aAcik,
        'aciklama': aciklama,
        "a_birim": aBirim,
    };
}

class Sliders {
    Sliders({
        this.cariBasariDonus,
        this.aId,
        this.aSira,
        this.aAksiyon,
        this.aAksiyontur,
    });

    String cariBasariDonus;
    int aId;
    int aSira;
    String aAksiyon;
    int aAksiyontur;

    factory Sliders.fromJson(Map<String, dynamic> json) => Sliders(
        cariBasariDonus: json["cari_basari_donus"],
        aId: json["a_id"],
        aSira: json["a_sira"],
        aAksiyon: json["a_aksiyon"],
        aAksiyontur: json["a_aksiyontur"],
    );

    Map<String, dynamic> toJson() => {
        "cari_basari_donus": cariBasariDonus,
        "a_id": aId,
        "a_sira": aSira,
        "a_aksiyon": aAksiyon,
        "a_aksiyontur": aAksiyontur,
    };
}

