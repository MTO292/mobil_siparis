/*Bu model,sipariş kısmında ödeme türlerini ekranda gösterebilmek için oluşturulmuştur*/
import 'dart:convert';

List<OdemeTur> odemeTurFromJson(String str) =>
    List<OdemeTur>.from(json.decode(str).map((x) => OdemeTur.fromJson(x)));

String odemeTurToJson(List<OdemeTur> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class OdemeTur {
  String aId;
  String aAdi;

  OdemeTur({
    this.aId,
    this.aAdi,
  });

  factory OdemeTur.fromJson(Map<String, dynamic> json) => OdemeTur(
        aId: json["a_id"],
        aAdi: json["a_adi"],
      );

  Map<String, dynamic> toJson() => {
        "a_id": aId,
        "a_adi": aAdi,
      };
}
