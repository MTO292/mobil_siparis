/*Bu model,cari bilgilerini kayıt edebilmek, ekranda gösterebilmek için oluşturulmuştur*/
import 'dart:convert';

Uye uyeFromJson(String str) => Uye.fromJson(json.decode(str));

String uyeToJson(Uye data) => json.encode(data.toJson());

class Uye {
  int cariIdDonus;
  String cariKodDonus;
  String cariAdDonus;
  String cariMailDonus;
  String cariSifreDonus;
  String cariTelefonDonus;
  String cariBasariDonus;

  Uye({
    this.cariIdDonus,
    this.cariKodDonus,
    this.cariAdDonus,
    this.cariMailDonus,
    this.cariSifreDonus,
    this.cariTelefonDonus,
    this.cariBasariDonus,
  });

  factory Uye.fromJson(Map<String, dynamic> json) => Uye(
        cariIdDonus: json["cari_id_donus"],
        cariKodDonus: json["cari_kod_donus"],
        cariAdDonus: json["cari_ad_donus"],
        cariMailDonus: json["cari_mail_donus"],
        cariSifreDonus: json["cari_sifre_donus"],
        cariTelefonDonus: json["cari_telefon_donus"],
        cariBasariDonus: json["cari_basari_donus"],
      );

  Map<String, dynamic> toJson() => {
        "cari_id_donus": cariIdDonus,
        "cari_kod_donus": cariKodDonus,
        "cari_ad_donus": cariAdDonus,
        "cari_mail_donus": cariMailDonus,
        "cari_sifre_donus": cariSifreDonus,
        "cari_telefon_donus": cariTelefonDonus,
        "cari_basari_donus": cariBasariDonus,
      };
}
