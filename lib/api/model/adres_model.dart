import 'dart:convert';

List<Adress> adressFromJson(String str) => List<Adress>.from(json.decode(str).map((x) => Adress.fromJson(x)));

String adressToJson(List<Adress> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Adress {
  Adress({
    this.cariBasariDonus,
    this.adresAdi,
    this.cariId,
    this.sira,
    this.ulkeId,
    this.ilId,
    this.ilceId,
    this.semtId,
    this.detay,
    this.ulkeAdi,
    this.ilAdi,
    this.ilceAdi,
    this.semtAdi,
  });

  String cariBasariDonus;
  String adresAdi;
  int cariId;
  int sira;
  int ulkeId;
  int ilId;
  int ilceId;
  int semtId;
  String detay;
  String ulkeAdi;
  String ilAdi;
  String ilceAdi;
  String semtAdi;



  factory Adress.fromJson(Map<String, dynamic> json) => Adress(
    cariBasariDonus: json["cari_basari_donus"],
    adresAdi: json["adres_adi"],
    cariId: json["cari_id"],
    sira: json["sira"],
    ulkeId: json["ulke_id"],
    ilId: json["il_id"],
    ilceId: json["ilce_id"],
    semtId: json["semt_id"],
    detay: json["detay"],
    ulkeAdi: json["ulke_adi"],
    ilAdi: json["il_adi"],
    ilceAdi: json["ilce_adi"],
    semtAdi: json["mahalle_adi"],
  );

  Map<String, dynamic> toJson() => {
    "cari_basari_donus": cariBasariDonus,
    "adres_adi": adresAdi,
    "cari_id": cariId,
    "sira": sira,
    "ulke_id": ulkeId,
    "il_id": ilId,
    "ilce_id": ilceId,
    "semt_id": semtId,
    "detay": detay,
    "ulke_adi": ulkeAdi,
    "il_adi": ilAdi,
    "ilce_adi": ilceAdi,
    "mahalle_adi": semtAdi,
  };

  //nesne klonlama işlemi için oluşturuldu
  Adress clone(adress) {
    final String jsonString = json.encode(adress);
    final jsonResponse = json.decode(jsonString);

    return Adress.fromJson(jsonResponse as Map<String, dynamic>);
  }
}


