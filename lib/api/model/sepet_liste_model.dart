/*BU MODEL, SİPARİŞLERİM SAYFASINDA TIKLANILAN(HAZIRLANAN,TAMAMLANAN SİPARİŞ) SEÇENEĞİNE GÖRE
DÖNEN JSON VERİSİNİN MODEL YAPISINI TEMSİL EDİYOR*/

import 'dart:convert';

List<SepetListe> sepetListeFromJson(String str) => List<SepetListe>.from(json.decode(str).map((x) => SepetListe.fromJson(x)));

String sepetListeToJson(List<SepetListe> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class SepetListe {
    String urunsayisi;
    String durumBir;
    String aId;
    String aTarih;
    String aSaat;
    String durumIki;
    String aTadresId;

    SepetListe({
        this.urunsayisi,
        this.durumBir,
        this.aId,
        this.aTarih,
        this.aSaat,
        this.durumIki,
        this.aTadresId,
    });

    factory SepetListe.fromJson(Map<String, dynamic> json) => SepetListe(
        urunsayisi: json["urunsayisi"],
        durumBir: json["durum_bir"],
        aId: json["a_id"],
        aTarih: json["a_tarih"],
        aSaat: json["a_saat"],
        durumIki: json["durum_iki"],
        aTadresId: json["a_tadres_id"],
    );

    Map<String, dynamic> toJson() => {
        "urunsayisi": urunsayisi,
        "durum_bir": durumBir,
        "a_id": aId,
        "a_tarih": aTarih,
        "a_saat": aSaat,
        "durum_iki": durumIki,
        "a_tadres_id": aTadresId,
    };
}
