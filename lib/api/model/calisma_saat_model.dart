
import 'dart:convert';

List<Saat> saatFromJson(String str) => List<Saat>.from(json.decode(str).map((x) => Saat.fromJson(x)));

String saatToJson(List<Saat> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Saat {
    Saat({
        this.cariBasariDonus,
        this.gun,
        this.durum,
        this.baslangic,
        this.bitis,
    });

    String cariBasariDonus;
    String gun;
    String durum;
    String baslangic;
    String bitis;

    factory Saat.fromJson(Map<String, dynamic> json) => Saat(
        cariBasariDonus: json["cari_basari_donus"],
        gun: json["gun"],
        durum: json["durum"],
        baslangic: json["Baslangic"],
        bitis: json["Bitis"],
    );

    Map<String, dynamic> toJson() => {
        "cari_basari_donus": cariBasariDonus,
        "gun": gun,
        "durum": durum,
        "Baslangic": baslangic,
        "Bitis": bitis,
    };
}
