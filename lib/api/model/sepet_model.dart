/*Bu model,sepetteki ürün bilgilerini ekranda gösterebilmek için oluşturulmuştur*/
class Urun {
  String aAdi;
  String aId;
  String aSfiyat;
  String toplamFiyat;
  String genelTopFiy;//ekstra ürünlerin Fiyatı dahil
  String urunMiktar;
  String aciklama;//ürün Not
  String aAcik;//ürün içindekiler
  String aBirim;
  String porsiyon;
  List<EkstraDetay> ekstraOzelik;

  Urun(
      this.aAdi,
      this.aId,
      this.aSfiyat,
      this.toplamFiyat,
      this.genelTopFiy,//ekstra ürünlerin Fiyatı dahil
      this.urunMiktar,
      this.aciklama,//ürün Not
      this.porsiyon,
      this.aAcik,//ürün içindekiler
      this.aBirim,
      this.ekstraOzelik,
      );

  Urun.fromJson(Map<String, dynamic> json)
      : aAdi = json['aAdi'],
        aId = json['aId'],
        aSfiyat = json['aSfiyat'],
        toplamFiyat = json['toplamFiyat'],
        genelTopFiy = json['genelTopFiy'],
        urunMiktar = json['urunMiktar'],
        aciklama = json["aciklama"],
        porsiyon = json["porsiyon"],
        aAcik = json["a_acik"],
        aBirim = json["a_birim"],
        ekstraOzelik= List<EkstraDetay>.from(json["ekstraOzelik"].map((x) => EkstraDetay.fromJson(x)));

  Map<String, dynamic> toJson() => {
    'aAdi': aAdi,
    'aId': aId,
    'aSfiyat': aSfiyat,
    'toplamFiyat': toplamFiyat,
    'genelTopFiy': genelTopFiy,
    'urunMiktar': urunMiktar,
    'aciklama': aciklama,
    'porsiyon': porsiyon,
    "a_acik": aAcik,
    "a_birim": aBirim,
    "ekstraOzelik": List<dynamic>.from(ekstraOzelik.map((x) => x.toJson())),
  };

  @override
  String toString() {
    return 'Urun {aAdi: $aAdi, aId: $aId, aSfiyat: $aSfiyat, toplamFiyat: $toplamFiyat, urunMiktar: $urunMiktar, aciklama: $aciklama, porsiyon : $porsiyon,a_acik: $aAcik,a_birim: $aBirim,}';
  }
}

class EkstraDetay {
  EkstraDetay({
    this.id,
    this.adi,
    this.fiyat,
    this.miktar, // ekstra miktar
    this.mik,// eklenen ekstra adedi
    this.mikCrp,// sıfır ise ana stoğun adedine bağımlı değil 1 ise bagımlı
    this.stokId,
    this.ozstokId,
    this.secilen,
  });

  int id;
  String adi;
  double fiyat;
  double miktar; //ekstra miktar
  double mik; // eklenecek toplam miktar
  int mikCrp;
  int stokId;
  int ozstokId;
  bool secilen;//FilterChip tarfından seçildiğinde bu değer true veya false olur(degerin seçilip seçilmediğini anlmak için)

  factory EkstraDetay.fromJson(Map<String, dynamic> json) => EkstraDetay(
    id: json["id"],
    adi: json["adi"],
    fiyat: json["fiyat"],
    miktar: json["miktar"],
    mik: json["mik"],
    mikCrp: json["mikCrp"],
    stokId: json["stokId"],
    ozstokId: json["ozstokId"],
    secilen: false,
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "adi": adi,
    "fiyat": fiyat,
    "miktar": miktar,
    "mik": mik,
    "mikCrp": mikCrp,
    "stokId": stokId,
    "ozstokId": ozstokId,
  };
}