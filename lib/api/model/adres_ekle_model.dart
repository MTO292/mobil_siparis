// To parse this JSON data, do
//
//     final adresSec = adresSecFromJson(jsonString);

import 'dart:convert';

List<AdresSec> adresSecFromJson(String str) => List<AdresSec>.from(json.decode(str).map((x) => AdresSec.fromJson(x)));

String adresSecToJson(List<AdresSec> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class AdresSec {
  AdresSec({
    this.cariBasariDonus,
    this.adres,
    this.id,
  });

  String cariBasariDonus;
  String adres;
  int id;

  factory AdresSec.fromJson(Map<String, dynamic> json) => AdresSec(
    cariBasariDonus: json["cari_basari_donus"],
    adres: json["Adres"],
    id: json["id"],
  );

  Map<String, dynamic> toJson() => {
    "cari_basari_donus": cariBasariDonus,
    "Adres": adres,
    "id": id,
  };
}
