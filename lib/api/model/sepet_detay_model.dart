/*BU MODEL, EN SON SAYFA OLARAK SİPARİŞ DETAYLARINI GÖRÜNTÜLEYEN
JSON VERİSİNİN MODEL YAPISINI TEMSİL EDİYOR*/
import 'dart:convert';

List<SiparisDetayModel> siparisDetayFromJson(String str) => List<SiparisDetayModel>.from(
    json.decode(str).map((x) => SiparisDetayModel.fromJson(x)));

String siparisDetayToJson(List<SiparisDetayModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class SiparisDetayModel {
  String urunId;
  String urunAdi;
  String urunAdet;
  String birimFiyat;
  String toplamTutar;
  String urunNot;
  String aNot;
  String odemeTur;
  String adres;
  String birim;
  String refSira;//ekstra ürün sırası
  String refId;// ekstra ürünün baglı olduğu stok
  List<SiparisDetayModel> ekstra;

  SiparisDetayModel({
    this.urunId,
    this.urunAdi,
    this.urunAdet,
    this.birimFiyat,
    this.toplamTutar,
    this.urunNot,
    this.aNot,
    this.odemeTur,
    this.adres,
    this.refId,
    this.refSira,
    this.ekstra,
    this.birim,
  });

  factory SiparisDetayModel.fromJson(Map<String, dynamic> json) => SiparisDetayModel(
    urunId: json["urun_id"],
    urunAdi: json["urun_adi"],
    urunAdet: json["urun_adet"],
    birimFiyat: json["birim_fiyat"],
    toplamTutar: json["toplam_tutar"],
    urunNot: json["urun_not"],
    aNot: json["a_not"],
    odemeTur: json["odeme_tur"],
    adres: json["adres"],
    refSira: json["ref_sira"],
    refId: json["ref_id"],
    birim: json["birim"],
    ekstra: [],
  );

  Map<String, dynamic> toJson() => {
    "urun_id": urunId,
    "urun_adi": urunAdi,
    "urun_adet": urunAdet,
    "birim": birim,
    "birim_fiyat": birimFiyat,
    "toplam_tutar": toplamTutar,
    "urun_not": urunNot,
    "a_not": aNot,
    "odeme_tur": odemeTur,
    "adres": adres,
    "ref_sira": refSira,
    "ref_id": refId,
  };
}



