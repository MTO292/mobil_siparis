/*Bu model, tamamlanan veya hazırlanma aşamasında olan ürünlerin gösterilmesi için oluşturulmuştur*/
import 'dart:convert';

List<SiparisDurum> siparisDurumFromJson(String str) => List<SiparisDurum>.from(json.decode(str).map((x) => SiparisDurum.fromJson(x)));

String siparisDurumToJson(List<SiparisDurum> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class SiparisDurum {
    String adet;
    String aDurum;

    SiparisDurum({
        this.adet,
        this.aDurum,
    });

    factory SiparisDurum.fromJson(Map<String, dynamic> json) => SiparisDurum(
        adet: json["adet"],
        aDurum: json["a_durum"],
    );

    Map<String, dynamic> toJson() => {
        "adet": adet,
        "a_durum": aDurum,
    };
}


