import 'dart:convert';

List<StokOzelik> stokOzelikFromJson(String str) => List<StokOzelik>.from(json.decode(str).map((x) => StokOzelik.fromJson(x)));

String stokOzelikToJson(List<StokOzelik> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class StokOzelik {
  StokOzelik({
    this.cariBasariDonus,
    this.kategoriAdi,
    this.kategoriId,
    this.kategoriTur,
    this.ekstraOzelik,
  });

  String cariBasariDonus;
  String kategoriAdi;
  int kategoriId;
  int kategoriTur;
  var ekstraOzelik;

  factory StokOzelik.fromJson(Map<String, dynamic> json) => StokOzelik(
    cariBasariDonus: json["cari_basari_donus"],
    kategoriAdi: json["kategoriAdi"],
    kategoriId: json["kategoriId"],
    kategoriTur: json["kategoriTur"],
    ekstraOzelik: json["ekstraOzelik"] != null ?  List<EkstraOzelik>.from(json["ekstraOzelik"].map((x) => EkstraOzelik.fromJson(x))): null,
  );

  Map<String, dynamic> toJson() => {
    "cari_basari_donus": cariBasariDonus,
    "kategoriAdi": kategoriAdi,
    "kategoriId": kategoriId,
    "kategoriTur": kategoriTur,
    "ekstraOzelik": List<dynamic>.from(ekstraOzelik.map((x) => x.toJson())),
  };
}

class EkstraOzelik {
  EkstraOzelik({
    this.id,
    this.adi,
    this.fiyat,
    this.miktar,
    this.mik,
    this.mikCrp,
    this.stokId,
    this.ozstokId,
    this.secilen,
  });

  int id;
  String adi;
  double fiyat;
  double miktar;//ekstra miktar
  double mik;// eklenecek toplam miktar
  int mikCrp;
  int stokId;
  int ozstokId;
  bool secilen;//FilterChip tarfından seçildiğinde bu değer true veya false olur(degerin seçilip seçilmediğini anlmak için)

  factory EkstraOzelik.fromJson(Map<String, dynamic> json) => EkstraOzelik(
    id: json["id"],
    adi: json["adi"],
    fiyat: json["fiyat"],
    miktar: json["miktar"],
    mik: json["mik"],
    stokId: json["stokId"],
    ozstokId: json["ozstokId"],
    mikCrp: json["mikCrp"],
    secilen: false,
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "adi": adi,
    "fiyat": fiyat,
    "miktar": miktar,
    "mik": mik,
    "mikCrp": mikCrp,
    "stokId": stokId,
    "ozstokId": ozstokId,
  };
}
