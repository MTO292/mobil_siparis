import 'dart:convert';

VersiyonKontrol versiyonKontrolFromJson(String str) => VersiyonKontrol.fromJson(json.decode(str));

String versiyonKontrolToJson(VersiyonKontrol data) => json.encode(data.toJson());

class VersiyonKontrol {
  VersiyonKontrol({
    this.cariBasariDonus,
    this.versiyon,
  });

  String cariBasariDonus;
  String versiyon;

  factory VersiyonKontrol.fromJson(Map<String, dynamic> json) => VersiyonKontrol(
    cariBasariDonus: json["cari_basari_donus"],
    versiyon: json["versiyon"],
  );

  Map<String, dynamic> toJson() => {
    "cari_basari_donus": cariBasariDonus,
    "versiyon": versiyon,
  };
}
