class ProfilCariModel {
  String _isim;
  String _mail;
  String _telefonNo;
  String _kod;
  int _Id;

  String get telefonNo => _telefonNo;

  String get isim => _isim;

  String get mail => _mail;

  int get cariId => _Id;

  String get cariKod => _kod;

  set telefonNo(String value) {
    _telefonNo = value;
  }

  set mail(String value) {
    _mail = value;
  }

  set isim(String value) {
    _isim = value;
  }

  set cariId(int value) {
    _Id = value;
  }

  set cariKod(String value) {
    _kod = value;
  }
}
