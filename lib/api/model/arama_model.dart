import 'dart:convert';
import 'package:flutter/cupertino.dart';

AramaModel aramaFromJson(String str) => AramaModel.fromJson(json.decode(str));

String aramaToJson(AramaModel data) => json.encode(data.toJson());

class AramaModel with ChangeNotifier {
  AramaModel({
    this.cariBasariDonus,
    this.stok,
  });

  String cariBasariDonus;
  List<Stok> stok;

  factory AramaModel.fromJson(Map<String, dynamic> json) => AramaModel(
    cariBasariDonus: json["cari_basari_donus"],
    stok: List<Stok>.from(json["Stok"].map((x) => Stok.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "cari_basari_donus": cariBasariDonus,
    "Stok": List<dynamic>.from(stok.map((x) => x.toJson())),
  };
}

class Stok {
  Stok({
    this.aId,
    this.aAdi,
    this.aSfiyat,
    this.aAcik,
    this.aBirim,
  });

  String aId;
  String aAdi;
  String aSfiyat;
  String aAcik;
  String aBirim;

  factory Stok.fromJson(Map<String, dynamic> json) => Stok(
    aId: json["a_id"],
    aAdi: json["a_adi"],
    aSfiyat: json["a_sfiyat"],
    aAcik: json["a_acik"],
    aBirim: json["a_birim"],
  );

  Map<String, dynamic> toJson() => {
    "a_id": aId,
    "a_adi": aAdi,
    "a_sfiyat": aSfiyat,
    "a_acik": aAcik,
    "a_birim": aBirim,
  };
}
