import 'dart:convert';

SubeBilgiModel subeBilgiFromJson(String str) => SubeBilgiModel.fromJson(json.decode(str));

String subeBilgiToJson(SubeBilgiModel data) => json.encode(data.toJson());

class SubeBilgiModel {
  SubeBilgiModel({
    this.cariBasariDonus,
    this.telefon,
  });

  String cariBasariDonus;
  String telefon;

  factory SubeBilgiModel.fromJson(Map<String, dynamic> json) => SubeBilgiModel(
    cariBasariDonus: json["cari_basari_donus"],
    telefon: json["telefon"],
  );

  Map<String, dynamic> toJson() => {
    "cari_basari_donus": cariBasariDonus,
    "telefon": telefon,
  };
}
