import 'dart:convert';

SipLimitFiyat sipLimitFiyatFromJson(String str) => SipLimitFiyat.fromJson(json.decode(str));

String sipLimitFiyatToJson(SipLimitFiyat data) => json.encode(data.toJson());


class SipLimitFiyat {
  SipLimitFiyat({
    this.cariBasariDonus,
    this.durum,
    this.kalan,
  });

  String cariBasariDonus;
  String durum;
  String kalan;

  factory SipLimitFiyat.fromJson(Map<String, dynamic> json) => SipLimitFiyat(
    cariBasariDonus: json["cari_basari_donus"],
    durum: json["durum"],
    kalan: json["kalan"],
  );

  Map<String, dynamic> toJson() => {
    "cari_basari_donus": cariBasariDonus,
    "durum": durum,
    "kalan": kalan,
  };
}
