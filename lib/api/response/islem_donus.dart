/*Bu model, web servis ile iletişim içinde olan metodların geri dönüş değerlerine göre 
işlem yapılabilmesi için oluşturulmuştur.*/
import 'dart:convert';
import '../../api/model/sube_bilgi_model.dart';

import '../model/adres_model.dart';
import '../model/calisma_saat_model.dart';
import '../model/odeme_tur_model.dart';
import '../model/sepet_detay_model.dart';
import '../model/sepet_liste_model.dart';
import '../model/siparis_durum_model.dart';
import '../model/siparis_limit_fiy.dart';
import '../model/stok_ozelik_model.dart';
import '../model/versiyon_model.dart';
import '../model/menu_model.dart';
import 'package:http/http.dart' as http;
import '../model/adres_ekle_model.dart';
import '../model/arama_model.dart';
import '../model/kod_model.dart';
import '../model/uye_model.dart';
import '../../global_degisken/global_degisken.dart' as global;

/*Bu sınıfı veritabanına ekleme, güncelleme işlemleri yaparken geridönüş değerini kontrol edebilmek için oluşturdum.*/
IslemDonus islemDonusFromJson(String str) =>
    IslemDonus.fromJson(json.decode(str));

String islemDonusToJson(IslemDonus data) => json.encode(data.toJson());

class IslemDonus {
  String cariBasariDonus;

  IslemDonus({
    this.cariBasariDonus,
  });

  factory IslemDonus.fromJson(Map<String, dynamic> json) => IslemDonus(
        cariBasariDonus: json["cari_basari_donus"],
      );

  Map<String, dynamic> toJson() => {
        "cari_basari_donus": cariBasariDonus,
      };

  /*Üye giriş yaparken bilgileri getiren metod*/
  Future<Uye> verileriGetir(String telefonNo) async {
    var gelenDeger;
    Map<String, String> headers = {"Content-type": "application/json"};
    String json =
        '{"key": "${global.key}","versiyon": "${global.AppInfo.versiyon}",	"islem_kodu": "001",	"cari_telefon": "$telefonNo"}';

    try {
      final response = await http.post(global.apiUrl + global.uyeGiris,
          headers: headers, body: json);

      if (response.statusCode == 200) {
        gelenDeger = uyeFromJson(response.body.toString());
      } else {
        print(response.statusCode.toString());
      }
    } catch (e) {
      return Uye();
    }
    return gelenDeger;
  }

  /*Veritabanına üye güncellerken kullanıyoruz*/
  Future<Uye> uyeGuncelle(
      String kKod, String ad, String mail, String telefon) async {
    var gelenDeger;
    Map<String, String> headers = {"Content-type": "application/json"};
    String json =
        '{"islem_kodu": "004", "key": "${global.key}","versiyon": "${global.AppInfo.versiyon}","cari_kod" : "$kKod" ,"cari_adi": "$ad","cari_mail": "$mail", "cari_telefon": "$telefon"}';
    try {
      final response = await http.post(global.apiUrl + global.uyeGuncelle,
          body: json, headers: headers);

      if (response.statusCode == 200) {
        gelenDeger = uyeFromJson(response.body.toString());
      } else {
        return null;
      }
    } catch (e) {
      return Uye();
    }
    return gelenDeger;
  }

  Future<AramaModel> aramaYap(String adi) async {
    AramaModel list;
    Map<String, String> headers = {"Content-type": "application/json"};
    String json =
        '{"key": "${global.key}","versiyon": "${global.AppInfo.versiyon}", "adi": "$adi"}';
    try {
      final response = await http.post(global.apiUrl + global.aramaListe,
          headers: headers, body: json);
      if (response.statusCode == 200) {
        list = aramaFromJson(response.body.toString());
      } else {
        print(response.statusCode.toString());
      }
    } catch (e) {
      return AramaModel();
    }
    return list;
  }

  /*Veritabanına üye eklerken kullanıyoruz*/
  Future<Uye> uyeEkle(
      body, String ad, String mail, String sifre, String telefon) async {
    var gelenDeger;
    Map<String, String> headers = {"Content-type": "application/json"};
    String json =
        '{"key": "${global.key}","versiyon": "${global.AppInfo.versiyon}",	"islem_kodu": "003","cari_adi": "$ad","cari_mail": "$mail","cari_sifre": "$sifre","cari_telefon": "$telefon"}';
    try {
      final response = await http.post(global.apiUrl + global.uyeKayit,
          body: json, headers: headers);

      if (response.statusCode == 200) {
        gelenDeger = uyeFromJson(response.body.toString());
      } else {
        print(response.statusCode.toString());
      }
    } catch (e) {
      return Uye();
    }
    return gelenDeger;
  }

  /*GSM Kod gönderirken kullanıyoruz*/
  Future<Kod> kodGonder(String telefon) async {
    var gelenDeger;
    Map<String, String> headers = {"Content-type": "application/json"};
    String json =
        '{"key": "${global.key}",	"versiyon": "${global.AppInfo.versiyon}",	"telefon_no": "$telefon"}';
    try {
      final response = await http.post(global.apiUrl + global.kodGonder,
          body: json, headers: headers);

      if (response.statusCode == 200) {
        gelenDeger = kodFromJson(response.body.toString());
        print(response.statusCode.toString());
      } else {
        print(response.statusCode.toString());
      }
    } catch (e) {
      return Kod();
    }
    return gelenDeger;
  }



  /*Anamenü sayfası açıldığında gelen bütün verileri parse edip class yapısına aktarıyoruz*/
  Future<Menu> anaMenuListeyiAl() async {
    Menu menuList;
    Map<String, String> headers = {"Content-type": "application/json"};
    String json =
        '{"key": "${global.key}","versiyon": "${global.AppInfo.versiyon}", "islem_kodu": "001"}';

    try {
      final response = await http.post(global.apiUrl + global.anaMenuListe,
          headers: headers, body: json);
      if (response.statusCode == 200) {
        menuList = menuFromJson(response.body.toString());
      } else {
        print(response.statusCode.toString());
      }
    } catch (e) {
      return Menu();
    }
    return menuList;
  }

  /*Slider aksiyonları stok getir*/
  Future<Grupurunler> stokGetir({String stok_id}) async {
    Grupurunler menuList;
    Map<String, String> headers = {"Content-type": "application/json"};
    String json =
        '{"key": "${global.key}","versiyon": "${global.AppInfo.versiyon}", "islem_kodu": "002", "a_id": $stok_id}';

    try {
      final response = await http.post(global.apiUrl + global.anaMenuListe,
          headers: headers, body: json);
      if (response.statusCode == 200) {
        menuList = grupurunlerFromJson(response.body.toString());
      } else {
        print(response.statusCode.toString());
      }
    } catch (e) {
      return Grupurunler();
    }
    return menuList;
  }

  /*Sube Bilgilerini Getirmek içi kulanılmakta*/
  Future<SubeBilgiModel> subeBilgiGetir() async {
    SubeBilgiModel subeBilgi;
    Map<String, String> headers = {"Content-type": "application/json"};
    String json =
        '{"key": "${global.key}","versiyon": "${global.AppInfo.versiyon}"}';

    try {
      final response = await http.post(global.apiUrl + global.subeBilgi,
          headers: headers, body: json);
      if (response.statusCode == 200) {
        subeBilgi = subeBilgiFromJson(response.body.toString());
      } else {
        print(response.statusCode.toString());
        subeBilgi = SubeBilgiModel();
      }
    } catch (e) {
      return SubeBilgiModel();
    }
    return subeBilgi;
  }

  /*Slider aksiyonları grup getir*/
  Future<Anagrup> grupGetir({String stok_id}) async {
    Anagrup menuList;
    Map<String, String> headers = {"Content-type": "application/json"};
    String json =
        '{"key": "${global.key}","versiyon": "${global.AppInfo.versiyon}", "islem_kodu": "003", "a_id": $stok_id}';

    try {
      final response = await http.post(global.apiUrl + global.anaMenuListe,
          headers: headers, body: json);
      if (response.statusCode == 200) {
        menuList = AnagrupFromJson(response.body.toString());
      } else {
        print(response.statusCode.toString());
      }
    } catch (e) {
      return Anagrup();
    }
    return menuList;
  }





  /*Adres Ekleme ekranında adresi getirmeyi sağlayan metod*/
  // 1 => ülke, 2 => il, 3 => ilçe, 4 => mahalle
  Future<List<AdresSec>> adresSec(
      {String islemKodu,
      int ulkeId,
      int ilId ,
      int ilceId,
      int cariId,
      int semtId,
      String detay,
      String adresAd,
      int adresSira}) async {
    var deger;
    Map<String, String> headers = {"Content-type": "application/json"};
    String json =
        '{"key": "${global.key}","versiyon": "${global.AppInfo.versiyon}", "islem_kodu": $islemKodu,"ulke_id": ${ulkeId ?? 0},"il_id": ${ilId ?? 0},"ilce_id": ${ilceId ?? 0},"cari_id": ${cariId ?? 0},"semt_id": ${semtId ?? 0},"detay": "${detay ?? ""}","adres_sira": ${adresSira ?? 0},"adres_ad": "${adresAd ?? ""}"}';
    try {
      final response = await http.post(global.apiUrl + global.adresSec,
          body: json, headers: headers);
      if (response.statusCode == 200) {
        deger = adresSecFromJson(response.body.toString());
      } else {
        print(response.statusCode.toString());
      }
      return deger;
    } catch (e) {
      return List<AdresSec>();
    }
  }

  /*Adres ekranında adresi getirmeyi sağlayan metod*/
  Future<List<Adress>> adresGetir(
      {int cariId,}) async {
    var deger;
    Map<String, String> headers = {"Content-type": "application/json"};
    String json =
        '{"key": "${global.key}","versiyon": "${global.AppInfo.versiyon}", "islem_kodu": "7","cari_id": ${cariId ?? 0}}';
    try {
      final response = await http.post(global.apiUrl + global.adresSec,
          body: json, headers: headers);
      if (response.statusCode == 200) {
        deger = adressFromJson(response.body.toString());
      } else {
        print(response.statusCode.toString());
      }
      return deger;
    } catch (e) {
      return List<Adress>();
    }
  }

  Future<VersiyonKontrol> versiyonKontrols() async {
    Map<String, String> headers = {"Content-type": "application/json"};
    String json = '{"key": "${global.key}","versiyon": "${global.AppInfo.versiyon}"}';

    try {
      final response = await http.post(global.apiUrl + global.versiyonKontrol,
          body: json, headers: headers);
      if (response.statusCode == 200) {
        versiyonKontrolFromJson(response.body.toString());
      } else {
        print(response.statusCode.toString());
      }
      return versiyonKontrolFromJson(response.body.toString());
    } catch (e) {
      return VersiyonKontrol();
    }
  }


  /*Siparişi tamamlama metodu-adisyon-adisyonmas tablosuna kayıt ekler*/
  Future<IslemDonus> sprsTamamla(
      body, var sepet, var cId, String spnot, var odeTur, var adresID) async {
    var gelenDeger;
    Map<String, String> headers = {"Content-type": "application/json"};
    String json =
        '{"key" : "${global.key}","versiyon": "${global.AppInfo.versiyon}", "islem_kodu" : "003","urunlerim" : $sepet,"cari_id": $cId,"spnot" : "$spnot","odeTur" : "$odeTur","adresID" : "$adresID"}';
    //dostum sen bu gelen bodyle bişey yapmıyorsun ben anlamadım bunu yani göstereyim hocam
    try {
      final response = await http.post(global.apiUrl + global.siparisEkle,
          body: json, headers: headers);

      if (response.statusCode == 200) {
        gelenDeger = islemDonusFromJson(response.body.toString());
      } else {
        return IslemDonus();
      }
    } catch (e) {}
    return gelenDeger;
  }
  /*Anamenü sayfası açıldığında gelen bütün verileri parse edip class yapısına aktarıyoruz*/
  Future<List<StokOzelik>> stokOzelik({String stokId}) async {
    List<StokOzelik> list;
    Map<String, String> headers = {"Content-type": "application/json"};
    String json =
        '{"key": "${global.key}","versiyon": "${global.AppInfo.versiyon}", "islem_kodu": "001", "stok_id":"$stokId"}';

    try {
      final response = await http.post(global.apiUrl + global.stokDetay,
          headers: headers, body: json);
      if (response.statusCode == 200) {
        list = stokOzelikFromJson(response.body.toString());
      } else {
        print(response.statusCode.toString());
      }
    } catch (e) {
      return list;
    }
    return list;
  }

  /*Siparişi tamamlama metodu-adisyon-adisyonmas tablosuna kayıt ekler*/
  Future<SipLimitFiyat> sipLimitFiyKontrol(topTutar) async {
    var gelenDeger;
    Map<String, String> headers = {"Content-type": "application/json"};
    String json =
        '{"key" : "${global.key}","versiyon": "${global.AppInfo.versiyon}","toptut" : "$topTutar"}';
    try {
      final response = await http.post(global.apiUrl + global.sipLimitFiyKontrol,
          body: json, headers: headers);
      if (response.statusCode == 200) {
        gelenDeger = sipLimitFiyatFromJson(response.body.toString());
      } else {
        return SipLimitFiyat();
      }
    } catch (e) {}
    return gelenDeger;
  }
  /*Dropdown list'e ödeme türlerini çekebilmek için yazılan metod*/
  Future<List<OdemeTur>> odemeTur() async {
    List<OdemeTur> odeTur = [];
    Map<String, String> headers = {"Content-type": "application/json"};
    String json =
        '{"key": "${global.key}","versiyon": "${global.AppInfo.versiyon}", "islem_kodu": "001"}';

    try {
      final response = await http.post(global.apiUrl + global.odemeTur,
          headers: headers, body: json);
      if (response.statusCode == 200) {
        odeTur = odemeTurFromJson(response.body.toString());
      } else {
        print("Bağlanamadık");
      }
    } catch (e) {
      return List<OdemeTur>();
    }
    return odeTur;
  }

  /*Tamamlanan,hazırlanan siparişleri ekranda göstermek için yazılan metod*/
  Future<List<SiparisDurum>> siparisIlkSayfa(int cId) async {
    List<SiparisDurum> liste = [];
    Map<String, String> headers = {"Content-type": "application/json"};
    String json =
        '{"key": "${global.key}","versiyon": "${global.AppInfo.versiyon}", "islem_kodu": "001","cari_id" : $cId}';
    try {
      final response = await http.post(global.apiUrl + global.siparisIlkSayfa,
          body: json, headers: headers);
      if (response.statusCode == 200) {
        liste = siparisDurumFromJson(response.body.toString());
      } else {
        print(response.statusCode.toString());
      }
    } catch (e) {
      List<SiparisDurum>();
    }
    return liste;
  }

  /*Durum koduna göre kullanıcının verdiği siparişleri tek bir sipariş bütünü olarak döndürür*/
  Future<List<SepetListe>> siparisListe(int cId, String durumKod) async {
    List<SepetListe> spListe = [];
    Map<String, String> headers = {"Content-type": "application/json"};
    String json =
        '{"key": "${global.key}","versiyon": "${global.AppInfo.versiyon}", "islem_kodu": "001","cari_id" : $cId,"durum_kodu" : "$durumKod"}';
    try {
      final response = await http.post(global.apiUrl + global.siparisListe,
          body: json, headers: headers);
      if (response.statusCode == 200) {
        spListe = sepetListeFromJson(response.body.toString());
      } else {
        print(response.statusCode.toString());
      }
    } catch (e) {
      List<SepetListe>();
    }
    return spListe;
  }

  /*Kullanıcının verdiği siparişin detay sayfasını görüntülemek için yazıldı*/
  Future<List<SiparisDetayModel>> siparisDetayGetir(
      var cId, String aId, var adresId,var durum) async {
    List<SiparisDetayModel> spDetay = [];
    Map<String, String> headers = {"Content-type": "application/json"};
    String json =
        '{"key": "${global.key}","versiyon": "${global.AppInfo.versiyon}", "islem_kodu": "001","cari_id" : $cId,"a_master_id" : "$aId","adres_id" : $adresId,"durum": $durum}';
    try {
      final response = await http.post(global.apiUrl + global.siparisDetay,
          body: json, headers: headers);
      if (response.statusCode == 200) {
        spDetay = siparisDetayFromJson(response.body.toString());
      } else {
        print(response.statusCode.toString());
      }
    } catch (e) {
      List<SiparisDurum>();
    }
    return spDetay;
  }

  /*Siparişlerin listelendiği sayfadaki iptal butonu altında çalışan metod*/
  Future<IslemDonus> siparisIptal(var cId, var masterId) async {
    var gelenDeger;
    Map<String, String> headers = {"Content-type": "application/json"};
    String json =
        '{"key" : "${global.key}","versiyon": "${global.AppInfo.versiyon}","islem_kodu" : "005","cari_id": $cId,"a_master_id" : $masterId}';
    try {
      final response = await http.post(global.apiUrl + global.siparisIptal,
          body: json, headers: headers);
      if (response.statusCode == 200) {
        gelenDeger = islemDonusFromJson(response.body.toString());
      } else {
        print(response.statusCode.toString());
      }
    } catch (e) {
      return IslemDonus();
    }
    return gelenDeger;
  }

  Future<List<Saat>> calismaSaatleri() async {
    List<Saat> cSaatler = [];
    Map<String, String> headers = {"Content-type": "application/json"};
    String json =
        '{"key": "${global.key}","versiyon": "${global.AppInfo.versiyon}", "islem_kodu": "001"}';
    try {
      final response = await http.post(global.apiUrl + global.calismaSaatleri,
          body: json, headers: headers);
      if (response.statusCode == 200) {
        cSaatler = saatFromJson(response.body.toString());
      } else {
        print(response.statusCode.toString());
      }
    } catch (e) {
      return List<Saat>();
    }
    return cSaatler;
  }
}
