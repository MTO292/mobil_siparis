import 'dart:async';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import '../web_view/web_view_goster.dart';
import '../anamenu/ana_menu.dart';
import '../global_degisken/get_it.dart';
import '../api/model/kod_model.dart';
import '../api/model/uye_model.dart';
import '../global_degisken/global_degisken.dart';
import '../locator.dart';
import '../responsive_class/size_config.dart';
import '../yardimci_fonksiyonlar/form_mesaj_dondur/mesaj_dondur.dart';
import '../yardimci_fonksiyonlar/form_regex_fonksiyonlar/regex_kontrol.dart';
import '../yardimci_widgetlar/appbar_widgets/appbar_text.dart';
import '../yardimci_widgetlar/form_label_widgets/text_form_field.dart';
import '../global_degisken/global_degisken.dart' as global;
import '../yardimci_widgetlar/label_text/label_widget.dart';
import 'package:connectivity/connectivity.dart';
import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';
import '../api/response/islem_donus.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';

class StepperGiris extends StatefulWidget {
  @override
  _StepperState createState() => _StepperState();
}

class _StepperState extends State<StepperGiris> {
  //region  Değişkenler
  String kKosulLink ="${global.resimUrl}${global.key}/kullanim_kosullari/kullanimkosullari.html";

  String telUlkeKod;
  int aktifAdim = 0;

  TextEditingController _telController;
  TextEditingController _kodController;
  TextEditingController _adController;

  //pin kod icin olusturuldu hata verdiğinde shake animasyonu yapsın
  StreamController<ErrorAnimationType> _errorAnimationController;

  final FocusNode _telefonFocus = FocusNode();
  final FocusNode _adFocus = FocusNode();

  String girilenPinKodu = "";
  String adSoyad = "";

  //tel no icin olusturuldu hata validator kontrolu
  Color tfEnableRenk = Colors.black;
  Color tfFocusRenk = Colors.black;

  Color baslikColor = GlobalDegiskenler.butonArkaPlanRengi;

  //Kod için oluşturuldu
  Timer _timer;
  int _counter = 120;

  String telefonNo;

  //ekran ilk yüklendiğinde textfield hata ayıklama calişmasın
  var kodAutovalidate = false;
  var adAutovalidate = false;

  /*Classlardan nesne oluşturup, tanımlanan fonksiyonlara erişim sağlayabilmek için bunları tanımladık*/
  IslemDonus islemDonus;

  ProgressDialog pr;

  GlobalKey<FormState> pinFormKey = GlobalKey<FormState>();
  GlobalKey<FormState> adFormKey = GlobalKey<FormState>();

  bool pinKodHata = false;

  Kod basarili;

  StreamSubscription<Kod> pinKodGonder;

  //endregion

  @override
  void setState(fn) {
    // TODO: implement setState
    super.setState(fn);
    if (aktifAdim == 0) {
      _startTimer();
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    if (aktifAdim != 0) {}
    _timer.cancel();
    _errorAnimationController.close();

    super.dispose();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _telController = TextEditingController();
    _adController = TextEditingController();
    _telController.text = "";
    _adController.text = "";

    //ProgressDialog paketini kulandik
    pr = ProgressDialog(context);
  }

  @override
  Widget build(BuildContext context) {
    /*Sayfa yüklendiğinde değişkenlerin atamasını yapıyoruz*/
    islemDonus = IslemDonus();
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        elevation: 3,
        backgroundColor: global.GlobalDegiskenler.appBarColor,
        centerTitle: true,
        title: BaslikText(
          label: 'Giriş',
        ),
      ),
      floatingActionButton: bottomSheetButonu(),
      body: _tumAdimlar(),
    );
  }

  //region Widgetlar

  Widget _tumAdimlar() {
    return Padding(
      padding: EdgeInsets.fromLTRB(
          SizeConfig.screenWidth * 0.05,
          SizeConfig.screenWidth * 0.05,
          SizeConfig.screenWidth * 0.05,
          SizeConfig.screenWidth * 0.05),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          aktifAdim == 0
              ? adimBir()
              : aktifAdim == 1
                  ? adimIki()
                  : adimUc(),
        ],
      ),
    );
  }

  Widget bottomSheetButonu() {
    return IntrinsicHeight(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          aktifAdim == 1
              ? GestureDetector(
                  onTap: () {
                    setState(() {
                      _kodController.clear();
                    });
                  },
                  child: Container(
                      padding: EdgeInsets.symmetric(
                        horizontal: MediaQuery.of(context).size.height * 0.02,
                      ),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        color: global.GlobalDegiskenler.butonArkaPlanRengi,
                        borderRadius: BorderRadius.circular(
                          15.0,
                        ),
                      ),
                      child: Icon(
                        Icons.delete_forever,
                        color: Colors.white,
                        size: SizeConfig.safeBlockHorizontal * 6,
                      )),
                )
              : SizedBox(),
          SizedBox(
            width: 8,
          ),
          FlatButton(
            splashColor: GlobalDegiskenler.splashColor,
            child: Padding(
              padding: EdgeInsets.symmetric(
                vertical: SizeConfig.safeBlockVertical * 1.6,
              ),
              child: Row(
                children: <Widget>[
                  LabelCard(
                    fontFamily: GlobalDegiskenler.tlSimgesi,
                    color: Colors.white,
                    label: 'İleri',
                    // aktifAdim == 0 ?   "Kod Gönder" :aktifAdim == 1 ? "Doğrula" : "Tamamla",
                    fontSize: SizeConfig.safeBlockHorizontal * 5,
                    satirSayisi: 2,
                    textAlign: TextAlign.center,
                  ),
                  Icon(
                    Icons.navigate_next,
                    color: Colors.white,
                  )
                ],
              ),
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(
                8.0,
              ),
            ),
            color: GlobalDegiskenler.butonArkaPlanRengi,
            onPressed: () {
              onStepContinue();
            },
          ),
        ],
      ),
    );
  }

  Widget ulkeKod() {
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.only(left: 8, right: 8),
      decoration: BoxDecoration(
        border: Border.all(
            color: Colors.black,
            width: MediaQuery.of(context).size.width * (0.001)),
        borderRadius: BorderRadius.all(
            Radius.circular(MediaQuery.of(context).size.width * (0.02))),
      ),
      margin: EdgeInsets.only(
        right: SizeConfig.safeBlockHorizontal * 1.5,
      ),
      child:
          CountryCodePicker(
        //  flagWidth: SizeConfig.safeBlockHorizontal * 8,
        textStyle: TextStyle(
          fontSize: SizeConfig.safeBlockHorizontal * 4.5,
          fontFamily: GlobalDegiskenler.genelFontStyle,
        ),

        emptySearchBuilder: (BuildContext context) {
          return Center(
              child: Text(
            "Uygun ülke bulunamadı",
            style: TextStyle(
              fontSize: SizeConfig.safeBlockHorizontal * 4,
              fontFamily: GlobalDegiskenler.genelFontStyle,
            ),
          ));
        },
        onChanged: print,
        flagWidth: (MediaQuery.of(context).size.width) * 0.0675,
        initialSelection: 'TR',
        favorite: ['AZ', 'TR'],
        hideSearch: false,
        searchDecoration: InputDecoration(
          labelStyle: TextStyle(
              color: Colors.black,
              fontFamily: GlobalDegiskenler.tlSimgesi,
              fontSize: SizeConfig.safeBlockHorizontal * 4.5),
        ),
        showFlag: true,
        // countryFilter: ['IQ', 'FR'],
        showFlagDialog: false,
        comparator: (a, b) => b.name.compareTo(a.name),
        //Get the country information relevant to the initial selection
        onInit: (code) {
          telUlkeKod = code.dialCode.substring(1);
          print("on init ${code.name} ${code.dialCode}");
        },
      ),
    );
  }

  //endregion

  //region Fonksiyonlar

  Future<void> onStepContinue() async {
    if (aktifAdim == 0) {
      try {
        var result = await Connectivity().checkConnectivity();
        if (result == ConnectivityResult.none) {
          MesajGoster.mesajGoster(
              context, "Cihazınızı internete bağlayınız.", SoruTipi.Hata);
        } else {
          butonKodGonderIslemleri();
        }
      } catch (e) {}
    } else if (aktifAdim == 1) {
      if (basarili == null) {
        _errorAnimationController.add(ErrorAnimationType.shake);
        kodAutovalidate = true;
        setState(() {});
      } else {
        if (pinFormKey.currentState.validate()) {
          if (girilenPinKodu == basarili.kod) {
            setState(() {
              aktifAdim = 2;
            });
          } else {
            _errorAnimationController.add(ErrorAnimationType.shake);
            setState(() {
              kodAutovalidate = true;
            });
          }
        } else {
          _errorAnimationController.add(ErrorAnimationType.shake);
          setState(() {
            kodAutovalidate = true;
          });
        }
      }
    } else {
      //pin koduna autodispose verdiğimiz için her geciş sayfasında kendi kapatıyor ve her severinde yeniden başlatıoruz
      _kodController = TextEditingController();
      try {
        var result = await Connectivity().checkConnectivity();
        if (result == ConnectivityResult.none) {
          MesajGoster.mesajGoster(
              context, "Cihazınızı internete bağlayınız.", SoruTipi.Hata);
        } else {
          butonKayitIslemleri();
        }
      } catch (e) {}
    }
  }

  Future girisFalse() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setBool("girisYapildi", false);
  }

  Future<void> progressGoster(String mesaj) {
    //ProgressDialog paketine style verdik
    pr.style(
      backgroundColor: GlobalDegiskenler.progressBarColor,
      progress: 4.0,
      message: mesaj,
      progressTextStyle: TextStyle(color: Colors.black),
      borderRadius: 15.0,
      elevation: 6.0,
      insetAnimCurve: Curves.easeIn,
      messageTextStyle: TextStyle(
        fontFamily: GlobalDegiskenler.tlSimgesi,
        fontSize: SizeConfig.safeBlockHorizontal * 5,
        color: Colors.black,
      ),
    );
  }

  butonKodGonderIslemleri() async {
    //pin koduna autodispose verdiğimiz için her geciş sayfasında kendi kapatıyor ve her severinde yeniden başlatıyoruz
    _kodController = TextEditingController();
    _errorAnimationController = StreamController<ErrorAnimationType>();
    _telefonFocus.unfocus();
    String telRegex = RegexKontrol.telefonKontrol(_telController.text);
    if (telRegex != null) {
      tfEnableRenk = Colors.red;
      tfFocusRenk = Colors.red;

      MesajGoster.mesajGoster(context, telRegex, SoruTipi.Uyari);
    } else {
      setState(() {
        _counter = 180;
        _startTimer();
        aktifAdim = 1;
      });
      telefonNo = telUlkeKod + _telController.text.trim();
      pinKodGonder = islemDonus.kodGonder(telefonNo).asStream().listen((deger) {
        basarili = deger;
        if (basarili.durum == numarator.basarili) {
          _adController.text = basarili.adSoyad;
        } else if (basarili.durum == numarator.hata) {
          MesajGoster.mesajGoster(context,
                  "Bir hata oluştu. Lütfen tekrar deneyiniz.", SoruTipi.Hata)
              .whenComplete(() {
            setState(() {
              aktifAdim = 0;
            });
          });
        } else if (basarili.durum == numarator.uygulamaVersiyonuEski) {
          logoEkraninaGit(context);
        } else if (basarili.durum == numarator.apiVersiyonuEski) {
          logoEkraninaGit(context);
        } else if (basarili.durum == numarator.hizmetDisi) {
          logoEkraninaGit(context);
        } else if (basarili.durum == numarator.smsHakki) {
          MesajGoster.mesajGoster(context, basarili.mesaj, SoruTipi.Hata)
              .whenComplete(() {
            setState(() {
              aktifAdim = 0;
            });
          });
        } else if (basarili.durum == null) {
          setState(() {
            MesajGoster.mesajGoster(
                    context,
                    "Sistemdeki bilgilere ulaşırken bir hata oluştu. Lütfen tekrar deneyiniz.",
                    SoruTipi.Hata)
                .whenComplete(() {
              setState(() {
                aktifAdim = 0;
              });
            });
          });
        } else {
          setState(() {
            MesajGoster.mesajGoster(
                    context,
                    "Sistemdeki bilgilere ulaşırken bir hata oluştu. Lütfen tekrar deneyiniz.",
                    SoruTipi.Hata)
                .whenComplete(() {
              setState(() {
                aktifAdim = 0;
              });
            });
          });
        }
      });
    }
  }

  butonKayitIslemleri() async {
    adSoyad = _adController.text.trim();
    _adController.text = _adController.text.trim();
    if (adFormKey.currentState.validate()) {
      progressGoster("Lütfen bekleyiniz.");
      await pr.show();
      final body = {
        // cari mail kapatıldı kulamıcı mail girmedende üye olabilir
        "key": global.key,
        "islem_kodu": "003",
        "cari_adi": _adController.text,
        "cari_mail": "",
        "cari_telefon": telUlkeKod + _telController.text.trim()
      };
      islemDonus.uyeEkle(body, adSoyad, "", "", telefonNo).then((basarili) {
        if (basarili.cariBasariDonus == "1" ||
            basarili.cariBasariDonus == "2") {
          Future.delayed(Duration(milliseconds: 2000), () {
            Future<Uye> uye = islemDonus.verileriGetir(telefonNo);

            /*Kayıt başarılı ise giriş yapmak için kontrol ediyoruz.*/
            uye.then((gelecekDeger) async {
              if (gelecekDeger.cariBasariDonus == "2") {
                SharedPreferences prefs = await SharedPreferences.getInstance();

                prefs.setString('cariMailDonus', gelecekDeger.cariMailDonus);
                prefs.setString(
                    'cariTelefonDonus', gelecekDeger.cariTelefonDonus);
                prefs.setString('cariKodDonus', gelecekDeger.cariKodDonus);
                prefs.setInt('cariIdDonus', gelecekDeger.cariIdDonus);
                prefs.setString('cariSifreDonus', gelecekDeger.cariSifreDonus);
                prefs.setString("cariAdDonus", gelecekDeger.cariAdDonus);
                /*Shared ile giriş yapıldı verisinin hafızaya kaydedilme anı*/
                prefs.setBool("girisYapildi", true);
                    locator<Get>().loggedin = true;
                pr.hide();
                Navigator.of(context).pushAndRemoveUntil(
                    MaterialPageRoute(
                        builder: (context) => AnaMenu()),
                    (e) => false);
              } else if (gelecekDeger.cariBasariDonus == "4") {
                pr.hide();
                await girisFalse();

                MesajGoster.mesajGoster(context,
                        "Kullanıcı bilgileri yanlış girildi.", SoruTipi.Hata)
                    .whenComplete(() {
                  pr.hide();
                });
              } else if (gelecekDeger.cariBasariDonus == "3") {
                pr.hide();
                await girisFalse();

                MesajGoster.mesajGoster(
                        context,
                        "Sistemden kaynaklı bir hata oluştu. Lütfen tekrar deneyiniz.",
                        SoruTipi.Hata)
                    .whenComplete(() {
                  pr.hide();
                });
              } else if (basarili.cariBasariDonus ==
                  numarator.uygulamaVersiyonuEski) {
                pr.hide();
                logoEkraninaGit(context);
              } else if (basarili.cariBasariDonus ==
                  numarator.apiVersiyonuEski) {
                pr.hide();
                logoEkraninaGit(context);
              } else if (basarili.cariBasariDonus == numarator.hizmetDisi) {
                pr.hide();
                logoEkraninaGit(context);
              } else if (gelecekDeger.cariBasariDonus == null) {
                pr.hide();
                MesajGoster.mesajGoster(
                        context,
                        "Bilgiler gönderilirken bir hata oluştu. Lütfen tekrar deneyiniz.",
                        SoruTipi.Hata)
                    .whenComplete(() {
                  pr.hide();
                });
              }
            });
          });
        } else if (basarili.cariBasariDonus ==
            numarator.uygulamaVersiyonuEski) {
          pr.hide();
          logoEkraninaGit(context);
        } else if (basarili.cariBasariDonus == numarator.apiVersiyonuEski) {
          pr.hide();
          logoEkraninaGit(context);
        } else if (basarili.cariBasariDonus == numarator.hizmetDisi) {
          pr.hide();
          logoEkraninaGit(context);
        } else if (basarili.cariBasariDonus == "0") {
          pr.hide();

          MesajGoster.mesajGoster(
                  context,
                  "Kayıt olurken bir hata oluştu. Lütfen tekrar deneyiniz.",
                  SoruTipi.Hata)
              .whenComplete(() {
            pr.hide();
          });
        } else if (basarili.cariBasariDonus == "3") {
          pr.hide();

          MesajGoster.mesajGoster(
                  context,
                  "Sistemden kaynaklı bir hata oluştu. Lütfen tekrar deneyiniz.",
                  SoruTipi.Hata)
              .whenComplete(() {
            pr.hide();
          });
        } else if (basarili.cariBasariDonus == null) {
          pr.hide();

          MesajGoster.mesajGoster(
                  context,
                  "Sistemdeki bilgilere ulaşırken bir hata oluştu. Lütfen tekrar deneyiniz.",
                  SoruTipi.Hata)
              .whenComplete(() {
            pr.hide();
          });
        }
      });
    } else {
      setState(() {
        adAutovalidate = true;
      });
    }
  }

  void _startTimer() {
    if (_timer != null) {
      _timer.cancel();
    }
    _timer = Timer.periodic(Duration(seconds: 1), (timer) {
      setState(() {
        if (_counter > 0) {
          _counter--;
        } else {
          _timer.cancel();
        }
      });
    });
  }

  sizedBox() {
    return SizedBox(height: MediaQuery.of(context).size.height * (0.035));
  }


  Widget adimBir() {
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(left: 4),
          child: LabelCard(
            textAlign: TextAlign.center,
            satirSayisi: 1,
            fontSize: SizeConfig.safeBlockHorizontal * 4.5,
            color: baslikColor,
            fontFamily: GlobalDegiskenler.genelFontStyle,
            fontWeight: FontWeight.bold,
            label: "Telefon Numarası",
          ),
        ),
        SizedBox(
          height: 8,
        ),
        LabelCard(
          textAlign: TextAlign.center,
          satirSayisi: 2,
          //fontSize: SizeConfig.safeBlockHorizontal * 4.5,
          color: Colors.black,
          fontFamily: GlobalDegiskenler.genelFontStyle,
          fontWeight: FontWeight.bold,
          label:
          "Telefon numaranızı doğrulamak için size SMS mesajı gönderilecek.",
        ),
        SizedBox(
          height: 8,
        ),
        IntrinsicHeight(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              ulkeKod(),
              Flexible(
                child: MyTextFormField(
                  label: '(5XX)',
                  labelText: 'Telefon',
                  enabledColor: tfEnableRenk,
                  focusedColor: tfFocusRenk,
                  focusNode: _telefonFocus,
                  onFieldSubmitted: (bitti) {
                    _telefonFocus.unfocus();
                    onStepContinue();
                  },
                  ileriTusu: TextInputAction.done,
                  satirSayisi: 1,
                  textInputType: TextInputType.phone,
                  onsavedGelenDeger: (gelenDeger) =>
                  _telController.text,
                  kontroller: _telController,
                  textStyle: TextStyle(
                      fontSize: MediaQuery.of(context).size.width *
                          (0.04)),
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          height: 14,
        ),
        kKText()
      ],
    );
  }

  Widget adimIki() {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: 4),
                child: LabelCard(
                  textAlign: TextAlign.center,
                  satirSayisi: 1,
                  fontSize:
                  SizeConfig.safeBlockHorizontal * 4.5,
                  color: baslikColor,
                  fontFamily: GlobalDegiskenler.genelFontStyle,
                  fontWeight: FontWeight.bold,
                  label: "Aktivasyon kodu",
                ),
              ),
              SizedBox(
                height: 8,
              ),
              LabelCard(
                textAlign: TextAlign.center,
                satirSayisi: 2,
                //fontSize: SizeConfig.safeBlockHorizontal * 4.5,
                color: Colors.black,
                fontFamily: GlobalDegiskenler.genelFontStyle,
                fontWeight: FontWeight.bold,
                label:
                "$telefonNo numarasına gönderilen doğrulama kodunu giriniz.",
              ),
              SizedBox(
                height: 8,
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                  horizontal:
                  MediaQuery.of(context).size.width * 0.12,
                  vertical:
                  MediaQuery.of(context).size.height * 0.01,
                ),
                child: Form(
                  key: pinFormKey,
                  child: PinCodeTextField(
                    length: 4,
                    autoValidate: kodAutovalidate,
                    obsecureText: false,
                    textInputType: TextInputType.number,
                    animationType: AnimationType.slide,
                    validator: (
                        String value,
                        ) {
                      if (girilenPinKodu.length >= 0 &&
                          girilenPinKodu.length < 4) {
                        pinKodHata = true;
                        return "4 Karakter kodunuzu giriniz";
                      } else if (basarili == null) {
                        pinKodHata = true;
                        return "Girdiğiniz kod yanlış";
                      } else if (girilenPinKodu !=
                          basarili.kod) {
                        pinKodHata = true;
                        return "Girdiğiniz kod yanlış";
                      }
                      return null;
                    },
                    pinTheme: PinTheme(
                      shape: PinCodeFieldShape.box,
                      borderRadius: BorderRadius.circular(8),
                      fieldHeight:
                      MediaQuery.of(context).size.height *
                          0.07,
                      fieldWidth:
                      MediaQuery.of(context).size.width *
                          0.11,
                      borderWidth:
                      MediaQuery.of(context).size.width *
                          (0.003),
                      activeFillColor: Colors.transparent,
                      activeColor: pinKodHata
                          ? Colors.red
                          : Colors.black,
                      disabledColor:
                      Color.fromRGBO(46, 77, 80, 1),
                      inactiveFillColor: Colors.transparent,
                      inactiveColor: pinKodHata
                          ? Colors.red
                          : Colors.black,
                      selectedColor: Colors.black,
                      selectedFillColor: GlobalDegiskenler
                          .butonArkaPlanRengi
                          .withOpacity(0.2),
                    ),
                    backgroundColor: Colors.transparent,
                    animationDuration:
                    Duration(milliseconds: 300),
                    enableActiveFill: true,
                    autoDisposeControllers: true,
                    errorAnimationController:
                    _errorAnimationController,
                    controller: _kodController,
                    onCompleted: (v) {
                      print("Completed");
                    },
                    onChanged: (value) {
                      print(value);
                      if (value.length == 4) {
                        girilenPinKodu = value;
                        onStepContinue();
                      } else {
                        setState(() {
                          girilenPinKodu = value;
                        });
                      }
                    },
                    beforeTextPaste: (text) {
                      print("Allowing to paste $text");
                      return true;
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            aktifAdim == 1
                ? Column(
              children: <Widget>[
                SizedBox(
                  height: 8,
                ),
                Row(
                  mainAxisAlignment:
                  MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.message,
                          color: Colors.grey,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                              left: 16),
                          child: GestureDetector(
                            child: LabelCard(
                              textAlign: TextAlign.center,
                              satirSayisi: 1,
                              fontSize: SizeConfig
                                  .safeBlockHorizontal *
                                  4,
                              fontFamily:
                              GlobalDegiskenler
                                  .genelFontStyle,
                              fontWeight: FontWeight.bold,
                              label: "Tekrar SMS gönder",
                              color: _counter == 0
                                  ? Colors.black
                                  : Colors.grey,
                            ),
                            onTap: () async {
                              if (_counter == 0) {
                                try {
                                  var result =
                                  await Connectivity()
                                      .checkConnectivity();
                                  if (result ==
                                      ConnectivityResult
                                          .none) {
                                    MesajGoster.mesajGoster(
                                        context,
                                        "Cihazınızı internete bağlayınız.",
                                        SoruTipi.Hata);
                                  } else {
                                    basarili = new Kod();
                                    pinKodGonder.cancel();
                                    kodAutovalidate =
                                    false;
                                    butonKodGonderIslemleri();
                                  }
                                } catch (e) {}
                              }
                            },
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 10),
                      child: Text(
                        " $_counter ",
                        style: TextStyle(
                          fontSize: SizeConfig
                              .safeBlockHorizontal *
                              4,
                          fontFamily: GlobalDegiskenler
                              .genelFontStyle,
                        ),
                      ),
                    ),
                  ],
                ),
                Divider(
                  color: Colors.grey.withOpacity(0.4),
                  thickness: 0.5,
                  height: 8,
                ),
                Row(
                  mainAxisAlignment:
                  MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.phone,
                          color: Colors.grey,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                              left: 16),
                          child: GestureDetector(
                            child: LabelCard(
                                textAlign:
                                TextAlign.center,
                                satirSayisi: 1,
                                fontSize: SizeConfig
                                    .safeBlockHorizontal *
                                    4,
                                fontFamily:
                                GlobalDegiskenler
                                    .genelFontStyle,
                                fontWeight:
                                FontWeight.bold,
                                label:
                                "Numarayı değiştir",
                                color: Colors.black),
                            onTap: () async {
                              basarili = new Kod();
                              pinKodGonder.cancel();
                              setState(() {
                                aktifAdim = 0;
                                pinKodHata = false;
                                kodAutovalidate = false;
                                _telController.text = "";
                                _kodController.text = "";
                              });
                            },
                          ),
                        ),
                      ],
                    ),
                  ],
                )
              ],
            )
                : sizedBox()
          ],
        )
      ],
    );
  }

  Widget adimUc() {
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(left: 4),
          child: LabelCard(
            textAlign: TextAlign.center,
            satirSayisi: 1,
            fontSize: SizeConfig.safeBlockHorizontal * 4.5,
            color: baslikColor,
            fontFamily: GlobalDegiskenler.genelFontStyle,
            fontWeight: FontWeight.bold,
            label: "Profil bilgisi",
          ),
        ),
        SizedBox(
          height: 8,
        ),
        LabelCard(
          textAlign: TextAlign.center,
          satirSayisi: 2,
          //fontSize: SizeConfig.safeBlockHorizontal * 4.5,
          color: Colors.black,
          fontFamily: GlobalDegiskenler.genelFontStyle,
          fontWeight: FontWeight.bold,
          label: "Lütfen isminizi belirleyiniz",
        ),
        SizedBox(
          height: 8,
        ),
        Padding(
          padding: EdgeInsets.only(bottom: 4),
          child: Form(
            key: adFormKey,
            autovalidate: adAutovalidate,
            child: MyTextFormField(
              kontroller: _adController,
              labelText: 'Ad Soyad',
              label: "Adınızı Soyadınızı giriniz",
              karakterSayisi: 50,
              validatorFonksiyon: RegexKontrol.isimKontrol,
              prefixIcon: global.formIcon(!Platform.isAndroid
                  ? CupertinoIcons.person_solid
                  : Icons.person),
              ileriTusu: TextInputAction.done,
              focusNode: _adFocus,
              onFieldSubmitted: (bitti) {
                _adFocus.unfocus();
                onStepContinue();
              },
              satirSayisi: 1,
              textStyle: TextStyle(
                fontSize:
                MediaQuery.of(context).size.width * (0.04),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget kKText() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        RichText(
            maxLines: 2,
            textAlign: TextAlign.left,
            text: TextSpan(
              text: '* Üye olmakla ',
              style: TextStyle(
                fontSize: SizeConfig.safeBlockHorizontal * 3,
                fontWeight: FontWeight.bold,
                color: Colors.black,
              ),
              children: [
                TextSpan(
                  text: 'Kullanım Koşullarını ',
                  style: TextStyle(
                    fontSize:
                    SizeConfig.safeBlockHorizontal * 3,
                    fontWeight: FontWeight.bold,
                    color:
                    GlobalDegiskenler.butonArkaPlanRengi,
                  ),
                  recognizer: TapGestureRecognizer()
                    ..onTap =
                        () async {
                          global.Navigate().navigatePushNoAnimation(
                            context,
                            WebViewGoster(appBarBaslik: 'Kullanım Koşulları', url: kKosulLink)
                          );
                    },),
                TextSpan(
                  text: 'onaylamış oluyorsunuz.',
                  style: TextStyle(
                    fontSize:
                    SizeConfig.safeBlockHorizontal * 3,
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                  ),
                ),
              ],
            )),
      ],
    );
  }
//endregion
}
