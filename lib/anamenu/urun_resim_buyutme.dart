import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';


///Ürün resmine tıklandığında resmi büyümek için yazıldı
class UrunResimBuyutme extends StatelessWidget {
  const UrunResimBuyutme({
    this.imageProvider,
    this.loadingBuilder,
    this.backgroundDecoration,
    this.minScale,
    this.maxScale,
    this.initialScale,
    this.basePosition = Alignment.center,
    this.filterQuality = FilterQuality.none,
    this.disableGestures,
  });

  final ImageProvider imageProvider;
  final LoadingBuilder loadingBuilder;
  final Decoration backgroundDecoration;
  final dynamic minScale;
  final dynamic maxScale;
  final dynamic initialScale;
  final Alignment basePosition;
  final FilterQuality filterQuality;
  final bool disableGestures;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Positioned(
            child: Container(
              color: Colors.black,
              constraints: BoxConstraints.expand(
                height: MediaQuery.of(context).size.height,
              ),
              child: PhotoView(
                imageProvider: imageProvider,
                loadingBuilder: loadingBuilder,
                backgroundDecoration: backgroundDecoration,
                minScale: minScale,
                maxScale: maxScale,
                initialScale: initialScale,
                basePosition: basePosition,
                filterQuality: filterQuality,
                disableGestures: disableGestures,
              ),
            ),
          ),
          Container(
            color: Colors.transparent,
            height: 65,
            width: 60,
            alignment: Alignment.bottomRight,
            child: IconButton(
                color: Colors.white,
                icon: Icon(
                  Icons.clear,
                  size: 24,
                  color: Colors.white,
                ),
                onPressed: () {
                  Navigator.of(context).pop(true);
                }),
          ),
        ],
      ),
    );
  }
}
