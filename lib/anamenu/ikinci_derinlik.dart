import '../yardimci_widgetlar/manu_kartlar/urun_cart.dart';
import 'package:connectivity_wrapper/connectivity_wrapper.dart';
import 'package:flutter/material.dart';
import '../anamenu/urun_profil.dart';
import '../bottom_menu/my_custom_bottom.dart';
import '../global_degisken/global_degisken.dart';
import '../api/model/menu_model.dart';
import '../responsive_class/size_config.dart';
import '../yardimci_widgetlar/appbar_widgets/appbar_text.dart';
import '../yardimci_fonksiyonlar/form_mesaj_dondur/net_kontrol.dart' as net;
import '../global_degisken/global_degisken.dart' as global;

class IkinciDerinlik extends StatefulWidget {
  final List<Grupurunler> grupUrunler;

  const IkinciDerinlik({Key key, @required this.grupUrunler})
      : super(key: key);

  @override
  _IkinciDerinlikState createState() => _IkinciDerinlikState();
}

class _IkinciDerinlikState extends State<IkinciDerinlik> {
  var liste;

  @override
  void initState() {
    super.initState();
    liste = widget.grupUrunler.length;
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      /*Sayfada en üst başlığı temsil eden kısım*/
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: GlobalDegiskenler.appBarColor,
        title: BaslikText(
          label: 'Ürünler',
        ),
      ),
      bottomNavigationBar:MyBottomNavBar(0),
      body: ConnectivityWidgetWrapper(
        offlineWidget: net.netKontrol(),
        disableInteraction: false,
        child: widget.grupUrunler.length == 0
            ? Padding(
          padding: const EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
          child: aciklama('Bilgi', 'Bu kategoriye kayıtlı ürün bulunmamaktadır.',
                  Icons.room_service),
            )
            : ListView(
              children: <Widget>[
                Padding(
                  padding:const EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
                  child: GridView.builder(
                    physics: ScrollBehavior().getScrollPhysics(context),
                    shrinkWrap: true,
                    itemCount: widget.grupUrunler.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      childAspectRatio: MediaQuery.of(context).size.width /
                          (MediaQuery.of(context).size.height / 1.4),
                    ),
                    itemBuilder: (context, index) {
                      /*Genel olarak bir menü başlığını temsil eden container*/
                      return Padding(
                        padding: const EdgeInsets.all(6.0),
                        child: UrunCart(
                          img:
                              "${global.resimUrl + global.key + "/s-" + widget.grupUrunler[index].aId + ".jpg"}",
                          imageSize: MediaQuery.of(context).size.height / 6,
                          name: widget.grupUrunler[index].aAdi,
                          fiyat: double.parse(
                                  widget.grupUrunler[index].aSfiyat)
                              .toStringAsFixed(2),
                          onTap: ()  {
                           global.Navigate()
                                .navigatePushNoAnimation(
                              context,
                              UrunProfil(
                                grupurunler: widget.grupUrunler[index],
                              ),
                            );
                          },
                        ),
                      );
                    },
                  ),
                ),
              ],
            ),
      ),
    );
  }
}
