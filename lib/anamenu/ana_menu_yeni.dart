/*
import '../anamenu/urun_profil.dart';
import '../giris_ekranlar/stepper_giris.dart';
import '../profil/sepet_ana_sayfa.dart';
import '../yardimci_widgetlar/manu_kartlar/urun_cart.dart';
import '../yardimci_widgetlar/manu_kartlar/grid_product_anamenu.dart';
import '../yardimci_widgetlar/manu_kartlar/slider_cart.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:connectivity/connectivity.dart';
import 'package:connectivity_wrapper/connectivity_wrapper.dart';
import '../yardimci_fonksiyonlar/sayfa_kadirma_efekti/scrool.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../api/model/calisma_saat_model.dart';
import '../api/model/islem_donus.dart';
import '../bottom_menu/my_custom_bottom.dart';
import '../global_degisken/global_degisken.dart';
import '../global_liste/menu_model.dart';
import '../responsive_class/size_config.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shimmer/shimmer.dart';
import '../yardimci_fonksiyonlar/form_mesaj_dondur/mesaj_dondur.dart';
import '../yardimci_widgetlar/label_text/label_widget.dart';
import 'package:line_awesome_icons/line_awesome_icons.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import '../yardimci_fonksiyonlar/form_mesaj_dondur/net_kontrol.dart'
as net;
import '../global_degisken/global_degisken.dart' as global;
import '../yardimci_fonksiyonlar/form_mesaj_dondur/bilgi_alert.dart'
as bilgi;
import 'package:store_redirect/store_redirect.dart';

import 'birinci_derinlik.dart';

class AnaMenuListe extends StatefulWidget {
  const AnaMenuListe({
    Key key,
  }) : super(key: key);

  @override
  _AnaMenuListeState createState() => _AnaMenuListeState();
}

class _AnaMenuListeState extends State<AnaMenuListe> {
  //region DEğişkenler
  MyBottomNavBar navbar = MyBottomNavBar();
  IslemDonus islemDonus;

  List<Anagrup> menu = List<Anagrup>();

  Future<List<Menu>> gelenMenuDegeri;

  bool giris = false;

  String cariAd = "";
  String adSoyadProfil = "";
  List<String> cariAdList;

  List<Saat> cSaatBilgi = List<Saat>();
  Future<List<Saat>> cSaatF;

  //endregion

  @override
  Future<void> initState() {
    super.initState();
    islemDonus = IslemDonus();
*/
/*    OneSignal.shared.init("32c44a1b-6f7c-4764-8409-acce2dd75cd9", iOSSettings: {
      OSiOSSettings.autoPrompt: false,
      OSiOSSettings.inAppLaunchUrl: true,
    });*//*

    OneSignal.shared
        .init("1184a935-530b-4ac6-b217-76f5c7d2902b"); //onesignal for ios

    OneSignal.shared
        .setInFocusDisplayType(OSNotificationDisplayType.notification);
    //  OneSignal.shared.init("32c44a1b-6f7c-4764-8409-acce2dd75cd9");//onesignal for android
    menuYukle();
    _sfYukle();
    //giriş yapıldımı diye bakııyor
  }

  @override
  void setState(fn) {
    // TODO: implement setState
    super.setState(fn);
    //status bar rengini degiştirdikten sonra tekrar yüklensin
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    //status bar rengini degiştirdik

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Image.asset(
          "assets/images/splash_logo.png",
          height: SizeConfig.safeBlockVertical * 4,
        ),
        automaticallyImplyLeading: false,
        backgroundColor: GlobalDegiskenler.appBarColor,
        //leading: GlobalDegiskenler.appCarIconResim,
        */
/*İşletme saat bilgilerini gösteren icon*//*

        leading: saatIcon(),
        actions: prfilIcon(),
      ),
      bottomNavigationBar: navbar.getMyBottomNavBar(context, 0),
      body: RefreshIndicator(
        child: ConnectivityWidgetWrapper(
          disableInteraction: false,
          offlineWidget: net.netKontrol(),
          child: ScrollConfiguration(
            behavior: MyBehavior(),
            child: FutureBuilder(
                future: gelenMenuDegeri,
                builder: (context, snapshot) {
                  */
/*Veriler yüklenirken bu ekran gösterilecek*//*

                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return shimmerGoster();
                    */
/*Veri varmı kontrol ediyoruz*//*

                  } else if (snapshot.connectionState == ConnectionState.done &&
                      !snapshot.hasError &&
                      snapshot.hasData) {
                    //menu = snapshot.data;
                    if (menu.length == 0) {
                      */
/*Menü eklenmemişse bu ekran gözükecek*//*

                      return tekrarYukle();
                      */
/*Menü varsa bu ekran gösterilecek*//*

                    } else if (menu[0].cariBasariDonus ==
                        numarator.uygulamaVersiyonuEski) {
                      return storeGit();
                    } else if (menu[0].cariBasariDonus ==
                        numarator.apiVersiyonuEski) {
                      return global.wsVerEski();
                    } else if (menu[0].cariBasariDonus == numarator.hizmetDisi) {
                      return hizmetDisi();
                    } else {
                      return listele();
                    }
                    */
/*Veri yoksa ve hata varsa burası gösterilecek*//*

                  } else if (snapshot.hasError) {
                    return global.snapshotHasError();
                  } else if (snapshot.data == null && snapshot.hasData == false)
                    return global.nullDegerDondur();
                }),
          ),
        ),
        onRefresh: () async {
          setState(() {
            menuYukle();
          });
          return null;
        },
      ),
    );
  }

  //region Metodlar
  List<T> map<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }

    return result;
  }

  void menuYukle() {
    gelenMenuDegeri = islemDonus.anaMenuListeyiAl();
    gelenMenuDegeri.then((gelenDeger) {
      menu = gelenDeger[0].anasayfa[0].anagrup;
    });
  }

  void saatleriGetir() {
    cSaatF = islemDonus.calismaSaatleri();
    cSaatF.then((gelenDeger) {
      cSaatBilgi = gelenDeger;
    });
  }

  Future _sfYukle() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    cariAd = (prefs.getString('cariAdDonus') ?? '');
    if (cariAd.isNotEmpty) {
      cariAdList = cariAd.split(" ");
      adSoyadProfil = cariAd.split(" ")[0].toUpperCase().substring(0, 1);
      if (cariAdList.length >= 2) {
        adSoyadProfil =
            adSoyadProfil + cariAd.split(" ")[1].toUpperCase().substring(0, 1);
      }
    }
    setState(() {});
  }

  //endregion

  //region Widgetler
  Widget imageCarousel(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
      child: CarouselSlider(
        height: MediaQuery.of(context).size.height / 3.2,
        items: map<Widget>(
          global.imgList,
              (index, i) {
            String img = imgList[index];
            return SliderItem(
              img: img,
              isFav: false,
            );
          },
        ).toList(),
        autoPlay: true,
//        enlargeCenterPage: true,
        viewportFraction: 1.0,
        //             aspectRatio: 2.0,
        */
/*   onPageChanged: (index) {
          setState(() {
            _current = index;
          });
        },*//*

      ),
    );
  }

  Widget listele() {
    return ListView(
      children: <Widget>[
        imageCarousel(context),
        Padding(
          padding: const EdgeInsets.only(left: 4.0, top: 24),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              LabelCard(
                textAlign: TextAlign.center,
                fontSize: SizeConfig.safeBlockHorizontal * 5,
                fontFamily: GlobalDegiskenler.genelFontStyle,
                fontWeight: FontWeight.bold,
                label: "Hizmetlerimiz",
              ),
            ],
          ),
        ),
        Divider(
          height: 8,
          color: Colors.black26,
        ),
        SizedBox(
          height: 8,
        ),
        ///Kategoriler Listesş
        Container(
          height: SizeConfig.screenHeight * 0.225,
          child: GridView.builder(
            scrollDirection: Axis.horizontal,
            primary: false,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 1,
              childAspectRatio: MediaQuery.of(context).size.height /
                  (MediaQuery.of(context).size.width * 1.6),
            ),
            itemCount: menu.length,
            itemBuilder: (BuildContext context, int index) {
              return Padding(
                padding: const EdgeInsets.only(top: 4,bottom: 4,left: 8),
                child: gridProductAnamenu(
                  img: //resimUrl + key + "/2.jpeg",
                  "${global.resimUrl + global.key + "/g-" + menu[index].anagrupId + ".jpg"}",
                  name: menu[index].anagrupBaslik,
                  onTop: () {
                    global.Navigate().navigatePushNoAnimation(
                      context,
                      AnamenuListeDetay(
                        menu: menu[index],
                      ),
                    );
                  },
                ),
              );
            },
          ),
        ),

        SizedBox(
          height: 40,
        ),
        ///Kategoriler başlığı ve içeriğiğ
        GridView.builder(
          scrollDirection: Axis.vertical,
          primary: false,
          shrinkWrap: true,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 1,
            childAspectRatio:   (MediaQuery.of(context).size.width  ) /
                (MediaQuery.of(context).size.height * 0.4),
          ),
          itemCount: menu.length,
          itemBuilder: (BuildContext context, int index) {
            return Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 8),
                  child: Align(
                    alignment:Alignment.topLeft,
                    child: LabelCard(
                      label: menu[index].anagrupBaslik,
                      textAlign: TextAlign.start,
                      fontFamily: GlobalDegiskenler.tlSimgesi,
                      fontSize: SizeConfig.safeBlockHorizontal * 4,
                    ),
                  ),
                ),
                SizedBox(
                  height: 4,
                ),
                ///Ürünler  Listesi
                Container(
                  height: SizeConfig.screenHeight * 0.35,
                  child: GridView.builder(
                    scrollDirection: Axis.horizontal,
                    primary: false,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 1,
                      childAspectRatio:  (MediaQuery.of(context).size.height) /
                          (MediaQuery.of(context).size.width * 1.2),
                    ),
                    itemCount: menu[index].grupurunler.length,
                    itemBuilder: (BuildContext context, int index1) {
                      return       Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: GridProduct(
                          img:
                          "${global.resimUrl + global.key + "/s-" + menu[index].grupurunler[index1].aId + ".jpg"}",
                          imageSize:MediaQuery.of(context).size.height / 7,
                          name: menu[index].grupurunler[index1].aAdi,
                          fiyat: double.parse(menu[index].grupurunler[index1].aSfiyat).toStringAsFixed(2),
                          onTap: () async{
                            var gelenDeger =await global.Navigate().navigatePushNoAnimation(
                                context,
                                UrunSepeteEkle(
                                    grupurunler:menu[index].grupurunler[index1]));
                            //sepete ürün eklenmiş ise bottom barda icon badge displaye yansızın diye tekrar build ediyoruz
                            if (gelenDeger != null && gelenDeger ){
                              setState(() {});
                            }
                          },
                        ),
                      );
                    },
                  ),
                ),
              ],
            );
          },
        ),
      ],
    );
  }

  Widget tekrarYukle() {
    return Container(
      margin: EdgeInsets.all(12.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Center(
            child: LabelCard(
              satirSayisi: 2,
              fontFamily: GlobalDegiskenler.tlSimgesi,
              textAlign: TextAlign.center,
              label: "Tekrar yüklemeyi deneyiniz.",
              fontSize: SizeConfig.safeBlockHorizontal * 6,
            ),
          ),
          SizedBox(
            height: 15.0,
          ),
          */
/*Menüyü tekrar yükle*//*

          ButtonBar(
            alignment: MainAxisAlignment.center,
            buttonHeight: SizeConfig.safeBlockVertical * 10,
            buttonMinWidth: SizeConfig.safeBlockHorizontal * 10,
            children: <Widget>[
              RaisedButton.icon(
                color: global.GlobalDegiskenler.butonArkaPlanRengi,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
                icon: Icon(Icons.refresh),
                label: LabelCard(
                  fontFamily: GlobalDegiskenler.tlSimgesi,
                  label: "Tekrar Yükle",
                  fontSize: SizeConfig.safeBlockHorizontal * 6,
                  color: Colors.white,
                ),
                onPressed: () async {
                  try {
                    var result = await Connectivity().checkConnectivity();
                    if (result == ConnectivityResult.none) {
                      MesajGoster.mesajGoster(context,
                          "Cihazınızı internete bağlayınız.", SoruTipi.Uyari);
                    } else {
                      setState(() {
                        menuYukle();
                      });
                    }
                  } catch (e) {}
                },
              ),
            ],
          ),
        ],
      ),
    );
  }



  //menü yüklerken yükleme ekren
  Widget shimmerGoster() {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: Shimmer.fromColors(
        baseColor: Colors.grey[500],
        highlightColor: Colors.grey[100],
        enabled: true,
        child: Padding(
          padding: EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
          child: ListView(
            physics: global.scrollpletform(),
            children: <Widget>[
              Stack(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(4),
                    height: MediaQuery.of(context).size.height / 3.21,
                    width: MediaQuery.of(context).size.width,
                    child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(4)),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.3),
                              spreadRadius: 0.5,
                              blurRadius: 2,
                              offset: Offset(0, 1),
                            )
                          ]),
                      height: MediaQuery.of(context).size.height / 3.2,
                      width: MediaQuery.of(context).size.width,
                      child: ClipRRect(
                          borderRadius: BorderRadius.circular(8.0),
                          child: SizedBox()),
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(left: 4.0, top: 24),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    LabelCard(
                      textAlign: TextAlign.center,
                      fontSize: SizeConfig.safeBlockHorizontal * 5,
                      fontFamily: GlobalDegiskenler.genelFontStyle,
                      fontWeight: FontWeight.bold,
                      label: "Hizmetlerimiz",
                    ),
                  ],
                ),
              ),
              Divider(
                height: 8,
                color: Colors.black26,
              ),
              SizedBox(
                height: 8,
              ),
              GridView.builder(
                shrinkWrap: true,
                primary: false,
                physics: NeverScrollableScrollPhysics(),
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 1,
                  childAspectRatio: 3,
                ),
                itemCount: 6,
                itemBuilder: (BuildContext context, int index) {
                  return Padding(
                    padding: const EdgeInsets.all(4),
                    child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.3),
                              spreadRadius: 0.5,
                              blurRadius: 2,
                              offset: Offset(0, 1),
                            )
                          ],
                        ),
                        child: SizedBox()),
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  List<Widget> prfilIcon() {
    return <Widget>[
      Tooltip(
        message: 'Profil',
        child: GestureDetector(
          child: Padding(
            padding:
            EdgeInsets.fromLTRB(0, 0, SizeConfig.screenWidth * 0.03, 0),
            child: cariAd.isEmpty
                ? Icon(
              Icons.account_circle,
              color: GlobalDegiskenler.appBarIconColor,
              size: SizeConfig.safeBlockHorizontal * 9.7,
            )
                : CircleAvatar(
              radius: SizeConfig.safeBlockHorizontal * 4.5,
              backgroundColor: GlobalDegiskenler.appBarIconColor,
              child: LabelCard(
                satirSayisi: 2,
                fontFamily: GlobalDegiskenler.tlSimgesi,
                textAlign: TextAlign.center,
                label: adSoyadProfil,
              ),
            ),
          ),
          onTap: () {
            cariAd.isEmpty
                ? Navigator.of(context, rootNavigator: true)
                .push(MaterialPageRoute(builder: (context) => stepper()))
                : Navigator.of(context, rootNavigator: true).push(
                MaterialPageRoute(
                    builder: (context) => ProfilTemelSayfa()));
          },
        ),
      )
    ];
  }

  Widget saatIcon() {
    return Tooltip(
      message: 'İşletme Hizmet Saatleri',
      child: GestureDetector(
        onTap: () {
          showDialog(
              context: context,
              barrierDismissible: true,
              builder: (context) {
                return AlertDialog(
                  backgroundColor: Colors.black.withOpacity(0.4),
                  title: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      LabelCard(
                        label: 'Hizmet Saatleri',
                        textAlign: TextAlign.center,
                        satirSayisi: 2,
                        color: Colors.white,
                      ),
                      Tooltip(
                        message: 'Menüyü Kapatır',
                        child: GestureDetector(
                            onTap: () {
                              Navigator.of(context).pop();
                            },
                            child: Icon(
                              Icons.exit_to_app,
                              size: SizeConfig.safeBlockHorizontal * 8,
                              color: Colors.white,
                            )),
                      )
                    ],
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                  content: FutureBuilder(
                    future: islemDonus.calismaSaatleri(),
                    builder: (context, snapshot) {
                      if (snapshot.connectionState == ConnectionState.waiting) {
                        return global.circularProgress();
                      } else if (snapshot.connectionState ==
                          ConnectionState.done &&
                          !snapshot.hasError &&
                          snapshot.hasData) {
                        cSaatBilgi = snapshot.data;
                        if (cSaatBilgi.length < 0) {
                          return global.kayitliVeriYok(
                              "Saat bilgisi henüz girilmedi.",
                              "Saatleri göster",
                              Icon(
                                Icons.alarm_off,
                                color: GlobalDegiskenler.appBarIconColor,
                              ),
                              "Veri yok");
                        } else {
                          return bilgi.bilgiAlert(context, widget, cSaatBilgi);
                        }
                      }
                    },
                  ),
                );
              });
        },
        child: Center(
          child: Icon(
            LineAwesomeIcons.clock_o,
            color: GlobalDegiskenler.appBarIconColor,
            size: SizeConfig.safeBlockHorizontal * 9,
          ),
        ),
      ),
    );
  }
//endregion

}
*/
