import 'package:beylik_kebap/yardimci_widgetlar/appbar_widgets/appbar_text.dart';

import '../anamenu/urun_profil.dart';
import '../giris/stepper_giris.dart';
import '../profil/adres_listele/adres_ana.dart';
import '../profil/profil/profil_ana.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:url_launcher/url_launcher.dart';
import '../yardimci_widgetlar/manu_kartlar/menu_cart.dart';
import '../yardimci_widgetlar/manu_kartlar/slider_cart.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:connectivity/connectivity.dart';
import 'package:connectivity_wrapper/connectivity_wrapper.dart';
import '../yardimci_fonksiyonlar/sayfa_kadirma_efekti/scrool.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../api/model/calisma_saat_model.dart';
import '../api/response/islem_donus.dart';
import '../bottom_menu/my_custom_bottom.dart';
import '../global_degisken/global_degisken.dart';
import '../api/model/menu_model.dart';
import '../responsive_class/size_config.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shimmer/shimmer.dart';
import '../yardimci_fonksiyonlar/form_mesaj_dondur/mesaj_dondur.dart';
import '../yardimci_widgetlar/label_text/label_widget.dart';
import 'package:line_awesome_icons/line_awesome_icons.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import '../yardimci_fonksiyonlar/form_mesaj_dondur/net_kontrol.dart' as net;
import '../global_degisken/global_degisken.dart' as global;
import '../yardimci_fonksiyonlar/form_mesaj_dondur/bilgi_alert.dart' as bilgi;
import 'birinci_derinlik.dart';

class AnaMenu extends StatefulWidget {
  const AnaMenu({
    Key key,
  }) : super(key: key);

  @override
  _AnaMenuState createState() => _AnaMenuState();
}

class _AnaMenuState extends State<AnaMenu> {
  //region DEğişkenler
  static Future<Menu> gelenMenuDegeri;

  ProgressDialog pr;
  IslemDonus islemDonus;

  List<Anagrup> menu = List<Anagrup>();

  bool giris = false;

  String cariAd = "";
  String adSoyadProfil = "";
  List<String> cariAdList;

  List<Saat> cSaatBilgi = List<Saat>();
  Future<List<Saat>> cSaatF;

  SharedPreferences prefs;

  List<String> imgUrlList = List<String>();

  List<Sliders> sliderList;


  //endregion

  @override
  Future<void> initState() {
    super.initState();
    islemDonus = IslemDonus();

/*    OneSignal.shared.init(
      "4c028847-4363-4c35-8f35-afd5f445b09a",
    );    */


    OneSignal.shared
        .init("8f937b0a-0121-4dd8-92dc-7531a25a7ac3"); //onesignal for ios

    OneSignal.shared
        .setInFocusDisplayType(OSNotificationDisplayType.notification);


    // Eger null ise yükle yoksa menu olduğu zaman olan menuyu korumak için yazıldı
    // ilk girişte menü yüklenecek tekrar ana ekrana geldiğinde bi önceki menu görüntülenecek
    menuYukle();
    _sfYukle();
    //giriş yapıldımı diye bakııyor
    debugPrint("init");
    OneSignal.shared
        .setNotificationOpenedHandler((OSNotificationOpenedResult result) {});
    //onesignal buton aksiyonu
    OneSignal.shared
        .setInAppMessageClickedHandler((OSInAppMessageAction action) {
      if (action.clickName == '1') {
        if (cariAd != '') {
          Navigate().navigatePushNoAnimation(
              context,
              AdresGetir(
                siparisModu: false,
              ));
        } else {
          Navigator.of(context, rootNavigator: true)
              .push(MaterialPageRoute(builder: (context) => StepperGiris()));
        }
      }
    });
    pr = ProgressDialog(context);
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    debugPrint("did");
  }

  @override
  void setState(fn) {
    // TODO: implement setState
    super.setState(fn);
    //status bar rengini degiştirdikten sonra tekrar yüklensin+
    debugPrint("setState");
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    //profil için oluşturuldu
    profilYukle();
    debugPrint("build");
    return Scaffold(
      key: PageStorageKey('ana_menu_key'),
      appBar: AppBar(
        centerTitle: true,
        title:  BaslikText(
          fontSize: 2,
          label: 'Mobil Sipariş',
        ),
        automaticallyImplyLeading: false,
        backgroundColor: GlobalDegiskenler.appBarColor,
        /*İşletme saat bilgilerini gösteren icon*/
        leading: saatIcon(),
        actions: prfilIcon(),
      ),
      bottomNavigationBar: MyBottomNavBar(0),
      body: RefreshIndicator(
        child: ConnectivityWidgetWrapper(
          offlineWidget: net.netKontrol(),
          child: ScrollConfiguration(
            behavior: ScrollBehavior(),
            child: GlowingOverscrollIndicator(axisDirection: AxisDirection.down,
              color: GlobalDegiskenler.butonArkaPlanRengi,
              child: FutureBuilder(
                  future: gelenMenuDegeri,
                  builder: (context, snapshot) {
                    /*Veriler yüklenirken bu ekran gösterilecek*/
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return shimmerGoster();
                      /*Veri varmı kontrol ediyoruz*/
                    } else
                    if (snapshot.connectionState == ConnectionState.done &&
                        !snapshot.hasError &&
                        snapshot.hasData) {
                      if (snapshot.data.cariBasariDonus ==
                          numarator.uygulamaVersiyonuEski) {
                        logoEkraninaGit(context);
                        return SizedBox();
                      } else if (snapshot.data.cariBasariDonus ==
                          numarator.apiVersiyonuEski) {
                        logoEkraninaGit(context);
                        return SizedBox();
                      } else if (snapshot.data.cariBasariDonus ==
                          numarator.hizmetDisi) {
                        logoEkraninaGit(context);
                        return SizedBox();
                      } else if (menu == null) {
                        return tekrarYukle();
                        /*Menü varsa bu ekran gösterilecek*/
                      }
                      if (menu.length == 0) {
                        /*Menü eklenmemişse bu ekran gözükecek*/
                        return tekrarYukle();
                        /*Menü varsa bu ekran gösterilecek*/
                      } else
                      if (menu[0].cariBasariDonus == numarator.basarili &&
                          snapshot.data.cariBasariDonus == numarator.basarili) {
                        return listele();
                      } else {
                        return tekrarYukle();
                      }
                      /*Veri yoksa ve hata varsa burası gösterilecek*/
                    } else if (snapshot.hasError) {
                      return tekrarYukle();
                    } else
                    if (snapshot.data == null && snapshot.hasData == false)
                      return tekrarYukle();
                    return null;
                  }),),
          ),
        ),
        onRefresh: () async {
          setState(() {
            menuTekrarYukle();
          });
          return null;
        },
        color: global.GlobalDegiskenler.butonArkaPlanRengi,
      ),
    );
  }

  //region Metodlar
  List<T> maps<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }

    return result;
  }

  //menuyu yüklemek için yazıldı
  Future<void> menuYukle() async {
    //eğer menüyüklenmiş ise ekran bidaha build edilir ise bidaha getirme
    if (gelenMenuDegeri == null)
      menuTekrarYukle();
    else
      gelenMenuDegeri.then((value) => menuAktar(value));
  }

  //menüyü tekrar yüklemek için kullanılmakta
  Future<void> menuTekrarYukle() async {
    gelenMenuDegeri = islemDonus.anaMenuListeyiAl();
    gelenMenuDegeri.then((gelenDeger) {
      menuAktar(gelenDeger);
    });
  }

  //gelen listey, ikiye ayır slider listesi ve menü
  void menuAktar(gelenDeger) {
    menu = gelenDeger.anagrup;
    sliderList = gelenDeger.slider;
    imgUrlList.clear();
    for (int i = 0; i < sliderList.length; i++) {
      if (sliderList[i].aSira != 0) {
        imgUrlList
            .add(resimUrl + key + "/SLIDER/slider${sliderList[i].aId}.jpg");
      }
    }
  }

  void saatleriGetir() {
    cSaatF = islemDonus.calismaSaatleri();
    cSaatF.then((gelenDeger) {
      cSaatBilgi = gelenDeger;
    });
  }

  Future _sfYukle() async {
    prefs = await SharedPreferences.getInstance();
    setState(() {});
  }

  //profil için oluşturuldu
  void profilYukle() {
    //prefs baslatılmışsa alıyoruz
    cariAd = prefs == null ? "" : (prefs.getString('cariAdDonus') ?? '');
    if (cariAd.isNotEmpty) {
      cariAdList = cariAd.split(" ");
      adSoyadProfil = cariAd.split(" ")[0].toUpperCase().substring(0, 1);
      if (cariAdList.length >= 2) {
        adSoyadProfil =
            adSoyadProfil + cariAd.split(" ")[1].toUpperCase().substring(0, 1);
      }
    }
  }

  Future<void> progressGoster(String mesaj) {
    //ProgressDialog paketine style verdik
    pr.style(
      backgroundColor: GlobalDegiskenler.progressBarColor,
      progress: 4.0,
      message: mesaj,
      progressTextStyle: TextStyle(color: Colors.black),
      borderRadius: 15.0,
      elevation: 6.0,
      insetAnimCurve: Curves.easeIn,
      messageTextStyle: TextStyle(
        fontFamily: GlobalDegiskenler.tlSimgesi,
        fontSize: SizeConfig.safeBlockHorizontal * 5,
        color: Colors.black,
      ),
    );
  }

  onTapSlider(int index) {
    return () async {
      if (sliderList[index].aAksiyontur == 1) {
        //URL'ye git
        var site = sliderList[index].aAksiyon;
        try {
          if (await canLaunch(site)) {
            await launch(site);
          } else {
            throw 'Hata ';
          }
        } catch (e) {}
      } else if (sliderList[index].aAksiyontur == 2 &&
          sliderList[index].aAksiyon != '0') {
        //Ürüne git
        progressGoster("Lütfen bekleyiniz.");
        await pr.show();
        try {
          islemDonus
              .stokGetir(stok_id: sliderList[index].aAksiyon)
              .then((gelenDeger) async {
            await pr.hide();
            global.Navigate().navigatePushNoAnimation(
                context, UrunProfil(grupurunler: gelenDeger));
          });
        } catch (e) {}
      } else if (sliderList[index].aAksiyontur == 3 &&
          sliderList[index].aAksiyon != '0') {
        //Menü'ye git
        progressGoster("Lütfen bekleyiniz.");
        await pr.show();
        islemDonus
            .grupGetir(stok_id: sliderList[index].aAksiyon)
            .then((gelenDeger) async {
          await pr.hide();
          if (gelenDeger.cariBasariDonus == global.numarator.basarili) {
            global.Navigate().navigatePushNoAnimation(
              context,
              BirinciDerinlik(
                menu: gelenDeger,
              ),
            );
          }
        });
      }
    };
  }

  //endregion

  //region Widgetler
  Widget imageCarousel(BuildContext context) {
    return imgUrlList.length != 0
        ? Stack(
      children: [
        carouselSlider(Axis.horizontal),
        Positioned(
            right: 10,
            top: 13,
            child: GestureDetector(
              child: CircleAvatar(
                backgroundColor: Colors.black54,
                child: Icon(
                  Icons.zoom_out_map,
                  color: Colors.white,
                  size: SizeConfig.safeBlockHorizontal * 4,
                ),
                radius: SizeConfig.safeBlockHorizontal * 4.0,
              ),
              onTap: () => bottomSheetGoster(context),
            ))
      ],
    )
        : Padding(
      padding: EdgeInsets.only(top: SizeConfig.screenHeight * 0.005),
      child: SliderCart(),
    );
  }

  bottomSheetGoster(BuildContext context) async {
    return showModalBottomSheet(
        backgroundColor: Colors.transparent,
        isScrollControlled: true,
        context: context,
        builder: (BuildContext context) {
          return Padding(
            padding: EdgeInsets.only(
                bottom: MediaQuery
                    .of(context)
                    .viewInsets
                    .bottom),
            child: StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
                return ScrollConfiguration(
                  behavior: MyBehavior(),
                  child: Column(
                    children: <Widget>[
                      Container(
                        height: SizeConfig.statusBarHeight,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              width: 40,
                              height: 4,
                              decoration: BoxDecoration(
                                color: Color.fromRGBO(213, 213, 215, 1),
                                borderRadius:
                                BorderRadius.all(Radius.circular(32)),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                        child: Container(
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(8),
                                topRight: Radius.circular(8),
                              )),
                          child: CupertinoScrollbar(
                            child: SingleChildScrollView(
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal:
                                    SizeConfig.safeBlockHorizontal * 4),
                                child: Column(
                                  children: [
                                    SizedBox(
                                      height:
                                      SizeConfig.safeBlockVertical * 1.5,
                                    ),
                                    Container(
                                      padding: EdgeInsets.symmetric(
                                          vertical:
                                          SizeConfig.safeBlockHorizontal *
                                              1),
                                      child: Align(
                                        alignment: Alignment.centerRight,
                                        child: Material(
                                            color: Colors.transparent,
                                            child: IconButton(
                                              icon: Icon(
                                                Icons.close,
                                                color: Colors.black,
                                                size: SizeConfig
                                                    .safeBlockHorizontal *
                                                    7,
                                              ),
                                              onPressed: () =>
                                                  Navigator.pop(context),)
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      height:
                                      SizeConfig.safeBlockVertical * 1.5,
                                    ),
                                    divider(height: 0),
                                    sizeBox(),
                                    ListView.builder(
                                        physics: NeverScrollableScrollPhysics(),
                                        shrinkWrap: true,
                                        itemCount: imgUrlList.length,
                                        itemBuilder: (context, index) {
                                          return Padding(
                                            padding: EdgeInsets.only(
                                                top: SizeConfig.screenHeight *
                                                    0.008),
                                            child: Column(
                                              children: [
                                                SizedBox(
                                                  height: SizeConfig
                                                      .safeBlockVertical *
                                                      4,
                                                ),
                                                Stack(
                                                  children: [
                                                    SliderCart(
                                                      img: imgUrlList[index],
                                                      onTap: onTapSlider(index),
                                                    ),
                                                    Positioned(
                                                        right: 10,
                                                        top: 13,
                                                        child: GestureDetector(
                                                          child: CircleAvatar(
                                                            backgroundColor:
                                                            Colors.white,
                                                            child: Icon(
                                                              Icons
                                                                  .fullscreen_exit,
                                                              color:
                                                              Colors.black,
                                                              size: SizeConfig
                                                                  .safeBlockHorizontal *
                                                                  6,
                                                            ),
                                                            radius: SizeConfig
                                                                .safeBlockHorizontal *
                                                                4.0,
                                                          ),
                                                          onTap: () =>
                                                              Navigator.pop(
                                                                  context),
                                                        ))
                                                  ],
                                                )
                                              ],
                                            ),
                                          );
                                        }),
                                    SizedBox(
                                      height: SizeConfig.safeBlockVertical * 1,
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                );
              },
            ),
          );
        });
  }

  Widget sizeBox() {
    return SizedBox(
      height: SizeConfig.safeBlockVertical * 0.5,
    );
  }

  Widget divider({double height}) {
    return Divider(
      thickness: 1,
      color: Color.fromRGBO(213, 213, 215, 1),
      height: height ?? null,
    );
  }

  Widget listele() {
    return Padding(
      padding: EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
      child: ListView(
        children: <Widget>[
          SizedBox(
            height: 2,
          ),
          imageCarousel(context),
          Padding(
            padding: EdgeInsets.only(
                left: 4.0, top: SizeConfig.screenHeight * 0.005),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                LabelCard(
                  textAlign: TextAlign.center,
                  fontSize: SizeConfig.safeBlockHorizontal * 5,
                  fontFamily: GlobalDegiskenler.genelFontStyle,
                  fontWeight: FontWeight.bold,
                  label: "Menüler",
                ),
              ],
            ),
          ),
          Divider(
            height: 8,
            color: Colors.black26,
          ),
          SizedBox(
            height: 2,
          ),

          ///Kategoriler Listesş
          GridView.builder(
            shrinkWrap: true,
            primary: false,
            physics: NeverScrollableScrollPhysics(),
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 1,
              childAspectRatio: 3,
            ),
            itemCount: menu.length,
            itemBuilder: (BuildContext context, int index) {
              return MenuCart(
                key: PageStorageKey('$index'),
                img:
                "${global.resimUrl + global.key + "/g-" +
                    menu[index].anagrupId + ".jpg"}",
                name: menu[index].anagrupBaslik,
                onTap: () {
                  global.Navigate().navigatePushNoAnimation(
                    context,
                    BirinciDerinlik(
                      menu: menu[index],
                    ),
                  );
                },
              );
            },
          ),
        ],
      ),
    );
  }

  Widget tekrarYukle() {
    return Container(
      padding: const EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          aciklama('Hata', 'Tekrar yüklemeyi deneyiniz.', Icons.error_outline),
          SizedBox(
            height: 15.0,
          ),
          /*Menüyü tekrar yükle*/
          Expanded(
            child: ButtonBar(
              alignment: MainAxisAlignment.center,
              children: <Widget>[
                RaisedButton(
                  color: global.GlobalDegiskenler.butonArkaPlanRengi,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: LabelCard(
                      fontFamily: GlobalDegiskenler.tlSimgesi,
                      label: "Tekrar Yükle",
                      fontSize: SizeConfig.safeBlockHorizontal * 6,
                      color: Colors.white,
                    ),
                  ),
                  onPressed: () async {
                    try {
                      var result = await Connectivity().checkConnectivity();
                      if (result == ConnectivityResult.none) {
                        MesajGoster.mesajGoster(context,
                            "Cihazınızı internete bağlayınız.", SoruTipi.Uyari);
                      } else {
                        setState(() {
                          menuTekrarYukle();
                        });
                      }
                    } catch (e) {}
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  //menü yüklerken yükleme ekren
  Widget shimmerGoster() {
    return Shimmer.fromColors(
      baseColor: Colors.grey.withOpacity(0.3),
      highlightColor: Colors.white.withOpacity(0.1),
      child: Padding(
        padding: EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
        child: ListView(
          physics: NeverScrollableScrollPhysics(),
          children: <Widget>[
            SizedBox(
              height: 4,
            ),
            AspectRatio(
              aspectRatio: 16 / 9,
              child: Padding(
                padding: EdgeInsets.all(SizeConfig.screenHeight * 0.006),
                child: Container(
                  padding: EdgeInsets.all(4),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(
                  left: 4.0, top: SizeConfig.screenHeight * 0.005),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  LabelCard(
                    textAlign: TextAlign.center,
                    fontSize: SizeConfig.safeBlockHorizontal * 5,
                    fontFamily: GlobalDegiskenler.genelFontStyle,
                    fontWeight: FontWeight.bold,
                    label: "Menüler",
                  ),
                ],
              ),
            ),
            Divider(
              height: 8,
              color: Colors.black26,
            ),
            SizedBox(
              height: 2,
            ),
            GridView.builder(
              shrinkWrap: true,
              primary: false,
              physics: NeverScrollableScrollPhysics(),
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 1,
                childAspectRatio: 3,
              ),
              itemCount: 6,
              itemBuilder: (BuildContext context, int index) {
                return Padding(
                  padding: const EdgeInsets.all(4),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                    ),
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }

  List<Widget> prfilIcon() {
    return <Widget>[
      Tooltip(
        message: 'Profil',
        child: GestureDetector(
          child: Padding(
            padding:
            EdgeInsets.fromLTRB(0, 0, SizeConfig.screenWidth * 0.03, 0),
            child: cariAd.isEmpty
                ? Icon(
              Icons.account_circle,
              color:GlobalDegiskenler.appBarIconColor,
              size: SizeConfig.safeBlockHorizontal * 9.7,
            )
                : CircleAvatar(
              radius: SizeConfig.safeBlockHorizontal * 4.5,
              backgroundColor: Colors.grey.shade200,
              child: LabelCard(
                satirSayisi: 1,
                fontSize: SizeConfig.safeBlockHorizontal * 3.5,
                fontFamily: GlobalDegiskenler.tlSimgesi,
                textAlign: TextAlign.center,
                fontWeight: FontWeight.bold,
                label: adSoyadProfil,
                color: GlobalDegiskenler.appBarColor,
              ),
            ),
          ),
          onTap: () {
            cariAd.isEmpty
                ? Navigator.of(context, rootNavigator: true)
                .push(MaterialPageRoute(builder: (context) => StepperGiris()))
                : global.Navigate()
                .navigatePushNoAnimation(context, ProfilTemelSayfa());
          },
        ),
      )
    ];
  }

  Widget saatIcon() {
    return Tooltip(
      message: 'İşletme Hizmet Saatleri',
      child: GestureDetector(
        onTap: () {
          showDialog(
              context: context,
              barrierDismissible: true,
              builder: (context) {
                return AlertDialog(
                  backgroundColor: Colors.black.withOpacity(0.4),
                  title: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      LabelCard(
                        label: 'Hizmet Saatleri',
                        textAlign: TextAlign.center,
                        satirSayisi: 2,
                        color: Colors.white,
                      ),
                      Tooltip(
                        message: 'Menüyü Kapatır',
                        child: GestureDetector(
                            onTap: () {
                              Navigator.of(context).pop();
                            },
                            child: Icon(
                              Icons.exit_to_app,
                              size: SizeConfig.safeBlockHorizontal * 8,
                              color: Colors.white,
                            )),
                      )
                    ],
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                  content: Container(
                    height: SizeConfig.safeBlockVertical * 55,
                    width: SizeConfig.safeBlockVertical * 50,
                    child: FutureBuilder(
                      future: islemDonus.calismaSaatleri(),
                      builder: (context, snapshot) {
                        if (snapshot.connectionState ==
                            ConnectionState.waiting) {
                          return saatBilgiShimmer();
                        } else if (snapshot.connectionState ==
                            ConnectionState.done &&
                            !snapshot.hasError &&
                            snapshot.hasData) {
                          cSaatBilgi = snapshot.data;
                          if (cSaatBilgi.length == 0) {
                            return aciklama(
                                'Hata',
                                'Lütfen internetinizi kontrol ediniz.',
                                Icons.alarm_off,
                                color: Colors.white);
                          }
                          if (cSaatBilgi[0].cariBasariDonus == '4') {
                            return aciklama(
                                'Bilgi',
                                'Saat bilgisi henüz girilmedi.',
                                Icons.alarm_off,
                                color: Colors.white);
                          } else {
                            return bilgi.bilgiAlert(
                                context, widget, cSaatBilgi);
                          }
                        }
                        return null;
                      },
                    ),
                  ),
                );
              });
        },
        child: Center(
          child: Icon(
            LineAwesomeIcons.clock_o,
            color: GlobalDegiskenler.appBarIconColor,
            size: SizeConfig.safeBlockHorizontal * 9,
          ),
        ),
      ),
    );
  }

  Widget saatBilgiShimmer() {
    return ScrollConfiguration(
      behavior: MyBehavior(),
      child: Container(
        alignment: Alignment.center,
        child: ListView.builder(
          physics: NeverScrollableScrollPhysics(),
          itemCount: 7,
          itemBuilder: (context, index) {
            return Shimmer.fromColors(
              baseColor: Colors.grey.withOpacity(0.3),
              highlightColor: Colors.white.withOpacity(0.1),
              child: ListTile(
                title: Row(
                  children: <Widget>[
                    Container(
                      color: Colors.white,
                      height: 10,
                      width: SizeConfig.safeBlockHorizontal * 45,
                    ),
                  ],
                ),
                subtitle: Row(
                  children: <Widget>[
                    Container(
                      color: Colors.white,
                      height: 10,
                      width: SizeConfig.safeBlockHorizontal * 25,
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  Widget mesajGoster(String baslik, String aciklama, IconData icon) {
    return Container(
      padding: const EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
      child: global.aciklama(baslik, aciklama, icon),
    );
  }

  carouselSlider(scrollDirection) {
    return CarouselSlider(
      enableInfiniteScroll: false,
      aspectRatio: 16 / 9,
      scrollDirection: scrollDirection,
      scrollPhysics: BouncingScrollPhysics(),
      items: maps<Widget>(
        imgUrlList,
            (index, i) {
          return Padding(
            padding: EdgeInsets.only(top: SizeConfig.screenHeight * 0.005),
            child: Stack(
              children: [
                SliderCart(
                  img: imgUrlList[index],
                  onTap: onTapSlider(index),
                ),
                Positioned(
                  right: 7,
                  bottom: 13,
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.black54,
                        borderRadius: BorderRadius.circular(SizeConfig.safeBlockHorizontal * 3)
                    ),
                    padding: EdgeInsets.symmetric(horizontal:SizeConfig.safeBlockHorizontal * 2.5,vertical: SizeConfig.safeBlockHorizontal * 1.4 ),
                    child: LabelCard(
                      textAlign: TextAlign.center,
                      fontSize: SizeConfig.safeBlockHorizontal * 2.5,
                      fontFamily: GlobalDegiskenler.genelFontStyle,
                      fontWeight: FontWeight.bold,
                      label: "${index + 1}/${imgUrlList.length}",
                      color: Colors.white,
                    ),
                  ),)
              ],

            ),
          );
        },
      ).toList(),
      autoPlay: false,
//        enlargeCenterPage: true,
      viewportFraction: 1.0,
      //             aspectRatio: 2.0,
      /*   onPageChanged: (index) {
        setState(() {
          _current = index;
        });
      },*/
    );
  }

//endregion

}
