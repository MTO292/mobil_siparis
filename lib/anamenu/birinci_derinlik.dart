
import '../yardimci_widgetlar/manu_kartlar/menu_cart.dart';
import '../yardimci_widgetlar/manu_kartlar/urun_cart.dart';
import 'package:connectivity_wrapper/connectivity_wrapper.dart';
import 'package:flutter/material.dart';
import '../anamenu/ikinci_derinlik.dart';
import '../anamenu/urun_profil.dart';
import '../bottom_menu/my_custom_bottom.dart';
import '../global_degisken/global_degisken.dart';
import '../api/model/menu_model.dart';
import '../responsive_class/size_config.dart';
import '../yardimci_widgetlar/appbar_widgets/appbar_text.dart';
import '../yardimci_fonksiyonlar/form_mesaj_dondur/net_kontrol.dart'
    as net;
import '../global_degisken/global_degisken.dart' as global;

class BirinciDerinlik extends StatefulWidget {
  final Anagrup menu;

  BirinciDerinlik({Key key, @required this.menu}) : super(key: key);

  @override
  _AnamenuListeDetay createState() => _AnamenuListeDetay();
}

class _AnamenuListeDetay extends State<BirinciDerinlik> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: GlobalDegiskenler.appBarColor,
        title: BaslikText(
          label: widget.menu.anagrupBaslik,
        ),
      ),
      bottomNavigationBar:MyBottomNavBar( 0),

      body: ConnectivityWidgetWrapper(
        offlineWidget: net.netKontrol(),
        disableInteraction: false,
        child: SafeArea(
          child: SingleChildScrollView(
            physics: ScrollBehavior().getScrollPhysics(context),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
              child: widget.menu.altgruplar.length == 0 && widget.menu.grupurunler.length == 0
                  ? aciklama('Bilgi', 'Bu kategoriye kayıtlı ürün bulunmamaktadır.',
                      Icons.room_service)
                  : Column(
                  children: <Widget>[
                  /*Tıklanılan menü başlığının bir alt kategorisine gitmeye yarar.*/
                  /*Tatlılar - sütlü tatlılar - sütlaç*/
                  GridView.builder(
                    shrinkWrap: true,
                    primary: false,
                    physics: NeverScrollableScrollPhysics(),
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 1,
                      childAspectRatio: 3,
                    ),
                    itemCount: widget.menu.altgruplar.length,
                    itemBuilder: (BuildContext context, int index) {
                      return MenuCart(
                        img:
                            "${global.resimUrl + global.key + "/g-" + widget.menu.altgruplar[index].altgrupId + ".jpg"}",
                        name: widget.menu.altgruplar[index].altgrupAdi,
                        onTap: () {
                          global.Navigate().navigatePushNoAnimation(
                            context,
                            IkinciDerinlik(
                              grupUrunler: widget.menu.altgruplar[index].altgrupurunler,
                            ),
                          );
                        },
                      );
                    },
                  ),
                  /*Kategoriler kısmına eklenen ürünün direkt olarak ürünü sepete ekle sayfasına yönlendirme yapar*/
                  /*Tatlılar - kabak tatlısı*/
                  GridView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: widget.menu.grupurunler.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      childAspectRatio: SizeConfig.screenWidth /
                          (SizeConfig.screenHeight / 1.4),
                    ),
                    itemBuilder: (context, index) {
                      /*Genel olarak bir menü başlığını temsil eden container*/
                      return Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: UrunCart(
                          img:
                              "${global.resimUrl + global.key + "/s-" + widget.menu.grupurunler[index].aId + ".jpg"}",
                          imageSize: MediaQuery.of(context).size.height / 6,
                          name: widget.menu.grupurunler[index].aAdi,
                          fiyat: double.parse(widget.menu.grupurunler[index].aSfiyat).toStringAsFixed(2),
                          onTap: ()  {
                            global.Navigate().navigatePushNoAnimation(
                                context,
                                UrunProfil(
                                    grupurunler:
                                        widget.menu.grupurunler[index]));
                          },
                        ),
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
