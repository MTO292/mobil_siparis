import 'dart:async';
import 'dart:convert';
import 'package:connectivity/connectivity.dart';
import 'package:connectivity_wrapper/connectivity_wrapper.dart';
import 'package:expansion_tile_card/expansion_tile_card.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../api/model/porisyon_model.dart';
import '../api/model/stok_ozelik_model.dart';
import '../api/response/islem_donus.dart';
import '../bloc/blocs_exports.dart';
import '../yardimci_fonksiyonlar/sayfa_kadirma_efekti/scrool.dart';
import '../yardimci_widgetlar/alt_bar_buton/alt_bar_buton.dart';
import 'package:line_awesome_icons/line_awesome_icons.dart';
import 'package:photo_view/photo_view.dart';
import '../api/model/sepet_model.dart';
import '../global_degisken/global_degisken.dart';
import '../api/model/menu_model.dart';
import '../responsive_class/size_config.dart';
import '../yardimci_fonksiyonlar/form_mesaj_dondur/mesaj_dondur.dart';
import '../yardimci_widgetlar/appbar_widgets/appbar_text.dart';
import '../yardimci_widgetlar/form_label_widgets/text_form_field.dart';
import '../yardimci_widgetlar/sepetim_sayfasi/miktar_butonlari.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../yardimci_widgetlar/label_text/label_widget.dart';
import '../yardimci_fonksiyonlar/form_mesaj_dondur/net_kontrol.dart' as net;
import '../global_degisken/global_degisken.dart' as global;
import 'urun_resim_buyutme.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class UrunProfil extends StatefulWidget {
  final Grupurunler grupurunler;

  const UrunProfil({Key key, @required this.grupurunler}) : super(key: key);

  @override
  _UrunProfilState createState() => _UrunProfilState();
}

class _UrunProfilState extends State<UrunProfil> {
  /*Sepete ekleme kısmında kullanılacak olan değişkenler*/
  var formKey = GlobalKey<FormState>();
  var aciklamaController = TextEditingController();
  IslemDonus islemDonus = IslemDonus();

  /*Sepete ekleme kısmında kullanılacak olan değişkenler*/
  //local Bildirim için oluşturuldu
  int urunMiktar = 1;
  String aciklama;
  double toplamFiyat;
  double urunFiyat;

  List<String> pKontrol = [];

  /*Sepete ürün eklerken map olarak atılabilmesi için oluşturulan değişken*/
  Map<String, dynamic> eklenenUrun = Map();

  //Eklenen Birden Fazla Özelik
  List<Map<String, dynamic>> eklenenBFSO = List();

  //Eklenen Tek Secmeli Özelik
  List<Map<String, dynamic>> eklenenTSO = List();

  /*Sepetim listesini doldurmak için açılan liste*/
  List<Urun> sepetim = [];

  Urun urunlerim;

  double porisyonMiktar = 1;

  var sepetJson;
  SharedPreferences prefs;

  List<Porisyon> porsiyonList = List();

  //Stok ozelik için oluşturuldu
  List<StokOzelik> stokOzelikList;

  //Tek secmeli ekstra ozelik için oluşturuldu
  List<StokOzelik> tekSecmeliOzelikList = List();

  //Birden fazla secmeli ekstra ozelik için oluşturuldu
  List<StokOzelik> BFazlaSecmeliOzelikList = List();

  Future<List<StokOzelik>> gelenMenuDegeri;

  String secilenUrunOzelik = '';

  double statusBarHeight;

  List<int> tekSecmeliRadioG;

  bool zorunluAlanHata = false;

  double ekstraStokTopFiy = 0;

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    urunFiyat = toDouble(widget.grupurunler.aSfiyat);
    toplamFiyat = urunFiyat;
    aciklama = aciklamaController.text;
    aciklamaController.text = widget.grupurunler.aciklama;
    pKontrol = [
      "PORSİYON",
      "PORSIYON",
      "PRSYN",
      "PORSYON",
      "PORSIYN",
      "POSIYON",
      "porsiyon",
      "porsıyon",
      "prsyn",
      "porsyon",
      "porsıyn",
      "posıyon"
    ];
    ekstraOzelikGetir();
    sharedBaslat();
    for (int i = 0; i < 2; i++) {
      Porisyon deger = Porisyon();
      deger.adi = i == 0 ? '1 Porisyon' : '1.5 Porisyon';
      deger.fiyat = i == 0
          ? toDouble(widget.grupurunler.aSfiyat)
          : toDouble(widget.grupurunler.aSfiyat) +
          (toDouble(widget.grupurunler.aSfiyat) / 2);
      porsiyonList.add(deger);
    }
  }

  /*Sayfa yüklendiğinde sepete kayıtlı ürünleri değişkene aktarıyoruz*/
  Future sharedBaslat() async {
    prefs = await SharedPreferences.getInstance();
    sepetJson = prefs.getString("sepettekiUrunlerim");
  }

  ekstraOzelikGetir() {
    gelenMenuDegeri = islemDonus.stokOzelik(stokId: widget.grupurunler.aId);
    gelenMenuDegeri.then((deger) {
      stokOzelikList = deger;
      //Ekstra ozelikleri türüne göre ayırıyoruz
      deger.forEach((element) {
        if (element.kategoriTur == 1) tekSecmeliOzelikList.add(element);
        if (element.kategoriTur == 2) BFazlaSecmeliOzelikList.add(element);
      });
      //tek seçmeli kategori uzunluğu kadar radio buton için grp degeri oluşturuyoruz
      tekSecmeliRadioG = List(tekSecmeliOzelikList.length);
      return null;
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    statusBarHeight = SizeConfig.statusBarHeight;
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      child: Scaffold(
        /*Sepete ürün ekleme butonu*/
        /*Sayfadaki en üst başlığı temsil ediyor*/
        appBar: AppBar(
          leading: IconButton(
              icon: Icon(
                Icons.clear,
              ),
              onPressed: () {
                Navigator.of(context).pop(true);
              }),
          centerTitle: true,
          iconTheme: IconThemeData(
            size: 40.0,
          ),
          title: BaslikText(
            label: widget.grupurunler.aAdi,
          ),
          backgroundColor: GlobalDegiskenler.appBarColor,
        ),
        body: ConnectivityWidgetWrapper(
          offlineWidget: net.netKontrol(),
          disableInteraction: false,
          child: FutureBuilder(
              future: gelenMenuDegeri,
              builder: (context, snapshot) {
                /*Veriler yüklenirken bu ekran gösterilecek*/
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return Center(
                    child: global.progressDondur(),
                  );
                  /*Veri varmı kontrol ediyoruz*/
                } else if (snapshot.connectionState == ConnectionState.done &&
                    !snapshot.hasError &&
                    snapshot.hasData) {
                  if (stokOzelikList[0].cariBasariDonus ==
                      numarator.uygulamaVersiyonuEski) {
                    logoEkraninaGit(context);
                    return SizedBox();
                  } else if (stokOzelikList[0].cariBasariDonus ==
                      numarator.apiVersiyonuEski) {
                    logoEkraninaGit(context);
                    return SizedBox();
                  } else if (stokOzelikList[0].cariBasariDonus ==
                      numarator.hizmetDisi) {
                    logoEkraninaGit(context);
                    return SizedBox();
                  } else {
                    return listele();
                  }
                  /*Veri yoksa ve hata varsa burası gösterilecek*/
                } else if (snapshot.hasError) {
                  return mesajGoster(
                      'Hata',
                      'Lütfen internetinizi kontrol ediniz.',
                      Icons.error_outline);
                } else if (snapshot.data == null && snapshot.hasData == false)
                  return mesajGoster(
                      'Hata',
                      'Lütfen internetinizi kontrol ediniz.',
                      Icons.error_outline);
                return mesajGoster(
                    'Hata',
                    'Lütfen internetinizi kontrol ediniz.',
                    Icons.error_outline);
              }),
        ),
      ),
    );
  }

  dropdownGoster(
      BuildContext context, {
        Function onTap(String deger),
      }) async {
    return showModalBottomSheet(
        backgroundColor: Colors.transparent,
        isScrollControlled: true,
        context: context,
        builder: (BuildContext context) {
          return Padding(
            padding: EdgeInsets.only(
                bottom: MediaQuery.of(context).viewInsets.bottom),
            child: StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
                return ScrollConfiguration(
                  behavior: MyBehavior(),
                  child: Column(
                    children: <Widget>[
                      Container(
                        height: statusBarHeight,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              width: 40,
                              height: 4,
                              decoration: BoxDecoration(
                                color: Color.fromRGBO(213, 213, 215, 1),
                                borderRadius:
                                BorderRadius.all(Radius.circular(32)),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                        child: Container(
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(8),
                                topRight: Radius.circular(8),
                              )),
                          child: CupertinoScrollbar(
                            child: SingleChildScrollView(
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal:
                                    SizeConfig.safeBlockHorizontal * 4),
                                child: Column(
                                  children: [
                                    Container(
                                      padding: EdgeInsets.symmetric(
                                          vertical:
                                          SizeConfig.safeBlockHorizontal *
                                              1),
                                      child: Row(
                                        mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                        children: [
                                          baslik(
                                            "Ürün Özellikleri",
                                            SizeConfig.safeBlockHorizontal * 5,
                                          ),
                                          Material(
                                            color: Colors.transparent,
                                            child: InkWell(
                                              child: Container(
                                                height: SizeConfig
                                                    .safeBlockHorizontal *
                                                    8,
                                                width: SizeConfig
                                                    .safeBlockHorizontal *
                                                    10,
                                                child: Icon(
                                                  Icons.close,
                                                  color: Colors.black,
                                                  size: SizeConfig
                                                      .safeBlockHorizontal *
                                                      7,
                                                ),
                                              ),
                                              onTap: () =>
                                                  Navigator.pop(context),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    divider(height: 0),
                                    SizedBox(
                                      height: 8,
                                    ),
                                    sizeBox(),
                                    urunKart(setState),
                                    sizeBox(),
                                    pKontrol.contains(widget.grupurunler.aBirim)
                                        ? Column(
                                      children: [
                                        divider(),
                                        baslik(
                                          "Porsiyon",
                                          SizeConfig.safeBlockHorizontal *
                                              4,
                                          fontWeight: FontWeight.w300,
                                          color: Color.fromRGBO(
                                              160, 160, 160, 1),
                                        ),
                                        porsiyonListele(setState),
                                      ],
                                    )
                                        : SizedBox(),
                                    stokOzelikList[0].cariBasariDonus == '1'
                                        ? ozelikListele(setState)
                                        : SizedBox(),
                                    divider(),
                                    ekstraSecButon(setState),
                                    SizedBox(
                                      height: SizeConfig.safeBlockVertical * 1,
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                );
              },
            ),
          );
        }).then((deger) {
      //Secilen Stok Özelikleri Listeye atıyoruz
      secilenStokOzelikleri();
      setState(() {});
    });
  }

  Container ekstraSecButon(StateSetter set) {
    return Container(
      width: double.infinity,
      child: RaisedButton(
        elevation: 2,
        padding: EdgeInsets.symmetric(
          vertical: SizeConfig.safeBlockVertical * 1,
        ),
        color: GlobalDegiskenler.butonArkaPlanRengi,
        child: LabelCard(
          label: 'Seç',
          fontSize: SizeConfig.safeBlockHorizontal * 5,
          color: Colors.white,
          fontFamily: GlobalDegiskenler.tlSimgesi,
        ),
        onPressed: () {
          //Secilen Stok Özelikleri Listeye atıyoruz
          secilenStokOzelikleri();
          if (tekSecmeliOzelikList.length != eklenenTSO.length) {
            MesajGoster.toastMesaj("Lütfen Zorunlu Alanları Seçiniz.");
            // Zorunlu alanlarda hata vermesi için ayarlandı
            zorunluAlanHata = true;
            set(() {});
          } else {
            sepetteEkle();
          }
        },
      ),
    );
  }

  void secilenStokOzelikleri() {
    //Secilen Stok Özelikleri Listeye atıyoruz
    try {
      secilenUrunOzelik = '';
      eklenenBFSO.clear();
      eklenenTSO.clear();
      //api den gelen stok kadar dön ve secilen alanı ture olan tüm stokları eklenenUrunOzelik Map'ine At
      // Tek Seçmeli Olanlar
      tekSecmeliOzelikList.forEach((StokOzelik kategori) {
        kategori.ekstraOzelik.forEach((ozelik) {
          if (ozelik.secilen) {
            eklenenTSO.add({
              "id": ozelik.id,
              "adi": ozelik.adi,
              "fiyat": ozelik.fiyat,
              "miktar": ozelik.miktar,
              "stokId": ozelik.stokId,
              "ozstokId": ozelik.ozstokId,
              "mikCrp": ozelik.mikCrp,
              "mik":
              ozelik.mikCrp == 0.0 ? 1.0 : toDouble(urunMiktar.toString()),
            });
          } //secilenUrunOzelik degişkenini ana ekreanında gostermek için oluşturuyoruz
          secilenUrunOzelik =
          '${secilenUrunOzelik != '' ? secilenUrunOzelik + ',' : ''}${ozelik.adi}';
        });
      });
      //Birden Fazla Seçmeli Olanlar
      BFazlaSecmeliOzelikList.forEach((kategori) {
        kategori.ekstraOzelik.forEach((ozelik) {
          if (ozelik.secilen) {
            eklenenBFSO.add({
              "id": ozelik.id,
              "adi": ozelik.adi,
              "fiyat": ozelik.fiyat,
              "miktar": ozelik.miktar,
              "stokId": ozelik.stokId,
              "ozstokId": ozelik.ozstokId,
              "mikCrp": ozelik.mikCrp,
              "mik":
              ozelik.mikCrp == 0.0 ? 1.0 : toDouble(urunMiktar.toString()),
            });
          } //secilenUrunOzelik degişkenini ana ekreanında gostermek için oluşturuyoruz
          secilenUrunOzelik =
          '${secilenUrunOzelik != '' ? secilenUrunOzelik + ',' : ''}${ozelik.adi}';
        });
      });
    } catch (e) {}
  }

  Widget urunKart(StateSetter setState) {
    return Container(
      height: SizeConfig.screenHeight * 0.14,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          /*Resimin temsil edildiği padding widgeti*/
          AspectRatio(
            aspectRatio: 1,
            child: Container(
              padding: EdgeInsets.all(4),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(16))),
              child: ClipRRect(
                child: FadeInImage.assetNetwork(
                  image:
                  "${global.resimUrl + global.key + "/s-" + widget.grupurunler.aId + "-t.jpg"}",
                  fit: BoxFit.cover,
                  placeholder: "assets/images/resim-yok.png",
                ),
                borderRadius: BorderRadius.all(Radius.circular(12)),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                LabelCard(
                  label: widget.grupurunler.aAdi,
                  satirSayisi: 2,
                  fontFamily: GlobalDegiskenler.tlSimgesi,
                  fontWeight: FontWeight.w500,
                  fontSize: SizeConfig.safeBlockHorizontal * 4,
                ),
                LabelCard(
                  label: toplamFiyat.toStringAsFixed(2) + " ₺",
                  satirSayisi: 2,
                  fontFamily: GlobalDegiskenler.tlSimgesi,
                  fontWeight: FontWeight.w500,
                  fontSize: SizeConfig.safeBlockHorizontal * 4,
                ),
                miktarButon(setState)
              ],
            ),
          ),
        ],
      ),
    );
  }

  void prefsSepeteEkle() {
    aciklamaController.clear();
    prefs.setString("sepettekiUrunlerim", json.encode(sepetim));
    //yemek sepetinin iconunun üstündeki bildirim için oluşturuldu
    //sepet sayacı için bloc la değerini atıyoruz
    context.bloc<SepetSayacCubit>().esitle(sepetim.length);

    ///Bildirim blocla beraber kullanımında hata verdiğinden şimdilik kapatıldı
    //bildirim.gunlukBildirim(baslik: localBildirimBaslik,altBaslik:  localBildirimMesaj);
    MesajGoster.toastMesaj(urunlerim.aAdi + " sepete eklendi");
    //eger ekstra ürün bottom sheeti açık ise iki defa arkaya dön yok ise iki kez dön
    if (stokOzelikList[0].cariBasariDonus == '1' ||
        pKontrol.contains(widget.grupurunler.aBirim)) {
      int i = 0;
      Navigator.of(context).popUntil((route) => i++ >= 2);
    } else {
      Navigator.pop(context);
    }
  }

  Widget mesajGoster(String baslik, String aciklama, IconData icon) {
    return Container(
      padding: const EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
      child: global.aciklama(baslik, aciklama, icon),
    );
  }

  /*Veritabanından gelen double değerini , ile gösterdiği için bu çevirmeyi yapmak zorundayız yoksa ekran patlıyor*/
  double toDouble(String aSfiyat) {
    var sp = aSfiyat.split(',');
    var sp2 = aSfiyat.split('.');
    if (sp.length == 1 && sp2.length == 1) {
      return int.parse(aSfiyat) * 1.0;
    } else if (sp.length == 1) {
      return double.parse(aSfiyat);
    } else {
      var a = sp[0] + "." + sp[1];
      return double.parse(a);
    }
  }

  Widget baslik(String baslik, double fontSize,
      {int satirS = 1, FontWeight fontWeight, Color color}) {
    return LabelCard(
      satirSayisi: satirS,
      color: color ?? Colors.black,
      label: baslik,
      fontFamily: GlobalDegiskenler.tlSimgesi,
      alignment: Alignment.centerLeft,
      fontSize: fontSize,
      fontWeight: fontWeight ?? FontWeight.w500,
      padding: EdgeInsets.only(left: 2),
    );
  }

  Future sepetteEkle() async {
    try {
      var result = await Connectivity().checkConnectivity();
      if (result == ConnectivityResult.none) {
        MesajGoster.mesajGoster(
            context, "Cihazınızı internete bağlayınız.", SoruTipi.Uyari);
      } else {
        var ekstraOzelikSon = eklenenBFSO + eklenenTSO;
        if (ekstraOzelikSon.length == 0) {
          ekstraOzelikSon.add({
            "id": null,
            "adi": null,
            "fiyat": null,
            "miktar": null,
            "stokId": null,
            "ozstokId": null,
            "mik": null,
            "mikCrp": null,
          });
        }
        eklenenUrun = {
          "aAdi": widget.grupurunler.aAdi.toString(),
          "aId": widget.grupurunler.aId.toString(),
          "aSfiyat": urunFiyat.toString(),
          "toplamFiyat":
          (porisyonMiktar * urunMiktar * urunFiyat).toStringAsFixed(2),
          "genelTopFiy": toplamFiyat.toStringAsFixed(2),
          "urunMiktar": urunMiktar.toString(),
          "aciklama": aciklamaController.text.toString(),
          "porsiyon": porisyonMiktar.toString(),
          "a_acik": widget.grupurunler.aAcik.toString(),
          "a_birim": widget.grupurunler.aBirim.toString(),
          "ekstraOzelik": ekstraOzelikSon,
        };
        urunlerim = Urun.fromJson(eklenenUrun);
        sepetJson = prefs.getString("sepettekiUrunlerim");
        if (sepetJson == null) {
          sepetim.add(urunlerim);
          prefsSepeteEkle();
        } else {
          sepetim = List.from(jsonDecode(sepetJson))
              .map((obj) => Urun.fromJson(obj))
              .toList();
          if (sepetim.length == 50) {
            MesajGoster.toastMesaj("Sepete en fazla 50 ürün ekleye bilirsiniz");
          } else {
            sepetim.add(urunlerim);
            prefsSepeteEkle();
          }
        }
      }
    } catch (e) {}
  }

  listele() {
    return Column(
      children: [
        Expanded(
          child: ListView(
            shrinkWrap: true,
            children: <Widget>[
              /*Resim kısmının temsil edildiği container*/
              GestureDetector(
                child: Padding(
                  padding: EdgeInsets.all(SizeConfig.safeBlockHorizontal * 2),
                  child: Container(
                    decoration: BoxDecoration(
                        border: Border.all(width: 1, color: Colors.black12),
                        borderRadius: BorderRadius.circular(8)),
                    alignment: Alignment.center,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(8.0),
                      child: FadeInImage.assetNetwork(
                          fadeOutCurve: Curves.slowMiddle,
                          fit: BoxFit.cover,
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.height / 3.2,
                          imageScale: 1,
                          placeholder: "assets/images/resim-yok.png",
                          image:
                          "${global.resimUrl + global.key + "/s-" + widget.grupurunler.aId + "-t.jpg"}"),
                    ),
                  ),
                ),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => UrunResimBuyutme(
                        imageProvider: NetworkImage(
                            "${global.resimUrl + global.key + "/s-" + widget.grupurunler.aId + "-t.jpg"}"),
                        minScale: PhotoViewComputedScale.contained * 0.8,
                        maxScale: PhotoViewComputedScale.covered * 1.1,
                        initialScale: PhotoViewComputedScale.contained,
                      ),
                    ),
                  );
                },
              ),
              /*Resimin altında kalan içeriğin kapsadığı Column*/
              Container(
                margin: EdgeInsets.only(
                    bottom:SizeConfig.safeBlockHorizontal * 2.5,
                    right:SizeConfig.safeBlockHorizontal * 2.5,
                    left:SizeConfig.safeBlockHorizontal * 2.5,),
                decoration: GlobalDegiskenler.Sablon,
                width: double.infinity,
                height: SizeConfig.safeBlockVertical * 66,
                child: Column(
                  children: <Widget>[
                    SizedBox(height: SizeConfig.safeBlockHorizontal * 4,),
                   Padding(padding: EdgeInsets.symmetric(horizontal: 16),
                   child:  Row(
                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                     children: [
                       /*Toplam fiyat label*/
                       baslik("Adet", SizeConfig.safeBlockHorizontal * 5.0),
                       /*Ürün miktar azaltma butonu*/
                       miktarButon(setState),
                     ],
                   ),),
                    SizedBox(height: SizeConfig.safeBlockHorizontal * 4,),
                    ExpansionTileCard(
                        baseColor: Colors.white,
                        expandedColor: Colors.white,
                        elevation: 0,
                        borderRadius: BorderRadius.all(Radius.zero),
                        title: baslik("Ürün Bilgisi", SizeConfig.safeBlockHorizontal * 5.0),
                        children: [
                          /*İçindekiler bölümü*/
                          Padding(padding: EdgeInsets.only(
                            bottom: SizeConfig.safeBlockVertical * 1,
                            left: SizeConfig.safeBlockHorizontal * 2,
                            right: SizeConfig.safeBlockHorizontal * 2,
                          ),
                          child: Padding(padding: EdgeInsets.symmetric(horizontal: 16),
                          child: baslik(widget.grupurunler.aAcik != '' ? widget.grupurunler.aAcik:'Ürün Bilgisi Eklenmedi.',
                              SizeConfig.safeBlockHorizontal * 3.75,
                              satirS: 7),),)
                        ]),
                    SizedBox(height: SizeConfig.safeBlockHorizontal * 4,),
                    /*Mesaj ekleme text-field alanı*/
                    Padding(padding: EdgeInsets.symmetric(horizontal: 16),
                    child: MyTextFormField(
                      satirSayisi: 2,
                      karakterSayisi: 200,
                      ileriTusu: TextInputAction.done,
                      label: 'Not bırakmak istiyorum',
                      labelText: 'Not',
                      prefixIcon: Icon(LineAwesomeIcons.sticky_note,color: GlobalDegiskenler.butonArkaPlanRengi,),
                      kontroller: aciklamaController,
                    ),)
                  ],
                ),
              ),
            ],
          ),
        ),
        AltBarButon(onTap: () async {
          //eger ekstra stok listesi yüklenmemiş ise button çalışmasın
          if (stokOzelikList != null) {
            if (stokOzelikList[0].cariBasariDonus == '1' ||
                pKontrol.contains(widget.grupurunler.aBirim)) {
              dropdownGoster(context);
            } else {
              sepetteEkle();
            }
          }
        },tutar: toplamFiyat,butontTxt: 'Sepete Ekle',),
      ],
    );
  }


  ozelikListele(StateSetter setState) {
    return Container(
        width: double.infinity,
        alignment: Alignment.bottomLeft,
        child: Column(
          children: [
            ListView.builder(
                shrinkWrap: true,
                itemCount: BFazlaSecmeliOzelikList.length,
                physics: NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  //Birden fzla seçmeli ekstra stok için oluşturuldu
                  return BFazlaSecmeliOzelikList[index].kategoriTur == 2
                      ? Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      divider(),
                      baslik(
                        BFazlaSecmeliOzelikList[index].kategoriAdi,
                        SizeConfig.safeBlockHorizontal * 4,
                        fontWeight: FontWeight.w300,
                        color: Color.fromRGBO(160, 160, 160, 1),
                      ),
                      Wrap(
                        spacing: SizeConfig.safeBlockVertical * 1,
                        children: BFazlaSecmeliOzelikList[index]
                            .ekstraOzelik
                            .map(
                              (item) => Column(
                            crossAxisAlignment:
                            CrossAxisAlignment.start,
                            children: [
                              FilterChip(
                                elevation: 2,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(8))),
                                showCheckmark: false,
                                label: richTextFiyat(
                                    item.adi, item.fiyat),
                                labelPadding: EdgeInsets.symmetric(
                                    horizontal:
                                    SizeConfig.safeBlockVertical *
                                        1,
                                    vertical:
                                    SizeConfig.safeBlockVertical *
                                        0.5),
                                selectedColor: global
                                    .GlobalDegiskenler
                                    .butonArkaPlanRengi,
                                selected: item.secilen ? true : false,
                                onSelected: (deger) {
                                  ekstraBirdenFazlaSecmeliSec(
                                      setState, index, deger, item);
                                },
                              ),
                              sizeBox(),
                            ],
                          ),
                        )
                            .toList()
                            .cast<Widget>(),
                      ),
                    ],
                  )
                      : SizedBox();
                }),
            //Tek seçmeli seçmeli ekstra stok için oluşturuldu
            ListView.builder(
                shrinkWrap: true,
                itemCount: tekSecmeliOzelikList.length,
                physics: NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  //Radio Buton tek seçmeli groupValue için dizi oluşturyoruz
                  return tekSecmeliOzelikList[index].kategoriTur == 1
                      ? Column(
                    children: [
                      divider(),
                      //Zorunlu alanları secmeden Seç butonuna tıklanırsa hata ver
                      zorunluAlanHata && index == 0
                          ? baslik(
                        '* Lütfen altaki boş bırakılan zorunlu alanları seçiniz !',
                        SizeConfig.safeBlockHorizontal * 3,
                        fontWeight: FontWeight.w300,
                        color: Colors.red,
                      )
                          : SizedBox(),
                      ExpansionTileCard(
                          baseColor: Colors.white,
                          expandedColor: Colors.grey.shade300,
                          elevation: 1,
                          initiallyExpanded: zorunluAlanHata,
                          title: Row(
                            mainAxisAlignment:
                            MainAxisAlignment.spaceBetween,
                            children: [
                              baslik(
                                tekSecmeliOzelikList[index].kategoriAdi,
                                SizeConfig.safeBlockHorizontal * 5,
                                fontWeight: FontWeight.w300,
                                color: Colors.black,
                              ),
                              //Zorunlu alanları secmeden Seç butonuna tıklanırsa hata ver
                              zorunluAlanHata
                                  ? Icon(
                                LineAwesomeIcons.exclamation_circle,
                                color: Colors.red,
                                size:
                                SizeConfig.safeBlockHorizontal *
                                    6,
                              )
                                  : SizedBox(),
                            ],
                          ),
                          children: [
                            ListView.builder(
                                physics: NeverScrollableScrollPhysics(),
                                shrinkWrap: true,
                                itemCount: tekSecmeliOzelikList[index]
                                    .ekstraOzelik
                                    .length,
                                itemBuilder: (context, tekSecmeliIndex) {
                                  return RadioListTile(
                                      activeColor: GlobalDegiskenler
                                          .butonArkaPlanRengi,
                                      title: richTextFiyat(
                                          tekSecmeliOzelikList[index].ekstraOzelik[tekSecmeliIndex].adi,
                                          tekSecmeliOzelikList[index].ekstraOzelik[tekSecmeliIndex].fiyat),
                                      controlAffinity:
                                      ListTileControlAffinity
                                          .trailing,
                                      value: tekSecmeliIndex,
                                      groupValue: tekSecmeliRadioG[index],
                                      onChanged: (secilenIndex) {
                                        ekstraTekSecmeliSec(setState,
                                            index, secilenIndex);
                                      });
                                }),
                          ]),
                    ],
                  )
                      : SizedBox();
                }),
          ],
        ));
  }

  RichText richTextFiyat(String txt, double fiy) {
    return RichText(
        text: TextSpan(
            text: txt,
            style: TextStyle(
              color: Colors.black,
              fontSize: SizeConfig.safeBlockHorizontal * 3.5,
              fontFamily: GlobalDegiskenler.tlSimgesi,
              fontWeight: FontWeight.w400,
            ),
            children: [
              TextSpan(
                  text: fiy != 0
                      ? ' (' + fiy.toStringAsFixed(2) + ' ₺)'
                      : ' (Ücretsiz)',
                  style: TextStyle(
                    fontWeight: FontWeight.w400,
                    color: Colors.red,
                    fontSize: SizeConfig.safeBlockHorizontal * 3,
                    fontFamily: GlobalDegiskenler.tlSimgesi,
                  ))
            ]));
  }

  Widget sizeBox() {
    return SizedBox(
      height: SizeConfig.safeBlockVertical * 0.5,
    );
  }

  Widget porsiyonListele(StateSetter setState) {
    return Container(
      width: double.infinity,
      alignment: Alignment.bottomLeft,
      child: Wrap(
        spacing: SizeConfig.safeBlockVertical * 1,
        children: porsiyonList
            .map(
              (item) => Column(
            children: [
              FilterChip(
                elevation: 2,
                showCheckmark: false,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(8))),
                selectedColor: global.GlobalDegiskenler.butonArkaPlanRengi,
                label: richTextFiyat(item.adi, item.fiyat),
                labelPadding: EdgeInsets.symmetric(
                    horizontal: SizeConfig.safeBlockVertical * 1,
                    vertical: SizeConfig.safeBlockVertical * 0.5),
                selected: item.adi == '1 Porisyon' && porisyonMiktar == 1
                    ? true
                    : item.adi == '1.5 Porisyon' && porisyonMiktar == 1.5
                    ? true
                    : false,
                onSelected: (deeger) {
                  setState(() {
                    if (item.adi == '1 Porisyon' && porisyonMiktar == 1.5) {
                      porisyonMiktar = 1;
                      urunMiktar = 1;
                      ekstraStokTopFiy = 0;
                      //eklenen ekstra tüm stokların toplam fiyatlarını hesaplıyoruz
                      ekstraTopFiyatHesapla();
                      toplamFiyat =
                          (urunMiktar * (porisyonMiktar * urunFiyat)) +
                              ekstraStokTopFiy;
                    } else if (item.adi == '1.5 Porisyon' &&
                        porisyonMiktar == 1) {
                      porisyonMiktar = 1.5;
                      urunMiktar = 1;
                      ekstraStokTopFiy = 0;
                      //eklenen ekstra tüm stokların toplam fiyatlarını hesaplıyoruz
                      ekstraTopFiyatHesapla();
                      toplamFiyat =
                          (urunMiktar * (porisyonMiktar * urunFiyat)) +
                              ekstraStokTopFiy;
                    }
                  });
                },
              ),
              sizeBox(),
            ],
          ),
        )
            .toList()
            .cast<Widget>(),
      ),
    );
  }

  Widget divider({double height}) {
    return Divider(
      thickness: 1,
      color: Color.fromRGBO(213, 213, 215, 1),
      height: height ?? null,
    );
  }

  //tek secmeli ekstra özelik seçildiğinde tetiklenen metod
  ekstraTekSecmeliSec(
      StateSetter setState, int kategoriIndex, int secilenIndex) {
    //secilen degerin indexini radio guruba atıyoruz
    tekSecmeliRadioG[kategoriIndex] = secilenIndex;
    //index li kategorinin tüm secilen degerlerini sıfırlıyoruz
    tekSecmeliOzelikList[kategoriIndex].ekstraOzelik.forEach((element) {
      if (element.secilen) {
        element.secilen = false;
        //eklenen ekstra miktara bağımlı ise Çarp yoksa çarpa
        if (element.mikCrp == 0) {
          ekstraStokTopFiy = ekstraStokTopFiy - (element.fiyat);
        } else {
          ekstraStokTopFiy = ekstraStokTopFiy - (element.fiyat * urunMiktar);
        }
      }
    });
    //secilen degeri true yapıyoruz
    tekSecmeliOzelikList[kategoriIndex].ekstraOzelik[secilenIndex].secilen =
    true;
    //eklenen ekstra özeliğin fiyatını genel fiyatta ekliyoruz
    //eklenen ekstra miktara bağımlı ise Çarp yoksa çarpa
    if (tekSecmeliOzelikList[kategoriIndex].ekstraOzelik[secilenIndex].mikCrp ==
        0) {
      ekstraStokTopFiy = ekstraStokTopFiy +
          (tekSecmeliOzelikList[kategoriIndex]
              .ekstraOzelik[secilenIndex]
              .fiyat);
    } else {
      ekstraStokTopFiy = ekstraStokTopFiy +
          (tekSecmeliOzelikList[kategoriIndex]
              .ekstraOzelik[secilenIndex]
              .fiyat *
              urunMiktar);
    }
    //eklenen ekstra miktara bağımlı ise Çarp yoksa çarpma
    if (tekSecmeliOzelikList[kategoriIndex].ekstraOzelik[secilenIndex].mikCrp ==
        0) {
      toplamFiyat =
          (urunMiktar * porisyonMiktar * urunFiyat) + ekstraStokTopFiy;
    } else {
      toplamFiyat =
          urunMiktar * ((porisyonMiktar * urunFiyat) + ekstraStokTopFiy);
    }
    secilenStokOzelikleri();
    //eger hata verilmiş ise ve tüm zorunlu stoklar tıklanmış ise hatayı gider
    if (tekSecmeliOzelikList.length == eklenenTSO.length) {
      // Zorunlu alanlarda hata varı ise giderilmesi için ayarlandı
      zorunluAlanHata = false;
    }
    setState(() {});
  }

  void ekstraBirdenFazlaSecmeliSec(StateSetter setState, int kategoriIndex,
      bool secildi, EkstraOzelik secilenOzelik) {
    setState(() {});
    if (secildi) {
      secilenOzelik.secilen = true;
      //eklenen ekstra miktara bağımlı ise Çarp yoksa çarpa
      if (secilenOzelik.mikCrp == 0) {
        //kaldırılan ekstra özeliğin fiyatını genel fiyattan cıkarıyoruz
        ekstraStokTopFiy = ekstraStokTopFiy + (secilenOzelik.fiyat);
      } else {
        //kaldırılan ekstra özeliğin fiyatını genel fiyattan cıkarıyoruz
        ekstraStokTopFiy =
            ekstraStokTopFiy + (secilenOzelik.fiyat * urunMiktar);
      }
    } else if (secildi == false || secilenOzelik.secilen == true) {
      secilenOzelik.secilen = false;
      //eklenen ekstra miktara bağımlı ise Çarp yoksa çarpa
      if (secilenOzelik.mikCrp == 0) {
        //kaldırılan ekstra özeliğin fiyatını genel fiyattan cıkarıyoruz
        ekstraStokTopFiy = ekstraStokTopFiy - (secilenOzelik.fiyat);
      } else {
        //kaldırılan ekstra özeliğin fiyatını genel fiyattan cıkarıyoruz
        ekstraStokTopFiy =
            ekstraStokTopFiy - (secilenOzelik.fiyat * urunMiktar);
      }
    }
    toplamFiyat = (urunMiktar * porisyonMiktar * urunFiyat) + ekstraStokTopFiy;
  }

  miktarButon(StateSetter setState) {
    return Row(
      children: [
        Tooltip(
          message: 'Ürün miktarını azaltır',
          child: MiktarButonlari(
            alignment: Alignment.center,
            yukseklik: SizeConfig.safeBlockVertical * 05,
            genislik: SizeConfig.safeBlockHorizontal * 10,
            color: GlobalDegiskenler.miktarButonRenk,
            onPresssed: () {
              setState(() {
                if (urunMiktar >= 2) {
                  urunMiktar--;
                  ekstraStokTopFiy = 0;
                  //eklenen ekstra tüm stokların toplam fiyatlarını hesaplıyoruz
                  ekstraTopFiyatHesapla();
                  toplamFiyat = (urunMiktar * (porisyonMiktar * urunFiyat)) +
                      ekstraStokTopFiy;
                }
              });
            },
            label: "-",
            fontSize: MediaQuery.of(context).size.width * 0.06,
          ),
        ),
        /*Ürün miktarın yazıldığı label*/
        Container(
          width: SizeConfig.safeBlockHorizontal * 7.5,
          child: Center(
            child: LabelCard(
              satirSayisi: 2,
              label: urunMiktar.toString(),
              fontSize: SizeConfig.safeBlockHorizontal * 4.5,
              fontFamily: GlobalDegiskenler.tlSimgesi,
            ),
          ),
        ),
        /*Ürün miktar arttırma butonu*/
        Tooltip(
          message: 'Ürün miktarını arttırır',
          child: MiktarButonlari(
            alignment: Alignment.center,
            yukseklik: SizeConfig.safeBlockVertical * 05,
            genislik: SizeConfig.safeBlockHorizontal * 10,
            color: GlobalDegiskenler.miktarButonRenk,
            onPresssed: () {
              setState(() {
                if (urunMiktar <= 19) {
                  urunMiktar++;
                  ekstraStokTopFiy = 0;
                  //eklenen ekstra tüm stokların toplam fiyatlarını hesaplıyoruz
                  ekstraTopFiyatHesapla();
                  toplamFiyat = (urunMiktar * (porisyonMiktar * urunFiyat)) +
                      ekstraStokTopFiy;
                } else {
                  MesajGoster.mesajGoster(context,
                      "Ürün miktarı 20 adetten fazla olamaz.", SoruTipi.Uyari);
                }
              });
            },
            label: "+",
            fontSize: MediaQuery.of(context).size.width * 0.06,
          ),
        ),
      ],
    );
  }

  void ekstraTopFiyatHesapla() {
    tekSecmeliOzelikList.forEach((kategori) {
      kategori.ekstraOzelik.forEach((ekstra) {
        if (ekstra.secilen && ekstra.mikCrp == 0) {
          ekstraStokTopFiy = ekstraStokTopFiy + ekstra.fiyat;
        } else if (ekstra.secilen && ekstra.mikCrp != 0) {
          ekstraStokTopFiy = ekstraStokTopFiy + (ekstra.fiyat * urunMiktar);
        }
      });
    });
    BFazlaSecmeliOzelikList.forEach((kategori) {
      kategori.ekstraOzelik.forEach((ekstra) {
        if (ekstra.secilen && ekstra.mikCrp == 0) {
          ekstraStokTopFiy = ekstraStokTopFiy + ekstra.fiyat;
        } else if (ekstra.secilen && ekstra.mikCrp != 0) {
          ekstraStokTopFiy = ekstraStokTopFiy + (ekstra.fiyat * urunMiktar);
        }
      });
    });
  }
}
