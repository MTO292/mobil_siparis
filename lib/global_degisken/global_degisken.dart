import '../splash/splash_ekrani.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../responsive_class/size_config.dart';
import '../yardimci_widgetlar/label_text/label_widget.dart';

/*BEYLIKKEBAP, menupost.bym.net.tr rest api dosyasının bulunduğu alandır...*/

const String key = "BEYLIKKEBAP";
//const String apiUrl = "http://menupos.bym.net.tr/MobilSiparis/";
//const String apiUrl = "http://cloud.bym.net.tr/MobilSiparis/";
//const String apiUrl = "http://192.168.1.69/WS_REST/MobilSiparis/";
//const String apiUrl = "http://demo5.bymyazilim.com/MobilSiparis/";
const String apiUrl = "http://mobilsiparis.bym.net.tr//MobilSiparis/";

/*resimUrl : Uygulam
ada gösterilen resimlerin geldi adres, testKey : Resimlerin geldiği Klasör adı*/
const String resimUrl = "https://gorsel.menupos.net/";

//Slider da gösterilern resimlerin linki
const String uyeGiris = "Uye";
const String uyeKayit = "Uye";
const String kodGonder = "GsmKodGonder";
const String uyeGuncelle = "Uye";
const String anaMenuListe = "AnaMenu";
const String subeBilgi = "SubeBilgileriGetir";
const String aramaListe = "AramaYap";
const String siparisEkle = "SiparisEkle";
const String siparisIlkSayfa = "SiparisIlkSayfa";
const String siparisListe = "SiparisListe";
const String siparisDetay = "SiparisDetay";
const String stokDetay = "StokDetay";
const String siparisIptal = "SiparisIptal";
const String adresSec = "AdresGetir";
const String sipLimitFiyKontrol = "sipLimitFiyKontrol";
const String versiyonKontrol = "VersiyonKontrol";
const String odemeTur = "OdemeTur";
const String calismaSaatleri = "CalismaSaatleri";

//sepette urun oldugu zaman Local Bildirimde Gösterilecek mesaj
const String localBildirimMesaj = "Sepetinizdeki ürünler sizi Bekliyor";
const String localBildirimBaslik = "Hatırlatma";

/*Uygulama içinde tek bir değişken tarafından kullanılan değerler.*/
class GlobalDegiskenler {
  static const Color butonArkaPlanRengi = Color.fromRGBO(0, 143, 165,1);
  static const Color baslikColor = Colors.black;
  static const Color appBarColor = Colors.white;
  static const Color backgroundColor = Colors.white;
  static const Color bottomMenuColor = Colors.white;
  static const Color appBarIconColor =  Color.fromRGBO(0, 143, 165,1);
  static const Color alertDialogButonArkaPlan =Color.fromRGBO(0, 143, 165,1);
  static const double containerKenarlik = 0.5;
  static const genelFontStyle = 'GenelFont';
  static const tlSimgesi = 'TL';
  static const Color miktarButonRenk = Colors.white;
  static final Color duzenlemeIcon = Color.fromRGBO(0, 143, 165,1);
  static const Color silmeIcon = Colors.red;
  static const Color splashColor = Colors.black12;
  static const Color progressBarColor = Colors.white;
  static const Color gysIconColor = Color.fromRGBO(0, 143, 165,1);

  /*Kutu şeklinde gözüken widgetin değerleri*/
  static final BoxDecoration Sablon = BoxDecoration(
    color: Colors.white,
    // border: Border.all(color: Colors.grey.withOpacity(0.5)),
    boxShadow: [
      BoxShadow(
        color: Colors.grey.withOpacity(0.3),
        spreadRadius: 0.5,
        blurRadius: 2,
        offset: Offset(0, 1),
      )
    ],
    borderRadius: BorderRadius.circular(8.0),
  );

  static final BoxDecoration kartSablon = BoxDecoration(
    color: Colors.white,
    border: Border.all(color: Colors.grey.withOpacity(0.5)),
    borderRadius: BorderRadius.circular(8.0),
    boxShadow: [
      BoxShadow(
        color: Colors.grey.withOpacity(0.3),
        spreadRadius: 0.5,
        blurRadius: 2,
        offset: Offset(0, 1),
      )
    ],
  );
}

//uygulama bilgileri
class AppInfo {
  static String versiyon = "";
  static String paketIsmi = "";
  static String uygulamaAdi = "Beylik Kebap";
  static String buildNumarasi = "";
  static const String appleStoreAppId = "1499450665";
}


//api ggelen değer durumları
class numarator {
  static const String basarili = "1";
  static const String hata = "2";
  static const String uygulamaVersiyonuEski = "15";
  static const String apiVersiyonuEski = "16";
  static const String hizmetDisi = "17";
  static const String smsHakki = "20";
}


/*Veriler yüklenirken gösterilecek olan ekran*/
progressDondur(
    {Color color,
      Color backgroundColor,
      int tur,
      double strokeWidth,
      AlignmentGeometry circleAlign}) {
  return tur == 1
      ? CircularProgressIndicator(
    strokeWidth: strokeWidth ?? 4,
    backgroundColor: backgroundColor ?? Colors.transparent,
    valueColor: AlwaysStoppedAnimation<Color>(
        color ?? GlobalDegiskenler.butonArkaPlanRengi),
  )
      : LinearProgressIndicator(
    minHeight: strokeWidth ?? 4,
    backgroundColor: backgroundColor ?? Colors.transparent,
    valueColor: AlwaysStoppedAnimation<Color>(
        color ?? GlobalDegiskenler.butonArkaPlanRengi),
  );
}
Widget formIcon(IconData iconData) {
  return Icon(
    iconData,
    color: GlobalDegiskenler.butonArkaPlanRengi,
    //  size: SizeConfig.safeBlockHorizontal * 5.3,
  );
}

//Ekranlarda bilgi verme sablonu
/*Sepette,sipariş kısmında listeler boşken gösterilecek olan ekran*/
Widget aciklama(String baslik, String altBaslik, IconData icon, {Color color}) {
  return Padding(
    padding: const EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 0),
    child: Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            LabelCard(
              alignment: Alignment.centerLeft,
              label: baslik,
              fontWeight: FontWeight.bold,
              fontSize: SizeConfig.safeBlockHorizontal * 5,
              color: color ?? Colors.black,
              satirSayisi: 1,
            ),
            SizedBox(
              width: 2,
            ),
            Icon(
              icon,
              color: GlobalDegiskenler.butonArkaPlanRengi,
              size: SizeConfig.safeBlockHorizontal * 6,
            ),
          ],
        ),
        SizedBox(
          height: SizeConfig.screenHeight * 0.008,
        ),
        LabelCard(
          alignment: Alignment.centerLeft,
          label: altBaslik,
          fontSize: SizeConfig.safeBlockHorizontal * 4,
          color: color ?? Colors.black,
          satirSayisi: 2,
        )
      ],
    ),
  );
}

//uygulama versyonu eski,WS versiyonu eski veya devre dışı olduğunda kullanılır
void logoEkraninaGit(BuildContext context) {
  Future.delayed(
      Duration.zero,
      () => Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(
              builder: (context) => SplashGoster(
                    versiyonKontrol: true,
                  )),
          (e) => false));
}

/*Sayfalar arası geçişlerde kullanılacak olan navigate ve animasyon kodları
Platform destekli olarak ayarlandı.*/
class Navigate {
  final int miliseconds;

  Navigate({this.miliseconds = 50});

  navigatePushNoAnimation(BuildContext context, Widget route) async {
    final gelenDeger = Navigator.push(context, FadeRoute(route));

    return gelenDeger;
  }
}

class NavigateAndRemoveUntil {
  final int miliseconds;

  NavigateAndRemoveUntil({this.miliseconds = 50});

  navigatePushNoAnimation(BuildContext context, Widget route) async {
    final gelenDeger = await Navigator.pushAndRemoveUntil(
        context, FadeRoute(route), (route) => false);
    return gelenDeger;
  }
}

class FadeRoute extends PageRouteBuilder {
  final Widget page;

  FadeRoute(this.page)
      : super(
          pageBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
          ) =>
              page,
          transitionsBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
            Widget child,
          ) =>
              FadeTransition(
            opacity: animation,
            child: child,
          ),
        );
}
