import 'package:flutter/material.dart';

///Sepetim sayfasındaki ekleme,listeleme ve sepeti tamamla butonlarını temsil eden sınıf
class MiktarButonlari extends StatefulWidget {
  final Color color;
  final double fontSize;
  final String label;
  final MediaQuery mediaQuery;
  final Function onPresssed;
  final double yukseklik;
  final double genislik;
  final Alignment alignment;
  MiktarButonlari({
    Key key,
    this.alignment,
    this.color,
    this.genislik,
    this.yukseklik,
    this.onPresssed,
    this.fontSize,
    this.label,
    this.mediaQuery,
  }) : super(key: key);

  @override
  _MiktarButonlariState createState() => _MiktarButonlariState();
}

class _MiktarButonlariState extends State<MiktarButonlari> {
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: widget.alignment,
      height: widget.yukseklik,
      width: widget.genislik,
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(6.0),
        ),
        color: widget.color,
        onPressed: widget.onPresssed,
        child: Text(
          widget.label,
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.black,
            fontSize: widget.fontSize,
            fontWeight: FontWeight.bold,
            fontFamily: 'TL',
          ),
        ),
      ),
    );
  }
}
