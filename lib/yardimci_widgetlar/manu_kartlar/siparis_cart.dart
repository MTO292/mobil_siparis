import '../../global_degisken/global_degisken.dart';
import '../../responsive_class/size_config.dart';
import '../../yardimci_widgetlar/label_text/label_widget.dart';
import 'package:flutter/material.dart';

///sipariş adımlarında kullanılan ürün kartı
class SiparisCart extends StatelessWidget
{
  String img;
  String name;
  String Fiy;
  String piece;
  String not;
  bool ekstra;
  Function notTop;
  Function ekstraTop;

  SiparisCart({
    Key key,
    @required this.img,
    @required this.name,
    @required this.Fiy,
    @required this.piece,
    @required this.not,
    @required this.ekstra,
    @required this.notTop,
    @required this.ekstraTop,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: SizeConfig.screenHeight * 0.01),
      child: Container(
        height: SizeConfig.screenHeight * 0.14,
        width: double.infinity,
        alignment: Alignment.centerLeft,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Flexible(
              flex: 4,
              child: AspectRatio(
                aspectRatio: 1,
                child: Container(
                  constraints: BoxConstraints.expand(),
                  decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage("assets/images/resim-yok.png"),
                        fit: BoxFit.contain,
                      ),
                      borderRadius: BorderRadius.all(Radius.circular(16))),
                  child: ClipRRect(
                    child: Image.network(
                      img,
                      fit: BoxFit.cover,
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(12)),
                  ),
                ),
              ),
            ),
            Flexible(
              flex: 7,
              child: Padding(
                padding: EdgeInsets.all(SizeConfig.screenWidth * 0.025),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    genelOzellikLabel(name,
                        fontSize: SizeConfig.safeBlockHorizontal * 3.5,
                        satirSayisi: 1),
                    ekstra ? Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        LabelCard(
                          satirSayisi: 3,
                          fontWeight: FontWeight.bold,
                          label: "Eklenen Ekstra ",
                          fontFamily: GlobalDegiskenler.tlSimgesi,
                          fontSize: SizeConfig.safeBlockHorizontal * 3.5,
                        ),
                        GestureDetector(
                          child: LabelCard(
                            satirSayisi: 3,
                            fontWeight: FontWeight.bold,
                            label: " Ürünler",
                            color: GlobalDegiskenler.butonArkaPlanRengi,
                            fontFamily: GlobalDegiskenler.tlSimgesi,
                            fontSize:
                            SizeConfig.safeBlockHorizontal * 3.5,
                          ),
                          onTap: ekstraTop,
                        ),
                      ],
                    ): SizedBox(),
                    not != "" ? Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        LabelCard(
                          satirSayisi: 3,
                          fontWeight: FontWeight.bold,
                          label: "Sipariş Notunu ",
                          fontFamily: GlobalDegiskenler.tlSimgesi,
                          fontSize: SizeConfig.safeBlockHorizontal * 3.5,
                        ),
                        GestureDetector(
                          child: LabelCard(
                            satirSayisi: 3,
                            fontWeight: FontWeight.bold,
                            label: "Görüntüle",
                            color: GlobalDegiskenler.butonArkaPlanRengi,
                            fontFamily: GlobalDegiskenler.tlSimgesi,
                            fontSize:
                            SizeConfig.safeBlockHorizontal * 3.5,
                          ),
                          onTap: notTop,
                        ),
                      ],
                    )
                        : SizedBox(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        genelOzellikLabel(piece + " Adet",
                            fontSize: SizeConfig.safeBlockHorizontal * 3.5),
                        LabelCard(
                          textAlign: TextAlign.center,
                          satirSayisi: 3,
                          fontWeight: FontWeight.bold,
                          label: Fiy + " ₺",
                          fontFamily: GlobalDegiskenler.tlSimgesi,
                          fontSize: SizeConfig.safeBlockHorizontal * 3.5,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget genelOzellikLabel(String value, {double fontSize, int satirSayisi}) {
    return LabelCard(
      fontFamily: GlobalDegiskenler.tlSimgesi,
      textAlign: TextAlign.center,
      satirSayisi: satirSayisi != null ? satirSayisi : 3,
      fontSize: fontSize,
      color: Colors.black,
      fontWeight: FontWeight.bold,
      label: value,
    );
  }
}
