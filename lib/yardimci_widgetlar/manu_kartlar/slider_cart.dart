import 'package:flutter/material.dart';

///ana ekranda kullanılan sliderin cartı
class SliderCart extends StatelessWidget {
  //final String name;
  final String img;
  final bool isFav;
  final Function onTap;

  SliderCart({
    Key key,
    //  @required this.name,
    @required this.img,
    @required this.isFav,
    @required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 16 / 9,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 4),
        child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(8)),
             // image: DecorationImage(image: AssetImage("assets/images/resim-yok.png")),
              boxShadow: [
                 BoxShadow(
                  color: Colors.grey.withOpacity(0.25),
                  spreadRadius: 1.0,
                  blurRadius: 2,
                ),
              ]),
          child: Stack(
            children: <Widget>[
              Positioned.fill(
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(8.0),
                  child:  Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      image: DecorationImage(image: AssetImage("assets/images/resim-yok.png",))
                    ),
                    child:  img != null ? Image.network(
                      img,
                      fit: BoxFit.cover,
                    ):Image.asset("assets/images/resim-yok.png",),
                  )
                ),
              ),
              Positioned.fill(
                  child: Material(
                      color: Colors.transparent,
                      child: InkWell(
                        highlightColor: Colors.white.withOpacity(0.1),
                        splashColor: Colors.transparent,
                        onTap: onTap,
                      )))
            ],
          ),
        ),
      ),
    );
  }
}