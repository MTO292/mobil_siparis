import '../../responsive_class/size_config.dart';
import 'package:flutter/material.dart';

///ürünleri sıralarken kullanılan ürün kartı
class UrunCart extends StatelessWidget {
  final String name;
  final String img;
  final String fiyat;
  final double imageSize;
  final Function onTap;
  final Function onTapSiperis;

  UrunCart({
    Key key,
    @required this.name,
    @required this.img,
    @required this.fiyat,
    @required this.imageSize,
    @required this.onTap,
    @required this.onTapSiperis,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(16)),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.1),
            spreadRadius:1,
            blurRadius: 1,
            offset: Offset(0, 1),
          )
        ],
      ),
      child: Material(
        color:Colors.transparent,
        child: InkWell(
          borderRadius: BorderRadius.all(
               Radius.circular(16),
          ),
          child: AspectRatio(
            aspectRatio: MediaQuery
                .of(context)
                .size
                .width /
                (MediaQuery
                    .of(context)
                    .size
                    .height / 1.4),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Flexible(
                  flex: 5,
                  child: ClipRRect(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(16),
                        topRight: Radius.circular(16)),
                    child: FadeInImage.assetNetwork(
                      height: imageSize,
                      fadeOutCurve: Curves.easeInCirc,
                      fit: BoxFit.cover,
                      imageScale: 1,
                      placeholder: "assets/images/resim-yok.png",
                      image: img,
                      // image: resimUrl+"kebapcibey/1.png",
                    ),
                  ),
                ),
                Flexible(
                  flex: 2,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 8, right: 8),
                    child: Text(
                      name,
                      style: TextStyle(
                        color: Color.fromRGBO(0, 0, 0, 0.7),
                        fontSize: SizeConfig.safeBlockVertical *
                            2,
                        fontWeight: FontWeight.w600,
                      ),
                      maxLines: 2,
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                Flexible(
                  flex: 1,
                  child: Text(
                    (fiyat) + " ₺",
                    style: TextStyle(
                      color: Color.fromRGBO(212, 170, 78, 1),
                      fontSize: SizeConfig.safeBlockVertical *
                          2.25,
                      fontWeight: FontWeight.w900,
                    ),
                    maxLines: 1,
                    textAlign: TextAlign.center,
                  ),
                ),
                Flexible(
                  flex: 2,
                  child: Padding(
                    padding: const EdgeInsets.only(
                        left: 8, right: 8, bottom: 8),
                    child: GestureDetector(
                      child: Container(
                        padding: EdgeInsets.all(8),
                        decoration: BoxDecoration(
                            border: Border.all(
                              width: 0.6,
                              color: Color.fromRGBO(0, 0, 0, 0.8),
                            ),
                            borderRadius: BorderRadius.circular(24)),
                        child: Text(
                          "Sipariş Ver",
                          style: TextStyle(
                            color: Color.fromRGBO(0, 0, 0, 0.7),
                            fontSize: SizeConfig.safeBlockVertical *
                                1.6,
                            fontWeight: FontWeight.w600,
                          ),
                          maxLines: 1,
                          textAlign: TextAlign.center,
                        ),
                      ),
                      onTap: onTapSiperis,
                    ),
                  ),
                ),
              ],
            ),
          ),
          onTap: onTap,),
      ),
    );
  }
}
