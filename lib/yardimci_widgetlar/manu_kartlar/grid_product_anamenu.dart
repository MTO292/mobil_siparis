
import '../../responsive_class/size_config.dart';
import 'package:flutter/material.dart';

class gridProductAnamenu extends StatelessWidget {
  final String name;
  final String img;
  final String fiyat;
  final Function onTop;

  gridProductAnamenu({
    Key key,
    @required this.name,
    @required this.img,
    @required this.fiyat,
    @required this.onTop,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(16)),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.3),
                spreadRadius: 0.5,
                blurRadius: 0.9,
                //  offset: Offset(0, 1),
              )
            ],
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              ClipRRect(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(16),
                    topRight: Radius.circular(16)),
                child: FadeInImage.assetNetwork(
                  height: MediaQuery.of(context).size.height / 6,
                  fadeOutCurve: Curves.easeInCirc,
                  fit: BoxFit.cover,
                  imageScale: 1,
                  placeholder: "assets/images/resim-yok.png",
                  image: img,
                  // image: resimUrl+"esmahall/1.png",
                ),
              ),
              Flexible(
                child: Padding(
                  padding: const EdgeInsets.only(left: 8, right: 8),
                  child: Center(
                    child: Text(
                      name,
                      style: TextStyle(
                        color: Color.fromRGBO(0, 0, 0, 0.7),
                        fontSize: SizeConfig.safeBlockHorizontal *
                            3.5,
                        fontWeight: FontWeight.w600,
                      ),
                      maxLines: 2,
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ),
            ],
          )),
      onTap: onTop,
    );
  }
}
