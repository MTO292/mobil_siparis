import '../../responsive_class/size_config.dart';
import 'package:flutter/material.dart';

///Menüleri sıralarken kullanılan Menü kartı
class MenuCart extends StatelessWidget {
  final String name;
  final String img;
  final Function onTap;

  MenuCart({
    Key key,
    @required this.name,
    @required this.img,
    @required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
        margin: EdgeInsets.fromLTRB(4, 4,4, 4),
      color: Colors.white,
      shadowColor: Colors.grey.shade100,
      elevation: 1.75,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(8))),
          child: Material(
            color: Colors.transparent,
            child: InkWell(
                borderRadius: BorderRadius.all(Radius.circular(8)),
                child: Row(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all( 10.0),
                    child: Container(
                      height: MediaQuery.of(context).size.width / 3.5,
                      width: MediaQuery.of(context).size.width / 3,
                      decoration: BoxDecoration(
                        //    border: Border.all(width: 1, color: Colors.black12),
                          borderRadius: BorderRadius.all(Radius.circular(8))),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(8.0),
                        child: FadeInImage.assetNetwork(
                          //image: "$img",
                          image:img,
                          fit: BoxFit.cover,
                          placeholder: "assets/images/resim-yok.png",
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Text(
                      name,
                      style: TextStyle(
                        color: Color.fromRGBO(0, 0, 0, 0.8),
                        fontSize: SizeConfig.safeBlockHorizontal *
                            4.5,
                        fontWeight: FontWeight.w500,
                      ),
                      maxLines: 2,
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
              onTap: onTap,
            ),
          ),
    );
  }
}
