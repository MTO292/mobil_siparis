import '../../responsive_class/size_config.dart';
import '../../yardimci_widgetlar/label_text/label_widget.dart';
import 'package:flutter/material.dart';

///Sipariş ekranlarında Sipariş aşamalarını gösteren üst barda cıkan stepperdir
class StepBar extends StatelessWidget {

  Color step1Color;
  Color step2Color;
  Color text1Color;
  Color text2Color;
  Color backgroundColor;
  Icon icon;

  StepBar({
  Key key,
  @required this.step1Color,
  @required this.step2Color,
  @required this.text1Color,
  @required this.text2Color,
  @required this.backgroundColor,
  @required this.icon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      color: backgroundColor,
      padding: EdgeInsets.symmetric(vertical: SizeConfig.
      screenHeight * 0.01),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          step(step1Color,text1Color,"Adres\nBilgileri"),
          Padding(
            padding: EdgeInsets.symmetric(
              horizontal: SizeConfig.screenWidth * 0.02,),
            child: icon,
          ),
          step(step2Color,text2Color,"Siparişi\nTamamla"),
        ],
      ),
    );
  }

  step(Color stepColor,Color textColor,String text) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10,
          vertical: 4),
      decoration: BoxDecoration(
        color: stepColor,
        //border: Border.all(color: Colors.grey.withOpacity(0.5)),
        borderRadius: BorderRadius.circular(14.0),
      ),
      child: LabelCard(
        fontSize: SizeConfig.safeBlockHorizontal * 2.3,
        textAlign: TextAlign.center,
        color: textColor,
        fontWeight: FontWeight.bold,
        label: text,
      ),
    );
  }
}
