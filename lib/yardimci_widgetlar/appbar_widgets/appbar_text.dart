import '../../global_degisken/global_degisken.dart';
import 'package:flutter/material.dart';
import '../../responsive_class/size_config.dart';

///Bu sınıf, kullanıcı login olduğunda üst menüde başlık yazdırmak için yapıldı
class BaslikText extends StatelessWidget {
  final Color color;
  final MediaQuery mediaQuery;
  final Padding padding;
  final Alignment alignment;
  final TextStyle textStyle;
  final String label;
  final double soldanBosluk;
  final double iconSize;
  final FontWeight fontWeight;
  final double fontSize;
  BaslikText(
      {Key key,
      this.color,
      this.soldanBosluk,
      this.label,
      this.fontSize,
      this.iconSize,
      this.fontWeight,
      this.mediaQuery,
      this.textStyle,
      this.alignment,
      this.padding})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      child: Text(
        this.label,
        style: TextStyle(
          fontFamily: GlobalDegiskenler.genelFontStyle,
          fontSize: SizeConfig.safeBlockHorizontal * 5.5,
          fontWeight: FontWeight.bold,
          color: GlobalDegiskenler.baslikColor,
        ),
      ),
    );
  }
}
