import 'package:flutter/material.dart';

///Bu sınıf, kullanıcı login olduğunda gereken sayfalara icon yazdırmak için yapıldı
class AppbarIcon extends StatelessWidget {
  final Alignment alignment;
  final Padding padding;
  final double iconSize;
  final MediaQuery mediaQuery;
  final Color color;
  final Icon icon;
  final Function onPressed;
  final String toolTip;
  AppbarIcon(
      {Key key,
      this.alignment,
      this.icon,
      this.iconSize,
      this.toolTip,
      this.onPressed,
      this.padding,
      this.mediaQuery,
      this.color})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      child: IconButton(
        tooltip: this.toolTip,
        iconSize: this.iconSize,
        onPressed: this.onPressed,
        icon: this.icon,
        color: Color.fromRGBO(0, 116, 117, 1),
      ),
    );
  }
}
