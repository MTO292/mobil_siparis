import 'package:flutter/material.dart';

enum OpenAction{longPress,tap,lock}

/// Liste karta tıklandığı zaman çıkan menüde kullanılmakta
class FocusedMenuItem {
    Color backgroundColor;
    Widget title;
    Icon trailingIcon;
    Function onPressed;

    FocusedMenuItem({this.backgroundColor,@required this.title, this.trailingIcon,@required this.onPressed});
}