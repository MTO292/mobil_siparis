import '../../global_degisken/global_degisken.dart';
import '../../responsive_class/size_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

///TextForm fieldin kullanılacağı yerlerde base sınıf olarak yazıldı
class MyTextFormField extends StatelessWidget {
  final String label;
  final TextInputType textInputType;
  final TextStyle textStyle;
  final Function validatorFonksiyon;
  final Function onsavedGelenDeger;
  final TextEditingController kontroller;
  final int satirSayisi;
  final String kaydedilecekDeger;
  final bool password;
  final int karakterSayisi;
  final FontWeight fontWeight;
  final TextInputAction ileriTusu;
  final IconButton sifreyiGoster;
  final Function onFieldSubmitted;
  final Function onChange;
  final FocusNode focusNode;
  final bool autoFocus;
  final bool autoValidate;
  final Function onTap;
  final String labelText;
  final Icon prefixIcon;
  final Color enabledColor;
  final Color focusedColor;
  final EdgeInsets contentPadding;
  final Widget icon;

  MyTextFormField(
      {Key key,
        this.label,
        this.focusNode,
        this.onChange,
        this.onFieldSubmitted,
        this.ileriTusu,
        this.sifreyiGoster,
        this.textStyle,
        this.fontWeight,
        this.textInputType,
        this.kaydedilecekDeger,
        this.password,
        this.kontroller,
        this.satirSayisi,
        this.validatorFonksiyon,
        this.karakterSayisi,
        this.onsavedGelenDeger,
        this.autoFocus,
        this.autoValidate,
        this.onTap,
        this.labelText,
        this.prefixIcon,
        this.icon,
        this.enabledColor,
        this.contentPadding,
        this.focusedColor,})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return TextFormField(
      textAlign: TextAlign.start,
      onTap: this.onTap,
      autovalidate: this.autoValidate ?? false,
      autofocus: this.autoFocus ?? false,
      focusNode: this.focusNode,
      onChanged: this.onChange,
      onFieldSubmitted: this.onFieldSubmitted,
      textInputAction: this.ileriTusu,
      obscureText: this.password ?? false,
      maxLines: this.satirSayisi,
      maxLength: this.karakterSayisi,
      controller: this.kontroller,
      validator: this.validatorFonksiyon,
      onSaved: this.onsavedGelenDeger,
      style: this.textStyle,
      keyboardType: this.textInputType,
      decoration: InputDecoration(
       // counterStyle: TextStyle(inherit: false,),
        contentPadding: contentPadding,
        suffixIcon: this.sifreyiGoster,
        icon: this.icon,
        hintText: this.label,
        labelText: this.labelText,
        fillColor: Colors.transparent,
        filled: true,
        labelStyle: TextStyle(
            color: Colors.black,
            fontFamily: GlobalDegiskenler.tlSimgesi,
            fontSize: SizeConfig.safeBlockHorizontal * 4),
        prefixIcon: this.prefixIcon,
        hintStyle: TextStyle(fontWeight: this.fontWeight),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(
              color: enabledColor ?? Colors.black,
              width: 0.5),
          borderRadius: BorderRadius.circular(4,
          ),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(
              color: focusedColor ?? Colors.black,
              width: 0.5),
          borderRadius: BorderRadius.circular(4,
          ),
        ),
        focusedErrorBorder: OutlineInputBorder(
          borderSide: BorderSide(
              color: Colors.red,
              width: 1),
          borderRadius: BorderRadius.circular(4,
          ),
        ),
        errorBorder: OutlineInputBorder(
          borderSide: BorderSide(
              color: Colors.red,
              width: 1),
          borderRadius: BorderRadius.circular(4,
          ),
        ),
        disabledBorder: OutlineInputBorder(
          borderSide: BorderSide(
              color: Colors.black,
              width: 1),
          borderRadius: BorderRadius.circular(4,
          ),
        ),
      ),
    );
  }
}
