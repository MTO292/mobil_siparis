import 'package:flutter/material.dart';
import '../../global_degisken/global_degisken.dart';
import '../../responsive_class/size_config.dart';
import '../../yardimci_widgetlar/label_text/label_widget.dart';

///Ürün profil ekranında ve sipariş adımlarda kullanılan alt bardaki buton ve fiyattır
class AltBarButon extends StatelessWidget {
  Function onTap;

  double tutar;

  String butontTxt;

  AltBarButon(
      {@override this.onTap, @override this.tutar, @override this.butontTxt});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Container(
          height: SizeConfig.screenHeight * 0.09,
          width: SizeConfig.screenWidth,
          decoration: BoxDecoration(color: Colors.white, boxShadow: [
            BoxShadow(
              color: Colors.black12.withOpacity(0.1),
              spreadRadius: 0.5,
              blurRadius: 20,
              offset: Offset(0, 0),
            ),
          ]),
          child: Padding(
            padding: EdgeInsets.symmetric(
              horizontal: SizeConfig.screenWidth * 0.04,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                /*Toplam tutar ve fiyat label*/
                Container(
                  alignment: Alignment.center,
                  height: SizeConfig.screenHeight * 0.07,
                  width: SizeConfig.screenWidth * 0.36,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(8),
                          bottomLeft: Radius.circular(8)),
                      boxShadow: [
                        BoxShadow(
                          color: GlobalDegiskenler.butonArkaPlanRengi
                              .withOpacity(0.5),
                          spreadRadius: 0.5,
                          blurRadius: 5,
                          offset: Offset(0, 3),
                        ),
                      ]),
                  child: Center(
                    child: LabelCard(
                        fontFamily: GlobalDegiskenler.tlSimgesi,
                        color: GlobalDegiskenler.butonArkaPlanRengi,
                        padding: EdgeInsets.only(top: 1.0),
                        label: tutar.toStringAsFixed(2) + " ₺",
                        fontWeight: FontWeight.w600,
                        fontSize: SizeConfig.safeBlockHorizontal * 4.6),
                  ),
                ),

                /*Sepeti tamamla butonu*/
                Container(
                  height: SizeConfig.screenHeight * 0.07,
                  width: SizeConfig.screenWidth * 0.56,
                  child: RaisedButton(
                    elevation: 5,
                    splashColor: GlobalDegiskenler.splashColor,
                    color: GlobalDegiskenler.butonArkaPlanRengi,
                    onPressed: onTap,
                    /*Sepeti tamamla label*/
                    child: Tooltip(
                      message: 'Sepeti kayıt eder',
                      child: LabelCard(
                          fontFamily: GlobalDegiskenler.tlSimgesi,
                          satirSayisi: 2,
                          label: butontTxt,
                          color: Colors.white,
                          fontSize: SizeConfig.safeBlockHorizontal * 4),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
