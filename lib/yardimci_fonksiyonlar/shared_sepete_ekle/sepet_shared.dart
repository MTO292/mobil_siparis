import 'dart:convert';
import '../../api/model/sepet_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

///Shared preferences kullanarak sepete ürün eklemeye yarar.
class SharedPrefLib {
/*Sepete eklenen ürünleri yazdırmak için kullanılan metod */
  static Future<void> sepetiYazdir(List<Urun> sepetim) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs = await SharedPreferences.getInstance();
    String urunJsonString = prefs.getString("sepettekiUrunlerim") ?? "";
    if (urunJsonString.isNotEmpty) {
      sepetim = List.from(jsonDecode(urunJsonString))
          .map((str) => Urun.fromJson(str))
          .toList();
      print("Toplam ürün sayısı : ${sepetim.length}");
      sepetim.forEach((v) {
        print(v);
      });
    } else {
      print("Sepetiniz boş");
    }
  }

  /*Sepetim sayfası girişinde ürünleri listeye atamak için kullanılan metod*/
  static Future<List<Urun>> listeyiGetir() async {
    List<Urun> sepetim = [];
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String result = prefs.getString("sepettekiUrunlerim") ?? "";
    if (result.isNotEmpty) {
      sepetim = List.from(jsonDecode(result))
          .map((obj) => Urun.fromJson(obj))
          .toList();
    }
    return sepetim;
  }

  static Future<void> sharedTemizle() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.clear();
  }

  /*Bazı kayıtları telefon hafızasından siliyoruz*/
  static Future<void> sharedSepetTemizle() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('sepettekiUrunlerim');
    prefs.remove('spnot');
    prefs.remove('adrsId');
    prefs.remove('odeTur');
    prefs.remove('spradrs');
  }

  /*Sepete eklenen ürünleri temizlemek için kullanılan metod */
  static Future<void> sepetTemizle() async {
    SharedPreferences shared = await SharedPreferences.getInstance();
    if (shared.getString('sepettekiUrunlerim') != null) {
      shared.remove('sepettekiUrunlerim');
    }
  }

  /*Sepetten ürün silerken kullanıyoruz*/
  static Future<void> urunSil(List<Urun> urun) async {
    SharedPreferences shared = await SharedPreferences.getInstance();
    shared.setString('sepettekiUrunlerim', json.encode(urun));
  }

  /*Sepete liste olarak ürün ekliyor*/
  static Future<void> urunEkle(List<Urun> urun) async {
    SharedPreferences shared = await SharedPreferences.getInstance();
    shared.setString('sepettekiUrunlerim', json.encode(urun));
  }

  /*Genel sipariş notu ekler*/
  static Future<void> spNot(String spnot) async {
    SharedPreferences shared = await SharedPreferences.getInstance();
    shared.setString('spnot', spnot ?? "");
  }
  /*Genel sipariş notu siler*/
  static Future<void> spNotSil() async {
    SharedPreferences shared = await SharedPreferences.getInstance();
    shared.remove('spnot');
  }

  /*Siparis adresini hafızada tutuyoruz*/
  static Future<void> siparisAdres(String spradrs) async {
    SharedPreferences shared = await SharedPreferences.getInstance();
    shared.setString('spradrs', spradrs ?? "");
  }

  /*Ödeme türü idsini tutacak olan shared kodu*/
  static Future<void> odemeTur(int secenek) async {
    SharedPreferences shared = await SharedPreferences.getInstance();
    shared.setInt('odeTur', secenek ?? "");
  }

  /*Vt'ye eklenecek olan adresin idsini tutacak olan shared kodu*/
  static Future<void> adrsId(int adrsId) async {
    SharedPreferences shared = await SharedPreferences.getInstance();
    shared.setInt('adrsId', adrsId ?? "");
  }

  /*Cari idsini getirir*/
  static Future<int> cariId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt('cariIdDonus');
  }

  static SharedPreferences _prefs2;
  static initialize() async {
    if (_prefs2 != null) return;
    _prefs2 = await SharedPreferences.getInstance();
  }

  static String get cariKod => _prefs2.getString('cariKodDonus');
  static String get eskiSifre => _prefs2.getString('cariSifreDonus');
  static bool get girisYapildi => _prefs2.getBool('girisYapildi') ?? false;
  static bool get uygulamaGuncelledi => _prefs2.getBool('uygulamaGuncellendi') ?? true;
  static bool get ilkGiris => _prefs2.getBool("ilkGiris") ?? false;
  static bool get sliderAktifMi => _prefs2.getBool('sliderAktifMi') ?? false;
}
