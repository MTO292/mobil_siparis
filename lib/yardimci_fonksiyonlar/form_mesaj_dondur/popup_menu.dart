import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../global_degisken/global_degisken.dart';
import '../../responsive_class/size_config.dart';
import '../../yardimci_widgetlar/label_text/label_widget.dart';

///Ürün için Not ekleme popupu
class Popup {
  void popupMenu(
      {BuildContext context, Widget widget, String baslik, Function kaydet}) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Platform.isAndroid ? _alertDialog(context, widget, baslik, kaydet) : _cupertinoAlertDialog(context, widget, baslik, kaydet);
        });
  }

  ///ios
  _cupertinoAlertDialog(
      BuildContext context, Widget widget, String baslik, Function kaydet) {
    return CupertinoAlertDialog(
      title: Column(
        children: <Widget>[
          LabelCard(
              fontFamily: GlobalDegiskenler.tlSimgesi,
              label: baslik,
              color: Colors.black,
              satirSayisi: 2,
              textAlign: TextAlign.center,
              fontSize: SizeConfig.safeBlockHorizontal * 4),
          SizedBox(height: 8,)
        ],
      ),
      content: widget,
      actions: <Widget>[
        CupertinoDialogAction(
          child: LabelCard(
            fontFamily: GlobalDegiskenler.tlSimgesi,
            label: 'Kaydet',
            textAlign: TextAlign.center,
            fontWeight: FontWeight.w500,
            color: GlobalDegiskenler.butonArkaPlanRengi,
            fontSize: SizeConfig.safeBlockHorizontal * 5,
          ),
          onPressed: kaydet,
        ),
        CupertinoDialogAction(
          child: LabelCard(
            fontFamily: GlobalDegiskenler.tlSimgesi,
            label: 'İptal',
            color: GlobalDegiskenler.butonArkaPlanRengi,
            fontSize: SizeConfig.safeBlockHorizontal * 5,
            fontWeight: FontWeight.w500,
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ],
    );
  }

  ///android
  _alertDialog(
      BuildContext context, Widget widget, String baslik, Function kaydet) {
    return AlertDialog(
      contentPadding: EdgeInsets.fromLTRB(15.0, 20.0, 15.0, 24.0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      title: LabelCard(
          fontFamily: GlobalDegiskenler.tlSimgesi,
          label: baslik,
          color: Colors.black,
          satirSayisi: 2,
          textAlign: TextAlign.center,
          fontSize: SizeConfig.safeBlockHorizontal * 5),
      content: widget,
      actions: <Widget>[
        FlatButton(
          disabledColor: Colors.red,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8.0),
          ),
          child: LabelCard(
            label: 'Kaydet',
            textAlign: TextAlign.center,
            color: GlobalDegiskenler.butonArkaPlanRengi,
            fontSize: SizeConfig.safeBlockHorizontal * 5,
          ),
          onPressed: kaydet,
        ),
        RaisedButton(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8.0),
          ),
          color: GlobalDegiskenler.butonArkaPlanRengi,
          child: LabelCard(
            fontFamily: GlobalDegiskenler.tlSimgesi,
            label: 'İptal',
            color: Colors.white,
            fontSize: SizeConfig.safeBlockHorizontal * 4.3,
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ],
    );
  }
}
