import 'package:flutter/material.dart';
import '../../global_degisken/global_degisken.dart';
import '../../responsive_class/size_config.dart';
import 'package:progress_dialog/progress_dialog.dart';

///uygulamada kullanılan progress loading ekranı
class progress {
  void prBaslat(BuildContext context, String mesaj) {
    var _pr = ProgressDialog(context,
        isDismissible: false, type: ProgressDialogType.Normal);
    _pr.style(
      backgroundColor: GlobalDegiskenler.progressBarColor,
      progress: 4.0,
      message: mesaj,
      progressTextStyle: TextStyle(color: Colors.black),
      borderRadius: 15.0,
      elevation: 6.0,
      insetAnimCurve: Curves.easeIn,
      messageTextStyle: TextStyle(
        fontFamily: GlobalDegiskenler.tlSimgesi,
        fontSize: SizeConfig.safeBlockHorizontal * 5,
        color: Colors.black,
      ),
    );

    _pr.show();
  }

  void prBitir(BuildContext context) {
    var _pr = ProgressDialog(context);
    _pr.hide();
  }

  void prUpdate(BuildContext context, String mesaj) {
    var _pr = ProgressDialog(context,
        isDismissible: true, type: ProgressDialogType.Normal);
    _pr.update(
      progressWidget: Text(mesaj),
      progress: 4.0,
      message: mesaj,
      messageTextStyle: TextStyle(
        fontSize: SizeConfig.safeBlockHorizontal * 5,
        color: Colors.black,
      ),
    );
  }
}
