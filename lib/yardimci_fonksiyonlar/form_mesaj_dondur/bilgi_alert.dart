import 'package:flutter/material.dart';
import '../../api/model/calisma_saat_model.dart';
import '../../responsive_class/size_config.dart';
import '../../yardimci_fonksiyonlar/sayfa_kadirma_efekti/scrool.dart';
import '../../yardimci_widgetlar/label_text/label_widget.dart';

///ana ekrandaki saat bilgisini gösteren popup
Widget bilgiAlert(BuildContext context, Widget widget, List<Saat> cSaatBilgi) {
  return ScrollConfiguration(
    behavior: MyBehavior(),
    child: Container(
      alignment: Alignment.center,
      height: SizeConfig.safeBlockVertical * 55,
      width: SizeConfig.safeBlockVertical * 50,
      child: ListView.builder(
        physics: ScrollBehavior().getScrollPhysics(context),
        itemCount: cSaatBilgi.length,
        itemBuilder: (context, index) {
          return ListTile(
            onLongPress: () {
              Navigator.of(context).pop();
            },
            title: LabelCard(
              color: Colors.white,
              label: cSaatBilgi[index].gun.toString(),
              fontSize: SizeConfig.safeBlockHorizontal * 4.3,
            ),
            subtitle: LabelCard(
              fontSize: SizeConfig.safeBlockHorizontal * 3.9,
              satirSayisi: 4,
              label: cSaatBilgi[index].durum == "1"
                  ? cSaatBilgi[index].baslangic +
                      " - " +
                      cSaatBilgi[index].bitis
                  : "\nBugün online sipariş departmanımız çalışmamaktadır.\n",
              color: cSaatBilgi[index].durum == "1"
                  ? Colors.white.withOpacity(0.7)
                  : Colors.yellowAccent,
              //color: Colors.white,
            ),
          );
        },
      ),
    ),
  );
}
