import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../global_degisken/global_degisken.dart';
import '../../responsive_class/size_config.dart';
import '../../yardimci_widgetlar/label_text/label_widget.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:line_awesome_icons/line_awesome_icons.dart';
import '../../global_degisken/global_degisken.dart' as global;

enum SoruTipi { Hata, Uyari, Soru, Basarili, NotGoster }

///Bu sınıf, ekrana mesaj döndürmek amacı ile yazılmıştır.
///Fonksiyon olarak gönderilen ontap nesnesi, bu metodu farklı sayfalarda farklı zamanlarda kullanabilmek için yapıldı
///yönlendirme veya aynı sayfada kalabilmek için fonksiyon tanımlandı.
///Platforma göre iki ayrılmaktadır
class MesajGoster {

  static Future toastMesaj(String message) async {
    Fluttertoast.showToast(
        timeInSecForIos: 1,
        msg: message,
        backgroundColor: Color.fromRGBO(0, 0, 0, 0.7),
        gravity: ToastGravity.CENTER,
        toastLength: Toast.LENGTH_SHORT,
        fontSize: 17.0,
        textColor: Colors.white);
  }

  static Future mesajGoster(
      BuildContext context, String icerik, SoruTipi soruTipi,
      {bool butonGorunurluk}) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return Platform.isAndroid
            ? _mesaj(context, icerik, soruTipi, butonGorunurluk: butonGorunurluk)
            : _cupertinoMesaj(context, icerik, soruTipi,
            butonGorunurluk: butonGorunurluk);
      },
    );
  }


  /*Evet butonu altında fonksiyon çalışan alert dialog*/
  static soruSor(BuildContext context, String icerik, SoruTipi soruTipi,
      {Function evet, String icerikEvet}) {
    return showDialog(
      barrierDismissible: true,
      context: context,
      builder: (BuildContext context) {
        return Platform.isAndroid
            ? _soru(context, icerik, soruTipi,
            evet: evet, icerikEvet: icerikEvet)
            : _cupertinoSoru(context, icerik, soruTipi,
            evet: evet, icerikEvet: icerikEvet);
      },
    );
  }

  ///Ios
  static _cupertinoMesaj(BuildContext context, String icerik, SoruTipi soruTipi,
      {bool butonGorunurluk}) {
    return CupertinoAlertDialog(
      title: Row(
        children: <Widget>[
          LabelCard(
            alignment: Alignment.centerLeft,
            label: soruTipi.index == 0
                ? 'Hata'
                : soruTipi.index == 1
                ? 'Uyarı'
                : soruTipi.index == 2 ? 'Soru' :soruTipi.index == 3 ?  'Başarılı' : 'Not',
            fontWeight: FontWeight.bold,
            fontSize: SizeConfig.safeBlockHorizontal * 5,
            color: Colors.black,
            satirSayisi: 1,
          ),
          SizedBox(
            width: 4,
          ),
          Icon(
            soruTipi.index == 0
                ? Icons.error_outline
                : soruTipi.index == 1
                ? Icons.warning
                : soruTipi.index == 2 ? LineAwesomeIcons.question_circle :  soruTipi.index == 3 ? Icons.done: null,
            color: GlobalDegiskenler.butonArkaPlanRengi,
            size: SizeConfig.safeBlockHorizontal * 6,
          ),
          SizedBox(
            height: 5,
          ),
        ],
      ),
      content: LabelCard(
        label: icerik,
        textAlign: TextAlign.start,
        satirSayisi: 8,
        fontSize: SizeConfig.safeBlockHorizontal * 4,
      ),
      actions: <Widget>[
        butonGorunurluk != false
            ? CupertinoDialogAction(
          onPressed: () {
            Navigator.pop(context);
          },
          child: LabelCard(
            fontFamily: GlobalDegiskenler.tlSimgesi,
            label: 'Tamam',
            textAlign: TextAlign.center,
            color: global.GlobalDegiskenler.butonArkaPlanRengi,
            fontSize: SizeConfig.safeBlockHorizontal * 4.3,
            fontWeight: FontWeight.w800,
          ),
        )
            : SizedBox(),
      ],
    );
  }

  ///Android
  static _mesaj(BuildContext context, String icerik, SoruTipi soruTipi,
      {bool butonGorunurluk}) {
    return AlertDialog(
      contentPadding: EdgeInsets.fromLTRB(24.0, 10.0, 24.0, 10.0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8.0),
      ),
      title: Row(
        children: <Widget>[
          LabelCard(
            alignment: Alignment.centerLeft,
            label: soruTipi.index == 0
                ? 'Hata'
                : soruTipi.index == 1
                ? 'Uyarı'
                : soruTipi.index == 2 ? 'Soru' :soruTipi.index == 3 ?  'Başarılı' : 'Not',
            fontWeight: FontWeight.bold,
            fontSize: SizeConfig.safeBlockHorizontal * 5,
            color: Colors.black,
            satirSayisi: 1,
          ),
          SizedBox(
            width: 4,
          ),
          Icon(
            soruTipi.index == 0
                ? Icons.error_outline
                : soruTipi.index == 1
                ? Icons.warning
                : soruTipi.index == 2 ? LineAwesomeIcons.question_circle :  soruTipi.index == 3 ? Icons.done: null,
            color: GlobalDegiskenler.butonArkaPlanRengi,
            size: SizeConfig.safeBlockHorizontal * 6,
          ),
        ],
      ),
      content: LabelCard(
        label: icerik,
        textAlign: TextAlign.start,
        satirSayisi: 8,
        fontSize: SizeConfig.safeBlockHorizontal * 4,
      ),
      actions: <Widget>[
        butonGorunurluk != false
            ? FlatButton(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8.0),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
          child: LabelCard(
            fontFamily: GlobalDegiskenler.tlSimgesi,
            label: 'Tamam',
            textAlign: TextAlign.center,
            color: global.GlobalDegiskenler.butonArkaPlanRengi,
            fontSize: SizeConfig.safeBlockHorizontal * 4.3,
            fontWeight: FontWeight.w800,
          ),
        )
            : SizedBox(),
      ],
    );
  }

  ///Android
  static _soru(BuildContext context, String icerik, SoruTipi soruTipi,
      {Function evet, String icerikEvet}) {
    return AlertDialog(
      elevation: 8,
      contentPadding: EdgeInsets.fromLTRB(24.0, 10.0, 24.0, 10.0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8.0),
      ),
      title: Row(
        children: <Widget>[
          LabelCard(
            alignment: Alignment.centerLeft,
            label: soruTipi.index == 0
                ? 'Hata'
                : soruTipi.index == 1
                ? 'Uyarı'
                : soruTipi.index == 2 ? 'Soru' : 'Başarılı',
            fontWeight: FontWeight.bold,
            fontSize: SizeConfig.safeBlockHorizontal * 5,
            color: Colors.black,
            satirSayisi: 1,
          ),
          Icon(
            soruTipi.index == 0
                ? Icons.error_outline
                : soruTipi.index == 1
                ? Icons.warning
                : soruTipi.index == 2
                ? LineAwesomeIcons.question_circle
                : Icons.done,
            color: GlobalDegiskenler.butonArkaPlanRengi,
            size: SizeConfig.safeBlockHorizontal * 6,
          ),
        ],
      ),
      content: LabelCard(
        label: icerik,
        textAlign: TextAlign.start,
        satirSayisi: 8,
        fontSize: SizeConfig.safeBlockHorizontal * 4,
      ),
      actions: <Widget>[
        /*Tamam butonu*/
        FlatButton(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8.0),
          ),
          onPressed: evet,
          child: LabelCard(
            label: icerikEvet ?? 'Evet',
            textAlign: TextAlign.center,
            color: global.GlobalDegiskenler.butonArkaPlanRengi,
            fontSize: SizeConfig.safeBlockHorizontal * 5,
          ),
        ),
        /*Hayır butonu*/
        RaisedButton(
          color: global.GlobalDegiskenler.butonArkaPlanRengi,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8.0),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
          child: LabelCard(
            fontFamily: GlobalDegiskenler.tlSimgesi,
            label: 'Hayır',
            fontSize: SizeConfig.safeBlockHorizontal * 4.3,
            color: Colors.white,
          ),
        ),
      ],
    );
  }

  ///Ios
  static _cupertinoSoru(BuildContext context, String icerik, SoruTipi soruTipi,
      {Function evet, String icerikEvet}) {
    return CupertinoAlertDialog(
      title: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              LabelCard(
                alignment: Alignment.centerLeft,
                label: soruTipi.index == 0
                    ? 'Hata'
                    : soruTipi.index == 1
                    ? 'Uyarı'
                    : soruTipi.index == 2 ? 'Soru' : 'Başarılı',
                fontWeight: FontWeight.bold,
                fontSize: SizeConfig.safeBlockHorizontal * 5,
                color: Colors.black,
                satirSayisi: 1,
              ),
              Icon(
                soruTipi.index == 0
                    ? Icons.error_outline
                    : soruTipi.index == 1
                    ? Icons.warning
                    : soruTipi.index == 2
                    ? LineAwesomeIcons.question_circle
                    : Icons.done,
                color: GlobalDegiskenler.butonArkaPlanRengi,
                size: SizeConfig.safeBlockHorizontal * 6,
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
        ],
      ),
      content: LabelCard(
        label: icerik,
        textAlign: TextAlign.start,
        satirSayisi: 8,
        fontSize: SizeConfig.safeBlockHorizontal * 4,
      ),
      actions: <Widget>[
        CupertinoDialogAction(
          onPressed: evet,
          child: LabelCard(
            fontFamily: GlobalDegiskenler.tlSimgesi,
            label: icerikEvet ?? 'Evet',
            color: Colors.red,
            fontWeight: FontWeight.w500,
            fontSize: SizeConfig.safeBlockHorizontal * 5,
          ),
        ),
        /*Hayır butonu*/
        CupertinoDialogAction(
          onPressed: () {
            Navigator.pop(context);
          },
          child: LabelCard(
            fontFamily: GlobalDegiskenler.tlSimgesi,
            label: 'Hayır',
            fontWeight: FontWeight.w500,
            fontSize: SizeConfig.safeBlockHorizontal * 5,
            color: global.GlobalDegiskenler.butonArkaPlanRengi,
          ),
        ),
      ],
    );
  }
}
