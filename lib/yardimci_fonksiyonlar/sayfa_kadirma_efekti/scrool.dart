import 'package:flutter/cupertino.dart';

///Bu class, sayfalarda kaydırma efekti yapmaya çalışırken yukarıda ve aşağıda çıkan
///efekti kaldırmak için yazıldı

class MyBehavior extends ScrollBehavior {
  @override
  Widget buildViewportChrome(
      BuildContext context, Widget child, AxisDirection axisDirection) {
    return child;
  }
}