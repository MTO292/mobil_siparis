///Bu sınıf, form işlemlerinde yapılan kontrollerin tek bir sayfadan yönetilmesi için yazılmıştır
class RegexKontrol {
  static String isimKontrol(String value) {
    value = value.trim();
    String isim = (r'\d');
    RegExp regExp = new RegExp(isim);
    if (value.length == 0) {
      return "Ad Soyad giriniz.";
    } else if (value.length <= 5 && value.length <= 40) {
      return "6 - 40 karakter arasında olmalıdır";
    } else if (regExp.hasMatch(value)) {
      return "Harf olmalıdır";
    }
    return null;
  }

  static String telefonKontrol(String value) {
    String patttern = r'(^[0-9]*$)';
    RegExp regExp = new RegExp(patttern);
    if (value.length == 0) {
      return "Telefon numaranızı giriniz.";
    } else if (value.startsWith("0")) {
      return "Telefon numaranızın başlığını '0' olmadan yazınız.";
    }  else if (value.length != 10) {
      return "Telefon numaranız 10 karakter olmalıdır";
    }else if (!regExp.hasMatch(value)) {
      return "Geçersiz telefon numarası.";
    } else if (regExp.hasMatch(r'(/^[a-z]+$/)')) {
      return "Geçersiz telefon numarası.";
    }
    return null;
  }

  static String mailKontrol(String deger) {
    String tasarim =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regExp = new RegExp(tasarim);
    if (deger.length == 0) {
      return null;
    } else if (!regExp.hasMatch(deger)) {
      return "Geçersiz mail adresi";
    } else {
      return null;
    }
  }

  static String adresKontrol(String deger) {
    if (deger.contains("!'^^^+%&/()=?_-*"))
      return "Farklı karakter içermemelidir";
    else if (deger.trim().length <= 14 && deger.trim().length <= 60)
      return "15 - 60 karakter arasında olmalıdır";
    else
      return null;
  }

  static String adresAdiKontrol(String deger) {
    if (deger.contains("!'^^^+%&/()=?_-*"))
      return "Farklı karakter içermemelidir";
    else if (deger.trim().length <= 0)
      return "Adres adı giriniz";
    else
      return null;
  }

  static String sifreKontrol(String deger) {
    if (deger.isEmpty) {
      return "Şifre alanını boş bırakmayınız.";
    } else if (deger.length <= 4 && deger.length <= 20) {
      return "Şifre 5-20 karakter arasında olmalıdır";
    } else {
      return null;
    }
  }
}
