import 'dart:io';
import 'package:flutter/cupertino.dart';
import '../anamenu/ana_menu.dart';
import '../anamenu/urun_profil.dart';
import '../api/response/islem_donus.dart';
import 'package:connectivity/connectivity.dart';
import 'package:connectivity_wrapper/connectivity_wrapper.dart';
import '../api/model/menu_model.dart';
import '../bloc/blocs_exports.dart';
import '../giris/stepper_giris.dart';
import 'package:flutter/material.dart';
import '../api/model/sepet_model.dart';
import '../global_degisken/global_degisken.dart';
import '../profil/adres_listele/adres_ana.dart';
import '../responsive_class/size_config.dart';
import '../yardimci_fonksiyonlar/form_mesaj_dondur/mesaj_dondur.dart';
import '../yardimci_fonksiyonlar/form_mesaj_dondur/popup_menu.dart';
import '../yardimci_fonksiyonlar/notifications/local_notifications.dart';
import '../yardimci_fonksiyonlar/sayfa_kadirma_efekti/scrool.dart';
import '../yardimci_fonksiyonlar/shared_sepete_ekle/sepet_shared.dart';
import '../yardimci_widgetlar/alt_bar_buton/alt_bar_buton.dart';
import '../yardimci_widgetlar/appbar_widgets/appbar_icon.dart';
import '../yardimci_widgetlar/appbar_widgets/appbar_text.dart';
import '../yardimci_widgetlar/form_label_widgets/text_form_field.dart';
import '../yardimci_widgetlar/label_text/label_widget.dart';
import '../yardimci_widgetlar/sepetim_sayfasi/miktar_butonlari.dart';
import 'package:line_awesome_icons/line_awesome_icons.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../yardimci_fonksiyonlar/form_mesaj_dondur/net_kontrol.dart'
    as net;
import '../global_degisken/global_degisken.dart' as global;
import 'package:flutter_bloc/flutter_bloc.dart';

class SepetAnaSayfa extends StatefulWidget {
  @override
  _SepetAnaSayfaState createState() => _SepetAnaSayfaState();
}

class _SepetAnaSayfaState extends State<SepetAnaSayfa> {
  //region Değişkenler
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  ///bloc la olan hatadan dolayı local bildirim kapatıldı
  //LocalNotifications bildirim = LocalNotifications();
  int urunMiktar;
  double genelTopFiy = 0;

  bool girisKontrol = false;

  Future girisKontrolEt() async {
    girisKontrol = SharedPrefLib.girisYapildi;
    setState(() {});
  }

  var gnController = TextEditingController();
  var notController = TextEditingController();
  var formKey = GlobalKey<FormState>();

  /*Telefon hafızasında tutulan bilgilerin nesnesini oluşturmak için tanımlanan nesne*/
  SharedPreferences prefs;

  /*Ekrana yazdırılacak olan listenin future olarak yazdırılmasını sağlayan menü*/
  Future<List<Urun>> listem;

  Map<String, dynamic> urunMap = Map();
  Urun urunList;
  static int sayac = 0;

  /*Ekrana yazdırılacak olan menünün bilgilerini tutan menü*/
  List<Urun> sepetim;
  var spnot = '';

  ProgressDialog pr;

  IslemDonus islemDonus;

  //endregion

  /*Sayfa yüklenirken menülerin içi burada doluyor*/
  @override
  void initState() {
    super.initState();
    islemDonus = IslemDonus();
    sepetim = List<Urun>();
    listem = SharedPrefLib.listeyiGetir();
    listem.then((gelenDeger) {
      sepetim = gelenDeger;
    });
    spnotGetir();
    girisKontrolEt();
    //ProgressDialog paketini kulandik
    pr = ProgressDialog(context);
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return WillPopScope(
      onWillPop: () {
        global.NavigateAndRemoveUntil()
            .navigatePushNoAnimation(context, AnaMenu());
        return null;
      },
      child: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: Scaffold(
          key: _scaffoldKey,
          appBar: AppBar(
            centerTitle: true,
            backgroundColor: GlobalDegiskenler.appBarColor,
            title: BaslikText(label: 'Sepetim'),
            actions: actionIcon(),
          ),
          body: ConnectivityWidgetWrapper(
            offlineWidget: net.netKontrol(),
            disableInteraction: false,
            child: FutureBuilder(
              future: listem,
              builder: (context, snapshot) {
                /*Veriler yüklenirken bu ekran gelecek*/
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return global.progressDondur();
                  /*Veri varken veya hiç ürün eklenmemişse bu ekranlar görüntülenecek*/
                } else if (snapshot.connectionState == ConnectionState.done &&
                    !snapshot.hasError &&
                    snapshot.hasData) {
                  /*Sepette ürün yoksa gösterilecek olan ekran*/
                  if (sepetim.length == 0) {
                    return urunYok();
                  } else {
                    /*Sepette bulunan bütün ürünlerin toplam fiyatlarını bu değişkene aktarır*/
                    genelTopFiy = 0;
                    sepetim.forEach((v) {
                      genelTopFiy = genelTopFiy + toDouble(v.genelTopFiy);
                    });
                    /*Ürünlerin listelenmesini temsil ediyor*/
                    return urunListele();
                  }
                  /*Bağlantıda bir hata varsa bu ekran görüntülenecek*/
                } else if (snapshot.hasError) {
                  return hataDondur();
                }
                return null;
              },
            ),
          ),
        ),
      ),
    );
  }


  //region

  sepetLimitKontrol() {
    return () async {
      try {
        var result = await Connectivity().checkConnectivity();
        if (result == ConnectivityResult.none) {
          MesajGoster.mesajGoster(
              context,
              "Cihazınızı internete bağlayınız.",
              SoruTipi.Uyari);
        } else {
          if (!girisKontrol) {
            MesajGoster.soruSor(
                context,
                "Devam edebilmek için üye girişi yapmalısınız, giriş sayfasına geçiş yapılsın mı?",
                SoruTipi.Soru, evet: () {
              Navigator.of(context, rootNavigator: true)
                  .pushReplacement(MaterialPageRoute(
                  builder: (context) => StepperGiris()));
            });
          } else {
            progressGoster("Lütfen bekleyiniz.");
            await pr.show();
            islemDonus.sipLimitFiyKontrol(genelTopFiy.toString()).then((deger) {
              if (deger == null) {
                pr.hide();
                MesajGoster.mesajGoster(
                    context,
                    "Beklenmedik bir hata oluştu. Lütfen daha sonra tekrar deneyiniz.",
                    SoruTipi.Hata);
              } else if (deger.cariBasariDonus == numarator.basarili) {
                if (deger.durum == '1') {
                  sepetim.forEach((d) {
                    d.aSfiyat = toDouble(d.aSfiyat).toString();
                  });
                  SharedPrefLib.urunEkle(sepetim);
                  pr.hide();
                  global.Navigate().navigatePushNoAnimation(
                      context,
                      AdresGetir(siparisModu: true,genelTopFiy: genelTopFiy));
                } else if (deger.durum == '2') {
                  pr.hide();
                  MesajGoster.mesajGoster(
                      context,
                      "Minimum paket tutarının altındasınız. Siparişinizi tamamlamak için sepetinize ${toDouble(deger.kalan).toStringAsFixed(2)} TL değerinde daha ürün eklemeniz gerekmektedir.",
                      SoruTipi.Uyari);
                } else {
                  pr.hide();
                  MesajGoster.mesajGoster(
                      context,
                      "Beklenmedik bir hata oluştu. Lütfen daha sonra tekrar deneyiniz.",
                      SoruTipi.Hata);
                }
              }else if (deger.cariBasariDonus ==
                  numarator.uygulamaVersiyonuEski) {
                pr.hide();
                logoEkraninaGit(context);
              } else if (deger.cariBasariDonus ==
                  numarator.apiVersiyonuEski) {
                pr.hide();
                logoEkraninaGit(context);
              } else if (deger.cariBasariDonus == numarator.hizmetDisi) {
                pr.hide();
                logoEkraninaGit(context);
              } else {
                pr.hide();
                MesajGoster.mesajGoster(
                    context,
                    "Beklenmedik bir hata oluştu. Lütfen daha sonra tekrar deneyiniz.",
                    SoruTipi.Hata);
              }
            });
          }
        }
      } catch (e) {}
    };
  }

  Future<void> progressGoster(String mesaj) {
    //ProgressDialog paketine style verdik
    pr.style(
      backgroundColor: GlobalDegiskenler.progressBarColor,
      progress: 4.0,
      message: mesaj,
      progressTextStyle: TextStyle(color: Colors.black),
      borderRadius: 15.0,
      elevation: 6.0,
      insetAnimCurve: Curves.easeIn,
      messageTextStyle: TextStyle(
        fontFamily: GlobalDegiskenler.tlSimgesi,
        fontSize: SizeConfig.safeBlockHorizontal * 5,
        color: Colors.black,
      ),
    );
  }
  /*Sepete eklenen genel notu getiriyor*/
  Future spnotGetir() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    spnot = pref.getString('spnot') ?? "";
  }

  double toDouble(String aSfiyat) {
    var sp = aSfiyat.split(',');
    var sp2 = aSfiyat.split('.');
    if (sp.length == 1 && sp2.length == 1) {
      return int.parse(aSfiyat) * 1.0;
    } else if (sp.length == 1) {
      return double.parse(aSfiyat);
    } else {
      var a = sp[0] + "." + sp[1]; //3.5
      return double.parse(a);
    }
  }

  urunDetayaGit(index) {
    return () {
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => UrunProfil(
                    grupurunler: Grupurunler(
                      aId: sepetim[index].aId,
                      aAdi: sepetim[index].aAdi,
                      aBirim: sepetim[index].aBirim,
                      aSfiyat: sepetim[index].aSfiyat,
                      aAcik: sepetim[index].aAcik != null
                          ? sepetim[index].aAcik
                          : "",
                      aciklama: sepetim[index].aciklama,
                    ),
                  )));
    };
  }

  urunEkstraDetay(int index){
    return () { showModalBottomSheet(
      backgroundColor: Colors.transparent,
      context: context,
      builder: (context) {
        return ScrollConfiguration(
          behavior: MyBehavior(),
          child: Container(
            height: SizeConfig.screenHeight * 0.5,
            child: Column(
              children: <Widget>[
                //status bar kadar bi şefaf container ouştururyoruz
                Expanded(
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(8),
                          topRight: Radius.circular(8),
                        )),
                    child: Column(
                      children: <Widget>[
                        SizedBox(
                          height: 10,
                        ),
                        Container(
                          width: 40,
                          height: 4,
                          decoration: BoxDecoration(
                            color: Color.fromRGBO(
                                213, 213, 215, 1),
                            borderRadius: BorderRadius.all(
                                Radius.circular(32)),
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Expanded(
                          child: CupertinoScrollbar(
                            child: ListView.builder(
                                shrinkWrap: true,
                                itemCount: sepetim[index].ekstraOzelik.length,
                                itemBuilder: (context, ekstraIndex) {
                                  double fiy = sepetim[index].ekstraOzelik[ekstraIndex].fiyat * sepetim[index].ekstraOzelik[ekstraIndex].mik;
                                  double mik = sepetim[index].ekstraOzelik[ekstraIndex].mik;
                                  return ListTile(
                                    title: labelCardEkstra(sepetim[index].ekstraOzelik[ekstraIndex].adi),
                                    trailing:  labelCardEkstra(fiy != 0 ?(fiy.toStringAsFixed(2) + " ₺"):"Ücretsiz",size: SizeConfig.safeBlockHorizontal * 3.5,),
                                    subtitle: labelCardEkstra(mik.toInt().toString() + ' Adet',size: SizeConfig.safeBlockHorizontal * 3, ),
                                  );
                                }),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );};
  }



  //endregion

  //region Widgetler
  Widget sizedBoxWidth() {
    return SizedBox(width: SizeConfig.safeBlockHorizontal * 2);
  }

  Widget urunListele() {
    return Container(
      width: double.infinity,
      child: Column(
        children: <Widget>[
          Expanded(
            child: Center(
              child: SizedBox(
                child: ListView.builder(
                  physics: ScrollBehavior().getScrollPhysics(context),
                  itemCount: sepetim.length,
                  itemBuilder: (context, index) {
                    sayac++;
                    return urunKart(index);
                  },
                ),
              ),
            ),
          ),
          /*Sepeti tamamla kısmı*/
          AltBarButon(onTap:sepetLimitKontrol(),tutar: genelTopFiy,butontTxt: 'Devam',),        ],
      ),
    );
  }



  Widget kaydirma() {
    return Container(
      height: SizeConfig.screenHeight * 0.18,
      padding: EdgeInsets.only(right: 1,left:1,top:0.25),
      child: Card(
        elevation: 0,
        shadowColor: Colors.white,
        color: Colors.red,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(8))),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Icon(
              !Platform.isAndroid ? CupertinoIcons.delete_solid : Icons.delete,
              color: Colors.white,
              size: SizeConfig.safeBlockHorizontal * 10,
            ),
            /*Ürün adı labelcard, silinme işleminde kullanılıyor*/
            LabelCard(
                fontFamily: GlobalDegiskenler.tlSimgesi,
                padding: EdgeInsets.only(
                  right: SizeConfig.safeBlockHorizontal * 4,
                ),
                label: " Sil",
                textAlign: TextAlign.right,
                color: Colors.white,
                fontSize: SizeConfig.safeBlockHorizontal * 5),
          ],
        ),
      ),
    );
  }

  Widget urunKart(index) {
    return Container(
      height: SizeConfig.screenHeight * 0.18,
      margin: EdgeInsets.symmetric(
        vertical: SizeConfig.screenHeight * 0.002,
        horizontal: SizeConfig.safeBlockHorizontal * 1,
      ),
      child: Stack(
        alignment: Alignment.topRight,
        children: <Widget>[
          kaydirma(),
          Positioned(
            child: Dismissible(
              direction: DismissDirection.endToStart,
              key: Key(sayac.toString()),
              /*Ürünü kaydırarak silmeyi temsil eden kod*/
              onDismissed: (sil) {
                sepetim.removeAt(index);
                SharedPrefLib.sepetTemizle();
                SharedPrefLib.urunEkle(sepetim);
                //eğer sepette ürün kalmamış ise sepet notunu temizle
                if (sepetim.length == 0)
                  SharedPrefLib.spNotSil();
                //sepet iconunun sayacı için oluşturuldu
                //sepet sayacı için bloc la değerini atıyoruz
                context.bloc<SepetSayacCubit>().esitle(sepetim.length);
                ///bloc la olan hatadan dolayı local bildirim kapatıldı
                /* if (sepetim.length == 0)
                  bildirim.tumBildirimleriIptalET();
                else //eger ürün var ise alarmı ayarlıyor
                  bildirim.gunlukBildirim(baslik: localBildirimBaslik,altBaslik: localBildirimMesaj);*/
                setState(() {});
              },
              child: GestureDetector(
                child: Card(
                  color: Colors.white,
                  shadowColor: Colors.grey.shade100,
                  elevation: 1.75,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(8))),
                  child: Padding(
                    padding: EdgeInsets.symmetric(
                        vertical: SizeConfig.screenHeight * 0.0125),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        /*Resimin temsil edildiği padding widgeti*/
                        Flexible(
                          flex: 3,
                          child: GestureDetector(
                            child: Container(
                              constraints: BoxConstraints.expand(),
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                    image:
                                        AssetImage("assets/images/resim-yok.png"),
                                    fit: BoxFit.cover,
                                  ),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(8))),
                              child: ClipRRect(
                                child: Image.network(
                                  "${global.resimUrl + global.key + "/s-" + sepetim[index].aId + ".jpg"}",
                                  fit: BoxFit.cover,
                                ),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(8)),
                              ),
                            ),
                            onTap:urunDetayaGit(index),
                          ),
                        ),
                        /*Ürün label, arttırma ve azaltma butonlarını temsil ediyor*/
                        Flexible(
                          flex: 4,
                          child: Container(
                            height: SizeConfig.screenHeight * 0.14,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: <Widget>[
                                /*Ürün adı label*/
                                Padding(
                                  padding: EdgeInsets.only(
                                    top: SizeConfig.screenHeight * 0.005,
                                  ),
                                  child: LabelCard(
                                    alignment: Alignment.center,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: GlobalDegiskenler.tlSimgesi,
                                    label: sepetim[index].aAdi,
                                    fontSize:
                                        SizeConfig.safeBlockHorizontal * 3.5,
                                  ),
                                ),
                                /*Toplam fiyat label*/
                                Center(
                                  child: FittedBox(
                                    child: LabelCard(
                                        fontFamily: GlobalDegiskenler.tlSimgesi,
                                        fontWeight: FontWeight.bold,
                                        label: sepetim[index].genelTopFiy + " ₺",
                                        fontSize:
                                            SizeConfig.safeBlockHorizontal * 4),
                                  ),
                                ),
                                /*Arttırma ve azaltma butonları*/
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    /*Ürün azaltma butonu*/
                                    MiktarButonlari(
                                      alignment: Alignment.center,
                                      color: GlobalDegiskenler.miktarButonRenk,
                                      onPresssed: () {
                                        setState(() {
                                          double toplam = 0;
                                          urunMiktar = int.parse(sepetim[index].urunMiktar);
                                          if (urunMiktar >= 2) {
                                            //ürün fiyatını güncelliyoruz
                                            sepetim[index].toplamFiyat = (toDouble(sepetim[index].toplamFiyat) - (toDouble(sepetim[index].toplamFiyat) / urunMiktar)).toStringAsFixed(2);
                                            urunMiktar--;
                                            //ekstra ürün adede bağımlı değil ise top fiyattan cıkartıyotuz
                                            sepetim[index].ekstraOzelik.forEach((element){
                                              //eklenen ekstra miktara bağımlı ise Çarp yoksa çarpa
                                              if(element.mikCrp == 1){
                                                toplam = toplam + element.fiyat;
                                                //eklenen ekstra ürün mikdaını değştiriyoruz
                                                element.mik = toDouble(urunMiktar.toString());
                                              }
                                            });
                                            //genel toplam fiyatını güncelliyoruz
                                            genelTopFiy = toDouble( sepetim[index].genelTopFiy) - toplam - (toDouble(sepetim[index].toplamFiyat) / urunMiktar);
                                            sepetim[index].genelTopFiy = genelTopFiy.toStringAsFixed(2);
                                            sepetim[index].urunMiktar = urunMiktar.toString();
                                          } else {
                                            MesajGoster.mesajGoster(context,"Ürünü sola doğru kaydırarak veya çöp kutusuna basarak silebilirsiniz.",SoruTipi.Uyari);
                                          }
                                        });
                                      },
                                      label: '-',
                                      fontSize:
                                          SizeConfig.safeBlockHorizontal * 7,
                                      genislik: SizeConfig.screenWidth * 0.1,
                                      yukseklik: SizeConfig.screenHeight * 0.05,
                                    ),
                                    sizedBoxWidth(),
                                    /*Ürün miktar label*/
                                    Container(
                                      width: SizeConfig.safeBlockHorizontal * 6,
                                      child: Center(
                                        child: LabelCard(
                                            fontFamily:
                                                GlobalDegiskenler.tlSimgesi,
                                            label: sepetim[index].urunMiktar,
                                            fontSize:
                                                SizeConfig.safeBlockHorizontal *
                                                    4.5),
                                      ),
                                    ),
                                    sizedBoxWidth(),
                                    /*Ürün arttırma butonu*/
                                    MiktarButonlari(
                                      alignment: Alignment.center,
                                      color: GlobalDegiskenler.miktarButonRenk,
                                      onPresssed: () {
                                        double toplam = 0;
                                        setState(() {
                                          urunMiktar =int.parse(sepetim[index].urunMiktar);
                                          if (urunMiktar <= 19) {
                                            //ürün fiyatını güncelliyoruz
                                            sepetim[index].toplamFiyat  = (toDouble( sepetim[index].toplamFiyat) + (toDouble( sepetim[index].toplamFiyat) / urunMiktar) ).toStringAsFixed(2);
                                            urunMiktar++;
                                            //ekstra ürün adede bağımlı değil ise top fiyattan cıkartıyotuz
                                            sepetim[index].ekstraOzelik.forEach((element){
                                              //eklenen ekstra miktara bağımlı ise Çarp yoksa çarpa
                                              if(element.mikCrp == 1){
                                                toplam = toplam + element.fiyat;
                                                //eklenen ekstra ürün mikdaını değştiriyoruz
                                                element.mik = toDouble(urunMiktar.toString());
                                              }
                                            });
                                            //genel toplam fiyatını güncelliyoruz
                                            genelTopFiy = toDouble( sepetim[index].genelTopFiy) + toplam + (toDouble(sepetim[index].toplamFiyat) / urunMiktar);
                                            sepetim[index].genelTopFiy = genelTopFiy.toStringAsFixed(2);
                                            sepetim[index].urunMiktar = urunMiktar.toString();
                                          } else {
                                            MesajGoster.mesajGoster(context,"Ürün miktarı 20 adetten fazla olamaz.",SoruTipi.Uyari);
                                          }
                                        });
                                      },
                                      label: '+',
                                      fontSize:
                                          SizeConfig.safeBlockHorizontal * 6,
                                      genislik: SizeConfig.screenWidth * 0.1,
                                      yukseklik: SizeConfig.screenHeight * 0.05,
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                        /*Ürün fiyatını, güncelleme ve silme butonlarını temsil ediyor*/
                        Flexible(
                          flex: 2,
                          child: Column(
                            children: <Widget>[
                              Spacer(),
                              /*Ürün güncelleme ve silme butonları*/
                              Container(
                                height: SizeConfig.safeBlockVertical * 7.0,
                                child: IconButton(
                                  onPressed: notEkle(index),
                                  icon: Icon(
                                    !Platform.isAndroid
                                        ? CupertinoIcons.pen
                                        : Icons.edit,
                                    size: SizeConfig.safeBlockVertical * 4.0,
                                    color: GlobalDegiskenler.duzenlemeIcon,
                                  ),
                                ),
                              ),
                              Spacer(),
                              Container(
                                height: SizeConfig.safeBlockVertical * 7.0,
                                child: IconButton(
                                  onPressed: () async {
                                    MesajGoster.soruSor(
                                        context,
                                        "Ürünü silmek istediğinize emin misiniz ?",
                                        SoruTipi.Soru, evet: () {
                                      sepetim.removeAt(index);
                                      SharedPrefLib.urunSil(sepetim);
                                      //sepet iconunun sayacı için oluşturuldu
                                      //sepet sayacı için bloc la değerini atıyoruz
                                      context.bloc<SepetSayacCubit>().esitle(sepetim.length);
                                      ///bloc la olan hatadan dolayı local bildirim kapatıldı
                                      //sepet dolu oldugunda gonderilen bildirimi iptal ediyotuz
/*                                      if (sepetim.length == 0)
                                        bildirim.tumBildirimleriIptalET();
                                      else //eger ürün var ise alarmı ayarlıyor
                                        bildirim.gunlukBildirim(baslik: localBildirimBaslik,altBaslik: localBildirimMesaj);*/
                                      Navigator.of(context, rootNavigator: true)
                                          .pop();
                                      setState(() {});
                                      return null;
                                    });
                                  },
                                  icon: Icon(
                                    !Platform.isAndroid
                                        ? CupertinoIcons.delete_solid
                                        : Icons.delete,
                                    size: SizeConfig.safeBlockVertical * 4.0,
                                    color: GlobalDegiskenler.silmeIcon,
                                  ),
                                ),
                              ),
                              Spacer(),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                onTap: sepetim[index].ekstraOzelik[0].id != null ?  urunEkstraDetay(index) : urunDetayaGit(index),
              ),
            ),
          ),
        ],
      ),
    );
  }



  List<Widget> actionIcon() {
    return [
      /*Sepete genel not eklemek iconu*/
      Tooltip(
        message: 'Genel sepet notu ekler',
        child: GestureDetector(
          child: Icon(
            !Platform.isAndroid
                ? CupertinoIcons.create
                : LineAwesomeIcons.sticky_note,
            color: GlobalDegiskenler.appBarIconColor,
            size: SizeConfig.blockSizeHorizontal * 8,
          ),
          onTap: () {
            if (sepetim.length == 0) {
              MesajGoster.mesajGoster(
                  context,
                  "Not eklemek için sepete ürün eklemelisiniz.",
                  SoruTipi.Uyari);
            } else {
              gnController.clear();
              gnController.value = TextEditingValue(
                  text: spnot.length != null ? spnot : 'Not eklemek istiyorum');
              Popup().popupMenu(
                widget: Card(
                  elevation: 0,
                  color: Colors.transparent,
                  child: Form(
                    key: formKey,
                    child: MyTextFormField(
                      labelText: 'Not',
                      kontroller: gnController,
                      ileriTusu: TextInputAction.done,
                      karakterSayisi: 50,
                      satirSayisi: 3,
                      label: spnot.length != 0 ? spnot : 'Not eklemek istiyorum',
                      sifreyiGoster: IconButton(
                        icon: Icon(Icons.remove_circle),
                        color: Colors.black,
                        tooltip: 'Notu siler',
                        onPressed: () {
                          WidgetsBinding.instance
                              .addPostFrameCallback((_) => gnController.clear());
                        },
                      ),
                    ),
                  ),
                ),
                context: context,
                baslik: 'Genel sepet notu',
                kaydet: () async {
                  if (gnController.text.length == 0) {
                    MesajGoster.toastMesaj("Lütfen notunuzu giriniz.");
                    Navigator.of(context, rootNavigator: true).pop();
                  } else {
                    SharedPrefLib.spNot(gnController.text);
                    spnot = gnController.text.trim();
                    MesajGoster.toastMesaj(
                        "Notunuz başarılı bir şekilde eklenmiştir.");
                    Navigator.of(context, rootNavigator: true).pop();
                  }
                },
              );
              setState(() {});
            }
          },
        ),
      ),
      SizedBox(
        width: 8,
      ),
      /*Silme iconuna basıldığında yapılacak olan işlemler burada gerçekleşiyor*/
      Padding(
        child: AppbarIcon(
          toolTip: 'Tüm ürünleri siler',
          onPressed: () {
            setState(() {
              if (sepetim.length == 0) {
                MesajGoster.mesajGoster(context,
                    "Sepetinizde ürün bulunmamaktadır.", SoruTipi.Uyari);
              } else {
                MesajGoster.soruSor(
                  context,
                  'Sepetinizdeki ürünler silinecek, emin misiniz ?',
                  SoruTipi.Soru,
                  evet: () {
                    setState(
                      () {
                        SharedPrefLib.sepetTemizle();
                        //sepet notunu temizle
                          SharedPrefLib.spNotSil();
                        //sepet iconunun sayacı için oluşturuldu
                        //sepet sayacı için bloc la değerini atıyoruz
                        context.bloc<SepetSayacCubit>().sifirla();
                        ///bloc la olan hatadan dolayı local bildirim kapatıldı
                        //sepet dolu oldugunda gonderilen bildirimi iptal ediyotuz
                        //bildirim.tumBildirimleriIptalET();
                        sepetim.clear();
                        Navigator.of(context, rootNavigator: true).pop();
                        return null;
                      },
                    );
                  },
                );
              }
            });
          },
          iconSize: SizeConfig.blockSizeHorizontal * 8.5,
          icon: Icon(
            !Platform.isAndroid
                ? CupertinoIcons.delete
                : LineAwesomeIcons.trash_o,
            color: GlobalDegiskenler.appBarIconColor,
          ),
        ),
        padding: EdgeInsets.only(
            bottom: SizeConfig.safeBlockVertical * 1,
            right: SizeConfig.safeBlockHorizontal * 1),
      ),
    ];
  }

  Widget hataDondur() {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
          child: aciklama(
              'Hata',
              'Bağlantınızda bir problem var. Lütfen tekrar deneyiniz.',
               Icons.error_outline),
        ),
        Spacer(),
        RaisedButton(
          splashColor: Colors.red,
          color: GlobalDegiskenler.butonArkaPlanRengi,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8.0),
          ),
          child: LabelCard(
            fontFamily: GlobalDegiskenler.tlSimgesi,
            label: "Tekrar yükle",
            fontSize: SizeConfig.safeBlockHorizontal * 6,
            color: Colors.white,
          ),
          onPressed: () async {
            setState(() {
              listem = SharedPrefLib.listeyiGetir();
            });
          },
        ),
        Spacer(),
      ],
    );
  }

  notEkle(index) {
    return () {
      notController.clear();
      notController.value = TextEditingValue(
          text: sepetim[index].aciklama == "" ? "" : sepetim[index].aciklama);
      Popup().popupMenu(
        context: context,
        baslik: sepetim[index].aAdi,
        kaydet: () {
          var eklenenOzelik = List();
          sepetim[index].ekstraOzelik.forEach((ozelik) {
              eklenenOzelik.add({
                "id": ozelik.id,
                "adi": ozelik.adi,
                "fiyat": ozelik.fiyat,
                "miktar": ozelik.miktar,
                "stokId": ozelik.stokId,
                "ozstokId": ozelik.ozstokId,
                "mikCrp": ozelik.mikCrp,
                "mik":ozelik.mik,
              });
          });
          urunMap = {
            "aAdi": sepetim[index].aAdi,
            "aId": sepetim[index].aId,
            "aSfiyat": sepetim[index].aSfiyat,
            "toplamFiyat": sepetim[index].toplamFiyat,
            "urunMiktar": sepetim[index].urunMiktar,
            "aciklama": notController.text.trim(),
            "porsiyon": sepetim[index].porsiyon,
            "a_acik": sepetim[index].aAcik.toString(),
            "a_birim": sepetim[index].aBirim.toString(),
            "genelTopFiy": sepetim[index].genelTopFiy.toString(),
            "ekstraOzelik": eklenenOzelik,
          };
          urunList = Urun.fromJson(urunMap);
          print("YENİ KAYIT EDİLECEK OLAN ÜRÜN DEĞERLERİ : " +
              urunList.toString());
          sepetim.removeAt(index);
          sepetim.add(urunList);
          SharedPrefLib.urunEkle(sepetim);
          //sepet iconunun sayacı için oluşturuldu
          // sepet sayacı için bloc la değerini atıyoruz
          context.bloc<SepetSayacCubit>().esitle(sepetim.length);
          ///bloc la olan hatadan dolayı local bildirim kapatıldı
          //sepet ürün oldüğündan alarmı ayarlıyoruz
         // bildirim.gunlukBildirim(baslik: localBildirimBaslik,altBaslik: localBildirimMesaj);
          notController.clear();
          Navigator.of(context, rootNavigator: true).pop();
          setState(() {});
        },
        widget: Card(
          elevation: 0,
          color: Colors.transparent,
          child: Form(
            key: formKey,
            child: MyTextFormField(
              labelText: 'Not',
              satirSayisi: 3,
              karakterSayisi: 200,
              sifreyiGoster: IconButton(
                onPressed: () {
                  WidgetsBinding.instance
                      .addPostFrameCallback((_) => notController.clear());
                },
                icon: Icon(
                  Icons.remove_circle,
                  color: Colors.black,
                ),
              ),
              ileriTusu: TextInputAction.done,
              textStyle: TextStyle(color: Colors.black),
              kontroller: notController,
              label: sepetim[index].aciklama == ""
                  ? "Notunuzu giriniz."
                  : sepetim[index].aciklama,
            ),
          ),
        ),
      );
    };
  }

  Widget urunYok() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
      child: aciklama(
          'Bilgi',
          'Sepetinize ürün eklemek için Anasayfayı ziyaret ediniz.',
          !Platform.isAndroid
              ? CupertinoIcons.shopping_cart
              : Icons.add_shopping_cart),
    );
  }

  labelCardEkstra(String txt,{double size}) {
    return LabelCard(
      satirSayisi: 1,
      fontWeight: FontWeight.w500,
      label: txt,
      fontFamily: GlobalDegiskenler.tlSimgesi,
      fontSize: size ?? SizeConfig.safeBlockHorizontal * 4.5,
    );
  }
//endregion
}
