import 'dart:convert';
import 'package:connectivity/connectivity.dart';
import 'package:connectivity_wrapper/connectivity_wrapper.dart';
import 'package:flutter/cupertino.dart';
import '../bloc/blocs_exports.dart';
import '../yardimci_fonksiyonlar/form_mesaj_dondur/dr_dialog.dart';
import '../yardimci_fonksiyonlar/notifications/local_notifications.dart';
import '../yardimci_fonksiyonlar/sayfa_kadirma_efekti/scrool.dart';
import '../yardimci_widgetlar/alt_bar_buton/alt_bar_buton.dart';
import '../yardimci_widgetlar/manu_kartlar/siparis_cart.dart';
import '../yardimci_widgetlar/step_bar/step_bar.dart';
import 'package:flutter/material.dart';
import '../api/response/islem_donus.dart';
import '../api/model/odeme_tur_model.dart';
import '../api/model/sepet_model.dart';
import '../global_degisken/global_degisken.dart';
import '../responsive_class/size_config.dart';
import '../siparis/siparis_ana_sayfa.dart';
import '../yardimci_fonksiyonlar/form_mesaj_dondur/mesaj_dondur.dart';
import '../yardimci_fonksiyonlar/shared_sepete_ekle/sepet_shared.dart';
import '../yardimci_widgetlar/appbar_widgets/appbar_text.dart';
import '../yardimci_widgetlar/label_text/label_widget.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../yardimci_fonksiyonlar/form_mesaj_dondur/net_kontrol.dart'
    as net;
import '../global_degisken/global_degisken.dart' as global;
import 'package:flutter_bloc/flutter_bloc.dart';

class SiparisTamamla extends StatefulWidget {
  final toplamTutar;

  const SiparisTamamla({Key key, @required this.toplamTutar}) : super(key: key);

  @override
  _SiparisTamamlaState createState() => _SiparisTamamlaState();
}

class _SiparisTamamlaState extends State<SiparisTamamla> {
  ///bloc la olan hatadan dolayı local bildirim kapatıldı
  //LocalNotifications bildirim = LocalNotifications();
  Future<List<Urun>> listem;
  List<Urun> sepetim = [];
  SharedPreferences prefs;
  IslemDonus islemDonus;

  List<OdemeTur> odemeTur;
  OdemeTur secenek;

  var spnot = '';
  var spradrs = '';
  var cariAd;
  var cId;
  var sepetjson;
  var bodyString;
  var odeTur;
  var adresId;
  var ilkOdemeDeger;

  /*Sayfa yüklenirken ataması yapılan değişkenler*/
  @override
  void initState() {
    super.initState();
    islemDonus = IslemDonus();
    spnotGetir();
    cari();
    adresGetir();
    adresIdGetir();

    odemeTur = [];
    sepetim = List<Urun>();

    listem = SharedPrefLib.listeyiGetir();
    listem.then((gelenDeger) {
      sepetim = gelenDeger;
    });
    sepetDoldur();

    /*Veritabanından gelen ödeme türlerinin atamasını yaptık.*/
    islemDonus.odemeTur().then((v) {
      try {
        if (v[0] != null) {
          odemeTur = v;
          secenek = v[0];
          ilkOdemeDeger = v[0].aId;
        }
      } catch (e) {}
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    islemDonus = IslemDonus();
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        elevation: 4,
        centerTitle: true,
        backgroundColor: GlobalDegiskenler.appBarColor,
        title: BaslikText(
          label: 'Siparişi Tamamla',
        ),
      ),
      body: ConnectivityWidgetWrapper(
        offlineWidget: net.netKontrol(),
        disableInteraction: false,
        child: ScrollConfiguration(
          behavior: MyBehavior(),
          child: Container(
            height: double.infinity,
            width: double.infinity,
            child: FutureBuilder(
              future: listem,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                } else if (snapshot.connectionState == ConnectionState.done &&
                    !snapshot.hasError &&
                    snapshot.hasData) {
                  /*Genel olarak içeriğin listelendiği container*/
                  return listele();
                }
                return null;
              },
            ),
          ),
        ),
      ),
    );
  }

  //region Foksiyonlar

  /*Sepete eklenen genel notu getiriyor*/
  Future spnotGetir() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    spnot = pref.getString('spnot');
  }

  /*Sepete eklenen adresi getiriyor*/
  Future adresGetir() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    spradrs = pref.getString('spradrs');
  }

  Future adresIdGetir() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    adresId = pref.getInt('adrsId');
  }

  Future cari() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    cariAd = pref.getString('cariAdDonus');
    cId = pref.getInt('cariIdDonus');
  }

  Future sepetDoldur() async {
    SharedPreferences shared = await SharedPreferences.getInstance();
    sepetjson = shared.getString('sepettekiUrunlerim');
    var sepetMap = jsonDecode(sepetjson);
    List<Urun> sepetList =
        List.of(sepetMap).map((object) => Urun.fromJson(object)).toList();
    bodyString = json.encode(sepetList);
  }

  siperisVer() {
    return () async {
      try {
        var result = await Connectivity().checkConnectivity();
        if (result == ConnectivityResult.none) {
          MesajGoster.mesajGoster(
              context, "Cihazınızı internete bağlayınız.", SoruTipi.Uyari);
        } else {
          progress().prBaslat(context, "Sipariş veriliyor\nLüten bekleyiniz.");

          SharedPreferences prefs = await SharedPreferences.getInstance();
          odeTur = prefs.getInt('odeTur');
          final body = {
            "key": global.key,
            "islem_kodu": "003",
            "urunlerim": bodyString,
            "cari_id": cId,
            "spnot": spnot,
            "odeTur": ilkOdemeDeger,
            "adresID": adresId
          };
          islemDonus
              .sprsTamamla(body, bodyString, cId, spnot, ilkOdemeDeger, adresId)
              .then((gelenDeger) {
            if (gelenDeger.cariBasariDonus == "1") {
              SharedPrefLib.sharedSepetTemizle();
              context.bloc<SepetSayacCubit>().sifirla();

              ///bloc la olan hatadan dolayı local bildirim kapatıldı
              //Sepet Bildirimlerini Iptal ediyoruz
              //bildirim.tumBildirimleriIptalET();
              Future.delayed(Duration(milliseconds: 1500), () {
                progress().prBitir(context);
                MesajGoster.mesajGoster(
                        context,
                        "Sipariş başarılı bir şekilde verildi.",
                        SoruTipi.Basarili)
                    .whenComplete(() {
                  progress().prBitir(context);
                  Navigator.of(context).pushAndRemoveUntil(
                      MaterialPageRoute(
                          builder: (context) => SiparisAnaSayfa(true)),
                      (e) => false);
                });
              });
            } else if (gelenDeger.cariBasariDonus == "14") {
              progress().prBitir(context);
              return MesajGoster.mesajGoster(
                      context,
                      "Bugün online sipariş alma departmanımız çalışmamaktadır.",
                      SoruTipi.Hata)
                  .whenComplete(() {
                progress().prBitir(context);
              });
            } else if (gelenDeger.cariBasariDonus ==
                numarator.uygulamaVersiyonuEski) {
              progress().prBitir(context);
              logoEkraninaGit(context);
            } else if (gelenDeger.cariBasariDonus ==
                numarator.apiVersiyonuEski) {
              progress().prBitir(context);
              logoEkraninaGit(context);
            } else if (gelenDeger.cariBasariDonus == numarator.hizmetDisi) {
              progress().prBitir(context);
              logoEkraninaGit(context);
            } else if (gelenDeger.cariBasariDonus == "10") {
              progress().prBitir(context);
              return MesajGoster.mesajGoster(
                      context,
                      "Online sipariş alma saatleri dışındasınız.",
                      SoruTipi.Hata)
                  .whenComplete(() {
                progress().prBitir(context);
              });
            } else if (gelenDeger.cariBasariDonus == "9") {
              progress().prBitir(context);
              return MesajGoster.mesajGoster(
                      context,
                      "Bazı ürünler eklenirken bir hata oluştu, ürünleri kontrol edip lütfen tekrar deneyiniz.",
                      SoruTipi.Hata)
                  .whenComplete(() {
                progress().prBitir(context);
              });
            } else if (gelenDeger.cariBasariDonus == "3") {
              progress().prBitir(context);
              return MesajGoster.mesajGoster(
                      context,
                      "Beklenmedik bir hata oluştu. Lütfen daha sonra tekrar deneyiniz.",
                      SoruTipi.Hata)
                  .whenComplete(() {
                progress().prBitir(context);
              });
            } else if (gelenDeger.cariBasariDonus == "13") {
              progress().prBitir(context);
              return MesajGoster.mesajGoster(
                      context,
                      "Sipariş verilirken bir hata oluştu. Lütfen tekrar deneyiniz.",
                      SoruTipi.Hata)
                  .whenComplete(() {
                progress().prBitir(context);
              });
            }
            return null;
          });
        }
      } catch (e) {}
    };
  }

  //endregion

  //region Widgetlar

  Widget listele() {
    return Column(
      children: <Widget>[
        Expanded(
            child: SingleChildScrollView(
          primary: true,
          child: Column(
            children: <Widget>[
              //Üst kısımda gosterilen Sipariş aadımları
              StepBar(
                step2Color: Colors.white,
                text2Color: GlobalDegiskenler.butonArkaPlanRengi,
                text1Color: Colors.white54,
                backgroundColor: Colors.black54,
                icon: Icon(
                  Icons.arrow_forward,
                  color: Colors.white54,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(
                  left: SizeConfig.safeBlockHorizontal * 4,
                  right: SizeConfig.safeBlockHorizontal * 4,
                ),
                child: Column(
                  children: <Widget>[
                    sizedBoxKucuk(),
                    /*Ödeme seçenekleri*/
                    odemeDropdown(),
                    sizedBoxKucuk(),
                    /*Adres label temsil eden container*/
                    adresGoster(),
                    sizedBoxKucuk(),
                    /*Ürünlerin listelendiği container*/
                    urunlerListele(),
                    sizedBoxKucuk(),
                  ],
                ),
              ),
            ],
          ),
        )),
        /*Siparişi tamamla butonu*/
        tamamlaButonu(),
      ],
    );
  }

  Widget labelCard(String mesaj) {
    return LabelCard(
      fontFamily: GlobalDegiskenler.tlSimgesi,
      fontWeight: FontWeight.w600,
      alignment: Alignment.centerLeft,
      label: mesaj,
      fontSize: SizeConfig.safeBlockHorizontal * 4,
    );
  }

  Widget sizedBoxKucuk() {
    return SizedBox(height: MediaQuery.of(context).size.height * 0.01);
  }

  Widget odemeDropdown() {
    return Container(

      alignment: Alignment.center,
      width: double.infinity,
      padding: EdgeInsets.symmetric(vertical: SizeConfig.safeBlockVertical,horizontal: SizeConfig.safeBlockHorizontal * 2),
      decoration: GlobalDegiskenler.kartSablon,
      child: Column(
        children: [
          labelCard("Ödeme Yöntemi"),
          SizedBox(height: SizeConfig.safeBlockVertical * 0.5,),
          DropdownButton<OdemeTur>(
            elevation: 1,
            isExpanded: true,
            isDense: true,
            underline: SizedBox(),
            style: TextStyle(  fontFamily: GlobalDegiskenler.tlSimgesi,
              fontWeight: FontWeight.w300,
              fontSize: SizeConfig.safeBlockHorizontal * 4,),
            icon: Icon(
              Icons.keyboard_arrow_down,
              size: SizeConfig.blockSizeVertical * 3,
            ),
            value: secenek,
            onChanged: (OdemeTur value) {
              setState(() {
                secenek = value;
                ilkOdemeDeger = secenek.aId;
              });
            },
            items: odemeTur.map((OdemeTur secenek) {
              return DropdownMenuItem<OdemeTur>(
                value: secenek,
                child: LabelCard(
                  alignment: Alignment.centerLeft,
                  fontFamily: GlobalDegiskenler.tlSimgesi,
                  satirSayisi: 8,
                  label: secenek.aAdi,
                  textAlign: TextAlign.justify,
                  color: Colors.black,
                  fontSize: SizeConfig.safeBlockHorizontal * 4,
                ),
              );
            }).toList(),
          ),
        ],
      ),
    );
  }

  Widget adresGoster() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: SizeConfig.safeBlockVertical,horizontal: SizeConfig.safeBlockHorizontal * 2),
      alignment: Alignment.center,
      width: double.infinity,
      child: Column(
        children: [
          labelCard('Sipariş Adresi'),
          SizedBox(height: SizeConfig.safeBlockVertical * 0.5,),
          LabelCard(
            alignment: Alignment.centerLeft,
            fontFamily: GlobalDegiskenler.tlSimgesi,
            satirSayisi: 8,
            label: spradrs,
            color: Colors.black,
            fontSize: SizeConfig.safeBlockHorizontal * 4,
          ),
        ],
      ),
      decoration: GlobalDegiskenler.kartSablon,
    );
  }

  Widget urunlerListele() {
    return Container(
      decoration: GlobalDegiskenler.kartSablon,
      padding: EdgeInsets.symmetric(vertical: SizeConfig.safeBlockVertical,horizontal: SizeConfig.safeBlockHorizontal * 4),
      child: Column(
        children: <Widget>[
          Column(
            children: <Widget>[
              labelCard('Sepet Özeti'),
              ListView.builder(
                shrinkWrap: true,
                primary: false,
                itemCount: sepetim.length,
                itemBuilder: (context, index) {
                  return Column(
                    children: <Widget>[
                      SiparisCart(
                        name: sepetim[index].aAdi,
                        img:
                            "${global.resimUrl + global.key + "/s-" + sepetim[index].aId + ".jpg"}",
                        Fiy: sepetim[index].genelTopFiy,
                        piece: sepetim[index].urunMiktar,
                        not: sepetim[index].aciklama,
                        notTop: () => MesajGoster.mesajGoster(context,
                            sepetim[index].aciklama, SoruTipi.NotGoster),
                        ekstra: sepetim[index].ekstraOzelik[0].id == null
                            ? false
                            : true,
                        ekstraTop: () => showModalBottomSheet(
                          backgroundColor: Colors.transparent,
                          context: context,
                          builder: (context) {
                            return ScrollConfiguration(
                              behavior: MyBehavior(),
                              child: Container(
                                height: SizeConfig.screenHeight * 0.8,
                                child: Column(
                                  children: <Widget>[
                                    //status bar kadar bi şefaf container ouştururyoruz
                                    Expanded(
                                      child: Container(
                                        decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius: BorderRadius.only(
                                              topLeft: Radius.circular(8),
                                              topRight: Radius.circular(8),
                                            )),
                                        child: Column(
                                          children: <Widget>[
                                            SizedBox(
                                              height: 10,
                                            ),
                                            Container(
                                              width: 40,
                                              height: 4,
                                              decoration: BoxDecoration(
                                                color: Color.fromRGBO(
                                                    213, 213, 215, 1),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(32)),
                                              ),
                                            ),
                                            SizedBox(
                                              height: 10,
                                            ),
                                            Expanded(
                                              child: CupertinoScrollbar(
                                                child: ListView.builder(
                                                    shrinkWrap: true,
                                                    itemCount: sepetim[index]
                                                        .ekstraOzelik
                                                        .length,
                                                    itemBuilder:
                                                        (context, ekstraIndex) {
                                                      double fiy = sepetim[
                                                                  index]
                                                              .ekstraOzelik[
                                                                  ekstraIndex]
                                                              .fiyat *
                                                          sepetim[index]
                                                              .ekstraOzelik[
                                                                  ekstraIndex]
                                                              .mik;
                                                      double mik =
                                                          sepetim[index]
                                                              .ekstraOzelik[
                                                                  ekstraIndex]
                                                              .mik;
                                                      return ListTile(
                                                        title: labelCardEkstra(
                                                            sepetim[index]
                                                                .ekstraOzelik[
                                                                    ekstraIndex]
                                                                .adi),
                                                        trailing:
                                                            labelCardEkstra(
                                                          fiy != 0
                                                              ? (fiy.toStringAsFixed(
                                                                      2) +
                                                                  " ₺")
                                                              : "Ücretsiz",
                                                          size: SizeConfig
                                                                  .safeBlockHorizontal *
                                                              3.5,
                                                        ),
                                                        subtitle:
                                                            labelCardEkstra(
                                                          mik
                                                                  .toInt()
                                                                  .toString() +
                                                              ' Adet',
                                                          size: SizeConfig
                                                                  .safeBlockHorizontal *
                                                              3,
                                                        ),
                                                      );
                                                    }),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                    ],
                  );
                },
              ),
            ],
          ),
          divider(),
          Padding(
            padding: EdgeInsets.symmetric(vertical: SizeConfig.safeBlockVertical * 0.5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                LabelCard(
                  fontFamily: GlobalDegiskenler.tlSimgesi,
                  label: "Toplam Tutar : ",
                  fontWeight: FontWeight.bold,
                  fontSize: SizeConfig.safeBlockHorizontal * 4.5,
                ),
                LabelCard(
                  fontFamily: GlobalDegiskenler.tlSimgesi,
                  label: widget.toplamTutar.toString() + " ₺",
                  fontWeight: FontWeight.bold,
                  fontSize: SizeConfig.safeBlockHorizontal * 4.5,
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  labelCardEkstra(String txt, {double size}) {
    return LabelCard(
      satirSayisi: 1,
      fontWeight: FontWeight.w500,
      label: txt,
      fontFamily: GlobalDegiskenler.tlSimgesi,
      fontSize: size ?? SizeConfig.safeBlockHorizontal * 4.5,
    );
  }

  Widget genelOzellikLabel(String value, {double fontSize, int satirSayisi}) {
    return LabelCard(
      fontFamily: GlobalDegiskenler.tlSimgesi,
      textAlign: TextAlign.center,
      satirSayisi: satirSayisi != null ? satirSayisi : 3,
      fontSize: fontSize,
      color: Colors.black,
      fontWeight: FontWeight.bold,
      label: value,
    );
  }

  Widget tamamlaButonu() {
    return AltBarButon(
      onTap: siperisVer(),
      tutar: widget.toplamTutar,
      butontTxt: 'Sipariş Ver',
    );
  }

  Widget divider() {
    return           Divider(
      height: SizeConfig.screenHeight * 0.01,
      thickness: SizeConfig.screenHeight * 0.001,
    );
  }
//endregion
}
