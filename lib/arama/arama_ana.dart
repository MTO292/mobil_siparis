
import '../arama/widget/arama.dart';
import 'package:provider/provider.dart';
import '../anamenu/ana_menu.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../global_degisken/global_degisken.dart' as global;
import 'view_model/arama_view_model.dart';

class AramaAna extends StatefulWidget {
  @override
  _aramaState createState() => _aramaState();
}

class _aramaState extends State<AramaAna> {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<AramaViewModel>(
      create: (context) => AramaViewModel(),
      child: WillPopScope(
        onWillPop: () {
          global.NavigateAndRemoveUntil()
              .navigatePushNoAnimation(context, AnaMenu());
          return null;
        },
        child: Arama(),
      ),
    );
  }
}


