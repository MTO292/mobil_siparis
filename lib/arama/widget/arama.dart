import 'dart:io';
import '../../api/model/menu_model.dart';
import '../../api/response/islem_donus.dart';
import '../../arama/view_model/arama_view_model.dart';
import '../../arama/widget/arama_appbar.dart';
import '../../arama/widget/arama_list.dart';
import '../../bottom_menu/my_custom_bottom.dart';
import '../../global_degisken/global_degisken.dart';
import 'package:provider/provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import '../../global_degisken/global_degisken.dart' as global;
import '../../api/model/arama_model.dart';

class Arama extends StatefulWidget {
  @override
  _AramaState createState() => _AramaState();
}

class _AramaState extends State<Arama> {
  IslemDonus aramaRepo = IslemDonus();
  //Klavyenini durumunu öğrenmek için oluşturuldu açık ise veya değil ise
  bool _keyboardState;
  Grupurunler grupurunler;

  Future<AramaModel> futurArama;
  @override
  void dispose() {
    super.dispose();
  }

  @protected
  void initState() {
    super.initState();
    //Klavyenini durumunu öğrenmek için oluşturuldu açı ise veya değil ise
    KeyboardVisibility.onChange.listen((bool visible) {
      setState(() {
        _keyboardState = visible;
        //klavye kapandığı zaman textfielde focusu kaldırıyoruz
        if (!_keyboardState) {
          FocusScope.of(context).unfocus();
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    // provider'dan (Katmanlı mimariden) alınan değer
    var urunAraProvider = Provider.of<AramaViewModel>(context);
    return Scaffold(
      appBar: AramaAppbar(AppBar().preferredSize.height),
      resizeToAvoidBottomInset: false,
      //Klavye acık ise bottom buton gizlenir
      bottomNavigationBar: _keyboardState != true
           ? MyBottomNavBar(1)
        : SizedBox(),
      body: Container(
          child: urunAraProvider.arananUrun != ""
              ? FutureBuilder(
                  future: aramaRepo.aramaYap(urunAraProvider.arananUrun),
                  builder: (BuildContext context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return global.progressDondur();
                    } else if (snapshot.connectionState == ConnectionState.done && !snapshot.hasError && snapshot.hasData) {
                      if (snapshot.data.cariBasariDonus == global.numarator.uygulamaVersiyonuEski) {
                        global.logoEkraninaGit(context);
                        return SizedBox();
                      } else if (snapshot.data.cariBasariDonus == global.numarator.apiVersiyonuEski) {
                        global.logoEkraninaGit(context);
                        return SizedBox();
                      } else if (snapshot.data.cariBasariDonus == global.numarator.hizmetDisi) {
                        global.logoEkraninaGit(context);
                        return SizedBox();
                      } else if (snapshot.data.cariBasariDonus == global.numarator.basarili) {
                       // urunAra.aramaList = snapshot.data;
                        return AramaList(snapshot.data);
                      } else if (snapshot.data.cariBasariDonus == global.numarator.hata) {
                        return  aciklama(
                            "Sonuç",
                            '"' +
                                urunAraProvider.arananUrun +
                                '"' +
                                " ile ilgili sonuç bulunamadı. ",
                            Icons.error_outline);
                      } else {
                        return aciklama(
                            "Hata",
                            "Lütfen internetinizi kontrol ediniz.",
                            Icons.error_outline);
                      }
                    } else if (snapshot.hasError) {
                      return aciklama(
                          "Hata",
                          "Lütfen internetinizi kontrol ediniz.",
                          Icons.error_outline);
                    } else {
                      return aciklama(
                          "Hata",
                          "Lütfen daha sonra tekrar deneyiniz.",
                          Icons.error_outline);
                    }
                  },
                )
              : aciklama(
                  "Ürün Ara",
                  "İstediğiniz ürünü ismi ile aratabilirsiniz.",
                  !Platform.isAndroid ? CupertinoIcons.search : Icons.search),
        ),
    );
  }
}
