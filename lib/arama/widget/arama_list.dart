import 'package:flutter/material.dart';
import '../../anamenu/urun_profil.dart';
import '../../api/model/menu_model.dart';
import '../../global_degisken/global_degisken.dart';
import '../../yardimci_widgetlar/manu_kartlar/urun_cart.dart';
import '../../api/model/arama_model.dart';
class AramaList extends StatefulWidget {
  AramaModel aramaList;

  AramaList(this.aramaList);

  @override
  _AramaListState createState() => _AramaListState();
}

class _AramaListState extends State<AramaList> {
  Grupurunler grupurunler;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
      child: GridView.builder(
        physics: ScrollBehavior().getScrollPhysics(context),
        shrinkWrap: true,
        itemCount: widget.aramaList.stok.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          childAspectRatio: MediaQuery.of(context).size.width /
              (MediaQuery.of(context).size.height / 1.4),
        ),
        itemBuilder: (context, index) {
          /*Genel olarak bir menü başlığını temsil eden container*/
          return Padding(
            padding: const EdgeInsets.all(6.0),
            child: UrunCart(
              img:
                  "${resimUrl + key + "/s-" + widget.aramaList.stok[index].aId + ".jpg"}",
              imageSize: MediaQuery.of(context).size.height / 6,
              name: widget.aramaList.stok[index].aAdi,
              fiyat: double.parse(widget.aramaList.stok[index].aSfiyat)
                  .toStringAsFixed(2),
              onTap: () {
                FocusScope.of(context).unfocus();
                grupurunler = Grupurunler();
                grupurunler.aAdi = widget.aramaList.stok[index].aAdi;
                grupurunler.aAcik = widget.aramaList.stok[index].aAcik;
                grupurunler.aSfiyat = widget.aramaList.stok[index].aSfiyat;
                grupurunler.aBirim = widget.aramaList.stok[index].aBirim;
                grupurunler.aId = widget.aramaList.stok[index].aId;
                Navigate().navigatePushNoAnimation(
                  context,
                  UrunProfil(
                    grupurunler: grupurunler,
                  ),
                );
              },
            ),
          );
        },
      ),
    );
  }
}
