import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../arama/view_model/arama_view_model.dart';
import '../../global_degisken/global_degisken.dart';
import '../../responsive_class/size_config.dart';
import '../../yardimci_widgetlar/appbar_widgets/appbar_text.dart';
import 'package:provider/provider.dart';
import 'package:simple_search_bar/simple_search_bar.dart';

class AramaAppbar extends StatefulWidget implements PreferredSizeWidget{
  double height;

  AramaAppbar(this.height);

  @override
  _AramaAppbarState createState() => _AramaAppbarState();

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.fromHeight(this.height);
}

class _AramaAppbarState extends State<AramaAppbar> {
  final AppBarController appBarController = AppBarController();

  @override
  void dispose() {
    // TODO: implement dispose
    appBarController.dispose();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    var urunAdi = Provider.of<AramaViewModel>(context);
    return SearchAppBar(
      //appbara primeri color rengini verdik
      primary: GlobalDegiskenler.appBarColor,
      appBarController: appBarController,
      autoSelected: false,
      searchHint: "Ara",
      mainTextColor: GlobalDegiskenler.baslikColor,
      onChange: (String deger) async {
        deger = deger.trim();
        if (deger.length >= 2 && deger.length <= 20 && deger != urunAdi.arananUrun) {
          urunAdi.arananUrun = deger;
          //setState(() {});
        } else if (deger == "") {
          urunAdi.arananUrun = "";
          //setState(() {});
        }
      },
      //Ana app bar
      mainAppBar: AppBar(
        backgroundColor: GlobalDegiskenler.appBarColor,
        title: Center(
          child: BaslikText(
            label: 'Arama',
          ),
        ),
        actions: <Widget>[
          InkWell(
            child: Container(
              width: SizeConfig.screenWidth * 0.16,
              child: Icon(
                !Platform.isAndroid ? CupertinoIcons.search:Icons.search,
                color: GlobalDegiskenler.appBarIconColor,
                size: SizeConfig.safeBlockHorizontal * 8,
              ),
            ),
            onTap: () {
              appBarController.stream.add(true);
            },
          ),
        ],
      ),
    );
  }
}
