import 'package:flutter/cupertino.dart';

class AramaViewModel with ChangeNotifier {
  String _arananUrun;

  AramaViewModel() {
    _arananUrun = '';
    debugPrint('init arananUrun = $_arananUrun');
  }

  String get arananUrun => _arananUrun;

  set arananUrun(String value) {
    _arananUrun = value;
    debugPrint('Set arananUrun = $_arananUrun');
    notifyListeners();
  }
}
